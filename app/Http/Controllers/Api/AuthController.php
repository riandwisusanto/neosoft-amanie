<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use Auth;
use Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;

class AuthController extends Controller
{
    public function sendPasswordResetLink(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }
    protected function sendResetResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Password reset successfully.']);
    }
    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Failed, Invalid Token.']);
    }
    public function callResetPassword(Request $request)
    {
        return $this->reset($request);
    }

    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);
        $user->save();
        event(new PasswordReset($user));
    }
    // public function refresh()
    // {
    //     if ($token = $this->guard('customer')->refresh()) {
    //         return response()
    //             ->json(['status' => 'successs'], 200)
    //             ->header('Authorization', $token);
    //     }
    //     return response()->json(['error' => 'refresh_token_error'], 401);
    // }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        // $customer = Customer::where('email', '=', $request->email)->firstOrFail();
        $status = false;
        $message = "";
        $data = null;
        // if ($customer) {
        //     if ($customer->email_verified_at) {
        //         if (Hash::check($request->password, $customer->password)) {
        //             if (!$customer->api_token) {
        //                 $customer->generateToken();
        //             }
        //             $status = true;
        //             $message = 'Login sukses';
        //             $data = $customer->toArray();
        //             $code = 200;
        //         } else {
        //             $message = "Login failed, password wrong";
        //         }
        //     } else {
        //         $message = "Email not Verified";
        //     }
        // } else {
        //     $message = "Login failed, email wrong";
        // }

        // return response()->json([
        //     'status' => $status,
        //     'message' => $message,
        //     'data' => $data
        // ], $code);

        $credentials = $request->only('email', 'password');
        if ($token = $this->guard('customer')->attempt($credentials)) {
            $customer = Customer::where('email', '=', $request->email)->firstOrFail();
            if ($customer->email_verified_at) {
                $data = $customer;
                $message = "Success";
                $status = true;
                $data['token'] = $token;
            } else {
                $message = "Email not Verified";
            }
            return response()->json([
                'status' => $status,
                'message' => $message,
                'data' => $data,
            ], 200);
        }
        return response()->json(['error' => 'login_error'], 401);
    }

    public function logout()
    {
        $this->guard('customer')->logout();
        return response()->json([
            'status' => true,
            'message' => 'Logged out Successfully.'
        ], 200);
    }

    private function guard($customer)
    {
        return Auth::guard($customer);
    }
}
