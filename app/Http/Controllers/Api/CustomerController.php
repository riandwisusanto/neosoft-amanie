<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Usage;
use App\Models\UsageDetail;
use App\Models\Appointment;
use App\Models\Country;
use App\Models\Province;
use App\Models\CustomerDevice;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
    public function SendEmail()
    {
        $send = Customer::find(1);
        Mail::to("arieansyahp.bun@gmail.com")->send(new SendEmail($send));
        //Mail::send(new SendEmail($send));
    }

    public function Location()
    {
        $status = true;
        $message = "get data success";
        $country = Country::all();
        $city = Province::all();
        $data = collect([
            'country' => $country,
            'city' => $city,
        ]);

        $code = 200;

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }
    public function CustomerCreate(Request $request)
    {
        $status = false;
        $message = "";
        $data = null;
        $code = 401;

        $validator = Validator::make($request->all(), [
            'induk_customer' => 'unique:customers',
            'full_name' => 'required',
            'phone' => 'required|min:10|unique:customers',
            'email' => 'required|string|email|max:255|unique:customers',
            'gender' => 'required',
            'country_id' => 'required',
            'birth_date' => 'required',
            'city_id' => 'required',
            'password' => 'required|string|min:6',
        ]);

        if (!$validator->fails()) {
            $status = true;
            $message = 'create customer success';

            $save = new Customer;
            $save->induk_customer = Customer::generateIndukCustomer();
            $save->full_name = $request->full_name;
            $save->phone = $request->phone;
            $save->email = $request->email;
            $save->gender = $request->gender;
            $save->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $save->join_date = now();
            $save->country_id = $request->country_id;
            $save->city_id = $request->city_id;
            $save->password = Hash::make($request->password);
            $save->save();

            $save->sendEmailVerificationNotification();

            $customer = Customer::find($save->id_customer);
            // Mail::to($request->email)->send(new SendEmail($customer));
            $data = $customer->toArray();
            $code = 200;
        } else {
            $errors = $validator->errors()->first();
            //dd($validator->errors()->toJson());
            $message = $errors;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function CustomerInvoice(Request $request)
    {
        $status = false;
        $message = "Param Required";
        $data = null;
        $code = 401;

        $id = $request->get('id');

        if ($id) {
            $status = true;
            $message = "get data success";
            $data =
                Invoice::where('customer_id', $id)
                ->with([
                    'outlet:id_outlet,outlet_name,outlet_code',
                    'marketing_source:id_marketing_source,marketing_source_name',
                    'consultant:id_consultant,consultant_name',
                    'therapist:id_therapist,therapist_name',
                    'user:id_user,name',
                    'agent:id_agent,agent_name',
                    'invoice_detail',
                    'invoice_detail.activity',
                    'invoice_detail.product_treatment',
                    'invoice_detail.package',
                    'invoice_detail.invoice_package',
                    'invoice_detail.invoice_package.product_treatment',
                ])
                ->get();
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function CustomerDetail(Request $request)
    {
        $status = false;
        $message = "Param Required";
        $data = [];
        $code = 401;
        $reschedule = false;

        $id = $request->get('id');
        if ($id) {
            $status = true;
            $message = "get data success";
            $rank = rank($id);
            $member = ($rank) ? $rank->name : 'No Membership' ;

            $appointment = Appointment::where('customer_id', $id)->get();
            foreach ($appointment as $key => $value) {
                $now = now('Asia/Jakarta')->diffInMinutes($value->created_at);
                $reschedule = false;
                if ($now < 120 && $value->status_appointment == 1) {
                    $reschedule = true;
                }

                $appointment[$key]['reschedule'] = $reschedule;

                if ($value->treatment_id) {
                    $appointment[$key]['product_treatment'] = $value->product_treatment;
                } else {
                    $appointment[$key]['product_treatment'] =  (object) [];
                }
            }

            $data =
                Customer::with([
                    'medical_alert.product',
                    'country:id_country,country',
                    'city:id_province,country_id,province',
                    'invoice.therapist',
                    'invoice.invoice_detail.package',
                    'invoice.invoice_detail.invoice_package.product_treatment',
                ])
                ->find($id);

            $now = now('Asia/Jakarta')->toDateString();
            //dd($now);
            foreach ($data->invoice as $key => $value) {
                //Carbon::createFromDate($value)
                $active = false;
                $expired_date = Carbon::createFromFormat('d/m/Y', $value->expired_date)->format('Y-m-d');
                //dd($expired_date);
                if ($expired_date >= $now) {
                    $active = true;
                }
                $data->invoice[$key]['active_invoice'] = $active;
            }

            $collection = collect($data);
            $collection->prepend($member, 'member');
            $collection->put('appointment', $appointment->load(
                'room',
                'therapist'
            ));
            //$collection->put('treatment_customer', $treatmentCustomerData);

            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $collection,
        ], $code);
    }

    public function ActiveInvoice(Request $request)
    {
        $status = false;
        $message = "param required";
        $code = 401;
        $invoice = null;

        if ($request->customer_id) {
            $message = "get data success";
            $status = true;
            $code = 200;
            $invoice = Invoice::where('customer_id', $request->customer_id)->get();

            $now = now('Asia/Jakarta')->format('d-m-Y');
            $treatment = [];

            foreach ($invoice as $a => $value) {
                $expired_date = Carbon::createFromFormat('d/m/Y', $value->expired_date)->format('Y-m-d');
                //dd($expired_date);
                if ($expired_date >= $now) {
                    foreach ($value->invoice_detail as $b => $detail) {
                        foreach ($detail->invoice_package as $c => $package) {
                            if (substr($package->product_treatment->product_treatment_code, 0, 1) == 'T') {
                                $treatment[] = $package->product_treatment;
                            }
                        }
                    }
                }
            }

            $collection = collect($treatment);
            $x = $collection->groupBy('id_product_treatment');
            $final = [];
            foreach ($x as $key => $value) {
                $final[] = $value[0];
            }
        }


        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $final,
        ], $code);
    }

    public function CustomerPastUsage(Request $request)
    {
        $status = false;
        $message = "Param Required";
        $data = null;
        $code = 401;

        $id = $request->get('id');
        if ($id) {
            $status = true;
            $message = "get data success";
            $data =
                UsageDetail::with([
                    'product_treatment',
                    'usage'
                ])
                ->get();
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function CustomerAppointment(Request $request)
    {
        $status = false;
        $message = "Param Required";
        $data = null;
        $code = 401;

        $id = $request->get('id');
        $status_id = $request->get('status');
        if ($id && $status_id) {
            $status = true;
            $message = "get data success";
            $data =
                Appointment::with([
                    'user:id_user,name',
                    'outlet:id_outlet,outlet_name,outlet_code',
                    'activity',
                    'therapist',
                    'product_treatment',
                    'room',
                ])
                ->where('customer_id', $id)
                ->where('status_appointment', $status_id)
                ->get();

            // $appointment = Appointment::where('customer_id', $id)->get();
            foreach ($data as $key => $value) {
                $now = now('Asia/Jakarta')->diffInMinutes($value->created_at);
                $reschedule = false;
                if ($now < 120 && $value->status_appointment == 1) {
                    $reschedule = true;
                }

                $data[$key]['reschedule'] = $reschedule;
            }

            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function CustomerUpdate(Request $request, $id)
    {
        $status = false;
        $message = "";
        $data = null;
        $code = 401;
        $validator = Validator::make($request->all(), [
            // 'email' => [
            //     'required',
            //     Rule::unique('customers')->ignore($id, 'id_customer')
            // ],
            'name' => 'required|max:100',
            'new_password' => 'string|min:6|confirmed',
        ]);

        if (!$validator->fails()) {
            $status = true;
            $message = "update success";

            $update = Customer::find($id);
            $update->full_name = $request->name;
            // if ($update->email != $request->email) {
            //     $update->sendEmailVerificationNotification();
            // }

            $img = $request->file('img');
            if ($img) {
                if ($update->img && file_exists(storage_path('app/public/' . $update->img))) {
                    \Storage::delete('public/' . $update->img);
                }
                $img_path = $img->store('profile', 'public');
                $update->img = $img_path;
            }

            //$update->email = $request->email;
            $datax = $update;

            if ($request->old_password && $request->new_password && $request->new_password_confirmation) {
                if (!(Hash::check($request->old_password, $update->password))) {
                    $message = "current password does not matches";
                    $status = false;
                    $datax = null;
                } elseif (Hash::check($request->new_password, $update->password)) {
                    $message = "new password cannot be same as your current password";
                    $status = false;
                    $datax = null;
                } else {
                    $update->password = Hash::make($request->new_password);
                    $datax = $update;
                }
            }
            $code = 200;

            $update->update();
            $data = $datax;
        } else {
            $errors = $validator->errors();
            $message = $errors;
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function CustomerDevice(Request $request, $id)
    {
        $status = false;
        $message = "param required";
        $data = null;
        $code = 401;
        $validator = Validator::make($request->all(), [
            'app_version' => 'required|max:100',
            'device_os' => 'required|max:100',
            'device_os_version' => 'required|max:100',
            'device_brand' => 'required|max:100',
            'device_version' => 'required|max:100',
        ]);

        if (!$validator->fails()) {
            $status = true;
            $message = "update success";

            $customer = Customer::find($id);
            //dd($customer->customer_device->id_device);
            if ($customer->customer_device) {
                $device = CustomerDevice::find($customer->customer_device->id_device);
            } else {
                $device = new CustomerDevice;
            }
            $device->customer_id = $id;
            $device->app_version = $request->app_version;
            $device->device_os = $request->device_os;
            $device->device_os_version = $request->device_os_version;
            $device->device_brand = $request->device_brand;
            $device->device_version = $request->device_version;
            $device->save();

            $data = $device;
            $code = 200;
        } else {
            $errors = $validator->errors();
            $message = $errors;
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function forgotPassword(Request $request)
    {
    }
}
