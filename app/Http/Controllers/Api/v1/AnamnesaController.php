<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Anamnesa;
use App\Models\AnamnesaDetail;
use App\Models\AnamnesaBom;
use App\Models\Queue;

class AnamnesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $anamnesaData = $request->only(['customer_id', 'suggestion', 'anamnesa', 'date_anamnesa']);
        $anamnesa = new Anamnesa($anamnesaData);
        $anamnesa->anamnesa_code = Anamnesa::generateCode();
        
        try {
            $anamnesa->save();
            $this->storeDetail($request, $anamnesa);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        $queue = Queue::where('customer_id',$request->customer_id)->count();
        if($queue)
        {
            $updateQueue = Queue::where('customer_id',$request->customer_id)->latest('created_at')->first();
            $updateQueue->status_queue = 1;
            $updateQueue->status_payment = 1;
            $updateQueue->update();
        }

        return response()->json(['model' => $anamnesa], 200);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeDetail(Request $request, $anamnesa)
    {
        if ( ! isset($request->details) || ! is_array($request->details)) {
            throw new Exception('expected details length to be > 0, got empty');
        }
        
        try {
            foreach ($request->details as $detailData) {
                $detail = new AnamnesaDetail($detailData);
                $detail->anamnesa_id = $anamnesa->id_anamnesa;
                $detail->save();

                if (count($detailData['boms']) > 0) {
                    $this->storeBoms($detail, $detailData['boms']);
                }
            }
        } catch (Exception $e) {
            throw new Exception('error while store anamnesa detail: '. $e->getMessage());
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeBoms($detail, $boms)
    {
        foreach ($boms as $bomData) {
            $bom = new AnamnesaBom($bomData);
            $bom->anamnesa_detail_id = $detail->id_anamnesa_detail;
            $bom->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'details.product_treatment' => function ($query) {
                $query->select('id_product_treatment', 'product_treatment_name', 'product_treatment_code');
            },

            'details.boms.rawItem' => function ($query) {
                $query->select('id', 'name', 'uom_id');
            },

            'details.boms.rawItem.uom' => function ($query) {
                $query->select('id', 'alias');
            },
        ];

        $model = Anamnesa::with($relations)->find($id);

        if ( ! $model) {
            return response()->json(['message' => 'anamnesa with id '. $id. ' does not exists.'], 404);
        }

        return response()->json(['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
