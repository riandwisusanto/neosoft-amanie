<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Customer;

class CustomerController extends Controller
{
    /**
     * Patient overview
     *
     * returns patient visit data and it's all activities
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function activities(Request $request, $id)
    {
        $yearlySQL = file_get_contents(__DIR__ .'/../../Queries/patientVisitYearly.sql.tpl');
        $rawYearly = DB::select($yearlySQL, [$id, $id]);

        $yearly = [];
        foreach ($rawYearly as $row) {
            $yearly[$row->visit_month] = $row->total_visit;
        }

        $startMonth = now()->firstOfMonth();
        $endOfMonth = now()->endOfMonth();

        $startWeek = now();
        $endOfWeek = now()->sub(1, 'week');

        $dailySQL = file_get_contents(__DIR__. '/../../Queries/patientVisitDaily.sql.tpl');
        $rawMonthly = DB::select($dailySQL, [
            $id, $startMonth, $endOfMonth,
            $id, $startMonth, $endOfMonth
        ]);

        $monthly = array_map(function($row) {
            return $row->visit_day;
        }, $rawMonthly);

        $rawWeekly = DB::select($dailySQL, [
            $id, $endOfWeek, $startWeek,
            $id, $endOfWeek, $startWeek
        ]);

        $weekly = array_map(function($row) {
            return $row->visit_day;
        }, $rawWeekly);

        $monthData = [];
        for ($i = (int) $startMonth->format('d'); $i <= (int) $endOfMonth->format('d'); $i++) {
            if (in_array($i, $monthly)) {
                $monthData[$i] = 1;
            } else {
                $monthData[$i] = 0;
            }
        }

        $weekData = [];
        for ($i = (int) $endOfWeek->format('d'); $i <= (int) $startWeek->format('d'); $i++) {
            Log::info('loop: ' .$i. ', DB: '. json_encode($weekly));
            if (in_array($i, $weekly)) {
                $weekData[$i] = 1;
            } else {
                $weekData[$i] = 0;
            }
        }
        
        $monthLabel = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $yearData = [];
        foreach ($monthLabel as $month) {
            if (isset($yearly[$month])) {
                $yearData[$month] = $yearly[$month];
            } else {
                $yearData[$month] = 0;
            }
        }

        $visit = [
            'yearly'    => $yearData,
            'monthly'   => $monthData,
            'weekly'    => $weekData
        ];

        return response()->json($visit);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function show($id)
    {
        $customer = Customer::find($id);

        return response()->json(['model' => $customer]);
    }

    /**
     * Treatment history.
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function histories($invoicePackageId)
    {
        
        $sql = file_get_contents(__DIR__. '/../../Queries/treatmentHistories.sql.tpl');
        $result = DB::select($sql, [$invoicePackageId]);

        $res = array_map(function ($row) {
            $therapists = json_decode($row->therapists);
            $row->therapists = implode(', ', $therapists);
            return $row;
        }, $result);
        
        return response()->json($result);
    }
}

