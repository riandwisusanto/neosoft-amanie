<?php

namespace App\Http\Controllers\Api\v1\Logistics\Purchases;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Logistics\Purchases\Order;
use App\Models\Logistics\Purchases\OrderDetail;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relations = [
            'supplier'
        ];

        $orders = Order::with($relations)->get();

        return response()->json(['collection' => $orders]);
    }

    /**
     * Display a listing of the resource by supplier id.
     *
     * @return \Illuminate\Http\Response
     */
    public function bySupplier($supplier_id)
    {
        $orders = Order::where('supplier_id', $supplier_id)
                            ->whereIn('status', ['created', 'proceed'])
                            ->get();

        return response()->json(['options' => $orders]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orderData = $request->only([
            'supplier_id',
            'term_id',
            'payment_method_id',
            'transdate',
            'duedate',
            'total',
            'remarks'
        ]);


        DB::beginTransaction();
        try {

            $order = new Order($orderData);

            $order->code = Order::generateCode();

            $order->save();
            
    
            $details = array_map(function ($detailData) {
                $orderDetail = new OrderDetail($detailData);
                $orderDetail->outstanding_qty = $detailData['qty'];
                return $orderDetail;
            }, $request->details);
    
    
            $order->details()->saveMany($details);
            DB::commit();

            return response()->json([
                'model' => $order
            ]);

        } catch (Exception $e) {
            DB::rollback();

            return response()->json([
                'message'   => $e->getMessage()
            ], 500);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'supplier',
            'paymentMethod',
            'details.item.uom'
        ];

        $model = Order::with($relations)->find($id);

        if ( ! $model) {
            return response()->json([
                'message'   => 'not found'
            ], 404);
        }

        return response()->json(['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Close purchase order.
     */
    public function close($id) {
        $model = Order::find($id);

        $model->status = 'closed';
        $model->save();

        return response()->json(['status' => 'closed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Order::find($id);

        foreach ($model->details as $detail) {
            if ($detail->qty != $detail->outstanding_qty) {
                return response()->json(['status' => 'denied']);
            }
        }

        OrderDetail::where('order_id', $id)->delete();
        Order::where('id', $id)->delete();

        return response()->json(['status' => 'deleted']);
    }
}
