<?php

namespace App\Http\Controllers\Api\v1\Logistics\Purchases;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Logistics\Purchases\Receive;
use App\Models\Logistics\Purchases\ReceiveDetail;
use App\Models\Logistics\Purchases\Order;
use App\Models\Logistics\Purchases\OrderDetail;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\Item;
use Illuminate\Support\Facades\DB;

class ReceiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relations = [
            'supplier'
        ];

        $receives = Receive::with($relations)->get();

        return response()->json(['collection' => $receives]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $receiveData = $request->only([
            'supplier_id',
            'received_by',
            'transdate',
            'remarks'
        ]);

        DB::beginTransaction();

        try {
            $receive = new Receive($receiveData);
            $receive->code = Receive::generateCode();
            $receive->save();
    
    
            $details = array_map(function ($detail) {
                return new ReceiveDetail($detail);
            }, $request->details);
    
    
            $receive->details()->saveMany($details);
            $this->decrementOrderOutstanding($request);
            $this->updateOrderStatus($request);
            $this->createIncomingMutation($request);
            $this->updateStock($request);

            DB::commit();

            return response()->json(['model' => $receive]);
            
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function byOrder($order_id)
    {
        $relations = [
            'receiver'
        ];

        $order_detail_id = OrderDetail::where('order_id', $order_id)->pluck('id')->toArray();

        $receives = Receive::with($relations)
                            ->whereHas('details', function ($query) use ($order_detail_id) {
                                $query->whereIn('order_detail_id', $order_detail_id);
                            })
                            ->get();

        return response()->json(['collection' => $receives]);
    }

    /**
     * Decrement order detail outstanding quantity.
     *
     * @return void
     **/
    public function decrementOrderOutstanding(Request $request)
    {
        foreach ($request->details as $receive) {
            $orderDetail = OrderDetail::find($receive['order_detail_id']);
            $orderDetail->outstanding_qty -= $receive['received_qty'];
            $orderDetail->save();
        }
    }

    /**
     * Update order status.
     *
     * @return void
     **/
    protected function updateOrderStatus(Request $request)
    {
        $ordered = OrderDetail::where('order_id', $request->order_id)->count();

        $completed = OrderDetail::where('order_id', $request->order_id)
                                    ->where('outstanding_qty', 0)
                                    ->count();

        $status = 'created';

        if ($completed > 0 && $completed < $ordered) {
            $status = 'proceed';
        }

        if ($completed == $ordered) {
            $status = 'closed';
        }

        $order = Order::find($request->order_id);
        $order->status = $status;
        $order->save();
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function createIncomingMutation(Request $request)
    {
        foreach ($request->details as $receive) {
            $item = Item::find($receive['item_id']);
            $mutation = [
                'warehouse_destination_id' => $receive['warehouse_id'],
                'rack_destination_id' => $receive['rack_id'],
                'purchase_invoice_id' => $request->order_id,
                'item_id' => $receive['item_id'],
                'transdate' => $request->transdate,
                'transtype' => 'in',
                'cogs'      => $item->cogs,
                'balance'   => $receive['received_qty'],
                'transdesc' => 'RECEIVE',
                'qty' => $receive['received_qty']
            ];

            Mutation::create($mutation);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function updateStock(Request $request)
    {
        foreach ($request->details as $receive) {
            $item = Item::find($receive['item_id']);
            
            $item->sellable_stock += $receive['received_qty'];
            $item->stock += $receive['received_qty'];
            $item->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'receiver',
            'supplier',
            'details.warehouse',
            'details.rack',
            'details.item'
        ];

        $model = Receive::with($relations)->find($id);

        return response()->json(['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
