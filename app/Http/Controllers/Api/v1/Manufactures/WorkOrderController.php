<?php

namespace App\Http\Controllers\Api\v1\Manufactures;

use App\Models\Manufactures\WorkOrder;
use App\Models\Manufactures\WorkOrderDetail;
use App\Models\Manufactures\Material;
use App\Models\Manufactures\MaterialRelease;
use App\Models\Logistics\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class WorkOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relations = [
            'pic'   => function ($query) {
                $query->select('id', 'name');
            }
        ];

        $collection = WorkOrder::with($relations)
                                    ->get();


        return response()->json(['collection' => $collection]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workOrderData = $request->only([
            'pic_id',
            'start_date',
            'due_date',
            'remarks',
        ]);

        $order = new WorkOrder($workOrderData);
        $order->code = WorkOrder::generateCode();

        DB::beginTransaction();

        try {
            $order->save();
            $this->storeDetails($request, $order);

            DB::commit();

            return response()->json(['model' => $order]);
        } catch (Exception $e) {
            
            DB::rollback();

            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * Store work order details.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Manufactures\WorkOrder  $order
     * @return void
     **/
    protected function storeDetails(Request $request, $order)
    {
        foreach ($request->details as $row) {
            $detail = new WorkOrderDetail($row);
            $detail->work_order_id = $order->id;
            $detail->save();

            $this->storeMaterial($detail);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function options()
    {
        $relations = [
            'details'   => function ($query) {
                $relations = [
                    'item'  => function ($query) {
                        $query->select('id', 'name');
                    }
                ];

                $query->select('id', 'work_order_id', 'item_id', 'finished_qty', 'qty')
                        ->where('mark_as_close', 0)
                        ->with($relations);
            },
            
            'pic'   => function ($query) {
                $query->select('id', 'name');
            }
        ];

        $options = WorkOrder::with($relations)
                                ->select('id', 'start_date', 'pic_id', 'code')
                                ->get();

        return response()->json(['options' => $options]);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function close($work_order_id)
    {
        $model = WorkOrderDetail::findOrFail($work_order_id);

        DB::beginTransaction();

        try {
            $model->mark_as_close = 1;
            $model->save();

            DB::commit();

            return response()->json(['model' => $model]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * Store work order details.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Manufactures\WorkOrder  $order
     * @return void
     **/
    public function storeMaterial($orderDetail)
    {
        $item = Item::find($orderDetail->item_id);

        // store bom to material.
        foreach ($item->boms()->get()->toArray() as $bom) {
            $material = new Material($bom);
            $material->work_order_detail_id = $orderDetail->id;
            $material->qty = $orderDetail->qty * $bom['qty'];
            $material->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'details.item.uom',
            'pic'
        ];

        $model = WorkOrder::with($relations)
                            ->findOrFail($id);

        return response()->json(['model' => $model]);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function showDetail($id)
    {
        $relations = [
            'materials.rawItem',
            'item.uom',
            'materials.releases',
            'materials.releases.warehouseOrigin',
            'materials.releases.releaser',
            'materials.uom'
        ];

        $model = WorkOrderDetail::with($relations)
                                    ->find($id);

        if ( ! $model) {
            return response()->json(['message' => 'detail not found.'], 404);
        }

        return response()->json([
            'model'     => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
