<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductTreatment;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function options()
    {
        $options = ProductTreatment::where('status_product_treatment', 1)
                                    ->select('id_product_treatment as id', 'product_treatment_name as name')
                                    ->get();

        return response()->json(['options' => $options]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'item' => function ($query) {

                $relations = [
                    'uom'   => function ($query) {
                        $query->select('id', 'alias');
                    }
                ];

                $query->select('id', 'sales_information_id', 'sellable_stock', 'brand', 'uom_id', 'unit_value')
                        ->with($relations);
            }
        ];

        $model = ProductTreatment::with($relations)->find($id);

        if ( ! $model) {
            return response()->json(['message' => 'not found'], 404);
        }


        return response()->json(['model' => $model]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
