<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function track(Request $request)
    {
        $form = $request->only(['waybill', 'courier']);

        $rajaongkir = new Client([
            'base_uri'  => 'https://pro.rajaongkir.com',
            'headers'   => ['key' => env('RAJAONGKIR_KEY')]
        ]);

        try {
            $response = $rajaongkir->request('POST', 'api/waybill', ['form_params' => $form]);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            
            if (strpos($message, 'Invalid waybil') > -1) {
                $result = [
                    'manifest'  => [],
                    'summary'   => [
                        'waybill_number'    => ''
                    ],
                    'delivery_status' => [
                        'status'    => null
                    ]
                ];

                return response()->json($result);
            }

            return response()->json(['message' => $message], 500);
        }

        $decoded = json_decode($response->getBody()->getContents(), true);

        return response()->json($decoded['rajaongkir']['result']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
