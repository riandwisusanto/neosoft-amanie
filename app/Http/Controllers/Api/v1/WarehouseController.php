<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Warehouse;
use App\Models\Logistics\Mutation;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing for select options.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     **/
    public function options(Request $request)
    {
        $outlets = $request->user()->userOutlets()->pluck('outlet_id');

        $options =  Warehouse::whereHas('warehouseOutlets', function ($query) use ($outlets) {
            $query->whereIn('outlet_id', $outlets);
        })->select('id_warehouse', 'name')
                                ->get();

        return response()->json(['options' => $options]);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function stock($warehouse_id, $item_id)
    {
        $stock = Mutation::where(function ($query) use ($warehouse_id) {
                                $query->where('warehouse_destination_id', $warehouse_id)
                                        ->orWhere('warehouse_origin_id', $warehouse_id);
                            })
                            ->where('item_id', $item_id)
                            ->sum('qty');
        
        // for demo purpose. please remove on production
        // @VickyHariadi
        sleep(1);

        return response()->json(['stock' => $stock]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
