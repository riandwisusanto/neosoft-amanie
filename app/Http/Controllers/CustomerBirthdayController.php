<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use DataTables;
use Carbon\Carbon;
use App\Models\Customer;
use App\Models\CustomerConsultant;
use App\Models\CustomerOutlet;
use App\Models\Outlet;
use App\Models\UserOutlet;
use App\Models\Invoice;
use App\Models\Message;
use App\Models\MessageDetail;
use Auth;
use KrmPesan\Client;


class CustomerBirthdayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('customer')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        return view('customerBirthdays.index')
            ->with('outlet', $outlet);
    }

    public function list(Request $request)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customer = new Customer;
        if ($request->month) {
            $dataMonth = $request->month;
            foreach ($dataMonth as $filterMonth) {
                $customer = $customer->orWhereMonth('birth_date', '=', $filterMonth);
            }
        }
        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
            $tempCustomer = CustomerOutlet::whereIn('outlet_id', $outlet_id)->get();
            $customer_id = [];
            for ($i = 0; $i < count($tempCustomer); $i++) {
                $customer_id[] = $tempCustomer[$i]->customer_id;
            }
            $customer = $customer->whereIn('id_customer', $customer_id);
        }
        $customer = $customer->with([
            'outlet:id_outlet,outlet_name',
            'consultant:id_consultant,consultant_name',
        ])
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();

        $data = array();
        foreach ($customer as $list) {
            $row = array();
            $outlet_row = '';
            foreach ($list->outlet as $outlet) {
                if ($outlet->customer_id == $list->id_customer) {
                    $outlet_row .= "<ol>" . $outlet->outlet_name . "</ol>";
                }
            }

            $row[] = '';
            $row[] = $list->full_name;
            $row[] = $list->induk_customer;
            $row[] = $outlet_row;
            $row[] = $list->phone;
            $row[] = $list->email;
            $row[] = $list->birth_date;
            $row[] = $list->address;
            $lastInvoice = Invoice::where('customer_id', $list->id_customer)->latest('inv_date')->first();
            if ($lastInvoice) {
                $row[] = Carbon::createFromFormat('d/m/Y', $lastInvoice->inv_date)->format('d-M-Y');
                // $row[] = $lastInvoice->inv_date;
            } else {
                $row[] = 'not have transaction';
            }
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function store(Request $request)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customer = new Customer;
        $month = explode(",", $request->months);
        if ($month) {
            $dataMonth = $month;
            foreach ($dataMonth as $filterMonth) {
                $customer = $customer->orWhereMonth('birth_date', '=', $filterMonth);
            }
        }

        $outlet = explode(",", $request->outlets);
        if ($outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($outlet); $i++) {
                $outlet_id[] = $outlet[$i];
            }
            $tempCustomer = CustomerOutlet::whereIn('outlet_id', $outlet_id)->get();
            $customer_id = [];
            for ($i = 0; $i < count($tempCustomer); $i++) {
                $customer_id[] = $tempCustomer[$i]->customer_id;
            }
            $customer = $customer->whereIn('id_customer', $customer_id);
        }
        $customer = $customer->with([
            'outlet:id_outlet,outlet_name',
            'consultant:id_consultant,consultant_name',
        ])
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();
        // dd($request->all());
        // $img = $request->file('image');
        // dd(mime_content_type($img));
        $save = new Message;
        $save->message = $request->message;
    
        $img = $request->file('image');
        if ($img != '') {
            $img_path = $img->store('imageMessage', 'public');
            $save->image = $img_path;
        }
        $save->source = "Patient's Birthday";
        $save->save();

        foreach ($customer as $list) {
            $customer = new MessageDetail;
            $customer->message_id = $save->id_message;
            $customer->customer_id = $list->id_customer;
            $customer->save();
        }

        return response()->json([
            'id_message' => $save->id_message,
        ]);

        // return redirect()->action('CustomerInactiveController@index');
    }

    public function sendWhatsApp($id)
   {
       $wa = new Client([
        'region' => '01',
        'token' => 'csAefgICgNZigLDxjlpIoBtdl56DJvSkQbndLvYx26Q6EcDdfERUw9V3TTSeUtz0yW9Vlf5Vt2jkU3FT'
       ]);

       $dataMessage = Message::find($id);
       foreach ($dataMessage->detail as $det) {
            $phone = $det->customer->phone;  
            $message = $dataMessage->message;
            $img = $dataMessage->image;
            $file = storage_path("app/public/". $img);
            // dd($file);
            // $filename = basename($file);
            // $filemime = mime_content_type($file);
            if ($img == '') {
                $send = $wa->sendMessageText($phone, $message);   
            } else {
                $send = $wa->sendMessageImage($phone, $file, $message);
            }
       }

       return $send;
   }
}
