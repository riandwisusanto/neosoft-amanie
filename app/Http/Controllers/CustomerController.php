<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Carbon\carbon;
use DataTables;
use Hash;
use Auth;
use Redirect;
use App\Models\Customer;
use App\Models\Country;
use App\Models\Province;
use App\Models\Outlet;
use App\Models\Consultant;
use App\Models\MarketingSource;
use App\Models\CustomerConsultant;
use App\Models\CustomerOutlet;
use App\Models\Invoice;
use App\Models\ProductTreatment;
use App\Models\InvoiceDetail;
use App\Models\LogVoidInvoice;
use App\Models\LogInvoice;
use App\Models\Logistics\Item;
use App\Models\LogVoidBalance;
use App\Models\Therapist;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Appointment;
use App\Models\Usage;
use App\Models\UsageDetail;
use App\Models\InvoicePoint;
use App\Models\LogVoidInvoicePoint;
use App\Models\LogConvert;
use App\Models\MedicalAlert;
use App\Models\InvoicePackage;
use App\Models\InvoiceBalance;
use App\Models\UserOutlet;
use App\Models\Anamnesa;
use App\Models\AnamnesaDetail;
use App\Models\CustomerConsignment;
use App\Models\PhysicalExamination;


use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('customer')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        $country = Country::all();
        $productTreatment = ProductTreatment::where('status_product_treatment', 1)
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        return view('customers.index', compact('country', 'productTreatment', 'outlet', 'consultant', 'marketing_source'));
    }

    public function getProvince($id)
    {
        $province = Province::where('country_id', $id)->pluck('province', 'id_province');
        return json_encode($province);
    }

    public function getCustomer(Request $request)
    {
        $query = $request->get('query');
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();

        $customer = Customer::where('induk_customer', 'LIKE', "%{$query}%")
            ->orWhere('full_name', 'LIKE', "%{$query}%")
            ->orderBy('full_name', 'asc')
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get()->take(13);

        if (count($customer) == 0) {
            $output = '<ul id="country-list" class="form-control">';
            $output .= '<li>Customer not Registered</li>';
            $output .= '</ul>';
        } else {
            $output = '<ul id="country-list" class="form-control">';
            foreach ($customer as $row) {
                $output .= '<li onclick="detailCustomer(
                    \'' . $row->id_customer . '\',
                    \'' . $row->induk_customer . '\',
                    \'' . addslashes($row->full_name) . '\',
                    \'' . addslashes($row->phone) . '\',
                    \'' . $row->birth_date . '\',
                    \'' . $row->point . '\',
                    \'' . $row->email . '\',
                    \'' . $row->address . '\',
                    )">
                    <strong>'
                    . strtoupper($row->full_name) .
                    '</strong></li>';
            }
            $output .= '</ul>';
        }

        return json_encode($output);
    }

    public function getAllCustomer()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customer = Customer::orderBy('full_name', 'asc')
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get()->take(15);
        $output = '<ul id="country-list" class="form-control">';
        foreach ($customer as $row) {
            $output .= '<li onclick="detailCustomer(
                \'' . $row->id_customer . '\',
                \'' . $row->induk_customer . '\',
                \'' . addslashes($row->full_name) . '\',
                \'' . addslashes($row->phone) . '\',
                \'' . $row->birth_date . '\',
                \'' . $row->point . '\',
                \'' . $row->email . '\',
                \'' . $row->address . '\',
                )">
                <strong>'
                . strtoupper($row->full_name) .
                '</strong></li>';
        }
        $output .= '</ul>';
        return json_encode($output);
    }

    public function listData(Request $request)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customer_id = CustomerOutlet::whereIn('outlet_id', $outlet_id)->pluck('customer_id')->toArray();
        $search = $request->search['value'];
        
            $customer = Customer::with([
                'outlet:id_outlet,outlet_name',
                'consultant:id_consultant,consultant_name',
            ])
                ->whereIn('id_customer', $customer_id);
    
            
        

        // $data = array();
        // foreach ($customer as $list) {
        //     $outlets = '';
        //     foreach ($list->outlet as $out) {
        //         $outlets .= '&#10004;' . $out->outlet_name . '<br>';
        //     }
        //     // $consultants = '';
        //     // foreach ($list->consultant as $consult) {
        //     //     $consultants .= '&#10004;' . $consult->consultant_name . '<br>';
        //     // }
        //     $rank = rank($list->id_customer);
        //     $row = array();
        //     $row[] = '';
        //     $row[] = $list->induk_customer;
        //     $row[] = $list->full_name;
        //     $row[] = $list->phone;
        //     $row[] = ($rank) ? $rank->name : 'No Membership';
        //     $row[] = Carbon::parse($list->join_date)->format('d/m/Y');
        //     $row[] = $outlets;
        //     // $row[] = $consultants;

        //     if (count($list->invoice) || count($list->appointment)) {
        //         $delete = "<a disabled class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>";
        //     } else {
        //         $delete = "<a onclick='deleteData(" . $list->id_customer . ")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>";
        //     }

        //     $row[] = "
        //         <a href='customers/" . $list->id_customer . "' class='btn btn-primary btn-sm'><i class='fa fa-eye'></i> Detail</a>
        //         <a onclick='editForm(" . $list->id_customer . ")' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
        //         " . $delete . "
        //         ";
        //     $data[] = $row;
        // }

        return DataTables::of($customer)
        ->addColumn('divider', function($row) {
            return '';
        })
        ->addColumn('rank', function($row) {
            $rank = rank($row->id_customer);
            return ($rank) ? $rank->name : 'No Membership';
        })
        
        ->addColumn('outlet', function($row) {
            $outlets = '';
            foreach ($row->outlet as $outlet) {
                $outlets .= '&#10004;' . $outlet->outlet_name . '<br>';
            }
            return $outlets;
        })
        ->addColumn('action', function($row) {
            if (count($row->invoice) || count($row->appointment)) {
                $delete = "<a disabled class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>";
            } else {
                $delete = "<a onclick='deleteData(" . $row->id_customer . ")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>";
            }

            $action = "
                <a href='customers/" . $row->id_customer . "' class='btn btn-primary btn-sm'><i class='fa fa-eye'></i> Detail</a>
                <a onclick='editForm(" . $row->id_customer . ")' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
                " . $delete . "
                ";

            return $action;
        })
        ->editColumn('join_date', function($row) {
            return Carbon::parse($row->join_date)->format('d/m/Y');
        })
        ->escapeColumns([])
        ->make(true);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'min:10|unique:customers',
            // 'email' => 'string|email|max:255|unique:customers',
        ]);

        if (!$validator->fails()) {
            $status = true;
            $message = "Successfully Created Customer";
            $save = new Customer;
            $save->induk_customer = Customer::generateIndukCustomer($request->outlet);
            $save->full_name = $request->fullname;
            $save->phone = $request->phone;
            $save->email = $request->email;
            $save->birth_place = $request->place;
            $save->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $save->join_date = Carbon::createFromFormat('d/m/Y', $request->join_date)->format('Y-m-d');
            $save->gender = $request->gender;
            $save->religion = $request->religion;
            $save->ktp = $request->noktp;
            $save->address = $request->address;
            $save->occupation = $request->occupation;
            $save->country_id = $request->country;
            $save->city_id = $request->city;
            $save->zip = $request->zip;
            $save->nextofikin = $request->familyname;
            $save->nextofikin_number = $request->familyphone;
            $save->source_id = $request->source;
            $save->source_remarks = $request->source_remarks;
            $save->purpose_treatment = $request->purpose_treatment;
            $save->staf_id = Auth::user()->id; //auth user login
            $save->password = Hash::make('123456789');
            // $save->sendEmailVerificationNotification();
            $save->save();

            $outlet_save = new CustomerOutlet;
            $outlet_save->customer_id = $save->id_customer;
            $outlet_save->outlet_id = $request->outlet;
            $outlet_save->save();

            $consultant_save = new CustomerConsultant;
            $consultant_save->customer_id = $save->id_customer;
            $consultant_save->consultant_id = $request->consultant;
            $consultant_save->save();
        } else {
            $errors = $validator->errors();
            $message = $errors;
            $status = false;
        }


        return response()->json([
            'message' => $message,
            'status' => $status,
        ]);
    }

    public function storeCustomerConsultant(Request $request, $id)
    {
        $storeConsultant = new CustomerConsultant;
        $storeConsultant->customer_id = $id;
        $storeConsultant->consultant_id = $request->consultant;
        $storeConsultant->save();

        return redirect(route('customers.show', $id));
    }

    public function storeCustomerOutlet(Request $request, $id)
    {
        $storeOutlet = new CustomerOutlet;
        $storeOutlet->customer_id = $id;
        $storeOutlet->outlet_id = $request->outlet;
        $storeOutlet->save();

        return redirect(route('customers.show', $id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $now = Carbon::now()->toDateString();

        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $listOutlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $listConsultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $overview = Customer::find($id);
        $appointment = Appointment::where('customer_id', $id)->get();
        $active_invoice = Invoice::where('customer_id', $id)
            ->where('void_invoice', 0)
            ->where('expired_date', '>=', $now)
            ->orderBy('created_at', 'DESC')
            ->get();
        $expired_invoice = Invoice::where('customer_id', $id)
            ->where('void_invoice', 0)
            ->where('expired_date', '<=', $now)
            ->get();
        $invoice = Invoice::where('void_invoice', 0)->where('customer_id', $id)->orderBy('created_at', 'DESC')->get();
        $sales_summary = Invoice::where('customer_id', $id)->where('void_invoice', 0)->get()->groupBy('outlet_id');
        $void_invoice = Invoice::where('customer_id', $id)->where('void_invoice', 1)->orderBy('created_at', 'DESC')->get();

        $balance = InvoiceBalance::where('customer_id', $id)->where('status_balance', 0)->orderBy('created_at', 'DESC')->get();
        $void_balance = InvoiceBalance::where('customer_id', $id)->where('status_balance', 1)->orderBy('created_at', 'DESC')->get();

        $usage = Usage::where('customer_id', $id)->where('status_usage', 0)->get();
        $void_usage = Usage::where('customer_id', $id)->where('status_usage', 1)->get();

        $medical_alert = MedicalAlert::where('customer_id', $id)->get();

        $redeem_points = $overview->active_invoice_points;
        foreach ($redeem_points as $redeem_point) {
            $redeem_point->created_date = (new Carbon($redeem_point->created_at))->format('d/m/Y');
        }
        $void_redeem_points = $overview->void_invoice_points;
        foreach ($void_redeem_points as $redeem_point) {
            $redeem_point->created_date = (new Carbon($redeem_point->created_at))->format('d/m/Y');
            $redeem_point->void->created_date = (new Carbon($redeem_point->void->created_at))->format('d/m/Y');
        }

        $anamnesa = Anamnesa::where('customer_id', $id)->orderBy('created_at', 'DESC')->get();
        $consignment = CustomerConsignment::where('customer_id', $id)->orderBy('created_at', 'DESC')->get();
        $physical = PhysicalExamination::where('customer_id', $id)->orderBy('created_at', 'DESC')->get();
        $productTreatment = ProductTreatment::where('status_product_treatment', 1)
        ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();

        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $country = Country::all();
        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $outstandingInvoice = Invoice::where('customer_id', $id)
        ->where('void_invoice', 0)
        ->orderBy('created_at', 'DESC')
        ->get();

        $outstandingSummary = Invoice::selectRaw('SUM(remaining) as total')->where('customer_id', $id)->where('void_invoice', 0)->first();
       
        return view('customers.detail')
            ->with('overview', $overview)
            ->with('medical_alert', $medical_alert)
            ->with('active_invoice', $active_invoice)
            ->with('expired_invoice', $expired_invoice)
            ->with('appointment', $appointment)
            ->with('invoice', $invoice)
            ->with('void_invoice', $void_invoice)
            ->with('void_usage', $void_usage)
            ->with('void_balance', $void_balance)
            ->with('list_consultant', $listConsultant)
            ->with('list_outlet', $listOutlet)
            ->with('balance', $balance)
            ->with('usage', $usage)
            ->with('sales_summary', $sales_summary)
            ->with('redeem_points', $redeem_points)
            ->with('void_redeem_points', $void_redeem_points)
            ->with('anamnesa', $anamnesa)
            ->with('consignment', $consignment)
            ->with('physical', $physical)
            ->with('productTreatment', $productTreatment)
            ->with('country', $country)
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('id', $id)
            ->with('outstandingInvoice', $outstandingInvoice)
            ->with('outstandingSummary', $outstandingSummary);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Customer::find($id);
        $date_birth = Carbon::createFromFormat('d/m/Y', $edit->birth_date)->format('d F Y');
        $edit->join_date = Carbon::createFromFormat('Y-m-d', $edit->join_date)->format('d/m/Y');

        $outlet = CustomerOutlet::where('customer_id', $edit->id_customer)->first();

        $consultant = CustomerConsultant::where('customer_id', $edit->id_customer)->first();
        $result = array('edit' => $edit, 'date_birth' => $date_birth, 'outlet' => $outlet, 'consultant' => $consultant);
        echo json_encode($result);
    }


    public function update(Request $request, $id)
    {
        $update = Customer::findOrFail($id);

        if ($request->verifikasi == true) {
            $update->status_customer = 1;
            $update->update();
            return response()->json(true);
        }

        if ($request->not == true) {
            $update->status_customer = 0;
            $update->update();
            return response()->json(true);
        }

        $validator = Validator::make($request->all(), [
            'phone' => [
                'required',
                Rule::unique('customers')->ignore($id, 'id_customer')
            ],
            // 'email' => [
            //     'required',
            //     Rule::unique('customers')->ignore($id, 'id_customer'),
            // ],
        ]);

        $email = $update->email;

        if (!$validator->fails()) {
            $status = true;
            $message = "Update Created Customer";
            $update->full_name = $request->fullname;
            $update->phone = $request->phone;
            $update->email = $request->email;
            $update->birth_place = $request->place;
            $update->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $update->join_date = Carbon::createFromFormat('d/m/Y', $request->join_date)->format('Y-m-d');
            $update->gender = $request->gender;
            $update->religion = $request->religion;
            $update->ktp = $request->noktp;
            $update->address = $request->address;
            $update->occupation = $request->occupation;
            $update->country_id = $request->country;
            $update->city_id = $request->city;
            $update->zip = $request->zip;
            $update->nextofikin = $request->familyname;
            $update->nextofikin_number = $request->familyphone;
            $update->source_id = $request->source;
            $update->source_remarks = $request->source_remarks;
            $update->purpose_treatment = $request->purpose_treatment;
            $update->staf_id = Auth::user()->id; //auth user login
            if (!$update->password) {
                $update->password = Hash::make('123456789');
            }

            // if (!$update->email_verified_at && $email != $request->email) {
            //     $update->sendEmailVerificationNotification();
            // }
            $update->update();
        } else {
            $errors = $validator->errors();
            $message = $errors;
            $status = false;
        }

        return response()->json([
            'message' => $message,
            'status' => $status,
        ]);
    }


    public function destroyCustomerConsultant($id)
    {
        $destroyConsultant = new CustomerConsultant;
        $customer_id = $destroyConsultant->find($id)->customer_id;
        if ($destroyConsultant->where('customer_id', $customer_id)->count() > 1) {
            $destroyConsultant->find($id)->delete();
        }
        return redirect(route('customers.show', $customer_id));
    }

    public function destroyCustomerOutlet($id)
    {
        $destroyOutlet = new CustomerOutlet;
        $customer_id = $destroyOutlet->find($id)->customer_id;
        if ($destroyOutlet->where('customer_id', $customer_id)->count() > 1) {
            $destroyOutlet->find($id)->delete();
        }
        return redirect(route('customers.show', $customer_id));
    }

    public function activeInvoice($id)
    {
        $invoice = Invoice::find($id);
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $bank = Bank::whereHas('bank_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();
        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $therapist = Therapist::where('status_therapist', 1)
            ->whereHas('therapist_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $log = LogInvoice::where('invoice_id', $id)->get();
        $log_usage = UsageDetail::where('inv_code', $invoice->inv_code)->get();
        $log_convert = LogConvert::where('from_invoice_id', $invoice->inv_code)->get();

        return view('customers.active-expired.detail', compact('invoice', 'marketing_source', 'therapist', 'consultant', 'bank', 'log', 'log_usage', 'log_convert'));
    }

    public function voidActiveInvoice(Request $request, $id)
    {
        $saveLog = new LogVoidInvoice;
        $saveLog->inv_id = $id;
        $saveLog->staff_id = Auth::user()->id;
        $saveLog->remarks = $request->remarks;
        $saveLog->save();

        $invoice = Invoice::find($id);
        $invoice->void_invoice = 1;
        $invoice->update();

        
        $void = InvoiceBalance::where('invoice_id', $id)->first();
        if ($void) {
            $log = new LogVoidBalance;
            $log->balance_id = $void->id_balance;
            $log->staff_id = Auth::user()->id;
            $log->remarks = $request->remarks;
            $log->save();
            
            $void->status_balance = 1;
            $void->update();
        }
        
        $getDetail = InvoiceDetail::where('inv_id', $id)->pluck('id_inv_detail')->toArray();
        $this->decrementSellable($invoice, $getDetail);
        InvoicePackage::whereIn('inv_detail_id', $getDetail)->delete();
        InvoiceDetail::where('inv_id', $id)->delete();

        $rank = Invoice::where('customer_id', $request->customer_id)
            ->where('status_inv', 1)
            ->where('void_invoice', 0)
            ->sum('total');

        //dd($rank);

        $update_point = Customer::find($invoice->customer_id);
        $update_point->point = round($rank / 25000);
        $update_point->update();
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function decrementSellable($invoice, $details)
    {
        foreach ($details as $id) {
            $invoiceDetail = InvoiceDetail::find($id);

            if ( ! isset($invoiceDetail->anamnesa_detail_id)) {
                $productTreatment = ProductTreatment::where('product_treatment_code', $invoiceDetail->package_code)->first();
                
                if ($productTreatment) {
                    $item = Item::where('sales_information_id', $productTreatment->id_product_treatment)->first();
                    if ($item->type == 'product') {
                        $item->sellable_stock += $invoiceDetail->qty;
                        $item->save();
                    } else {
                        foreach ($item->boms as $material) {
                            $sellable = $material->rawItem->sellable_stock;
                            $material->rawItem()->update([
                                'sellable_stock' => $sellable + ($invoiceDetail->qty * $material->qty)
                            ]);
                        }
                    }
                }
            } else {
                $anamnesaDetail = AnamnesaDetail::find($invoiceDetail->anamnesa_detail_id);
                foreach ($anamnesaDetail->boms as $material) {
                    $sellable = $material->rawItem->sellable_stock;
                    $material->rawItem()->update([
                        'sellable_stock' => $sellable + ($invoiceDetail->qty * $material->qty)
                    ]);
                }
            }
        }
    }

    public function redeemInvoice($id)
    {
        $invoice = InvoicePoint::findOrFail($id);
        $invoice->created_date = (new Carbon($invoice->created_at))->format('d/m/Y');
        if ($invoice->void) {
            $invoice->void->created_date = (new Carbon($invoice->void->created_at))->format('d/m/Y');
        }
        return view('customers.point.detail', compact('invoice'));
    }

    public function voidInvoicePoint(Request $request, $id)
    {
        $invoice = InvoicePoint::findOrFail($id);
        $invoice->void_invoice = 1;
        $invoice->update();

        $saveLog = new LogVoidInvoicePoint();
        $saveLog->inv_point_id = $id;
        $saveLog->staff_id = Auth::user()->id;
        $saveLog->remarks = $request->remarks ? $request->remarks : '';
        $saveLog->save();
        $update_point = Customer::find($invoice->customer->id_customer);
        $update_point->point = round($update_point->point + $invoice->total);
        $update_point->update();
    }

    public function updateActiveInvoice(Request $request, $id)
    {
        $log = new LogInvoice;
        $log->invoice_id = $id;
        $log->staff_id = Auth::user()->id;
        $log->save();

        $update_invoice = Invoice::find($id);
        $update_invoice->inv_date = Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d');
        $update_invoice->expired_date = Carbon::createFromFormat('d/m/Y', $request->date_expired)->format('Y-m-d');
        $update_invoice->therapist_id = $request->therapist;
        $update_invoice->consultant_id = $request->consultant;
        $update_invoice->marketing_source_id = $request->marketing_source;
        $update_invoice->update();

        for ($i = 0; $i < count($request->payment_type); $i++) {
            $payment = Payment::find($request->id_payment[$i]);
            $payment->bank_id = $request->payment_type[$i];
            $payment->update();
        }
    }


    public function treatmentBalance(Request $request, $id)
    {
        if ($request->check_code) {
            $check_code = [];
            for ($i = 0; $i < count($request->check_code); $i++) {
                $check_code[] = $request->check_code[$i];
            }

            $invoice = Invoice::where('customer_id', $id)->first();
            $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
            $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

            $product = InvoicePackage::whereIn('id_invoice_package', $check_code)->get();
            $therapist = Therapist::where('status_therapist', 1)
                ->whereHas('therapist_outlets', function ($q) use ($outlet_id) {
                    return $q->whereIn('outlet_id', $outlet_id);
                })->get();

            return view('customers.treatment-balance.usage', compact('invoice', 'outlet', 'product', 'therapist'));
        }

        return Redirect::back()->withErrors('No Treatment Selected');
    }

    public function destroy($id)
    {
        $user = Customer::find($id);
        if (!count($user->invoice) || count($user->appointment)) {
            $user->delete();
        }
    }

    public function storeAnamnesa(Request $request, $id)
    {
        $custom = Customer::findOrFail($id);

        $saveAnamnesa = new Anamnesa;
        $saveAnamnesa->anamnesa_code = Anamnesa::generateTreatmentCode('ANM');
        $saveAnamnesa->customer_id = $custom->id_customer;  
        $saveAnamnesa->anamnesa = $request->anamnesas;
        $saveAnamnesa->suggestion = $request->suggestion;
        $saveAnamnesa->date_anamnesa =  Carbon::createFromFormat('d/m/Y', $request->date_anamnesa)->format('Y-m-d');
        $saveAnamnesa->save();

        $save_json = json_decode($request->save_json);
        foreach ($save_json as $key => $data) {
            $anamnesaDetail = new AnamnesaDetail;
            $anamnesaDetail->anamnesa_id = $saveAnamnesa->id_anamnesa;
            $anamnesaDetail->product_id = $data->type_id;
            $anamnesaDetail->qty = $data->qty;
            $anamnesaDetail->price = intval(str_replace(array('.', ','), "", $data->price));
            $anamnesaDetail->save();
        }

        return redirect(route('customers.show', $id));

    }
    public function editAnamnesa($id)
    {
        $anamnesa =Anamnesa::find($id);
        $product = array();
        foreach ($anamnesa->anamnesa_detail as $productx) {
            if($productx->product_treatment)
            {
             $product[] = array(
                'type_name' => $productx->product_treatment->product_treatment_name,
                'type_id' => $productx->product_id,
                'qty' => $productx->qty,
                'price' => $productx->price,
                'current_price' => $productx->product_price,
                'id_anamnesa_detail' => $productx->id_anamnesa_detail,
            );
            }
            //dd($productx->product_price);
         
        }
        return response()->json([
            'product' => $product,
            'anamnesa' => $anamnesa,
        ]);
    }

    public function updateAnamnesa(Request $request, $id)
    {
        $update =Anamnesa::find($request->id_anamnesa);

        $update->anamnesa = $request->anamnesas;
        $update->suggestion = $request->suggestion;
        $update->date_anamnesa =  Carbon::createFromFormat('d/m/Y', $request->date_anamnesa)->format('Y-m-d');
        
   
        $save_json = json_decode($request->save_json);
        $product_id = [];
        foreach ($save_json as $key => $data) {
            $product_id[] = $data->type_id;
        }

        $product = AnamnesaDetail::where('anamnesa_id', $update->id_anamnesa)->pluck('product_id')->toArray();
        $productDelete = array_diff($product, $product_id);

        if (count($productDelete)) {
            AnamnesaDetail::where('anamnesa_id', $update->id_anamnesa)
                ->whereIn('product_id', $productDelete)
                ->delete();
        }
        foreach ($save_json as $key => $data) {
            $cek = '';
            if (isset($data->id_anamnesa_detail)) {
                $cek = $data->id_anamnesa_detail;
            }
            AnamnesaDetail::firstOrCreate(
                ['id_anamnesa_detail' => $cek],
                [
                    'anamnesa_id' => $update->id_anamnesa,
                    'product_id' => $data->type_id,
                    'qty' => $data->qty,
                    'price' => intval(str_replace(array('.', ','), "", $data->price)),
                ]
            );
        }
        $update->update();
    }
    public function destroyAnamnesa($id)
    {
        $destroyAnamnesa = new Anamnesa;
        $customer_id = $destroyAnamnesa->find($id)->customer_id;
        $destroyAnamnesa->find($id)->delete();
        AnamnesaDetail::where('anamnesa_id', $id)->delete();

        return redirect(route('customers.show', $customer_id));
    }


    public function storeConsignment(Request $request, $id)
    {
        $custom = Customer::findOrFail($id);

        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,svg',
        ]);
        
        $saveConsignment = new CustomerConsignment;
        $saveConsignment->consignment_date =  Carbon::createFromFormat('d/m/Y', $request->consignment_date)->format('Y-m-d');
        $saveConsignment->customer_id = $custom->id_customer;

        $image = $request->file('image');
        if ($image) {
            $img_path = $image->store('customerConsigment', 'public');
            $saveConsignment->image = $img_path;
        }
        
        $saveConsignment->save();
    }
    public function storePhysical(Request $request, $id)
    {
        $custom = Customer::findOrFail($id);

        request()->validate([

            'photodata' => 'required',

        ]);
        $savePhysical = new PhysicalExamination;
        $savePhysical->examination_date =  Carbon::createFromFormat('d/m/Y', $request->examination_date)->format('Y-m-d');
        $savePhysical->customer_id = $custom->id_customer;
        $savePhysical->description = $request->description;

        $filename  = date('mdYHis') . "-physicalexam.png";
        $encoded_image = explode(",", $request->photodata)[1];
        $decoded_image = base64_decode($encoded_image);
        file_put_contents(__DIR__. '/../../../storage/app/public/physicalExamination/'. $filename, $decoded_image);

        $savePhysical->image = 'physicalExamination/'. $filename;

        
        // $image = $request->file('image');
        // if ($image) {
        //     $img_path = $image->store('physicalExamination', 'public');
        //     $savePhysical->image = $img_path;
        // }
        
        $savePhysical->save();
    }

    public function destroyPhysical($id)
    {
        $destroyPhysical = new PhysicalExamination;
        $customer_id = $destroyPhysical->find($id)->customer_id;
        $destroyPhysical->find($id)->delete();
        
        return redirect(route('customers.show', $customer_id));
    }

    public function showPict($id)
    {

        $model =CustomerConsignment::find($id);
        return response()->json($model);

    }
    public function showPictPhysical($id)
    {

        $model =PhysicalExamination::find($id);
        return response()->json($model);

    }

    public function chooseProduct($id)
    {
        $product = ProductTreatment::find($id);
        return json_encode($product);
    }
}
