<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Carbon\Carbon;
use DataTables;
use App\Models\Customer;
use App\Models\CustomerConsultant;
use App\Models\CustomerOutlet;
use App\Models\Outlet;
use App\Models\Appointment;
use App\Models\Invoice;
use App\Models\Message;
use App\Models\MessageDetail;
use KrmPesan\Client;


class CustomerInactiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('customer')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $outlet = Outlet::all();

        return view('customerInactive.index')
        ->with('outlet', $outlet);
    }

    public function list(Request $request)
    {
        $invoice = [];
        if ($request->range == 0) {
            $backdate = date('Y-m-d', strtotime('-1 year', strtotime(now())));
        } elseif ($request->range == 1) {
            $backdate = date('Y-m-d', strtotime('-6 month', strtotime(now())));
        } elseif ($request->range == 2) {
            $backdate = date('Y-m-d', strtotime('-3 month', strtotime(now())));
        } elseif ($request->range == 3) {
            $backdate = date('Y-m-d', strtotime('-1 month', strtotime(now())));
        } else {
            $backdate = date('Y-m-d', strtotime('-1 week', strtotime(now())));
        }
        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
            
            $invoice = Invoice::where('inv_date', '<', $backdate)
            ->whereIn('outlet_id', $outlet_id)
            ->get()
            ->groupBy('customer_id');
        }

        $data = array();
        foreach ($invoice as $list) {
            $row = array();
            $row[] = $list[0]->customer->full_name;
            $row[] = $list[0]->customer->induk_customer;
            $row[] = $list[0]->customer->phone;
            $row[] = $list[0]->customer->email;
            $row[] = $list[0]->customer->birth_date;
            $last = [];
            foreach ($list as $value) {
                $last[] = $value->inv_date;
            }
            $row[] = last($last);
            $appointment = Appointment::where('come_date', '<', $backdate)
            ->where('customer_id', $list[0]->customer->customer_id)
            ->latest('come_date')->first();
            $row[] = $appointment;
            

            $row[] = last($last);
            $row[] = "<a href=customers/".$list[0]->customer->id_customer." class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a>";
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function store(Request $request)
    {

        $invoices = [];
        if ($request->customer == 0) {
            $backdate = date('Y-m-d', strtotime('-1 year', strtotime(now())));
        } elseif ($request->customer == 1) {
            $backdate = date('Y-m-d', strtotime('-6 month', strtotime(now())));
        } elseif ($request->customer == 2) {
            $backdate = date('Y-m-d', strtotime('-3 month', strtotime(now())));
        } elseif ($request->customer == 3) {
            $backdate = date('Y-m-d', strtotime('-1 month', strtotime(now())));
        } else {
            $backdate = date('Y-m-d', strtotime('-1 week', strtotime(now())));
        }
        $outlet = explode(",", $request->outlets);
        if ($outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($outlet); $i++) {
                $outlet_id[] = $outlet[$i];
            }
            
            $invoices = Invoice::where('inv_date', '<', $backdate)
            ->whereIn('outlet_id', $outlet_id)
            ->get();
        }
        // $img = $request->file('image');
        // dd(mime_content_type($img));
        $save = new Message();
        $save->message = $request->message;
    
        $img = $request->file('image');
        if ($img != '') {
            $img_path = $img->store('imageMessage', 'public');
            $save->image = $img_path;
        }
        $save->source = 'inactive patient';
        $save->save();

        foreach ($invoices as $list) {
            $customer = new MessageDetail;
            $customer->message_id = $save->id_message;
            $customer->customer_id = $list->customer->id_customer;
            $customer->save();
        }

        return response()->json([
            'id_message' => $save->id_message,
        ]);

        // return redirect()->action('CustomerInactiveController@index');
    }

    public function sendWhatsApp($id)
   {
       $wa = new Client([
        'region' => '01',
        'token' => 'csAefgICgNZigLDxjlpIoBtdl56DJvSkQbndLvYx26Q6EcDdfERUw9V3TTSeUtz0yW9Vlf5Vt2jkU3FT'
       ]);

       $dataMessage = Message::find($id);
       foreach ($dataMessage->detail as $det) {
            $phone = $det->customer->phone;  
            $message = $dataMessage->message;
            $img = $dataMessage->image;
            $file = storage_path("app/public/". $img);
            // dd($file);
            // $filename = basename($file);
            // $filemime = mime_content_type($file);
            if ($img == '') {
                $send = $wa->sendMessageText($phone, $message);   
            } else {
                $send = $wa->sendMessageImage($phone, $file, $message);
            }
       }

       return $send;
   }
}
