<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Outlet;
use App\Models\Usage;
use App\Models\UserOutlet;
use Auth;
use Carbon\carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            if(Gate::allows('admin')) return $next($request);
            return redirect('/by-room');
        })->except('changePassword', 'updatePassword');
    }

    public function changePassword()
    {
        return view('users.changepassword');
    }

    public function updatePassword(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), auth()->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }
        if (Hash::check(auth()->user()->password, $request->get('new_password'))) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        $user = auth()->user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return redirect()->back()->with("success", "Password changed successfully !");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $outlet_user = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_user)->get();

        if ($request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $getOutlet = $outlet->find($request->outlet);
            $collect = collect($getOutlet->outlet_name);

            $from2 = Carbon::now()->toDateString();
            $to2 = Carbon::now()->toDateString();
            $getOutlet2 = $outlet->find($request->outlet);
            $collect2 = collect($getOutlet2->outlet_name);

            $from3 = Carbon::now()->toDateString();
            $to3 = Carbon::now()->toDateString();
            $getOutlet3 = $outlet->find($request->outlet);
            $collect3 = collect($getOutlet3->outlet_name);
        } else {
            $from = date('Y').'-01-01';
            $to = date('Y').'-12-30';
            $getOutlet = $outlet->first();
            $collect = collect($getOutlet->outlet_name);

            $from2 = Carbon::now()->toDateString();
            $to2 =  Carbon::now()->toDateString();
            $getOutlet2 = $outlet->first();
            $collect2 = collect($getOutlet2->outlet_name);

            $from3 = Carbon::now()->toDateString();
            $to3 = Carbon::now()->toDateString();
            $getOutlet3 = $outlet->first();
            $collect3 = collect($getOutlet3->outlet_name);
        }

        $parseFrom = Carbon::parse($from)->format('F Y');
        $parseTo = Carbon::parse($to)->format('F Y');

        $collect->prepend('Month');
        $array = [];
        $array[] = $collect;
        $title = collect($array);

        $collectCustomer = collect('New');
        $collectCustomer->prepend('Existing');
        $collectCustomer->prepend('Month');
        $arrayCustomer = [];
        $arrayCustomer[] = $collectCustomer;
        $titleCustomer = collect($arrayCustomer);

        $collection = array();
        $treatment = array();
        $product = array();
        $customer = array();
        while (strtotime($from) <= strtotime($to)) {
            $tanggal = $from;

            $day = Carbon::createFromFormat('Y-m-d', $tanggal)->day;
            $month = Carbon::createFromFormat('Y-m-d', $tanggal)->month;
            $tahun = Carbon::createFromFormat('Y-m-d', $tanggal)->year;
            $month_full = Carbon::createFromFormat('Y-m-d', $tanggal)->format('M');

            $collection[] = $this->collectionOutlet($tahun, $month, $getOutlet, $month_full);

            $treatment[] = $this->TreatmentRevenue($tahun, $month, $getOutlet, $month_full);
            $product[] = $this->ProductRevenue($tahun, $month, $getOutlet, $month_full);
            $customer[] = $this->Customer($tahun, $month, $getOutlet, $month_full);

            $from = date('Y-m-d', strtotime("next month", strtotime($from)));
        }

        while (strtotime($from2) <= strtotime($to2)) {
            $tanggal = $from2;

            $day_full = Carbon::createFromFormat('Y-m-d', $tanggal)->format('D');
            $day = Carbon::createFromFormat('Y-m-d', $tanggal)->format('d M Y');

            $collection_today[] = $this->collectionToday($getOutlet2, $day_full,$day);
            $from2 = date('Y-m-d', strtotime("next day", strtotime($from2)));

        
        }

        while (strtotime($from3) <= strtotime($to3)) {
            $tanggal = $from3;

            $month = Carbon::createFromFormat('Y-m-d', $tanggal)->month;
            $month_full = Carbon::createFromFormat('Y-m-d', $tanggal)->format('F Y');

            $collection_month[] = $this->collectionMonth($month,$getOutlet3, $month_full);
            $from3 = date('Y-m-d', strtotime("next month", strtotime($from3)));

        
        }

        $collection = $title->concat($collection);
        $collection_month = $title->concat($collection_month);
        $collection_today = $title->concat($collection_today);

        $treatment_done = $title->concat($treatment);
        $product_done = $title->concat($product);
        $customers = $titleCustomer->concat($customer);

        return view('dashboard')
            ->with('data', json_encode($collection))
            ->with('data_month', json_encode($collection_month))
            ->with('data_today', json_encode($collection_today))
            ->with('data_treatment', json_encode($treatment_done))
            ->with('data_product', json_encode($product_done))
            ->with('data_customer', json_encode($customers))
            ->with('outlet', $outlet)
            ->with('outlet_name', 'Outlet ' . $getOutlet->outlet_name)
            ->with('outlet_id', $getOutlet->id_outlet)
            ->with('from', $parseFrom)
            ->with('to', $parseTo);
    }

    public function collectionOutlet($tahun, $month, $getOutlet, $month_full)
    {
        $summary = Invoice::whereYear('inv_date', $tahun)
            ->whereMonth('inv_date', $month)
            ->where('void_invoice', 0)
            ->where('outlet_id', $getOutlet->id_outlet)
            ->get();

        $total_paid = 0;
        foreach ($summary as $item) {
            foreach ($item->payment as $payment) {
                if (!$payment->balance_id) {
                    $total_paid += str_replace(",", "", $payment->amount);
                }
            }
        }

        $row = array();
        $row[] = $month_full . ' ' . $tahun;
        $row[] = $total_paid;
        return $row;
    }

    public function collectionMonth($month, $getOutlet3, $month_full)
    {
        $summary = Invoice::whereMonth('inv_date', $month)
            ->where('void_invoice', 0)
            ->where('outlet_id', $getOutlet3->id_outlet)
            ->get();

        $total_paid = 0;
        foreach ($summary as $item) {
            foreach ($item->payment as $payment) {
                if (!$payment->balance_id) {
                    $total_paid += str_replace(",", "", $payment->amount);
                }
            }
        }

        $row = array();
        $row[] = $month_full;
        $row[] = $total_paid;
        return $row;
    }


    public function collectionToday($getOutlet2, $day_full,$day)
    {

        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        $summary = Invoice::whereBetween('inv_date', [$from,$to])
            ->where('void_invoice', 0)
            ->where('outlet_id', $getOutlet2->id_outlet)
            ->get();

        $total_paid = 0;
        foreach ($summary as $item) {
            foreach ($item->payment as $payment) {
                if (!$payment->balance_id) {
                    $total_paid += str_replace(",", "", $payment->amount);
                }
            }
        }

        $row = array();
        $row[] = $day_full.','.$day;
        $row[] = $total_paid;
        return $row;

    }
    public function TreatmentRevenue($tahun, $month, $getOutlet, $month_full)
    {
        $sum_treatment = Usage::whereYear('usage_date', $tahun)
            ->whereMonth('usage_date', $month)
            ->where('status_usage', 0)
            ->where('outlet_id', $getOutlet->id_outlet)
            ->get();

        $value = 0;
        foreach ($sum_treatment as $item) {
            foreach ($item->usage_detail as $usage) {
                if (substr($usage->product_treatment->product_treatment_code, 0, 1) == 'T') {
                    $value += $usage->value * $usage->used;
                }
            }
        }

        $row_treatment = array();
        //$row_treatment[] = $month_full;
        $row_treatment[] = $month_full . ' ' . $tahun;
        $row_treatment[] = $value;
        return $row_treatment;
    }

    public function ProductRevenue($tahun, $month, $getOutlet, $month_full)
    {
        $sum_product = Usage::whereYear('usage_date', $tahun)
            ->whereMonth('usage_date', $month)
            ->where('outlet_id', $getOutlet->id_outlet)
            ->get();

        $value = 0;
        foreach ($sum_product as $item) {
            foreach ($item->usage_detail as $usage) {
                if (substr($usage->product_treatment->product_treatment_code, 0, 1) == 'P') {
                    $value += $usage->value * $usage->used;
                }
            }
        }

        $row_product = array();
        $row_product[] = $month_full . ' ' . $tahun;
        $row_product[] = $value;
        return $row_product;
    }

    public function Customer($tahun, $month, $getOutlet, $month_full)
    {
        $invoices = Invoice::whereMonth('inv_date', $month)->whereYear('inv_date', $tahun)
            ->where('void_invoice', 0)
            ->where('outlet_id', $getOutlet->id_outlet)->get();
        $new_customer = array();
        $old_customer  = array();
        foreach ($invoices as $invoice) {
            if ($invoice->customer) {
                $customerData = $invoice->customer;
                $dateCarbon = Carbon::createFromFormat('Y-m-d', $customerData->join_date);
                if ($dateCarbon->year < $tahun) {
                    if (!in_array($customerData->id_customer, $old_customer)) {
                        $old_customer[] = $customerData->id_customer;
                    }
                } elseif ($dateCarbon->year > $tahun) {
                    if (!in_array($customerData->id_customer, $new_customer)) {
                        $new_customer[] = $customerData->id_customer;
                    }
                } else {
                    if ($dateCarbon->month < $month) {
                        if (!in_array($customerData->id_customer, $old_customer)) {
                            $old_customer[] = $customerData->id_customer;
                        }
                    } else {
                        if (!in_array($customerData->id_customer, $new_customer)) {
                            $new_customer[] = $customerData->id_customer;
                        }
                    }
                }
            }
        }
        $row = array();
        $row[] = $month_full;
        $row[] = intval(count($old_customer));
        $row[] = intval(count($new_customer));
        return $row;
    }

    public function all_outlet(Request $request)
    {
        $outlet_user = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_user)->get();

        $from = Carbon::parse($request->from)->format('Y-m-d');
        $to = date('Y').'-12-30';


        $from2 = Carbon::now()->toDateString();
        $to2 = Carbon::now()->toDateString();

        $from3 = Carbon::now()->toDateString();
        $to3 = Carbon::now()->toDateString();

        $fromx = Carbon::parse($from)->format('F Y');
        $tox = Carbon::parse($to)->format('F Y');

        $outlet_name = [];
        foreach ($outlet as $name) {
            $outlet_name[] = $name->outlet_name;
        }

        $collect = collect($outlet_name);
        $collect->prepend('Month');
        $array = [];
        $array[] = $collect;
        $title = collect($array);

        $month = Carbon::createFromFormat('Y-m-d', $from)->month;
        $tahun = Carbon::createFromFormat('Y-m-d', $from)->year;
        $month_full = Carbon::createFromFormat('Y-m-d', $from)->format('F');

        $month3 = Carbon::createFromFormat('Y-m-d', $from3)->month;
        $month_full3 = Carbon::createFromFormat('Y-m-d', $from3)->format('F');

        $day_full = Carbon::createFromFormat('Y-m-d', $from2)->format('D');
        $day = Carbon::createFromFormat('Y-m-d', $from2)->format('d M Y');

        $sum = [];
        $sum_today = [];
        $sum_month = [];
        $sum_treatment = [];
        $sum_product = [];

        foreach ($outlet as $id) {
            $sum_int = Invoice::whereYear('inv_date', $tahun)
                ->whereMonth('inv_date', $month)
                ->where('void_invoice', 0)
                ->where('outlet_id', $id->id_outlet)
                ->get();
            
            $sum_int_today = Invoice::whereBetween('inv_date', [$from2,$to2])
                ->where('void_invoice', 0)
                ->where('outlet_id', $id->id_outlet)
                ->get();

            $sum_int_month = Invoice::whereMonth('inv_date', $month3)
                ->where('void_invoice', 0)
                ->where('outlet_id', $id->id_outlet)
                ->get();

            $total_paid = 0;
            foreach ($sum_int as $item) {
                foreach ($item->payment as $payment) {
                    if (!$payment->balance_id) {
                        $total_paid += str_replace(",", "", $payment->amount);
                    }
                }
            }

            $total_paid2 = 0;
            foreach ($sum_int_today as $item) {
                foreach ($item->payment as $payment) {
                    if (!$payment->balance_id) {
                        $total_paid2 += str_replace(",", "", $payment->amount);
                    }
                }
            }

            $total_paid_month = 0;
            foreach ($sum_int_month as $item) {
                foreach ($item->payment as $payment) {
                    if (!$payment->balance_id) {
                        $total_paid_month += str_replace(",", "", $payment->amount);
                    }
                }
            }
    

            $treatment = Usage::whereYear('usage_date', $tahun)
                ->whereMonth('usage_date', $month)
                ->where('status_usage', 0)
                ->where('outlet_id', $id->id_outlet)
                ->get();

            $value = 0;
            foreach ($treatment as $itemx) {
                foreach ($itemx->usage_detail as $usagex) {
                    if (substr($usagex->product_treatment->product_treatment_code, 0, 1) == 'T') {
                        $value += $usagex->value * $usagex->used;
                    }
                }
            }

            $product = Usage::whereYear('usage_date', $tahun)
                ->whereMonth('usage_date', $month)
                ->where('status_usage', 0)
                ->where('outlet_id', $id->id_outlet)
                ->get();

            $product_sum = 0;
            foreach ($product as $item) {
                foreach ($item->usage_detail as $usage) {
                    if (substr($usage->product_treatment->product_treatment_code, 0, 1) == 'P') {
                        $product_sum += $usage->value * $usage->used;
                    }
                }
            }

            $sum_product[] = intval($product_sum);
            $sum_treatment[] = intval($value);
            $sum_today[] = intval($total_paid2);
            $sum_month[] = intval($total_paid_month);
            $sum[] = intval($total_paid);
        }

        $collect_sum = collect($sum);
        $collect_sum->prepend($month_full . ' ' . $tahun);
        $data = array();
        $data[] = $collect_sum;
        $collect_data = $data;
        $result = $title->concat($collect_data);


        $collect_sum_month = collect($sum_month);
        $collect_sum_month->prepend($month_full3);
        $data_month = array();
        $data_month[] = $collect_sum_month;
        $collect_data_month = $data_month;
        $result_month = $title->concat($collect_data_month);

        $collect_sum_today = collect($sum_today);
        $collect_sum_today->prepend($day_full.','.$day);
        $data_today = array();
        $data_today[] = $collect_sum_today;
        $collect_data_today = $data_today;
        $result_today = $title->concat($collect_data_today);

        
        $collect_sum_treatment = collect($sum_treatment);
        $collect_sum_treatment->prepend($month_full . ' ' . $tahun);
        $data_treatment = array();
        $data_treatment[] = $collect_sum_treatment;
        $collect_data_treatment = $data_treatment;
        $result_treatment = $title->concat($collect_data_treatment);

        $collect_sum_product = collect($sum_product);
        $collect_sum_product->prepend($month_full . ' ' . $tahun);
        $data_product = array();
        $data_product[] = $collect_sum_product;
        $collect_data_product = $data_product;
        $result_product = $title->concat($collect_data_product);
        $customers = [];

        return view('dashboard')
            ->with('data', json_encode($result))
            ->with('data_month', json_encode($result_month))
            ->with('data_today', json_encode($result_today))
            ->with('data_treatment', json_encode($result_treatment))
            ->with('data_product', json_encode($result_product))
            ->with('data_customer', json_encode($customers))
            ->with('from', $fromx)
            ->with('outlet', $outlet)
            ->with('outlet_name', 'All Outlet')
            ->with('outlet_id', 0)
            ->with('to', $tox);
    }

    public function InvoiceToday($outlet_id)
    {
        $outlet_user = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();

        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($outlet_id == 0) {
            $detail_invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->whereIn('outlet_id', $outlet_user)
                ->orderBy('id_invoice', 'ASC')
                ->get();
        } else {
            $detail_invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->where('outlet_id', $outlet_id)
                ->orderBy('id_invoice', 'ASC')
                ->get();
        }

        $data = array();
        foreach ($detail_invoice as $list) {
            $row = array();
            if (count($list->log_convert) > 0) {
                $row[] = '<a href=" ' . route("invoice-convert.print", $list->id_invoice) . ' " target="_blank">' . $list->inv_code . '</a>';
            } else {
                $row[] = '<a href="' . route("invoice.print", $list->id_invoice) . '" target="_blank">' . $list->inv_code . '</a>';
            }

            $row[] = $list->inv_date;
            $row[] = $list->outlet->outlet_name;
            $row[] = '<a href=" ' . route("customers.show", $list->customer_id) . ' " target="_blank">' . $list->customer->induk_customer . '</a>';
            $row[] = $list->customer->full_name;
            $row[] = $list->user->username;
            $PaymentType = '';
            $paid = 0;
            foreach ($list->payment as $payment) {
                if (!$payment->balance_id) {
                    $PaymentType .= "&#10004;" . $payment->bank->bank_name . "<br>";
                    $paid += str_replace(",", "", $payment->amount);
                }
            }

            $row[] = $PaymentType;
            $row[] = $list->total;
            $row[] = format_money($paid);
            $row[] = '<a href=" ' . route("customer.active", $list->id_invoice) . ' " class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}
