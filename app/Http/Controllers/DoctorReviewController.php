<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Gate;

use App\Models\Customer;
use App\Models\ProductTreatment;
use App\Models\UserOutlet;
use App\Models\DoctorReviewImage;
use App\Models\DoctorReviewImageDetail;
use App\Models\Usage;
use App\Models\UsageDetail;

use Auth;

use View;

class DoctorReviewController extends Controller
{
    protected $view = 'dr-review.';
    protected $route = 'dr-review.';
    protected $path = 'storage/';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('doctor')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
    }

    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customers = Customer::select('id_customer', 'full_name', 'induk_customer')
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();
    
        return view($this->view . 'index',compact('customers'));
    }

    public function upload()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customers = Customer::select('id_customer', 'full_name', 'induk_customer')
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();

        return view($this->view . 'upload', compact('customers'));
    }

    public function uploadStore(Request $req)
    {
        $req->validate([
            'image'   => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg',
        ]);
        $input = $req->all();
        $input['usage_detail_id'] =$req->treatment;
        $input['desc'] = "Front,Left & Right";
        $input['folder'] = $this->path;
        $input['date_in'] = date('Y-m-d');
        unset($input['image']);

        $detail = [];
        foreach ($req->image as $r) {
            if ($r) {
                $img_path = $r->store('customer', 'public');
                $a = new DoctorReviewImageDetail;
                $a->image = $img_path;
            }

            $detail[] = $a;
        }

        $data = DoctorReviewImage::create($input);
        if ($data) {
            $data->details()->saveMany($detail);
            session()->flash('swal_success', 'Berhasil disimpan');
        } else {
            session()->flash('swal_error', 'Gagal disimpan');
        }
        return redirect()->back();
    }

    public function history()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customers = Customer::select('id_customer', 'full_name', 'induk_customer')
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();
        return view($this->view . 'history', compact('customers'));
    }

    public function historyDetail($id)
    {
        $customer = Customer::findOrFail($id);
        return view($this->view . 'history_detail', compact('customer'));
    }

    public function historyDestroy($id)
    {
        $data = DoctorReviewImage::findOrFail($id);
        $datas = DoctorReviewImageDetail::where('dr_img_id', $id)->get();
        // dd($datas);

        for ($i = 0; $i < count($datas); $i++) {
            if ($datas[$i]->image && file_exists(storage_path('app/public/' . $datas[$i]->image))) {
                \Storage::delete('public/' . $datas[$i]->image);
            }
        }
        $data->delete();
        DoctorReviewImageDetail::where('dr_img_id', $id)->delete();

        session()->flash('swal_success', 'Berhasil dihapus');
        return redirect()->back();
    }

    public function photo($id)
    {
        $data = DoctorReviewImage::findOrFail($id);
        return view($this->view . 'photo', compact('data', 'id'));
    }

    public function sandingkan()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customers = Customer::select('id_customer', 'full_name', 'induk_customer')
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();
        return view($this->view . 'sandingkan', compact('customers'));
    }

    public function sandingkanCustomer(Request $req)
    {
        $usage = Usage::whereCustomerId($req->id)->pluck('id_usage')->toArray();
        $data = UsageDetail::whereIn('usage_id',$usage)
        ->join('usages', 'usages.id_usage', 'usage_details.usage_id')
        ->join('product_treatments', 'product_treatments.id_product_treatment', 'usage_details.product_id')
        ->where('product_treatment_code', 'like', 'T-%')
        ->get();
        
        $res['success'] = true;
        $res['data'] = $data;
        return response()->json($res);
    }

    public function getTreatment(Request $req)
    {

        $data = UsageDetail::join('dr_img','dr_img.usage_detail_id','usage_details.id_usage_detail')
        ->where('dr_img.usage_detail_id',$req->id)->get();

        $res['success'] = true;
        $res['data'] = $data;
        return response()->json($res);
    }

    public function getPhoto(Request $req)
    {
        $data = DoctorReviewImage::find($req->id);
        if ($data) {
            $res['success'] = true;
            $res['data'] = [];
            foreach ($data->details as $r) {
                $res['data'][] = asset($data->folder . $r->image);
            }
        } else {
            $res['success'] = false;
            $res['data'] = [];
        }
        return response()->json($res);
    }
    
}
