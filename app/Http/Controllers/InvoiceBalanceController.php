<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Auth;
use Carbon\Carbon;
use App\Models\Outlet;
use App\Models\Invoice;
use App\Models\Bank;
use App\Models\InvoiceDetail;
use App\Models\Payment;
use App\Models\UserOutlet;
use App\Models\Customer;
use App\Models\InvoiceBalance;
use App\Models\LogVoidBalance;

class InvoiceBalanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $bank = Bank::where('status_bank', 1)
            ->whereHas('bank_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        return view('invoice.balance.index')
            ->with('outlet', $outlet)
            ->with('bank', $bank);
    }

    public function getInvoice($id)
    {
        $invoice = Invoice::where('customer_id', $id)->where('remaining', '>', 0)->get();
        $select = "<option>- Select Invoice -</option>";
        foreach ($invoice as $outstanding) {
            $select .= "<option value=" . $outstanding->id_invoice . ">" . $outstanding->inv_code . "</option>";
        }

        return response()->json($select);
    }

    public function loadInvoice($id)
    {
        $load = Invoice::find($id);
        return response()->json($load);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $balance = new InvoiceBalance;
        $balance->balance_code = InvoiceBalance::generateBalanceCode($request->outlet);
        $balance->balance_date = Carbon::createFromFormat('d/m/Y', $request->date_balance)->format('Y-m-d');
        $balance->customer_id = $request->customer_id;
        $balance->invoice_id = $request->invoice_outstanding;
        $balance->outlet_id = $request->outlet;
        $balance->outstanding = str_replace(",", "", $request->outstanding);
        $balance->remarks = $request->remarks;
        $balance->staff_id = Auth::user()->id;
        $balance->status_balance = 0;
        $balance->save();

        $invoice = Invoice::find($request->invoice_outstanding);
        $invoice->remaining = str_replace(",", "", $request->unpaid);
        if ($request->unpaid == 0) {
            $invoice->status_inv = 1;
        }
        $invoice->update();

        for ($i = 0; $i < count($request->payment_type); $i++) {
            $payment = new Payment;
            $payment->inv_id = $request->invoice_outstanding;
            $payment->bank_id = $request->payment_type[$i];
            $payment->balance_id = $balance->id_balance;
            $payment->amount = $request->amount[$i];
            $payment->info = addslashes($request->payment_info[$i]);
            $payment->save();
        }

        $invcc = Invoice::where('void_invoice', 0)
            ->where('status_inv', 1)
            ->where('customer_id', $request->customer_id)
            ->sum('total');

        $sum_client = $invcc / 25000;
        $update_point = Customer::find($request->customer_id);
        $update_point->point = round($sum_client);
        $update_point->update();

        return response()->json([
            'id_balance' => $balance->id_balance
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $balance = InvoiceBalance::find($id);
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $bank = Bank::where('status_bank', 1)
            ->whereHas('bank_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        return view('customers.balance.detail', compact('balance', 'outlet', 'bank'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = InvoiceBalance::find($id);
        if ($update->outlet_id != $request->outlet) {
            $update->balance_code = InvoiceBalance::generateBalanceCode($request->outlet);
        }
        $update->outlet_id = $request->outlet;
        $update->balance_date = Carbon::createFromFormat('d/m/Y', $request->date_balance)->format('Y-m-d');
        $update->remarks = $request->remarks;
        $update->update();


        for ($i = 0; $i < count($request->payment_type); $i++) {
            $payment = Payment::find($request->id_payment[$i]);
            $payment->bank_id = $request->payment_type[$i];
            $payment->info = addslashes($request->payment_info[$i]);
            $payment->update();
        }
    }

    public function void(Request $request, $id)
    {
        $void = InvoiceBalance::find($id);
        $void->status_balance = 1;
        $void->update();

        //Payment::where('balance_id', $id)->delete();

        $delete_payment = Payment::where('balance_id', $id)->first();
        $void_invoice = Invoice::find($void->invoice_id);
        $remining = str_replace(array('.', ','), "", $void_invoice->remaining);
        $result = $remining + str_replace(array('.', ','), "", $delete_payment->amount);
        //dd($result);
        $void_invoice->remaining = $result;
        $void_invoice->status_inv = 0;
        $void_invoice->update();

        $delete_payment->delete();
        $log = new LogVoidBalance;
        $log->balance_id = $id;
        $log->staff_id = Auth::user()->id;
        $log->remarks = $request->remarks;
        $log->save();


        $rank = Invoice::where('customer_id', $void->customer_id)
            ->where('status_inv', 1)
            ->where('void_invoice', 0)
            ->sum('total');

        $update_point = Customer::find($void->customer_id);
        $update_point->point = round($rank / 25000);
        $update_point->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generatePdf($id)
    {
        $invoice = Invoice::find($id);
        $payment = Payment::where('inv_id', $id)->get();
        $inv_detail = InvoiceDetail::where('inv_id', $id)->get();
        $balance = InvoiceBalance::where('invoice_id', $id)->first();
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');

        return view('invoice.balance.print', compact('invoice', 'payment', 'inv_detail', 'balance'));
    }
}
