<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\Consultant;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoicePackage;
use App\Models\LogInvoice;
use App\Models\MarketingSource;
use App\Models\MedicalAlert;
use App\Models\Outlet;
use App\Models\UserOutlet;
use App\Models\Package;
use App\Models\Logistics\Item;
use App\Models\Payment;
use App\Models\Customer;
use App\Models\ProductTreatment;
use App\Models\Therapist;
use App\Models\Anamnesa;
use App\Models\AnamnesaDetail;
use Auth;
use App\Models\Usage;
use App\Models\UsageDetail;
use App\Models\UsageTherapist;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\InventoryAdjustment;
use App\Models\LogUsage;
use App\Models\WarehouseOutlet;
use Carbon\carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
// use KrmPesan\Client;
use App\Models\Queue;
use App\Services\StarSender as Client;


class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $package = Package::where('status_package', 1)
            ->whereHas('package_outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $product = ProductTreatment::where('status_product_treatment', 1)
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $allItem = $package->concat($product);
        $count = count($allItem);

        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $bank = Bank::where('status_bank', 1)
            ->whereHas('bank_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $therapist = Therapist::where('status_therapist', 1)
            ->whereHas('therapist_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $activity = Activity::where('status_activity', 1)
            ->where('end_date', '>=', now()->toDateString())
            ->where('start_date', '<=', now()->toDateString())
            ->whereHas('activity_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();

        $agent = Agent::where('status', 0)->whereNotNull('uniq_code')->get();

        return view('invoice.create.index')
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('activity', $activity)
            ->with('therapist', $therapist)
            ->with('package', $allItem)
            ->with('count', $count)
            ->with('agent', $agent)
            ->with('bank', $bank);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $getRestraction = MedicalAlert::where('customer_id', $request->customer_id)->pluck('product_id')->toArray();
        $save_json = json_decode($request->save_json);
        $shouldDelete = [];
        foreach ($save_json as $key => $data) {
            if ($data->package_id) {
                $package = Package::find($data->package_id);
                $getIdPackage = [];
                foreach ($package->package_detail as $set) {
                    $getIdPackage[] = $set->product_id;
                }
                $shouldDelete = array_intersect($getRestraction, $getIdPackage);
            }
        }

        if (count($shouldDelete)) {
            echo "error";
        } else {
            $store_invoice = new Invoice;
            $store_invoice->inv_code = Invoice::generateInvoiceCode($request->outlet);
            $store_invoice->inv_date = Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d');
            $store_invoice->expired_date = Carbon::createFromFormat('d/m/Y', $request->date_expired)->format('Y-m-d');
            $store_invoice->customer_id = $request->customer_id;
            $store_invoice->outlet_id = $request->outlet;
            $store_invoice->therapist_id = $request->therapist;
            $store_invoice->consultant_id = $request->consultant;
            $store_invoice->marketing_source_id = $request->marketing_source;
            $store_invoice->remarks = $request->remarks;
            $store_invoice->inv_remarks = $request->old_invoice;
            $store_invoice->total = str_replace(array('.', ','), "", $request->total);
            $store_invoice->staff_id = Auth::user()->id;
            $store_invoice->agent_id = $request->agent;
            $store_invoice->remaining = str_replace(array('.', ','), "", $request->unpaid);
            if($request->kembali > 0) {
                $store_invoice->remaining = 0;
            }else{
                $store_invoice->remaining = str_replace(array('.', ','), "", $request->unpaid);
            }
            
            if($request->kembali > 0)
            {
                $store_invoice->status_inv = 1;
            }else if ($request->unpaid == 0) 
            {
                $store_invoice->status_inv = 1;
            }
            $store_invoice->save();

            $log = new LogInvoice;
            $log->invoice_id = $store_invoice->id_invoice;
            $log->staff_id = Auth::user()->id;
            $log->save();


            $immediates = [];
            $invoiceDetails = [];
            $invoicePackages = [];

            $save_json = json_decode($request->save_json);
            foreach ($save_json as $key => $data) {
                $subtotal = str_replace(array('.', ','), "", $data->subtotal);
                $save = new InvoiceDetail;
                $save->inv_id = $store_invoice->id_invoice;
                $save->package_code = $data->code;
                $save->current_price = $data->price;
                $save->qty = $data->qty;
                $save->subtotal = $subtotal;
                $save->discount_1 = $data->discount_1;
                if ($data->discount_2) {
                    $save->discount_2 = $data->discount_2;
                }
                $save->anamnesa_detail_id = isset($data->anamnesa_detail_id) ? $data->anamnesa_detail_id : null;
                $save->type_line = $data->type_line_id;
                $save->activity_id = $data->activity_id;
                $save->save();

                if ($data->immediate_usage) {
                    $invoiceDetails[] = $save;
                }

                if ($data->package_id) {
                    $package = Package::find($data->package_id);
                    foreach ($package->package_detail as $set) {
                        $save_package = new InvoicePackage;
                        $save_package->inv_detail_id = $save->id_inv_detail;
                        $save_package->product_id = $set->product_id;
                        $save_package->qty = $set->qty * $data->qty;
                        $save_package->used = 0;

                        $item = Item::where('sales_information_id', $set->product_id)->first();
                        
                        if ($item->type == 'product') {
                            $item->sellable_stock -= $set->qty * $data->qty;
                            $item->save();
                        } else {
                            foreach ($item->boms as $material) {
                                $sellable = $material->rawItem->sellable_stock;
                                $material->rawItem()->update([
                                    'sellable_stock' => $sellable - ($set->qty * $data->qty * $material->qty)
                                ]);
                            }
                        }

                        $totalx = $set->price * $data->qty;

                        if ($data->discount_2) {
                            if ($data->type_line_id == 0) {
                                $total1 = ($totalx - (($data->discount_1 / 100) * $totalx)) / ($set->qty * $data->qty);
                                $grandTotal = ($total1 - (($data->discount_2 / 100) * $total1));
                            } else {
                                $total1 = ($totalx - $data->discount_1) / ($set->qty * $data->qty);
                                $grandTotal = $total1 - $data->discount_2;
                            }
                        } else {
                            //rp = 1, % = 0
                            if ($data->type_line_id == 0) {
                                if ($data->discount_1 > 0) {
                                    $grandTotal = ($totalx - (($data->discount_1 / 100) * $totalx)) / ($set->qty * $data->qty);
                                } else {
                                    $grandTotal = $totalx / ($set->qty * $data->qty);
                                }
                            } else {
                                $grandTotal = ($totalx - $data->discount_1) / ($set->qty * $data->qty);
                            }
                        }

                        $save_package->nominal = $grandTotal;
                        $save_package->save();

                        if ($data->immediate_usage) {
                            $invoicePackages[] = $save_package;
                        }
                    }
                } else {
                    $save_package = new InvoicePackage;
                    $save_package->inv_detail_id = $save->id_inv_detail;
                    $save_package->product_id = $data->productTreatment_id;
                    $save_package->anamnesa_detail_id = isset($data->anamnesa_detail_id) ? $data->anamnesa_detail_id : null;
                    $save_package->qty = $data->qty;
                    
                    $save_package->used = 0;
                    $save_package->nominal = $subtotal / $data->qty;
                    $save_package->save();

                    if ($data->immediate_usage) {
                        $invoicePackages[] = $save_package;
                    }

                    $item = Item::where('sales_information_id', $data->productTreatment_id)->first();

                    if ( ! isset($data->anamnesa_detail_id)) {
                        if ($item->type == 'product') {
                            $item->sellable_stock -= $data->qty;
                            $item->save();
                        } else {
                            foreach ($item->boms as $material) {
                                $sellable = $material->rawItem->sellable_stock;
                                $material->rawItem()->update([
                                    'sellable_stock' => $sellable - ($data->qty * $material->qty)
                                ]);
                            }
                        }
                    } else {
                        $anamnesaDetail = AnamnesaDetail::find($data->anamnesa_detail_id);

                        if (count($anamnesaDetail->boms) > 0) {
                            foreach ($anamnesaDetail->boms as $material) {
                                $sellable = $material->rawItem->sellable_stock;
                                $material->rawItem()->update([
                                    'sellable_stock' => $sellable - ($data->qty * $material->qty)
                                ]);
                            }
                        } else {
                            $item->sellable_stock -= $data->qty;
                            $item->save();
                        }
                    }
                }
                if ($save->immediate_usage) {
                    $immediates[] = $key;
                }
            }


            for ($i = 0; $i < count($request->payment_type); $i++) {
                $payment = new Payment;
                $payment->inv_id = $store_invoice->id_invoice;
                $payment->bank_id = $request->payment_type[$i];
                if($request->kembali > 0)
                {
                    $payment->amount = $request->amount[$i] - str_replace(array('.', ','), "",$request->kembali);

                }else{
                    $payment->amount = $request->amount[$i];
                }
                $payment->info = addslashes($request->payment_info[$i]);
                $payment->save();
            }

            $agentccc = Agent::where('agent_code', $request->customer)->first();
            $sum_agentcc = 0;
            if ($agentccc) {
                $agentcc = Invoice::where('void_invoice', 0)
                    ->where('agent_id', $agentccc->id_agent)
                    ->sum('total');
                $sum_agentcc = ($agentcc * 0.1) / 25000;
            }

            // siap siap
            $invcc = Invoice::where('void_invoice', 0)
                ->where('status_inv', 1)
                ->where('customer_id', $request->customer_id)
                ->sum('total');

            $sum_client = $invcc / 25000;
            $update_point = Customer::find($request->customer_id);
            $update_point->point = round($sum_client + $sum_agentcc);
            $update_point->update();

            $agent = Invoice::where('void_invoice', 0)
                ->where('agent_id', $request->agent)
                ->sum('total');

            if ($request->agent) {
                $id_agent = Agent::find($request->agent);
                if ($id_agent->customer) {
                    $agentx = Invoice::where('void_invoice', 0)
                        ->where('status_inv', 1)
                        ->where('customer_id', $id_agent->customer->id_customer)
                        ->sum('total');

                    $totalBefore = $agentx / 25000;
                    $sum_agent = ($agent * 0.1) / 25000;

                    $point_agent = Customer::find($id_agent->customer->id_customer);
                    $point_agent->point = round($sum_agent + $totalBefore);
                    $point_agent->update();
                }
            }

            $queue = Queue::where('customer_id',$store_invoice->customer_id)->count();
            if($queue)
            {
                $updateQueue = Queue::where('customer_id',$store_invoice->customer_id)->latest('created_at')->first();
                $updateQueue->status_payment = 2;
                $updateQueue->update();
            }

            if (count($invoicePackages) > 0) {
                $this->storeUsage($request, $store_invoice, $invoiceDetails, $invoicePackages);
            }
 
            return response()->json([
                'id_invoice' => $store_invoice->id_invoice,
            ]);
        }
    }

    public function storeUsage(Request $request, $invoice, $invoiceDetails, $invoicePackages)
    {
        $usage = new Usage;
        $usage->usage_code = Usage::generateUsageCode($request->outlet);
        $usage->usage_date = Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d');
        $usage->customer_id = $request->customer_id;
        $usage->outlet_id = $request->outlet;
        $usage->staff_id = Auth::user()->id;
        $usage->remarks = 'Immediate Usage';
        $usage->status_usage = 0;
        // $filename       = date('mdYHis') . "-signature.png";
        $usage->file    = null;
        $usage->save();

        $therapist = Therapist::where('therapist_name', 'NONE')->first();

        foreach ($invoicePackages as $invoice_package) {
            $usage_detail = new UsageDetail;
            $usage_detail->usage_id = $usage->id_usage;
            $usage_detail->inv_code = $invoice->inv_code;
            $usage_detail->product_id = $invoice_package->product_id;
            $usage_detail->invoice_package_id = $invoice_package->id_invoice_package;
            $usage_detail->point = 0;

            $usage_detail->qty = 0;
            $usage_detail->used = $invoice_package->qty;
            $usage_detail->duration = 0;
            $usage_detail->value = $invoice_package->nominal;
            $usage_detail->save();

            $invoice_package->used = $invoice_package->qty;
            $invoice_package->qty = 0;
            $invoice_package->update();

            $usage_therapist = new UsageTherapist;
            $usage_therapist->usage_detail_id = $usage_detail->id_usage_detail;
            $usage_therapist->therapist_id = $therapist->id_therapist;
            $usage_therapist->save();
            
        }

        $this->createMutations($request, $invoicePackages, $invoice, $usage);

        return json_encode($usage->id_usage);
    }

    /**
     * Create mutations.
     *
     * @return void
     **/
    public function createMutations(Request $request, $invoicePackages, $invoice, $usage)
    {
        foreach ($invoicePackages as $package) {
            $product = ProductTreatment::find($package->product_id);

            $userOutlets = $request->user()->userOutlets()->pluck('outlet_id');
            $grantedWarehouses = WarehouseOutlet::whereIn('outlet_id', $userOutlets)->pluck('warehouse_id');

            // check if current usage is treatment.
            if ($product->item->type == 'service') {
                foreach ($product->item->boms as $material) {
                    $usageCount = $package->used;
                    $sort = $material->rawItem->dispense_method == 'lifo' ? 'asc' : 'desc';

                    $currentStock = $material->rawItem->stock - ($usageCount * $material->qty);

                    $material->rawItem()->update([
                        'stock' => $currentStock
                    ]);

                    while ($usageCount > 0) {
                        $mutationLog = Mutation::where('transtype', 'in')
                                                ->where('transdesc', '<>', 'BEGIN EMPTY')
                                                ->where('item_id', $material->raw_item_id)
                                                ->whereIn('warehouse_destination_id', $grantedWarehouses)
                                                ->orderBy('transdate', $sort)
                                                ->where('balance', '>', 0)
                                                ->first();
    
                        if ( ! $mutationLog) break;
        
                       
                        $outQty = 0;
                        
                        // current item at this warehouse has not enough stock.
                        if ($mutationLog->balance < $usageCount) {
                            $usageCount -= $mutationLog->balance;
                            $outQty = $mutationLog->balance;
    
                            $mutationLog->balance = 0;
                        } else {
                            $mutationLog->balance -= $usageCount;
                            $outQty = $usageCount;
    
                            $usageCount = 0;
                        }
    
                        $mutationLog->save();
    
                        $mutation = [
                            'warehouse_origin_id' => $mutationLog->warehouse_destination_id,
                            'rack_origin_id' => $mutationLog->rack_destination_id,
                            'sales_invoice_id' => $usage->id_usage,
                            'item_id' => $material->raw_item_id,
                            'transdate' => Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d'),
                            'transtype' => 'out',
                            'cogs'     => $mutationLog->cogs,
                            'price'     => str_replace(',', '', $material->rawItem->sales->price),
                            'ref_id'    => $mutationLog->id,
                            'balance'   => 0,
                            'transdesc' => 'USAGE',
                            'qty' => $outQty * $material->qty * -1
                        ];
                        
                        Mutation::create($mutation);
                    }
                }
            } else {

                $usageCount = $package->used;
                $sort = $product->item->dispense_method == 'lifo' ? 'asc' : 'desc';

                $currentStock = $product->item->stock - $usageCount;

                $product->item()->update([
                    'stock' => $currentStock
                ]);

                while ($usageCount > 0) {
                    $mutationLog = Mutation::where('transtype', 'in')
                                            ->where('transdesc', '<>', 'BEGIN EMPTY')
                                            ->where('item_id', $product->item->id)
                                            ->whereIn('warehouse_destination_id', $grantedWarehouses)
                                            ->orderBy('transdate', $sort)
                                            ->where('balance', '>', 0)
                                            ->first();

                    if ( ! $mutationLog) break;
    
                   
                    $outQty = 0;
                    
                    // current item at this warehouse has not enough stock.
                    if ($mutationLog->balance < $usageCount) {
                        $usageCount -= $mutationLog->balance;
                        $outQty = $mutationLog->balance;

                        $mutationLog->balance = 0;
                    } else {
                        $mutationLog->balance -= $usageCount;
                        $outQty = $usageCount;

                        $usageCount = 0;
                    }

                    $mutationLog->save();

                    $mutation = [
                        'warehouse_origin_id' => $mutationLog->warehouse_destination_id,
                        'rack_origin_id' => $mutationLog->rack_destination_id,
                        'sales_invoice_id' => $usage->id_usage,
                        'item_id' => $product->item->id,
                        'transdate' => Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d'),
                        'transtype' => 'out',
                        'cogs'     => $mutationLog->cogs,
                        'price'     => str_replace(',', '', $product->item->sales->price),
                        'ref_id'    => $mutationLog->id,
                        'balance'   => 0,
                        'transdesc' => 'USAGE',
                        'qty' => $outQty * -1
                    ];
                    
                    Mutation::create($mutation);
                }

            }
        }
    }

    public function sendWhatsApp($id)
    {
        $wa = new Client();

        $dataInv = Invoice::find($id);
        $nameTreatment = '';
        foreach ($dataInv->invoice_detail as $inv) {
            if ($inv->product_treatment) {
                $nameTreatment .= "▪" .$inv->product_treatment->product_treatment_name. "\n" ;    
            }else if($inv->package){
                $nameTreatment .= "▪" .$inv->package->package_name. "\n" ;    
            }
        }
        $message = "*".$dataInv->outlet->outlet_name."*( _By:".$dataInv->user->name."_ )"
                ."\n▪ Nomor Invoice : ". $dataInv->inv_code
                ."\n▪ Total : ". $dataInv->total
                ."\n▪ Invoice Date : ". $dataInv->inv_date
                ."\n*ITEM*\n". $nameTreatment
                ."\n\n*Info selengkapnya hubungi:*"
                ."\n*.Hallo ". $dataInv->outlet->outlet_name . " : " . $$dataInv->outlet->outlet_phone . "*"
                ."\n(". $dataInv->outlet->address . ")"
                ."\n\nTerima kasih.";
    
        if ($dataInv->customer->phone) {
            $send = $wa->sendText($dataInv->customer->phone, $message);    
        }
        
        return response()->json($send);
    }

    public function sign($id) {
        $invoice = Invoice::find($id);
        $payment = Payment::where('inv_id', $id)->get();
        $inv_detail = InvoiceDetail::where('inv_id', $id)->get();
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');

        return view('invoice.create.sign', compact('invoice', 'payment', 'inv_detail'));   
    }

    public function updateSign(Request $request, $id) {
        $invoice = Invoice::find($id);
        
        $filename = "inv-". date('mdYHis') . "-signature.png";
        $invoice->customer_sign = $filename;
        
        $data_uri = explode(',', $request->signature);
        $encoded_image = $data_uri[1];
        $decoded_image = base64_decode($encoded_image);
        file_put_contents(__DIR__. '/../../../storage/app/public/customer/'. $filename, $decoded_image);
        
        $invoice->save();
        return json_encode($invoice->id_invoice);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        InvoiceDetail::find($id)->delete();
        InvoicePackage::where('inv_detail_id', $id)->delete();
    }

    public function generatePdf($id)
    {
        $invoice = Invoice::find($id);
        $payment = Payment::where('inv_id', $id)->get();
        $inv_detail = InvoiceDetail::where('inv_id', $id)->get();
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');

        return view('invoice.create.print', compact('invoice', 'payment', 'inv_detail'));
    }
    public function generateThermalPdf($id)
    {
        $invoice = Invoice::find($id);
        $payment = Payment::where('inv_id', $id)->get();
        $inv_detail = InvoiceDetail::where('inv_id', $id)->get();
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');
        $treatments = array_filter($inv_detail->load('product_treatment')->toArray(), function($row) {
            if (isset($row['product_treatment'])) {
                return substr($row['product_treatment']['product_treatment_code'], 0, 1) == 'T';
            }

            return false;
        });

        $products = array_filter($inv_detail->load('product_treatment')->toArray(), function ($row) {
            if (isset($row['product_treatment'])) {
                return substr($row['product_treatment']['product_treatment_code'], 0, 1) == 'P';
            }

            return false;
        });

        $packages = array_filter($inv_detail->load('package')->toArray(), function($row) {
            return isset($row['package']);
        });
        return view('invoice.create.print_thermal', compact('invoice', 'payment', 'inv_detail', 'treatments', 'products', 'packages'));
    }
    public function getPackage($id)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $package = Package::where('status_package', 1)
            ->whereHas('package_outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $product = ProductTreatment::where('status_product_treatment', 1)
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $allItem = $package->concat($product);
        $oke = $allItem[$id];
        // dd($oke);
        return json_encode($oke);
    }

    public function getActivity($id)
    {
        $activity = Activity::find($id);
        return json_encode($activity);
    }
    public function getAnamnesa($id)
    {
        $anamnesa = Anamnesa::where('customer_id', $id)->get();
        $select = "<option>- Select Anamnesa -</option>";
        foreach ($anamnesa as $anam) {
            $select .= "<option value=" . $anam->id_anamnesa . ">" . $anam->anamnesa . "</option>";
        }

        return response()->json($select);
    }

    public function loadAnamnesa($id)
    {
        $load = AnamnesaDetail::where('anamnesa_id',$id)
        ->join('product_treatments', 'product_treatments.id_product_treatment', 'anamnesa_details.product_id')
        ->get();
        return response()->json(['load' => $load,]);
        
    }
}
