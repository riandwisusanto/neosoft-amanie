<?php

namespace App\Http\Controllers;

use App\Models\Consultant;
use App\Models\Customer;
use App\Models\InvoicePoint;
use App\Models\InvoicePointDetail;
use App\Models\Outlet;
use App\Models\ProductPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class InvoicePointController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = ProductPoint::where('status_product_point', 1)->get();

        $outlet = Outlet::all();
        $consultant = Consultant::all();

        return view('invoice.point.index')
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('product', $product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return response()->json($request);
        $store_invoice = new InvoicePoint();
        $store_invoice->inv_point_code = InvoicePoint::generateInvoiceCode($request->outlet);
        $store_invoice->customer_id = $request->customer_id;
        $store_invoice->outlet_id = $request->outlet;
        $store_invoice->remarks = $request->remarks;
        $store_invoice->total = str_replace(array('.', ','), "", $request->total);
        $store_invoice->staff_id = auth()->user()->id;
        $store_invoice->status_inv_point = 1;
        $store_invoice->save();

        $save_json = json_decode($request->save_json);
        foreach ($save_json as $key => $data) {
            $subtotal = str_replace(array('.', ','), "", $data->subtotal);
            $save = new InvoicePointDetail();
            $save->invoice_point_id = $store_invoice->id_invoice_point;
            $save->product_point_id = $data->product_id;
            $save->current_point = $data->point;
            $save->qty = $data->qty;
            $save->subtotal = $subtotal;
            $save->save();
        }
        $update_point = Customer::find($request->customer_id);
        $update_point->point = round($update_point->point - $store_invoice->total);
        $update_point->update();

        return response()->json([
            'id_invoice' => $store_invoice->id_invoice_point
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvoicePoint  $invoicePoint
     * @return \Illuminate\Http\Response
     */
    public function show(InvoicePoint $invoicePoint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvoicePoint  $invoicePoint
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoicePoint $invoicePoint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvoicePoint  $invoicePoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoicePoint $invoicePoint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvoicePoint  $invoicePoint
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoicePoint $invoicePoint)
    {
        //
        $invoicePoint->delete();
    }
    public function generatePdf($id)
    {
        $invoice = InvoicePoint::find($id);

        return view('invoice.point.print', compact('invoice'));
    }
}
