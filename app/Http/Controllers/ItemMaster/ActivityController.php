<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;

use App\Models\Activity;
use App\Models\ActivityGroup;
use App\Models\Outlet;
use App\Models\ActivityOutlet;
use App\Models\UserOutlet;
use Auth;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $group = ActivityGroup::all();
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        return view('item-master.activities.index', compact('group', 'outlet'));
    }/**
 * undocumented function summary
 *
 * Undocumented function long description
 *
 * @param Type $var Description
 * @return type
 * @throws conditon
 **/
    public function listActivity()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $collection = Activity::where('id_activity', 'like', '%')
                    ->whereHas('activity_outlets', function ($q) use ($outlet_id) {
                        return $q->whereIn('outlet_id', $outlet_id);
                    })
                    ->orderBy('created_at', 'desc')->get();
                    
        $data = array();

        foreach ($collection as $activities) {
            $row = [];
            $status_activity = '';
            if ($activities->status_activity) {
                $status_activity = '&#10004;';
            }

            $row[] = $activities->activity_name;
            $row[] = $activities->activity_code;
            $group = ActivityGroup::findOrFail($activities->activity_group_id);
            $row[] = $group->activity_group_name;
            $row[] = $activities->activity_pic;
            $row[] = $activities->start_date;
            $row[] = $activities->end_date;
            if ($activities->disc_1_type == 0) {
                $row[] = $activities->discount_1."%";
            } else {
                $row[] = "Rp".$activities->discount_1;
            }
            if ($activities->disc_2_type == 0) {
                $row[] = $activities->discount_2."%";
            } else {
                $row[] = "Rp".$activities->discount_2;
            }
            $row[] = $activities->remarks;
            $row[] = $status_activity;

            $row[] = '
                        <a class="btn btn-success btn-sm" href="#" onclick="editFormActivity(\''.$activities->id_activity. '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="#" onclick="deleteDataActivity(\''.$activities->id_activity. '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';

            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
        // return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $save= new Activity;
        $save->activity_group_id = $request->activity_group_id;
        $save->activity_pic = $request->activity_pic;
        $save->activity_name = $request->activity_name;
        $save->disc_1_type = $request->disc_type;
        $save->discount_1 = $request->discount_1;
        $save->disc_2_type = $request->disc_type;
        $save->discount_2 = $request->discount_2;
        $save->remarks = $request->remarks;
        $save->end_date = Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d');
        $save->start_date = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');
        // dd($request->start_date);
        $save->status_activity = 1;
        $save->activity_code = Activity::generateActivityCode();
        $save->save();

        foreach ($request->outlet as $key => $value) {
            $activity_outlets = new ActivityOutlet;
            $activity_outlets->outlet_id = $value;
            $activity_outlets->activity_id = $save->id_activity;
            $activity_outlets->save();
        }
        // return response()->json($request);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $outlet = ActivityOutlet::join('activities', 'activities.id_activity', 'activity_outlets.activity_id')
        ->join('outlets', 'outlets.id_outlet', 'activity_outlets.outlet_id')
        ->where('activity_id', $id)
        ->get();
        $listOutlet = Outlet::all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Activity::find($id);
        $model->outlet = ActivityOutlet::where('activity_id', $id)->pluck('outlet_id');

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model= Activity::findOrFail($id);
        if ($request->disable == true) {
            $model->status_activity = 0;
            $model->update();
            return response()->json(true);
        }

        $model->activity_group_id = $request->activity_group_id;
        $model->activity_pic = $request->activity_pic;
        $model->activity_name = $request->activity_name;
        $model->disc_1_type = $request->disc_type;
        $model->discount_1 = $request->discount_1;
        $model->disc_2_type = $request->disc_type;
        $model->discount_2 = $request->discount_2;
        $model->remarks = $request->remarks;
        $model->end_date = Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d');
        $model->start_date = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');

        if ($request->status_activity) {
            $model->status_activity = 1;
        } else {
            $model->status_activity = 0;
        }

        $model->update();

        $before = ActivityOutlet::where('activity_id', $id)->pluck('outlet_id')->toArray();
        $shouldDelete = array_diff($before, $request->outlet);
        if (count($shouldDelete)) {
            ActivityOutlet::where('activity_id', $id)
                            ->whereIn('outlet_id', $shouldDelete)
                            ->delete();
        }
        foreach ($request->outlet as $outlet_id) {
            ActivityOutlet::firstOrCreate([
                'activity_id' => $id,
                'outlet_id'  => $outlet_id
            ]);
        }
        // return response()->json($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete= Activity::findOrFail($id);

        if ($delete->invoice_detail) {
            echo "error";
        } else {
            $delete->delete();
            $before = ActivityOutlet::where('activity_id', $id)->pluck('outlet_id')->toArray();
            ActivityOutlet::where('activity_id', $id)
              ->whereIn('outlet_id', $before)
              ->delete();
        }
    }
}
