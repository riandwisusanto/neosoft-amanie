<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\Country;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Auth;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('item-master.countries.index');
    }

    public function listData()
    {
        $collection = Country::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $country) {
            $row = [];

            $row[] = $country->country;
            
            $row[] = '
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $country->id_country . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $country->id_country . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new Country;
        $save->country   = $request->country;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Country::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Country::find($id);

        $model->country  = $request->country;
        $model->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Country::find($id);
        if ($model->province) {
            echo "error";
        } else {
            $model->delete();
        }
    }
}
