<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Carbon\Carbon;
use DataTables;
use App\Models\ProductTreatment;
use App\Models\ProductTreatmentPoint;
use App\Models\LogProductTreatment;
use App\Models\Outlet;
use App\Models\ProductTreatmentOutlet;
use App\Models\UserOutlet;
use Auth;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        return view('item-master.products.index', compact('outlets'));
    }

    public function listData()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $treatment = ProductTreatment::where('product_treatment_code', 'like', 'P-%')
                    ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                        return $q->whereIn('outlet_id', $outlet_id);
                    })
                    ->orderBy('created_at', 'desc')->get();
        $data = array();
        foreach ($treatment as $list) {
            $row = array();
            $point = '';
            foreach ($list->product_treatment_points as $key) {
                $point .= "<i class='fa fa-caret-right' aria-hidden='true'></i> ".$key->point."<br>";
            }

            $row[] = $list->product_treatment_code;
            $row[] = $list->product_treatment_name;
            $row[] = Carbon::parse($list->created_at)->format('d/m/Y');
            $row[] = $list->price;
            $row[] = $list->duration;
            $row[] = $point;
            $row[] = $list->status_product_treatment?'&#10004;':'';
            $row[] = "<div class='btn-group-vertical'>
                  <a onclick='editForm(".$list->id_product_treatment.")' title='Edit Data' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
                  </div>";
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'unique:product_treatments,product_treatment_name'
        ]);

        $product = new ProductTreatment;
        $save_point = json_decode($request->save_point);
        $product->product_treatment_code = ProductTreatment::generateTreatmentCode('P');
        $product->product_treatment_name = $request->name;
        $product->price = str_replace(",", "", $request->harga);
        $product->duration = $request->durasi;
        $product->status_product_treatment = 1;
        $product->save();

        $outlets = [];
        foreach ($request->outlet as $key => $value) {
            $outlet = new ProductTreatmentOutlet;
            $outlet->outlet_id = $value;
            $outlets[] = $outlet;
        }
        $product->product_treatment_outlets()->saveMany($outlets);

        $points=array();
        foreach ($save_point as $data) {
            $point = new ProductTreatmentPoint;
            $point->point = $data->point;
            $points[] = $point;
        }
        $product->product_treatment_points()->saveMany($points);

        $log = new LogProductTreatment;
        $log->product_treatment_id = $product->id_product_treatment;
        $log->staff_id = auth()->user()->id;
        $log->price = str_replace(",", "", $request->harga);
        $log->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = ProductTreatment::with('product_treatment_points')->with('price_logs')->findOrFail($id);
        foreach ($edit->price_logs as $priceLog) {
            $priceLog->price = format_money($priceLog->price);
            $priceLog->edited_by;
        }
        $edit->outlet = ProductTreatmentOutlet::where('product_id', $id)->pluck('outlet_id');
        return response()->json($edit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $update = ProductTreatment::findOrFail($id);
        $request->validate([
          'name' => 'unique:product_treatments,product_treatment_name,'.$id.',id_product_treatment'
        ]);
        if ($request->disable == true) {
            $update->status_product_treatment = 0;
            $update->update();
            return response()->json(true);
        }

        $update->product_treatment_name = $request->name;
        if (str_replace(",", "", $update->price) != str_replace(",", "", $request->harga)) {
            $log = new LogProductTreatment;
            $log->product_treatment_id = $update->id_product_treatment;
            $log->staff_id = auth()->user()->id;
            $log->price = str_replace(",", "", $request->harga);
            $log->save();
        }
        $update->price = str_replace(",", "", $request->harga);
        $update->duration = $request->durasi;
        if (empty($request->status_product_treatment)) {
            $update->status_product_treatment = 0;
        } else {
            $update->status_product_treatment = $request->status_product_treatment;
        }
        $update->update();
        //update and delete point
        foreach ($update->product_treatment_points as $point) {
            $found = false;
            foreach (json_decode($request->save_point) as $save) {
                if ($save->id == $point->id_product_treatment_points) {
                    $found = true;
                    if ($point->point != $save->point) {
                        $point->point = $save->point;
                        $point->update();
                    }
                    break;
                }
            }
            if ($found == false) {
                $point->delete();
            }
        }

        //create any new point
        foreach (json_decode($request->save_point) as $save) {
            $found = false;
            foreach ($update->product_treatment_points  as $point) {
                if ($save->id == $point->id_product_treatment_points) {
                    $found = true;
                    break;
                }
            }
            if ($found == false) {
                $insert = new ProductTreatmentPoint;
                $insert->product_treatment_id = $update->id_product_treatment;
                $insert->point = $save->point;
                $insert->save();
            }
        }

        $before = ProductTreatmentOutlet::where('product_id', $id)->pluck('outlet_id')->toArray();
        $shouldDelete = array_diff($before, $request->outlet);
        if (count($shouldDelete)) {
            ProductTreatmentOutlet::where('product_id', $id)
                            ->whereIn('outlet_id', $shouldDelete)
                            ->delete();
        }
        foreach ($request->outlet as $outlet_id) {
            ProductTreatmentOutlet::firstOrCreate([
            'product_id' => $id,
            'outlet_id'  => $outlet_id
          ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = ProductTreatment::findOrFail($id);
        print_r($delete->package_detail);
        //dd($delete->invoice_package->id_invoice_package);
        if ($delete->invoice_package) {
            echo "error";
        } elseif ($delete->package_detail) {
            echo "error";
        } else {
            $delete->delete();
            $before = ProductTreatmentOutlet::where('product_id', $id)->pluck('outlet_id')->toArray();
            ProductTreatmentOutlet::where('product_id', $id)
              ->whereIn('outlet_id', $before)
              ->delete();
        }
    }
}
