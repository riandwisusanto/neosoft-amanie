<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use DataTables;
use App\Models\Group;
use App\Models\Therapist;
use App\Models\Outlet;
use App\Models\TherapistOutlet;
use App\Models\UserOutlet;
use Auth;

class TherapistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        return view('item-master.therapists.index', compact('groups', 'outlets'));
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function listData()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $collection = Therapist::whereHas('therapist_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();
        $data = array();
        foreach ($collection as $therapist) {
            $row = [];
            $status_therapist = '';

            if ($therapist->status_therapist) {
                $status_therapist = '&#10004;';
            }

            $row[] = $therapist->therapist_name;
            if ($therapist->group_id == 0) {
                $row[] = 'Without Group';
            } else {
                $row[] = $therapist->group->group_name;
            }
            $row[] = $therapist->join_date->format('d F Y');
            $row[] = '<img src=\''.'storage/' . $therapist->img . '\' alt = \'no image\' class=\'img-responsive\' style=\'width:60px;\'>';

            $row[] = $status_therapist;
            $row[] = "
                    <a onclick='editForm(".$therapist->id_therapist.")' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
                    <a class='btn btn-info btn-sm' href='".route('therapist-schedules.index', [
                        'id' => $therapist->id_therapist
                    ])."'><i class='fa fa-clock-o'></i> Schedules</a>
                    <a class='btn btn-default btn-sm' href='".route('therapist-leaves.index', [
                        'id' => $therapist->id_therapist
                    ])."'><i class='fa fa-calendar'></i> Leave</a>
                    <a onclick='deleteData(".$therapist->id_therapist.")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>
                    ";
            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'therapist_name' => 'unique:therapists,therapist_name'
        ]);
        $model = new Therapist($request->all());
        $model->status_therapist = 1;
        $model->join_date = now();
        $model->description;

        $img = $request->file('img');
        if ($img) {
            $img_path = $img->store('therapist', 'public');
            $model->img = $img_path;
        }
        $model->save();

        $outlets = [];
        foreach ($request->outlet as $key => $value) {
            $outlet = new TherapistOutlet;
            $outlet->outlet_id = $value;
            $outlets[] = $outlet;
        }
        $model->therapist_outlets()->saveMany($outlets);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Therapist::find($id);
        $model->outlet = TherapistOutlet::where('therapist_id', $id)->pluck('outlet_id');
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $therapist = Therapist::findOrFail($id);
        $request->validate([
            'therapist_name' => 'unique:therapists,therapist_name,'.$id.',id_therapist'
        ]);
        if ($request->disable == true) {
            $therapist->status_therapist = 0;
            $therapist->update();
            return response()->json(true);
        }
        $therapist->therapist_name = $request->therapist_name;
        $therapist->group_id = $request->group_id;
        $therapist->description = $request->description;
        $therapist->type = $request->type;

        if (empty($request->status_therapist)) {
            $therapist->status_therapist = 0;
        } else {
            $therapist->status_therapist = $request->status_therapist;
        }

        $img = $request->file('img');
        if ($img) {
            if ($therapist->img && file_exists(storage_path('app/public/' . $therapist->img))) {
                \Storage::delete('public/' . $therapist->img);
            }
            $img_path = $img->store('therapist', 'public');
            $therapist->img = $img_path;
        }
        


        $therapist->update();

        $before = TherapistOutlet::where('therapist_id', $id)->pluck('outlet_id')->toArray();
        $shouldDelete = array_diff($before, $request->outlet);
        if (count($shouldDelete)) {
            TherapistOutlet::where('therapist_id', $id)
                            ->whereIn('outlet_id', $shouldDelete)
                            ->delete();
        }

        foreach ($request->outlet as $outlet_id) {
            TherapistOutlet::firstOrCreate([
                'therapist_id' => $id,
                'outlet_id'  => $outlet_id
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Therapist::findOrFail($id);
        if ($delete->invoice) {
            echo "error";
        } else {
            $delete->delete();
            $before = TherapistOutlet::where('therapist_id', $id)->pluck('outlet_id')->toArray();
            TherapistOutlet::where('therapist_id', $id)
            ->whereIn('outlet_id', $before)
            ->delete();
        }
    }
}
