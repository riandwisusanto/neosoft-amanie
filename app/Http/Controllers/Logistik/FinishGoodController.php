<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Item;
use App\Models\Uom;
use App\Models\WorkInProcess;
use App\Models\FinishGood;

use DataTables;
use Auth;
use View;

use Carbon\Carbon;

class FinishGoodController extends Controller
{
    protected $view = 'logistik.finish-goods.';
    protected $route = 'finish-goods';
    protected $title = 'Finished Goods';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $work_in_processes = WorkInProcess::where('is_active', '1')->get();
        $uom = Uom::where('is_active', '1')->get();
        $items = Item::where('is_active', '1')->get();
        return view($this->view.'index', compact('items', 'work_in_processes', 'uom'));
    }

    public function listData()
    {
        $collection = FinishGood::orderBy('created_at', 'desc')->get();
        $status = [
            1 => 'Available',
            'Discontinued',
            'Disposal',
        ];
        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->item ? $r->item->description : '';
            $row[] = date('d M Y', strtotime($r->mfg_date));
            $row[] = date('d M Y', strtotime($r->expired_date));
            $row[] = $r->description;
            $row[] = $r->qty;
            $row[] = '<span class="label label-primary">'.$status[$r->status].'</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function getWip($id){
        $data = WorkInProcess::with('work_order.item', 'work_in_process_details', 'uom')->findOrFail($id);
        if($data){
            return response()->json($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new FinishGood;
        $save->work_in_process_id = $request->work_in_process_id;
        $save->item_id            = $request->item_id;
        $save->uom_id             = $request->uom_id;
        $save->description        = $request->description;
        $save->mfg_date           = Carbon::createFromFormat('d/m/Y', $request->mfg_date)->format('Y-m-d');
        $save->expired_date       = Carbon::createFromFormat('d/m/Y', $request->expired_date)->format('Y-m-d');
        $save->qty                = $request->qty;
        $save->status             = $request->status;
        $save->price              = $request->price;
        $save->is_active          = 1;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = FinishGood::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = FinishGood::find($id);

        $model->work_in_process_id = $request->work_in_process_id;
        $model->item_id            = $request->item_id;
        $model->uom_id             = $request->uom_id;
        $model->description        = $request->description;
        $model->mfg_date           = Carbon::createFromFormat('d/m/Y', $request->mfg_date)->format('Y-m-d');
        $model->expired_date       = Carbon::createFromFormat('d/m/Y', $request->expired_date)->format('Y-m-d');
        $model->qty                = $request->qty;
        $model->price              = $request->price;
        $model->status             = $request->status;

        $model->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = FinishGood::find($id);
        $model->delete();

    }
}
