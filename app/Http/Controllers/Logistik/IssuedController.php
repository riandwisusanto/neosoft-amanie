<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Customer;
use App\Models\Item;
use App\Models\Uom;
use App\Models\Issued;
use App\Models\IssuedDetail;
use App\Models\PurchaseOrder;
use App\Models\Warehouse;
use App\Models\Invoice;

use DataTables;
use Auth, View, DB;

use Carbon\Carbon;

class IssuedController extends Controller
{
    protected $view = 'logistik.issued.';
    protected $route = 'issued';
    protected $title = 'Issued';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::all();
        $pos = PurchaseOrder::all();
        $uom = Uom::where('is_active', '1')->get();
        $items = Item::where('is_active', '1')->get();
        $customers = Customer::all();
        $warehouses = Warehouse::where('is_active', '1')->get();
        $code = Issued::generateCode();
        return view($this->view.'index', compact('items', 'customers', 'uom', 'pos', 'warehouses', 'code', 'invoices'));
    }

    public function listData()
    {
        $collection = Issued::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->issued_num;
            $row[] = date('d M Y', strtotime($r->issued_date));
            $row[] = $r->description;
            $row[] = $r->invoice_no;
            $row[] = date('d M Y', strtotime($r->invoice_date));
            $row[] = $r->warehouse ? $r->warehouse->name : '-';
            $row[] = '<span class="label label-primary">'.$r->status_desc.'</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function getInvoice($id){
        $data = Invoice::with('customer')->findOrFail($id);
        if($data){
            return response()->json($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try{
            $save = new Issued;
            $save->warehouse_id = $request->warehouse_id;
            $save->customer_id  = $request->customer_id;
            $save->issued_num   = $request->issued_num;
            $save->issued_date  = Carbon::createFromFormat('d/m/Y', $request->issued_date)->format('Y-m-d');
            $save->description  = $request->description;
            $save->invoice_no   = $request->invoice_no;
            $save->invoice_date = Carbon::createFromFormat('d/m/Y', $request->invoice_date)->format('Y-m-d');
            $save->pay_method   = $request->pay_method;
            $save->due_date     = Carbon::createFromFormat('d/m/Y', $request->due_date)->format('Y-m-d');
            $save->status       = $request->status;

            $items = [];
            foreach ($request->items as $r) {
                $dummy = new IssuedDetail;
                $dummy->item_id     = $r['item_id'];
                $dummy->uom_id      = $r['uom_id'];
                $dummy->description = $r['description'];
                $dummy->price       = $r['price'] ? $r['price'] : 0;
                $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                $dummy->total       = $r['total'] ? $r['total'] : 0;

                $items[] = $dummy;
            }

            $save->save();
            $save->issued_details()->saveMany($items);

            DB::commit();

            return Issued::generateCode();
        }catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Issued::with('issued_details')->find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            $model = Issued::find($id);

            $model->warehouse_id = $request->warehouse_id;
            $model->customer_id  = $request->customer_id;
            $model->issued_date  = Carbon::createFromFormat('d/m/Y', $request->issued_date)->format('Y-m-d');
            $model->description  = $request->description;
            $model->invoice_no   = $request->invoice_no;
            $model->invoice_date = Carbon::createFromFormat('d/m/Y', $request->invoice_date)->format('Y-m-d');
            $model->pay_method   = $request->pay_method;
            $model->due_date     = Carbon::createFromFormat('d/m/Y', $request->due_date)->format('Y-m-d');
            $model->status       = $request->status;

            $items = [];
            foreach ($request->items as $r) {
                if(isset($r['id'])){
                    $dummy = IssuedDetail::find($r['id']);
                    $dummy->item_id     = $r['item_id'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->description = $r['description'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                    $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
                    $dummy->update();
                }else{
                    $dummy = new IssuedDetail;
                    $dummy->item_id     = $r['item_id'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->description = $r['description'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                    $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
        
                    $items[] = $dummy;
                }
            }
            $model->update();
            $model->issued_details()->saveMany($items);

            $delId = json_decode($request->delId);
            IssuedDetail::destroy($delId);

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Issued::find($id);
        $model->delete();
        return Issued::generateCode();
    }
}
