<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Uom;
use App\Models\Item;
use App\Models\Category;
use App\Models\ProductTreatment;
use App\Models\UserOutlet;
use App\Models\Outlet;

use DataTables;
use Auth, View;

class ItemController extends Controller
{
    protected $title = 'Items';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('is_active', 1)->get();
        $uoms = Uom::where('is_active', 1)->get();

        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $product = ProductTreatment::where('product_treatment_code', 'like', 'P-%')
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->orderBy('created_at', 'desc')->get();

        return view('logistik.items.index', compact('categories', 'uoms', 'product'));
    }

    public function listData()
    {
        $collection = Item::orderBy('created_at', 'desc')->get();


        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->code;
            $row[] = $r->product_treatment ? $r->product_treatment->product_treatment_name : '-';
            $row[] = $r->description;
            $row[] = $r->brand;
            $row[] = $r->category ? $r->category->name : '-';
            $row[] = $r->begin_stock;
            
            $row[] = $r->is_active ? '<span class="label label-primary">Active</span>' : '<span class="label label-default">Not Active</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id_item . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id_item . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new Item;
        $save->product_treatment_code = $request->product_treatment_code == '-' ? null : $request->product_treatment_code;
        $save->description            = $request->description;
        $save->brand                  = $request->brand;
        $save->category_id            = $request->category_id;
        $save->uom_id                 = $request->uom_id;
        $save->begin_stock            = $request->begin_stock;
        $save->buffer_stock           = $request->buffer_stock;
        $save->begin_price            = $request->begin_price;
        $save->is_active              = $request->filled('is_active') ? 1: 0;
        $save->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Item::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Item::find($id);

        $model->product_treatment_code = $request->product_treatment_code == '-' ? null : $request->product_treatment_code;
        $model->description            = $request->description;
        $model->brand                  = $request->brand;
        $model->category_id            = $request->category_id;
        $model->uom_id                 = $request->uom_id;
        $model->begin_stock            = $request->begin_stock;
        $model->buffer_stock           = $request->buffer_stock;
        $model->begin_price            = $request->begin_price;
        $model->is_active              = $request->filled('is_active') ? 1: 0;
        $model->update();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Item::find($id);
        if ($model->purchase_order_details->count() > 0 || 
            $model->service_details->count() > 0 || 
            $model->retur_details->count() > 0 || 
            $model->bom_details->count() > 0 || 
            $model->issued_details->count() > 0 || 
            $model->product_locations->count() > 0 || 
            $model->product_mutations->count() > 0 || 
            $model->work_orders->count() > 0 || 
            $model->work_in_process_details->count() > 0 || 
            $model->finish_goods->count() > 0 ) {
            return "error";
        } else {
            $model->delete();
        }

    }
}
