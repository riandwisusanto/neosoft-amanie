<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\VItemStock;

use DataTables;
use Auth;
use View;

class ItemStockController extends Controller
{
    protected $view = 'logistik.item-stock.';
    protected $route = 'item-stock';
    protected $title = 'Item stock';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view.'index');
    }

    public function listData()
    {
        $collection = VItemStock::orderBy('item_code', 'asc')->get();
        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->item_code;
            $row[] = number_format($r->balance);
            $row[] = number_format($r->stock_in);
            $row[] = number_format($r->stock_out);
            $row[] = number_format($r->current_balance);

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}
