<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Item;
use App\Models\Rack;
use App\Models\Warehouse;
use App\Models\ProductLocation;

use DataTables;
use Auth, View;

class ProductLocationController extends Controller
{
    protected $title = 'Product location';
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        $warehouses = Warehouse::all();
        return view('logistik.product-locations.index', compact('items', 'warehouses'));
    }

    public function listData()
    {
        $collection = ProductLocation::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->item ? $r->item->description : '-';
            $row[] = $r->rack ? $r->rack->main_rack_code : '-';
            $row[] = $r->rack ? $r->rack->sub_rack_code: '-';
            $row[] = $r->warehouse ? $r->warehouse->name : '-';
            $row[] = $r->description;
            
            $row[] = $r->is_active ? '<span class="label label-primary">Active</span>' : '<span class="label label-default">Not Active</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new ProductLocation;
        $save->item_id      = $request->item_id;
        $save->description  = $request->description;
        $save->warehouse_id = $request->warehouse_id;
        $save->rack_id      = $request->rack_id;
        $save->is_active    = $request->filled('is_active') ? 1 : 0;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = ProductLocation::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = ProductLocation::find($id);

        $model->item_id      = $request->item_id;
        $model->description  = $request->description;
        $model->warehouse_id = $request->warehouse_id;
        $model->rack_id      = $request->rack_id;
        $model->is_active    = $request->filled('is_active') ? 1 : 0;
        $model->update();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = ProductLocation::find($id);
        if($model){
            $model->delete();
        }

    }
}
