<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Supplier;
use App\Models\Item;
use App\Models\Uom;
use App\Models\Receive;
use App\Models\ReceiveDetail;
use App\Models\PurchaseOrder;
use App\Models\Warehouse;

use DataTables;
use Auth, View, DB;

class ReceiveController extends Controller
{
    protected $view = 'logistik.receives.';
    protected $route = 'receives';
    protected $title = 'Receives';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pos = PurchaseOrder::all();
        $uom = Uom::where('is_active', '1')->get();
        $items = Item::where('is_active', '1')->get();
        $suppliers = Supplier::where('is_active', '1')->get();
        $warehouses = Warehouse::where('is_active', '1')->get();
        $code = Receive::generateCode();
        return view($this->view.'index', compact('items', 'suppliers', 'uom', 'pos', 'warehouses', 'code'));
    }

    public function listData()
    {
        $collection = Receive::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->receive_code;
            $row[] = $r->purchase_order ? $r->purchase_order->purchase_order_num : '-';
            $row[] = date('d M Y', strtotime($r->receive_date));
            $row[] = $r->supplier ? $r->supplier->name : '';
            $row[] = $r->invoice_num;
            $row[] = $r->warehouse ? $r->warehouse->name : '-';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-primary btn-sm" href="'.route('receives.print', $r->id).'" target="_blank">
                            <i class="fa fa-print"></i> Print
                        </a>
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function print($id){
        $data = Receive::findOrFail($id);
        return view($this->view.'print', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $save = new Receive;
            $save->purchase_order_id = $request->purchase_order_id;
            $save->receive_date      = $request->receive_date;
            $save->receive_by        = $request->receive_by;
            $save->supplier_id       = $request->supplier_id;
            $save->invoice_num       = $request->invoice_num;
            $save->description       = $request->description;
            $save->warehouse_id      = $request->warehouse_id;
            $save->status            = $request->status;
            $save->save();

            $items = [];
            foreach ($request->items as $r) {
                $dummy = new ReceiveDetail;
                $dummy->expire_date = $r['expire_date'];
                $dummy->batch_no    = $r['batch_no'];
                $dummy->item_id     = $r['item_id'];
                $dummy->rack_id     = $r['rack_id'];
                $dummy->uom_id      = $r['uom_id'];
                $dummy->price       = $r['price'] ? $r['price'] : 0;
                $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                $dummy->total       = $r['total'] ? $r['total'] : 0;

                $items[] = $dummy;
                
                // $update = Item::where('code',$dummy->code)->first();
                // if ($dummy->qty <= $update['begin_stock'])
                // {
                //     $update->begin_stock= $update['begin_stock'] + $dummy->qty;
                //     $update->update();
                // }
            }
            $save->receive_details()->saveMany($items);
            DB::commit();

            return Receive::generateCode();
        } catch (\Throwable $th) {
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Receive::with('receive_details')->find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        
        try {
            $model = Receive::find($id);

            $model->purchase_order_id = $request->purchase_order_id;
            $model->receive_date      = $request->receive_date;
            $model->receive_by        = $request->receive_by;
            $model->supplier_id       = $request->supplier_id;
            $model->invoice_num       = $request->invoice_num;
            $model->description       = $request->description;
            $model->warehouse_id      = $request->warehouse_id;
            $model->status            = $request->status;

            $items = [];
            foreach ($request->items as $r) {
                if(isset($r['id'])){
                    $dummy = ReceiveDetail::find($r['id']);
                    $dummy->expire_date = $r['expire_date'];
                    $dummy->item_id     = $r['item_id'];
                    $dummy->rack_id     = $r['rack_id'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->batch_no    = $r['batch_no'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
                    $dummy->update();
                }else{
                    $dummy = new ReceiveDetail;
                    $dummy->expire_date = $r['expire_date'];
                    $dummy->item_id     = $r['item_id'];
                    $dummy->rack_id     = $r['rack_id'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->batch_no    = $r['batch_no'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
        
                    $items[] = $dummy;
                }
            }
            $model->update();
            $model->receive_details()->saveMany($items);

            $delId = json_decode($request->delId);
            ReceiveDetail::destroy($delId);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Receive::find($id);
        $model->delete();
        return Receive::generateCode();
    }
}
