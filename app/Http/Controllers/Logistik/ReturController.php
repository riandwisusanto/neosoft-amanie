<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Item;
use App\Models\Uom;
use App\Models\Retur;
use App\Models\ReturDetail;
use App\Models\Warehouse;
use App\Models\Receive;

use DataTables;
use Auth, View, DB;

use Carbon\Carbon;

class ReturController extends Controller
{
    protected $view = 'logistik.returs.';
    protected $route = 'returs';
    protected $title = 'Retur';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $receives = Receive::get();
        $uom = Uom::where('is_active', '1')->get();
        $items = Item::where('is_active', '1')->get();
        $warehouses = Warehouse::where('is_active', '1')->get();
        $code = Retur::generateCode();
        return view($this->view.'index', compact('items', 'uom', 'warehouses', 'code','receives'));
    }

    public function listData()
    {
        $collection = Retur::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->retur_num;
            $row[] = date('d M Y', strtotime($r->retur_date));
            $row[] = $r->description;
            $row[] = $r->invoice_no;
            $row[] = $r->invoice_date;
            $row[] = $r->warehouse ? $r->warehouse->name : '-';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $save = new Retur;
            $save->warehouse_id = $request->warehouse_id;
            $save->supplier_id  = $request->supplier_id;
            $save->retur_date   = $request->retur_date;
            $save->description  = $request->description;
            $save->invoice_no   = $request->invoice_no;
            $save->invoice_date = $request->invoice_date;
            $save->pay_method   = $request->pay_method;
            $save->receive_id   = $request->receive_id;
            $save->due_date     = $request->due_date;
            $save->save();

            $items = [];
            foreach ($request->items as $r) {
                $dummy = new ReturDetail;
                $dummy->item_id     = $r['item_id'];
                $dummy->uom_id      = $r['uom_id'];
                $dummy->description = $r['description'];
                $dummy->price       = $r['price'] ? $r['price'] : 0;
                $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                $dummy->total       = $r['total'] ? $r['total'] : 0;

                $items[] = $dummy;
            }
            $save->retur_details()->saveMany($items);

            DB::commit();

            return Retur::generateCode();
        }catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Retur::with('retur_details')->find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $model = Retur::find($id);

            $model->warehouse_id = $request->warehouse_id;
            $model->supplier_id  = $request->supplier_id;
            $model->retur_date   = $request->retur_date;
            $model->description  = $request->description;
            $model->invoice_no   = $request->invoice_no;
            $model->invoice_date = $request->invoice_date;
            $model->pay_method   = $request->pay_method;
            $model->receive_id   = $request->receive_id;
            $model->due_date     = $request->due_date;

            $items = [];
            foreach ($request->items as $r) {
                if(isset($r['id'])){
                    $dummy = ReturDetail::find($r['id']);
                    $dummy->item_id     = $r['item_id'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->description = $r['description'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                    $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
                    $dummy->update();
                }else{
                    $dummy = new ReturDetail;
                    $dummy->item_id     = $r['item_id'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->description = $r['description'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                    $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
        
                    $items[] = $dummy;
                }
            }
            $model->update();
            $model->retur_details()->saveMany($items);

            $delId = json_decode($request->delId);
            ReturDetail::destroy($delId);

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Retur::find($id);
        $model->delete();
        return Retur::generateCode();
    }

    public function getReceive(Request $req) {
        $receive = Receive::whereId($req->id)->with(['purchase_order', 'supplier'])->first();
        return response()->json($receive);
    }
}
