<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Item;
use App\Models\Uom;
use App\Models\UserOutlet;
use App\Models\Outlet;
use App\Models\ProductTreatment;
use App\Models\Service;
use App\Models\ServiceDetail;

use DataTables;
use Auth, View, DB;

use Carbon\Carbon;

class ServiceController extends Controller
{
    protected $title = 'Service';
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::where('is_active', '1')->get();
        $uom = Uom::where('is_active', '1')->get();

        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $treatment = ProductTreatment::where('product_treatment_code', 'like', 'T-%')
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->orderBy('created_at', 'desc')->get();
        return view('logistik.services.index', compact('items', 'uom', 'treatment'));
    }

    public function listData()
    {
        $collection = Service::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->product_treatment ? $r->product_treatment->product_treatment_code : '-';
            $row[] = $r->name;
            $row[] = $r->description;

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function getTreatment($id){
        $data = ProductTreatment::where('product_treatment_code', 'like', 'T-%')->where('product_treatment_code', $id)->firstOrFail();
        if($data){
            return response()->json($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $save = new Service;
            $save->name = $request->name;
            $save->product_treatment_code = $request->product_treatment_code;
            $save->description = $request->description;
            $save->save();

            $items = [];
            foreach ($request->items as $r) {
                $dummy = new ServiceDetail;
                $dummy->item_id     = $r['item_id'];
                $dummy->expire_date = $r['expire_date'];
                $dummy->uom_id      = $r['uom_id'];
                $dummy->price       = $r['price'] ? $r['price'] : 0;
                $dummy->qty         = $r['qty'] ? $r['qty'] : 0;

                $items[] = $dummy;
            }
            $save->service_details()->saveMany($items);
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Service::with('service_details')->find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $model = Service::find($id);

            $model->name = $request->name;
            $model->product_treatment_code = $request->product_treatment_code;
            $model->description = $request->description;

            $items = [];
            foreach ($request->items as $r) {
                if(isset($r['id'])){
                    $dummy = ServiceDetail::find($r['id']);
                    $dummy->item_id     = $r['item_id'];
                    $dummy->expire_date = $r['expire_date'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->update();
                }else{
                    $dummy = new ServiceDetail;
                    $dummy->item_id     = $r['item_id'];
                    $dummy->expire_date = $r['expire_date'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;

                    $items[] = $dummy;
                }
            }
            $model->update();
            $model->service_details()->saveMany($items);

            $delId = json_decode($request->delId);
            ServiceDetail::destroy($delId);

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Service::find($id);
        $model->delete();

    }
}
