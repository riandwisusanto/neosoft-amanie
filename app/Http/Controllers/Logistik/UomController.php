<?php

namespace App\Http\Controllers\Logistik;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Uom;

use DataTables;
use Auth, View;

class UomController extends Controller
{
    protected $title = 'Unit of Measurement';
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('logistik.uoms.index');
    }

    public function listData()
    {
        $collection = Uom::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->name;
            
            $row[] = $r->is_active ? '<span class="label label-primary">Active</span>' : '<span class="label label-default">Not Active</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id_uom . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id_uom . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new Uom;
        $save->name      = $request->name;
        $save->is_active = $request->filled('is_active') ? 1 : 0;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Uom::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Uom::find($id);

        $model->name      = $request->name;
        $model->is_active = $request->filled('is_active') ? 1 : 0;
        $model->update();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Uom::find($id);
        if ($model->purchase_order_details->count() > 0 || 
            $model->service_details->count() > 0 || 
            $model->retur_details->count() > 0 || 
            $model->bom_details->count() > 0 || 
            $model->work_in_process_details->count() > 0 || 
            $model->finish_goods->count() > 0) {
            echo "error";
        } else {
            $model->delete();
        }

    }
}
