<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Item;
use App\Models\Uom;
use App\Models\WorkOrder;
use App\Models\WorkInProcess;
use App\Models\WorkInProcessDetail;

use DataTables;
use Auth;
use View;

use Carbon\Carbon;

class WorkInProcessController extends Controller
{
    protected $view = 'logistik.work-in-processes.';
    protected $route = 'work-in-processes';
    protected $title = 'Work In Processes';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wo = WorkOrder::where('is_active', '1')->get();
        $uom = Uom::where('is_active', '1')->get();
        $items = Item::where('is_active', '1')->get();
        return view($this->view.'index', compact('items', 'wo', 'uom'));
    }

    public function listData()
    {
        $collection = WorkInProcess::orderBy('created_at', 'desc')->get();
        $status = [
            1 => 'Completed',
            'Incompleted',
            'In Process',
            'Failed Process',
            'Cancel Process',
        ];
        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = date('d M Y', strtotime($r->wip_date));
            $row[] = $r->description;
            $row[] = date('d M Y', strtotime($r->due_date));
            $row[] = $r->uom ? $r->uom->name : '-';
            $row[] = $r->order_qty;
            $row[] = '<span class="label label-primary">'.$status[$r->status].'</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function getWo($id){
        $data = WorkOrder::with('uom')->findOrFail($id);
        if($data){
            return response()->json($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new WorkInProcess;
        $save->work_order_id = $request->work_order_id;
        $save->wip_date      = Carbon::createFromFormat('d/m/Y', $request->wip_date)->format('Y-m-d');
        $save->due_date      = Carbon::createFromFormat('d/m/Y', $request->due_date)->format('Y-m-d');
        $save->uom_id     = $request->uom_id;
        $save->order_qty     = $request->order_qty;
        $save->description   = $request->description;
        $save->status        = $request->status;
        $save->is_active     = 1;
        $save->save();

        $items = [];
        foreach ($request->items as $r) {
            $dummy = new WorkInProcessDetail;
            $dummy->real_qty    = $r['real_qty'] ? $r['real_qty']: 0;
            $dummy->bom_qty     = $r['bom_qty'] ? $r['bom_qty'] : 0;
            $dummy->item_id     = $r['item_id'];
            $dummy->uom_id      = $r['uom_id'];
            $dummy->price       = $r['price'] ? $r['price'] : 0;
            $dummy->total       = $r['total'] ? $r['total'] : 0;

            $items[] = $dummy;
        }
        $save->work_in_process_details()->saveMany($items);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = WorkInProcess::with('work_in_process_details')->find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = WorkInProcess::find($id);

        $model->work_order_id = $request->work_order_id;
        $model->wip_date      = $request->wip_date;
        $model->due_date      = $request->due_date;
        $model->uom_id     = $request->uom_id;
        $model->order_qty     = $request->order_qty;
        $model->description   = $request->description;
        $model->status        = $request->status;

        $items = [];
        foreach ($request->items as $r) {
            if(isset($r['id'])){
                $dummy = WorkInProcessDetail::find($r['id']);
                $dummy->real_qty = $r['real_qty'] ? $r['real_qty']: 0;
                $dummy->bom_qty  = $r['bom_qty'] ? $r['bom_qty'] : 0;
                $dummy->item_id  = $r['item_id'];
                $dummy->uom_id   = $r['uom_id'];
                $dummy->price    = $r['price'] ? $r['price'] : 0;
                $dummy->total    = $r['total'] ? $r['total'] : 0;
                $dummy->update();
            }else{
                $dummy = new WorkInProcessDetail;
                $dummy->real_qty = $r['real_qty'] ? $r['real_qty']: 0;
                $dummy->bom_qty  = $r['bom_qty'] ? $r['bom_qty'] : 0;
                $dummy->item_id  = $r['item_id'];
                $dummy->uom_id   = $r['uom_id'];
                $dummy->price    = $r['price'] ? $r['price'] : 0;
                $dummy->total    = $r['total'] ? $r['total'] : 0;
    
                $items[] = $dummy;
            }
        }
        $model->update();
        $model->work_in_process_details()->saveMany($items);

        $delId = json_decode($request->delId);
        WorkInProcessDetail::destroy($delId);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = WorkInProcess::find($id);
        $model->delete();

    }
}
