<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Item;
use App\Models\WorkOrder;
use App\Models\Bom;
use App\Models\Uom;

use DataTables;
use Auth, View;

use Carbon\Carbon;

class WorkOrderController extends Controller
{
    protected $view = 'logistik.work-orders.';
    protected $route = 'work-orders';
    protected $title = 'Work Orders';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bom = Bom::all();
        $uom = Uom::all();
        $items = Item::where('is_active', '1')->get();

        return view($this->view.'index', compact('bom', 'items', 'uom'));
    }

    public function listData()
    {
        $collection = WorkOrder::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->bom ? $r->bom->code : '-';
            $row[] = date('d M Y', strtotime($r->wo_date));
            $row[] = $r->item ? $r->item->code : '-';
            $row[] = $r->description;
            $row[] = date('d M Y', strtotime($r->start_date));
            $row[] = date('d M Y', strtotime($r->due_date));

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function getBom($id){
        $data = Bom::with('item')->find($id);
        if($data){
            return response()->json($data);
        }
        return response()->json([]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new WorkOrder;
        $save->bom_id      = $request->bom_id;
        $save->item_id     = $request->item_id;
        $save->wo_date     = Carbon::createFromFormat('d/m/Y', $request->wo_date)->format('Y-m-d');
        $save->uom_id      = $request->uom_id;
        $save->order_qty   = $request->order_qty;
        $save->start_date  = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');
        $save->due_date    = Carbon::createFromFormat('d/m/Y', $request->due_date)->format('Y-m-d');
        $save->description = $request->description;
        $save->is_active   = $request->filled('is_active') ? 1 : 0;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = WorkOrder::find($id);
        $model->wo_date = Carbon::createFromFormat('Y-m-d', $model->wo_date)->format('d/m/Y');
        $model->start_date = Carbon::createFromFormat('Y-m-d', $model->start_date)->format('d/m/Y');
        $model->due_date = Carbon::createFromFormat('Y-m-d', $model->due_date)->format('d/m/Y');

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = WorkOrder::find($id);

        $model->bom_id      = $request->bom_id;
        $model->item_id     = $request->item_id;
        $model->wo_date     = Carbon::createFromFormat('d/m/Y', $request->wo_date)->format('Y-m-d');
        $model->uom_id      = $request->uom_id;
        $model->order_qty   = $request->order_qty;
        $model->start_date  = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');
        $model->due_date    = Carbon::createFromFormat('d/m/Y', $request->due_date)->format('Y-m-d');
        $model->description = $request->description;
        $model->is_active   = $request->filled('is_active') ? 1 : 0;

        $model->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = WorkOrder::find($id);
        $model->delete();

    }
}
