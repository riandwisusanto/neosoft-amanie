SELECT
	inv_date,
	SUM(total) as collection
FROM
	invoices
WHERE
	outlet_id IN (%s)
    AND inv_date BETWEEN '%s' AND '%s'
	AND void_invoice = 0
GROUP BY
	%s
ORDER BY
	inv_date ASC