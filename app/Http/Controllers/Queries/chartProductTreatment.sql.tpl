SELECT
	all_data.inv_date,
    all_data.outlet_id,
    all_data.outlet_name,
    SUM(case when all_data.type = 'P' then all_data.total_qty else 0 end) product,
    SUM(case when all_data.type = 'T' then all_data.total_qty else 0 end) treatment
FROM (
	SELECT 
      inv.inv_date,
      inv.outlet_id,
      ot.outlet_name,
      substring(pt.product_treatment_code, 1, 1) as type,
      SUM(ip.qty + ip.used) as total_qty
    FROM 
      invoice_packages ip 
      INNER JOIN product_treatments pt ON ip.product_id = pt.id_product_treatment 
      INNER JOIN invoice_details inv_d ON inv_d.id_inv_detail = ip.inv_detail_id 
      INNER JOIN invoices inv ON inv.id_invoice = inv_d.inv_id
      INNER JOIN outlets ot ON ot.id_outlet = inv.outlet_id
    WHERE 
      inv.outlet_id IN (%s)
      AND inv.inv_date BETWEEN '%s' AND '%s'
    GROUP BY 
      inv.inv_date, type
) AS all_data
GROUP BY
    %s
%s