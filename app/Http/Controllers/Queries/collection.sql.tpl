SELECT
	SUM(total) as collection
FROM
	invoices
WHERE
	outlet_id IN (%s)
    AND inv_date BETWEEN '%s' AND '%s'
	AND void_invoice = 0