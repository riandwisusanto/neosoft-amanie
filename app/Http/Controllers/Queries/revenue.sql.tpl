SELECT
    CAST(SUM(ip.used * (inv_d.subtotal / inv_d.qty)) AS DECIMAL) AS revenue
FROM
    invoice_packages ip
    LEFT JOIN invoice_details inv_d ON inv_d.id_inv_detail = ip.inv_detail_id
    LEFT JOIN invoices inv ON inv.id_invoice = inv_d.inv_id
WHERE
	inv.outlet_id IN (%s)
  	AND inv.inv_date BETWEEN '%s' AND '%s'
	AND inv.void_invoice = 0
HAVING revenue > -1