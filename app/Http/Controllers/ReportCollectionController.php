<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\ActivityGroup;
use App\Models\Bank;
use App\Models\Invoice;
use App\Models\InvoiceBalance;
use App\Models\InvoiceDetail;
use App\Models\Outlet;
use App\Models\Payment;
use Carbon\carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ReportCollectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    //collection by Outlet
    public function index_collection_outlet()
    {
        $outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d-m-Y');
        $to = Carbon::parse(now())->format('d-m-Y');
        return view('report.collection.outlet.report')
            ->with('outlet', $outlet)
            ->with('from', $from)
            ->with('to', $to);
    }

    public function detailCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $invoices = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->orderBy('id_invoice', 'ASC')
            ->get();

        $invoice_id = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->orderBy('id_invoice', 'ASC')
            ->pluck('id_invoice')->toArray();

        $invoice_details = InvoiceDetail::whereIn('inv_id', $invoice_id)->pluck('inv_id')->toArray();


        $data = array();
        foreach ($invoices as $list) {
            foreach($list->invoice_detail as $detailIndex => $detail){
                $row = array();
                //invoicenum & customer
                if ($detailIndex == 0) {
                    if (count($list->log_convert) > 0) {
                        $row[] = '<a href=" ' . route("invoice-convert.print", $list->id_invoice) . ' " target="_blank">' . $list->inv_code . '</a>';
                    } else {
                        $row[] = '<a href="' . route("invoice.print", $list->id_invoice) . '" target="_blank">' . $list->inv_code . '</a>';
                    }
                    $row[] = $list->customer->full_name ." | ". $list->customer->induk_customer;
                } else {
                    if ($list->invoice_detail[$detailIndex]->inv_id == $list->invoice_detail[$detailIndex - 1]->inv_id) {
                        $row[] = "";
                        $row[] = "";
                    } else {
                        if (count($list->log_convert) > 0) {
                            $row[] = '<a href=" ' . route("invoice-convert.print", $list->id_invoice) . ' " target="_blank">' . $list->inv_code . '</a>';
                        } else {
                            $row[] = '<a href="' . route("invoice.print", $list->id_invoice) . '" target="_blank">' . $list->inv_code . '</a>';
                        }
                        $row[] = $list->customer->full_name ." | ". $list->customer->induk_customer;
                    }
                }
                //treatment logic
                    $type_line = $detail->type_line;
                    if(substr($detail->package_code,0,1) == "T"){
                        foreach($detail->invoice_package as $package){
                            $treatment = $package->product_treatment->product_treatment_name;
                        }
                        $hargatreatment = $detail->current_price;
                        if($detail->discount_2 == ''){
                            $disctreatment = $detail->discount_1;
                        }else{
                            $disctreatment = $detail->discount_1 . " + " . $detail->discount_2;
                        }
                        $totaltreatment = $detail->subtotal;
                    }else{
                        $treatment = "";
                        $hargatreatment = 0;
                        $disctreatment = 0;
                        $totaltreatment = 0;
                    }
                    if($detail->therapist_id != ''){
                        $terapis = $detail->therapist->therapist_name;
                    }else if($list->therapist_id != ''){
                        $terapis = $list->therapist->therapist_name;
                    }else{
                        $terapis = "";
                    }
                //treatment
                $row[] = $treatment;
                //price
                $row[] = $hargatreatment;
                //disc and disc rp
                if($type_line == 1){
                    $row[] = "0";
                    $row[] = $disctreatment;
                }else{
                    $row[] = $disctreatment;
                    $row[] = "0";
                }
                //total
                $row[] = $totaltreatment;
                //therapist
                $row[] = $terapis;
                //product logic
                    foreach($list->payment as $pay){
                        $bayar = $pay->bank->bank_name;
                    }
                    if(substr($detail->package_code,0,1) == "P"){
                        foreach($detail->invoice_package as $package){
                            $product = $package->product_treatment->product_treatment_name;
                        }  
                        $qty = $detail->qty;
                        $hargaproduct = $detail->current_price;
                        if($detail->discount_2 == ''){
                            $discproduct = $detail->discount_1;
                        }else{
                            $discproduct = $detail->discount_1 . " + " . $detail->discount_2;
                        }
                        $totalproduct = $detail->subtotal;
                    }else{
                        $product = '';
                        $qty = 0;
                        $hargaproduct = 0;
                        $discproduct = 0;
                        $totalproduct = 0;
                    } 
                //product            
                $row[] = $product;
                //qty
                $row[] = $qty;
                //price
                $row[] = $hargaproduct;
                //disc and disc rp
                if($type_line == 1){
                    $row[] = $discproduct;
                    $row[] = "0";
                }else{
                    $row[] = "0";
                    $row[] = $discproduct;
                }
                //total
                $row[] = $totalproduct;
                $row[] = "";
                //bayar
                $row[] = $bayar;
                $data[] = $row;
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function subtotalCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $invoices = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        // return $invoices;
        $data = array();
        $items = array("Product", "Treatment");
        // return $items;
        foreach($items as $item){
            $row = array();
            $row[] = $item;
            if($item == "Product"){
                $subtotal = InvoiceDetail::whereIn('inv_id', $invoices)
                            ->where('package_code', 'like', 'P%')
                            ->sum('current_price');
                
                $grandtotal = InvoiceDetail::whereIn('inv_id', $invoices)
                            ->where('package_code', 'like', 'P%')
                            ->sum('subtotal');
            }else{
                $subtotal = InvoiceDetail::whereIn('inv_id', $invoices)
                            ->where('package_code', 'like', 'T-%')
                            ->sum('current_price');

                $grandtotal = InvoiceDetail::whereIn('inv_id', $invoices)
                            ->where('package_code', 'like', 'T-%')
                            ->sum('subtotal');
            }
            // return $subtotal;
            $row[] = format_money($subtotal);
            $row[] = format_money($grandtotal);
            $data[] = $row;
        }
            
        
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function grandtotalCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $invoices = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        // return $invoices;
        $data = array();
        $bank = Bank::all();
        // return $items;

        foreach($bank as $list){
            $row = array();
            $row[] = $list->bank_name;
            $total = Payment::whereIn('inv_id', $invoices)
                    ->where('bank_id', $list->id_bank)
                    ->sum('amount');
            $row[] = format_money($total);
            $data[] = $row;
        }
            
        
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function totalCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $invoices = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        // return $invoices;
        $data = array();
        $row = array();
        $customers = Invoice::whereIn('id_invoice', $invoices)->get()->groupBy('customer_id')->count();
        // return $customers;
        $row[] = $customers;
        $total = Payment::whereIn('inv_id', $invoices)
                    ->sum('amount');
        $row[] = format_money($total);
        $data[] = $row;
            
        
        return DataTables::of($data)->escapeColumns([])->make(true);
    }
    public function index_collection_payment()
    {
        $bank = Bank::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $countBank = 0;
        $payment = [];
        $invoice = [];
        $detail_invoice = [];

        return view('report.collection.payment.index')
            ->with('payment', $payment)
            ->with('invoice', $invoice)
            ->with('detail_invoice', $detail_invoice)
            ->with('from', $from)
            ->with('to', $to)
            ->with('countBank', $countBank)
            ->with('bank', $bank);
    }

    public function bank_payment_collection(Request $request)
    {
        $invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->bank) {
            $bank_id = [];
            for ($i = 0; $i < count($request->bank); $i++) {
                $bank_id[] = $request->bank[$i];
            }

            $countFilter = count($request->filter);
            for ($i = 0; $i < $countFilter; $i++) {
                if ($countFilter == 1) {
                    if ($request->filter[$i] == 1) {
                        $invoice = Invoice::leftJoin('payments', 'payments.inv_id', 'invoices.id_invoice')
                            ->whereBetween('inv_date', [$from, $to])
                            ->where('void_invoice', 0)
                            ->where('payments.balance_id', null)
                            ->whereIn('payments.bank_id', $bank_id)
                            ->get()
                            ->groupBy('outlet_id');
                    } else {
                        $invoice = Invoice::leftJoin('payments', 'payments.inv_id', 'invoices.id_invoice')
                            ->whereBetween('inv_date', [$from, $to])
                            ->where('void_invoice', 0)
                            ->where('payments.balance_id', 1)
                            ->whereIn('payments.bank_id', $bank_id)
                            ->get()
                            ->groupBy('outlet_id');
                    }
                } else {
                    $invoice = [];
                }
            }
        }
        $data = array();
        foreach ($invoice as $list) {
            $row = array();
            $sales_amount = 0;
            $total_payment = 0;
            foreach ($list as $item) {
                $total_payment += str_replace(",", "", $item->amount);
                foreach ($item->invoice_detail as $package) {
                    $sales_amount += str_replace(",", "", $package->subtotal);
                }
            }
            $row[] = $list[0]->outlet->outlet_name;
            $row[] = count($list);
            $row[] = format_money($sales_amount);
            $row[] = format_money($total_payment);
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function payment_type_collection(Request $request)
    {
        $payment = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->bank) {
            $bank_id = [];
            for ($i = 0; $i < count($request->bank); $i++) {
                $bank_id[] = $request->bank[$i];
            }

            $countFilter = count($request->filter);
            for ($i = 0; $i < $countFilter; $i++) {
                if ($countFilter == 1) {
                    if ($request->filter[$i] == 1) {
                        $payment = Payment::leftJoin('invoices', 'invoices.id_invoice', 'payments.inv_id')
                            ->whereBetween('invoices.inv_date', [$from, $to])
                            ->where('invoices.void_invoice', 0)
                            ->where('balance_id', null)
                            ->whereIn('bank_id', $bank_id)
                            ->get()
                            ->groupBy('bank_id');
                    } else {
                        $payment = Payment::leftJoin('invoices', 'invoices.id_invoice', 'payments.inv_id')
                            ->whereBetween('invoices.inv_date', [$from, $to])
                            ->where('invoices.void_invoice', 0)
                            ->where('balance_id', 1)
                            ->whereIn('bank_id', $bank_id)
                            ->get()
                            ->groupBy('bank_id');
                    }
                } else {
                    $payment = Payment::leftJoin('invoices', 'invoices.id_invoice', 'payments.inv_id')
                        ->whereBetween('invoices.inv_date', [$from, $to])
                        ->where('invoices.void_invoice', 0)
                        ->whereIn('bank_id', $bank_id)
                        ->get()
                        ->groupBy('bank_id');
                }
            }
        } else {
            $payment = Payment::leftJoin('invoices', 'invoices.id_invoice', 'payments.inv_id')
                ->whereBetween('invoices.inv_date', [$from, $to])
                ->where('invoices.void_invoice', 0)
                ->get()
                ->groupBy('bank_id');
        }

        $data = array();
        foreach ($payment as $list) {
            $row = array();
            $total_balance = 0;
            $total_payment = 0;
            foreach ($list as $item) {
                if ($item->balance_id) {
                    $total_balance += str_replace(",", "", $item->amount);
                }
                $total_payment += str_replace(",", "", $item->amount);
            }
            $row[] = $list[0]->bank->bank_code;
            $row[] = $list[0]->bank->bank_name;
            $row[] = count($list);
            $row[] = format_money($total_payment);
            $row[] = format_money($total_balance);
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
    public function detail_payment_collection(Request $request)
    {
        $detail_invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->bank) {
            $bank_id = [];
            for ($i = 0; $i < count($request->bank); $i++) {
                $bank_id[] = $request->bank[$i];
            }

            $countFilter = count($request->filter);
            for ($i = 0; $i < $countFilter; $i++) {
                if ($countFilter == 1) {
                    if ($request->filter[$i] == 1) {
                        $detail_invoice = Invoice::leftJoin('payments', 'payments.inv_id', 'invoices.id_invoice')
                            ->whereBetween('inv_date', [$from, $to])
                            ->where('void_invoice', 0)
                            ->where('payments.balance_id', null)
                            ->whereIn('payments.bank_id', $bank_id)
                            ->get();
                    } else {
                        $detail_invoice = Invoice::leftJoin('payments', 'payments.inv_id', 'invoices.id_invoice')
                            ->whereBetween('inv_date', [$from, $to])
                            ->where('void_invoice', 0)
                            ->where('payments.balance_id', 1)
                            ->whereIn('payments.bank_id', $bank_id)
                            ->get();
                    }
                } else {
                    $detail_invoice = Invoice::leftJoin('payments', 'payments.inv_id', 'invoices.id_invoice')
                        ->whereBetween('inv_date', [$from, $to])
                        ->where('void_invoice', 0)
                        ->whereIn('payments.bank_id', $bank_id)
                        ->get();
                }
            }
        } else {
            $detail_invoice = Invoice::leftJoin('payments', 'payments.inv_id', 'invoices.id_invoice')
                ->whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->get();
        }
        $data = array();
        foreach ($detail_invoice as $list) {
            $row = array();
            $row[] = $list->inv_code;
            $row[] = $list->inv_date;
            $row[] = $list->outlet->outlet_name;
            $row[] = $list->customer->induk_customer;
            $row[] = $list->customer->full_name;
            $row[] = $list->user->username;
            $PaymentType = '';
            $paid = 0;
            foreach ($list->payment as $payment) {
                $PaymentType .= "&#10004;" . $payment->bank->bank_name . "<br>";
                $paid += str_replace(",", "", $payment->amount);
            }

            $row[] = $PaymentType;
            $row[] = $list->consultant->consultant_name;
            $row[] = $list->therapist->therapist_name;
            $row[] = format_money($paid);
            $row[] = '<a href=' . route('customer.active', $list->id_invoice) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_collection_activity()
    {
        $activity = Activity::all();
        $group = ActivityGroup::all();
        $outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $detail_invoice = [];

        return view('report.collection.activity.index')
            ->with('from', $from)
            ->with('to', $to)
            ->with('activity', $activity)
            ->with('group', $group)
            ->with('outlet', $outlet)
            ->with('detail_invoice', $detail_invoice);
    }

    public function collection_activity(Request $request)
    {
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }
        $detail_invoice = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0);
        if ($request->outlet) {
            $detail_invoice->whereIn('outlet_id', $request->outlet);
        }
        if ($request->activity) {
            $activity_id = $request->activity;
            $detail_invoice->with(['invoice_detail' => function ($query) use ($activity_id) {
                $query->whereIn('activity_id', $activity_id);
            }]);
        }
        $detail_invoice = $detail_invoice->get();
        $data = array();

        foreach ($detail_invoice as $list) {
            foreach ($list->invoice_detail as $detail) {
                foreach ($detail->invoice_package as $package) {
                    $row = array();
                    if (count($list->log_convert) > 0) {
                        $row[] = '<a href = ' . route('invoice-convert.print', $list->id_invoice) . ' target = "_blank" >' . $list->inv_cod . '</a >';
                    } else {
                        $row[] = '<a href = ' . route('invoice.print', $list->id_invoice) . ' target = "_blank" >' . $list->inv_code . '</a >';
                    }
                    $row[] = $list->inv_date;
                    $row[] = $list->outlet->outlet_name;
                    $row[] = '<a href = ' . route('customer.active', $list->id_invoice) . 'target = "_blank">' . $list->customer->induk_customer . '</a >';
                    $row[] = $list->customer->full_name;
                    $row[] = $list->user->username;
                    $row[] = $list->marketing_source->marketing_source_name;
                    $row[] = $detail->activity->activity_name;
                    $row[] = $list->consultant->consultant_name;
                    $row[] = $list->therapist->therapist_name;
                    $row[] = $detail->package_code;

                    if ($detail->package) {
                        $row[] = $package->product_treatment->product_treatment_name ?? 'deleted';
                        $row[] = 'Package';
                    } else {
                        $row[] = $detail->product_treatment->product_treatment_name ?? 'deleted';
                        $product = $detail->product_treatment->product_treatment_code ?? 'deleted';
                        if ($product) {
                            if (substr($product, 0, 1) == 'T') {
                                $row[] = 'Treatment';
                            } else {
                                $row[] = 'Product';
                            }
                        } else {
                            $row[] = '-';
                        }
                    }
                    $row[] = format_money($package->nominal);
                    if ($detail->type_line == 0) {
                        $disc = '%';
                    } else {
                        $disc = 'Rp';
                    }
                    $row[] = $detail->discount . $disc;
                    $row[] = $package->qty + $package->used;
                    $row[] = format_money(str_replace(",", "", $package->nominal) * ($package->qty + $package->used));
                    $row[] = $list->remarks;
                    $row[] = '<a href=' . route('customer.active', $list->id_invoice) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                    $data[] = $row;
                }
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function choose_activity(Request $request)
    {
        if ($request->group) {
            $activity = Activity::where('activity_group_id', $request->group)->get();
            foreach ($activity as $value) {
                echo "<option value='" . $value->id_activity . "' selected>" . $value->activity_name . "</option>";
            }
        }
    }
}
