<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Agent;
use App\Models\Consultant;
use App\Models\Therapist;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Usage;
use App\Models\UsageDetail;
use App\Models\UsageTherapist;
use Carbon\Carbon;
use DataTables;

class ReportCommission extends Controller
{
    public function indexCommissionSales()
    {
        $agent = Agent::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $invoice = [];
        $detail_usage = [];

        return view('report.commission.sales.index')
            ->with('invoice', $invoice)
            ->with('from', $from)
            ->with('to', $to)
            ->with('agent', $agent)
            ->with('detail_usage', $detail_usage);
    }

    public function summarySales(Request $request)
    {
        $invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->agent) {
            $agentId = [];
            for ($i = 0; $i < count($request->agent); $i++) {
                $agentId[] = $request->agent[$i];
            }

            $invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->whereIn('agent_id', $agentId)
                ->get()
                ->groupBy('agent_id');
        }

        $data = array();
        $payments = 0;
        foreach ($invoice as $list) {
            foreach ($list as $key) {
                foreach ($key->payment as $payment) {
                    $payments += str_replace(array('.', ','), "", $payment->amount);
                }
            }
            $row = [];
            $row[] = $list[0]->agent->agent_name;
            $row[] = count($list);
            $row[] = format_money($payments);
            $row[] = format_money(0.1 * $payments);
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function commissionSales(Request $request)
    {
        $invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->agent) {
            $agentId = [];
            for ($i = 0; $i < count($request->agent); $i++) {
                $agentId[] = $request->agent[$i];
            }

            $invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->whereIn('agent_id', $agentId)
                ->get();
        }

        $data = array();
        foreach ($invoice as $list) {
            foreach ($list->invoice_detail as $detail) {
                $row = [];
                $row[] = $list->inv_code;
                $row[] = $list->inv_date;
                $row[] = $list->customer->induk_customer;
                $row[] = $list->customer->full_name;
                $row[] = $list->user->username;
                if ($detail->package) {
                    $row[] = $detail->package->package_name;
                    $row[] = $detail->package->package_code;
                } else {
                    $row[] = $detail->product_treatment->product_treatment_name;
                    $row[] = $detail->product_treatment->product_treatment_code;
                }
                $row[] = $detail->qty;
                $row[] = $detail->subtotal;
                $row[] = $list->agent->agent_name;
                $row[] = format_money(0.1 * str_replace(",", "", $detail->subtotal));
                $row[] = $list->remarks;
                $data[] = $row;
            }
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function indexCommissionConsultant()
    {
        $consultant = Consultant::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $invoice = [];
        $detail_usage = [];

        return view('report.commission.consultant.index')
            ->with('invoice', $invoice)
            ->with('from', $from)
            ->with('to', $to)
            ->with('consultant', $consultant)
            ->with('detail_usage', $detail_usage);
    }

    public function summaryConsultant(Request $request)
    {
        $invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->consultant) {
            $consultantId = [];
            for ($i = 0; $i < count($request->consultant); $i++) {
                $consultantId[] = $request->consultant[$i];
            }

            $invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->whereIn('consultant_id', $consultantId)
                ->get()
                ->groupBy('consultant_id');
        }

        // dd($invoice);

        $data = array();
        $payments = 0;
        foreach ($invoice as $list) {
            foreach ($list as $key) {
                foreach ($key->payment as $payment) {
                    $payments += str_replace(array('.', ','), "", $payment->amount);
                }
            }
            $row = [];
            $row[] = $list[0]->consultant->consultant_name;
            $row[] = count($list);
            $row[] = format_money($payments);
            $row[] = format_money(0.1 * $payments);
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function commissionConsultant(Request $request)
    {
        $invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->consultant) {
            $consultantId = [];
            for ($i = 0; $i < count($request->consultant); $i++) {
                $consultantId[] = $request->consultant[$i];
            }

            $invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->whereIn('consultant_id', $consultantId)
                ->get();
        }

        $data = array();
        foreach ($invoice as $list) {
            foreach ($list->invoice_detail as $detail) {
                $row = [];
                $row[] = $list->inv_code;
                $row[] = $list->inv_date;
                $row[] = $list->customer->induk_customer;
                $row[] = $list->customer->full_name;
                $row[] = $list->user->username;
                if ($detail->package) {
                    $row[] = $detail->package->package_name;
                    $row[] = $detail->package->package_code;
                } else {
                    $row[] = $detail->product_treatment->product_treatment_name;
                    $row[] = $detail->product_treatment->product_treatment_code;
                }
                $row[] = $detail->qty;
                $row[] = $detail->subtotal;
                $row[] = $list->consultant->consultant_name;
                $row[] = format_money(0.1 * str_replace(",", "", $detail->subtotal));
                $row[] = $list->remarks;
                $data[] = $row;
            }
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function indexCommissionTherapist()
    {
        $therapist = Therapist::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $invoice = [];
        $detail_usage = [];

        return view('report.commission.therapist.index')
            ->with('invoice', $invoice)
            ->with('from', $from)
            ->with('to', $to)
            ->with('therapist', $therapist)
            ->with('detail_usage', $detail_usage);
    }

    public function summaryTherapist(Request $request)
    {
        $therapistId = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->therapist) {
            $therapistId = [];
            for ($i = 0; $i < count($request->therapist); $i++) {
                $therapistId[] = $request->therapist[$i];
            }
        }
        //wrong way
        $data = array();
        foreach ($therapistId as $list) {
            $row = [];
            //therapist name
            $therapistname = Therapist::where('id_therapist', $list)->pluck('therapist_name')->toArray();
            $row[] = $therapistname;
            $invoices = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->where('therapist_id', $list)
                ->get();
            $comm = 0;
            $total = 0;
            foreach ( $invoices as $invoice ) {
                $total += str_replace(",", "", $invoice->total);
                foreach ( $invoice->invoice_detail as $detail ) {
                    if ( $detail->package ) {
                        if (@$detail->package->commissionType == 0){
                            $comm += ($detail->package->commission / 100 * str_replace(",", "", $detail->subtotal));
                        } else {
                            $comm += $detail->package->commission * $detail->qty;
                        }
                    } else {
                        $comm += 0.1 * str_replace(",", "", $detail->subtotal);
                    }
                }
            }
            //numb of invoices
            $row[] = count($invoices);
            //numb of sales
            $row[] = format_money($total);
            //commission
            $row[] = format_money($comm);
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function commissionTherapist(Request $request)
    {
        $invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->therapist) {
            $therapistId = [];
            for ($i = 0; $i < count($request->therapist); $i++) {
                $therapistId[] = $request->therapist[$i];
            }

            $invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('void_invoice', 0)
                ->whereIn('therapist_id', $therapistId)
                ->get();
        }

        $data = array();
        foreach ($invoice as $list) {
            foreach ($list->invoice_detail as $detail) {
                $row = [];
                $row[] = $list->inv_code;
                $row[] = $list->inv_date;
                $row[] = $list->customer->induk_customer;
                $row[] = $list->customer->full_name;
                $row[] = $list->user->username;
                if ($detail->package) {
                    $row[] = $detail->package->package_name;
                    $row[] = $detail->package->package_code;
                } else {
                    $row[] = $detail->product_treatment->product_treatment_name;
                    $row[] = $detail->product_treatment->product_treatment_code;
                }
                $row[] = $detail->qty;
                $row[] = $detail->subtotal;
                $row[] = $list->therapist->therapist_name;
                if ( $detail->package ) {
                    if (@$detail->package->commissionType == 0){
                        $comm = format_money($detail->package->commission / 100 * str_replace(",", "", $detail->subtotal));
                    } else {
                        $comm = format_money($detail->package->commission * $detail->qty);
                    }
                } else {
                    $comm = format_money(0.1 * str_replace(",", "", $detail->subtotal));
                }
                $row[] = $comm;
                $row[] = $list->remarks;
                $data[] = $row;
            }
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function indexCommissionTherapistDoing()
    {
        $therapist = Therapist::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $invoice = [];
        $detail_usage = [];

        return view('report.commission.therapist-doing.index')
            ->with('invoice', $invoice)
            ->with('from', $from)
            ->with('to', $to)
            ->with('therapist', $therapist)
            ->with('detail_usage', $detail_usage);
    }

    public function summaryTherapistDoing(Request $request)
    {
        $usageTherapist = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->therapist) {
            $therapistId = [];
            for ($i = 0; $i < count($request->therapist); $i++) {
                $therapistId[] = $request->therapist[$i];
            }

            $usageId = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->pluck('id_usage')
                ->toArray();

            $usageDetail = UsageDetail::whereIn('usage_id', $usageId)
                ->pluck('id_usage_detail')
                ->toArray();

            $usageTherapist = UsageTherapist::whereIn('usage_detail_id', $usageDetail)
                ->whereIn('therapist_id', $therapistId)
                ->get()
                ->groupBy('therapist_id');
        }

        $data = array();
        $total = 0;
        $commission = 0;
        $numInvoice = 0;
        foreach ($usageTherapist as $list) {
            foreach ($list as $value) {
                if (substr($value->usage_detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                    $total += $value->usage_detail->used * $value->usage_detail->value;
                    if ($list[0]->therapist->type == 0) {
                        if($value->usage_detail->product_treatment->commtype == 0)
                        {
                            $commission += ($value->usage_detail->product_treatment->commissionDr/100 )* $total;
                        }else{
                            $commission += $value->usage_detail->product_treatment->commissionDr;
                        }
                    }else{
                        if($value->usage_detail->product_treatment->commtype == 0)
                        {
                            $commission += ($value->usage_detail->product_treatment->commissionTr/100 )* $total;
                        }else{
                            $commission +=  $value->usage_detail->product_treatment->commissionTr;
                        }
                    }
                    $numInvoice++;
                }
                //$numInvoice = count($value);
                // foreach ($value->usage_detail as $key) {
                // }
            }
            $row = [];
            $row[] = $list[0]->therapist->therapist_name;
            $row[] = $numInvoice;
            $row[] = format_money($total);
            $row[] = format_money($commission);
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function commissionTherapistDoing(Request $request)
    {
        $usage = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->therapist) {
            $therapistId = [];
            for ($i = 0; $i < count($request->therapist); $i++) {
                $therapistId[] = $request->therapist[$i];
            }

            $usageId = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->pluck('id_usage')
                ->toArray();

            $usage = UsageDetail::with([
                'usage',
                'usage_therapist' => function ($query) use ($therapistId) {
                    $query->whereIn('therapist_id', $therapistId);
                }
            ])
                ->whereIn('usage_id', $usageId)
                ->get();
        }



        $data = array();
        foreach ($usage as $list) {
            if (substr($list->product_treatment->product_treatment_code, 0, 1) == 'T') {
                foreach ($list->usage_therapist as $therapist) {
                    $row = [];
                    $row[] = $list->usage->usage_code;
                    $row[] = $list->usage->usage_date;
                    $row[] = $list->usage->customer->induk_customer;
                    $row[] = $list->usage->customer->full_name;
                    $row[] = $list->usage->user->username;

                    $row[] = $list->product_treatment->product_treatment_name;
                    $row[] = $list->product_treatment->product_treatment_code;
                    $total = $list->used * $list->value;

                    if ($therapist->therapist->type == 0) {
                        if($list->product_treatment->commtype == 0)
                        {
                            $commission = ($list->product_treatment->commissionDr/100 )* $total;
                        }else{
                            $commission = $list->product_treatment->commissionDr;
                        }
                    }else{
                        if($list->product_treatment->commtype == 0)
                        {
                            $commission = ($list->product_treatment->commissionTr/100 )* $total;
                        }else{
                            $commission = $list->product_treatment->commissionTr;
                            
                        }
                    }

                    $row[] = $list->used;
                    $row[] = format_money($total);
                    $row[] = $therapist->therapist->therapist_name;
                    $row[] = format_money($commission);
                    $row[] = $list->usage->remarks;
                    $data[] = $row;
                }
            }
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}
