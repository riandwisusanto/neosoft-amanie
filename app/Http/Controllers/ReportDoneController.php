<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Outlet;
use App\Models\Therapist;
use App\Models\Usage;
use Carbon\carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ReportDoneController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    //collection by Outlet
    public function index_treatmentdone_outlet()
    {
        $outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $summary = [];
        $detail_usage = [];

        return view('report.done.outlet.treatment.index')
            ->with('summary', $summary)
            ->with('from', $from)
            ->with('to', $to)
            ->with('outlet', $outlet)
            ->with('detail_usage', $detail_usage);
    }
    public function treatmentdone_outlet_summary(Request $request)
    {
        $summary = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
            $summary = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->whereIn('outlet_id', $outlet_id)
                ->get()
                ->groupBy('outlet_id');
        }
        $data = array();
        foreach ($summary as $list) {
            $total_usage = 0;
            $total = 0;
            $count_data = 0;
            foreach ($list as $item) {
                $row = array();
                foreach ($item->usage_detail as $usage) {
                    foreach ($item->usage_detail as $usage) {
                        if (substr($usage->product_treatment->product_treatment_code, 0, 1) == 'T') {
                            $total_usage = $total_usage + $usage->used;
                            $total += $usage->value * $usage->used;
                            $count_data++;
                        }
                    }
                }
                $row[] = $list[0]->outlet->outlet_name;
                $row[] = $count_data;
                $row[] = $total_usage;
                $row[] = format_money($total);
                $data[] = $row;
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function treatmentdone_outlet_detail(Request $request)
    {
        $detail_usage = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
            $detail_usage = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->whereIn('outlet_id', $outlet_id)
                ->get();
        }
        $data = array();
        foreach ($detail_usage as $list) {
            foreach ($list->usage_detail as $detail) {
                if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                    $row = array();
                    $therapist = '';
                    foreach ($detail->usage_therapist as $gettherapist) {
                        $therapist .= $gettherapist->therapist->therapist_name . ".<br>";
                    }
                    $row[] = '<a href=' . route('usage-treatment.print', $list->id_usage) . ' target="_blank">' . $list->usage_code . '</a>';
                    $row[] = $list->usage_date;
                    $row[] = '<a href=' . route('invoice.print', $detail->invoice->id_invoice) . ' target="_blank">' . $detail->inv_code . '</a>';
                    $row[] = $detail->invoice->inv_date;

                    $row[] = $list->outlet->outlet_name;
                    $row[] = '<a href=' . route('customers.show', $list->customer_id) . ' target="_blank">' . $list->customer->induk_customer . '</a>';
                    $row[] = $list->customer->full_name;

                    $row[] = $list->user->username;
                    $row[] = $detail->product_treatment->product_treatment_code;
                    $row[] = $detail->product_treatment->product_treatment_name;
                    foreach ($detail->invoice_package->invoice_detail as $inv) {
                        if ($inv->package) {
                            $row[] = 'Package';
                        } else {
                            if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                                $row[] = 'Treatment';
                            } else {
                                $row[] = 'Product';
                            }
                        }
                    }

                    $row[] = format_money($detail->value * $detail->used);
                    $row[] = $detail->used;
                    $row[] = format_money($detail->value * $detail->qty);
                    $row[] = $detail->qty;
                    $row[] = $detail->duration;
                    $row[] = $detail->point;
                    $row[] = $therapist;
                    $row[] = $list->remarks;
                    $row[] = '<a href=' . route('usage-treatment.show', $list->id_usage) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                    $data[] = $row;
                }
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_productdone_outlet()
    {
        $outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $summary = [];
        $detail_usage = [];

        return view('report.done.outlet.product.index')
            ->with('summary', $summary)
            ->with('from', $from)
            ->with('to', $to)
            ->with('outlet', $outlet)
            ->with('detail_usage', $detail_usage);
    }

    public function productdone_outlet_detail(Request $request)
    {
        $detail_usage = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
            $detail_usage = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->whereIn('outlet_id', $outlet_id)
                ->get();
        }
        $data = array();
        foreach ($detail_usage as $list) {
            foreach ($list->usage_detail as $detail) {
                if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'P') {
                    $row = array();
                    $therapist = '';
                    foreach ($detail->usage_therapist as $gettherapist) {
                        $therapist .= $gettherapist->therapist->therapist_name . ".<br>";
                    }
                    $row[] = '<a href=' . route('usage-treatment.print', $list->id_usage) . ' target="_blank">' . $list->usage_code . '</a>';
                    $row[] = $list->usage_date;
                    $row[] = '<a href=' . route('invoice.print', $detail->invoice->id_invoice) . ' target="_blank">' . $detail->inv_code . '</a>';
                    $row[] = $detail->invoice->inv_date;

                    $row[] = $list->outlet->outlet_name;
                    $row[] = '<a href=' . route('customers.show', $list->customer_id) . ' target="_blank">' . $list->customer->induk_customer . '</a>';
                    $row[] = $list->customer->full_name;

                    $row[] = $list->user->username;
                    $row[] = $detail->product_treatment->product_treatment_name;
                    $row[] = $detail->product_treatment->product_treatment_code;
                    foreach ($detail->invoice_package->invoice_detail as $inv) {
                        if ($inv->package) {
                            $row[] = 'Package';
                        } else {
                            if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                                $row[] = 'Treatment';
                            } else {
                                $row[] = 'Product';
                            }
                        }
                    }

                    $row[] = format_money($detail->value * $detail->used);
                    $row[] = $detail->used;
                    $row[] = format_money($detail->value * $detail->qty);
                    $row[] = $detail->qty;
                    $row[] = $detail->duration;
                    $row[] = $detail->point;
                    $row[] = $therapist;
                    $row[] = $list->remarks;
                    $row[] = '<a href=' . route('usage-treatment.show', $list->id_usage) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                    $data[] = $row;
                }
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_treatmentdone_therapist()
    {
        $therapist_data = Therapist::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $summary = [];
        $detail_usage = [];

        return view('report.done.therapist.treatment.index')
            ->with('summary', $summary)
            ->with('from', $from)
            ->with('to', $to)
            ->with('therapist_data', $therapist_data)
            ->with('detail_usage', $detail_usage);
    }

    public function treatmentdone_therapist(Request $request)
    {
        $summary = [];
        $detail_usage = [];
        $therapist_data = Therapist::all();
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->therapist) {
            $therapist = [];
            for ($i = 0; $i < count($request->therapist); $i++) {
                $therapist[] = $request->therapist[$i];
            }

            $detail_usage = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->with(['usage_detail.usage_therapist' => function ($query) use ($therapist) {
                    $query->whereIn('therapist_id', $therapist);
                }])
                ->get();
        }
        $data = array();
        foreach ($detail_usage as $list) {
            foreach ($list->usage_detail as $detail) {
                foreach ($detail->usage_therapist as $gettherapist) {
                    $row = array();
                    $row[] = '<a href=' . route('usage-treatment.print', $list->id_usage) . ' target="_blank">' . $list->usage_code . '</a>';
                    $row[] = $list->usage_date;
                    $row[] = '<a href=' . route('invoice.print', $detail->invoice->id_invoice) . ' target="_blank">' . $detail->inv_code . '</a>';
                    $row[] = $detail->invoice->inv_date;
                    $row[] = '<a href=' . route('customers.show', $list->customer_id) . ' target="_blank">' . $list->customer->induk_customer . '</a>';
                    $row[] = $list->customer->full_name;
                    $row[] = $list->user->username;
                    $row[] = $detail->product_treatment->product_treatment_code;
                    $row[] = $detail->product_treatment->product_treatment_name;

                    foreach ($detail->invoice_package->invoice_detail as $inv) {
                        if ($inv->package) {
                            $row[] = 'Package';
                        } else {
                            if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                                $row[] = 'Treatment';
                            } else {
                                $row[] = 'Product';
                            }
                        }
                    }
                    $row[] = format_money($detail->value * $detail->used);
                    $row[] = $detail->used;
                    $row[] = format_money($detail->value * $detail->qty);
                    $row[] = $detail->qty;
                    $row[] = $detail->duration;
                    $row[] = $detail->point;
                    $row[] = $gettherapist->therapist->therapist_name;
                    $row[] = $list->remarks;
                    $row[] = '<a href=' . route('usage-treatment.show', $list->id_usage) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                    $data[] = $row;
                }
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}
