<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Outlet;
use App\Models\Package;
use App\Models\ProductTreatment;
use Carbon\carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ReportItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    public function index_item_sold_outlet()
    {
        $sold_outlet = Outlet::all();
        $package = Package::all();
        $product = ProductTreatment::all();
        $allItem = $package->concat($product);
        $count = count($allItem);

        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $detail_invoice = [];

        return view('report.item-sold.sales')
            ->with('item', $allItem)
            ->with('count', $count)
            ->with('from', $from)
            ->with('to', $to)
            ->with('sold_outlet', $sold_outlet)
            ->with('detail_invoice', $detail_invoice);
    }

    public function item_sold_outlet(Request $request)
    {
        $detail_invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->item) {
            $item_id = [];
            for ($i = 0; $i < count($request->item); $i++) {
                $item_id[] = $request->item[$i];
            }

            $detail_invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('outlet_id', $request->outlet)
                ->where('void_invoice', 0)
                ->with(['invoice_detail' => function ($query) use ($item_id) {
                    $query->whereIn('package_code', $item_id);
                }])
                ->get();
        }
        $data = array();
        foreach ($detail_invoice as $list) {
            foreach ($list->invoice_detail as $detail) {
                $row = array();
                if ($detail->type_line == 0) {
                    $disc = '%';
                } else {
                    $disc = 'Rp';
                }
                if (count($list->log_convert) > 0) {
                    $row[] = '<a href=' . route('invoice-convert.print', $list->id_invoice) . ' target="_blank">' . $list->inv_code . '</a>';
                } else {
                    $row[] = '<a href=' . route('invoice.print', $list->id_invoice) . ' target="_blank">' . $list->inv_code . '</a>';
                }
                $row[] = $list->inv_date;
                $row[] = $list->outlet->outlet_name;
                $row[] = '<a href=' . route('customers.show', $list->customer_id) . ' target="_blank">' . $list->customer->induk_customer . '</a>';
                $row[] = $list->customer->full_name;
                $row[] = $list->user->username;
                $row[] = $list->marketing_source->marketing_source_name;
                $row[] = $detail->activity->activity_name;
                $row[] = $list->consultant->consultant_name;
                $row[] = $list->therapist->therapist_name;
                $row[] = $detail->package_code;

                if ($detail->package) {
                    $row[] = $detail->package->package_name;
                    $row[] = 'Package';
                } else {
                    $row[] = $detail->product_treatment->product_treatment_name;
                    if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                        $row[] = 'Treatment';
                    } else {
                        $row[] = 'Product';
                    }
                }

                $row[] = $detail->current_price;

                if ($detail->type_line == 0) {
                    $disc = '%';
                } else {
                    $disc = 'IDR';
                }

                if ($detail->discount_2) {
                    $row[] = $detail->discount_1 . '+' . $detail->discount_2 . $disc;
                } else {
                    $row[] = $detail->discount_1 . $disc;
                }

                $row[] = $detail->qty;
                $row[] = $detail->subtotal;
                $row[] = $list->remarks;
                $row[] = '<a href=' . route('customer.active', $list->id_invoice) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                $data[] = $row;
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    // ------------------------------------------------------------------------------------

    public function index_item_sold_collection()
    {
        $sold_outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $detail_invoice = [];
        $package = Package::all();
        $product = ProductTreatment::all();
        $allItem = $package->concat($product);
        $count = count($allItem);

        return view('report.item-sold.collection')
            ->with('item', $allItem)
            ->with('from', $from)
            ->with('count', $count)
            ->with('to', $to)
            ->with('sold_outlet', $sold_outlet)
            ->with('detail_invoice', $detail_invoice);
    }

    public function item_sold_collection(Request $request)
    {
        $detail_invoice = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->item) {
            $item_id = [];
            for ($i = 0; $i < count($request->item); $i++) {
                $item_id[] = $request->item[$i];
            }

            $detail_invoice = Invoice::whereBetween('inv_date', [$from, $to])
                ->where('outlet_id', $request->outlet)
                ->where('void_invoice', 0)
                ->with(['invoice_detail' => function ($query) use ($item_id) {
                    $query->whereIn('package_code', $item_id);
                }])
                ->get();
        }
        $data = array();
        $total_payment = 0;
        foreach ($detail_invoice as $list) {
            foreach ($list->payment as $payment) {
                $total_payment += str_replace(",", "", $payment->amount);
            }

            foreach ($list->invoice_detail as $detail) {
                foreach ($detail->invoice_package as $package) {
                    $row = array();
                    if (count($list->log_convert) > 0) {
                        $row[] = '<a href=' . route('invoice-convert.print', $list->id_invoice) . ' target="_blank">' . $list->inv_code . '</a>';
                    } else {
                        $row[] = '<a href=' . route('invoice.print', $list->id_invoice) . ' target="_blank">' . $list->inv_code . '</a>';
                    }
                    $row[] = $list->inv_date;
                    $row[] = $list->outlet->outlet_name;
                    $row[] = '<a href=' . route('customers.show', $list->customer_id) . ' target="_blank">' . $list->customer->induk_customer . '</a>';
                    $row[] = $list->customer->full_name;
                    $row[] = $list->user->username;
                    $row[] = $list->marketing_source->marketing_source_name;
                    $row[] = $detail->activity['activity_name'];
                    $row[] = $list->consultant->consultant_name;
                    $row[] = $list->therapist->therapist_name;
                    $row[] = $package->product_treatment->product_treatment_code;
                    $row[] = $package->product_treatment->product_treatment_name;
                    if (substr($package->product_treatment->product_treatment_code, 0, 1) == 'T') {
                        $row[] = 'Treatment';
                    } else {
                        $row[] = 'Product';
                    }
                    if ($detail->type_line == 0) {
                        $disc = '%';
                    } else {
                        $disc = 'IDR';
                    }

                    if ($detail->discount_2) {
                        $row[] = $detail->discount_1 . '+' . $detail->discount_2 . $disc;
                    } else {
                        $row[] = $detail->discount_1 . $disc;
                    }
                    $row[] = $package->qty + $package->used;
                    $row[] = format_money($package->nominal * ($package->qty + $package->used));
                    $row[] = $list->remarks;
                    $row[] = '<a href=' . route('customer.active', $list->id_invoice) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                    $data[] = $row;
                }
            }
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}
