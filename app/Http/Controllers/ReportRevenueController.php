<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Carbon\carbon;
use DataTables;
use App\Models\Outlet;
use App\Models\Invoice;

class ReportRevenueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    public function index_revenue()
    {
        $outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $detail = [];
        return view('report.revenue.index')
            ->with('from', $from)
            ->with('to', $to)
            ->with('outlet', $outlet)
            ->with('detail', $detail);
    }

    public function revenue(Request $request)
    {
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }
        $details = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0);
        if ($request->outlet) {
            $details->whereIn('outlet_id', $request->outlet);
        }
        $details = $details->get();
        $data = array();
        foreach ($details as $list) {
            foreach ($list->invoice_detail as $detail) {
                foreach ($detail->invoice_package as $package) {
                    $row = array();
                    if (count($list->log_convert) > 0) {
                        $row[] = "<a href=\"".route('invoice-convert.print', $list->id_invoice)."\" target=\"_blank\">".$list->inv_code."</a>";
                    } else {
                        $row[] = "<a href=\"".route('invoice.print', $list->id_invoice)."\" target=\"_blank\">".$list->inv_code."</a>";
                    }
                    $row[] = $list->inv_date;
                    $row[] = $list->outlet->outlet_name;
                    $row[] = "<a href=\"".route('customers.show', $list->customer_id)."\" target=\"_blank\">".$list->customer->induk_customer."</a>";
                    $row[] = $list->customer->full_name;
                    $row[] = $package->product_treatment->product_treatment_code;
                    $row[] = $package->product_treatment->product_treatment_name;
                    $row[] = $package->qty + $package->used;
                    $row[] = format_money($package->nominal * ($package->qty + $package->used));
                    $row[] = $package->used;
                    $row[] = format_money($package->nominal * $package->used);
                    $row[] = $package->qty;
                    $row[] = format_money($package->nominal * $package->qty);
                    $row[] = "<a href=\"".route('customer.active', $list->id_invoice)."\" class='btn btn-info btn-xs' target=\"_blank\"><i class=\"fa fa-eye\"></i></a>";
                    $data[] = $row;
                }
            }
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}
