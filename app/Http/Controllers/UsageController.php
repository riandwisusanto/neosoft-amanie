<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Auth;
use Carbon\Carbon;
use App\Models\Usage;
use App\Models\UsageDetail;
use App\Models\UsageTherapist;
use App\Models\InvoicePackage;
use App\Models\LogUsage;
use App\Models\Item;
use App\Models\Service;
use App\Models\ServiceDetail;
use App\Models\ProductTreatment;
use App\Models\Therapist;
use App\Models\UserOutlet;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\InventoryAdjustment;
use App\Models\Outlet;
use App\Models\WarehouseOutlet;
use App\Models\Invoice;
use App\Models\AnamnesaDetail;
use Illuminate\Support\Facades\Log;
use App\Models\Queue;

class UsageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('customer')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }

    public function store(Request $request)
    {

        // $dayname = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('l');
        // $date = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('Y-m-d');
        // foreach ($request->therapist as $key => $value) {
        //     $therapist = Therapist::whereIdTherapist($value)->first();
        //     // check leaves
        //     $therapistLeave = TherapistLeave::whereTherapistId($value)
        //         ->where('start_date', '>=', $date)
        //         ->where('end_date', '<=', $date)
        //         ->first();

        //     if (!empty($therapistLeave)) {
        //         return response()->json($therapist->therapist_name . " leave on date " . $therapistLeave->start_date . " to " .  $therapistLeave->end_date, 422);
        //     }

        //     // check schedule
        //     $therapistSchedules = TherapistSchedule::where('day', $dayname)
        //         ->whereTherapistId($value)
        //         ->whereTime('start_time', '<=', $request->start)
        //         ->whereTime('end_time', '>=', date('H:i:s', strtotime('+60 minutes', strtotime($request->start))))
        //         ->get();
        //     if (count($therapistSchedules) == 0) {
        //         return response()->json($therapist->therapist_name . " no schedule", 422);
        //     }
        // }

        $usage = new Usage;
        $usage->usage_code = Usage::generateUsageCode($request->outlet);
        $usage->usage_date = Carbon::createFromFormat('d/m/Y', $request->utilisationdate)->format('Y-m-d');
        $usage->customer_id = $request->customer_id;
        $usage->outlet_id = $request->outlet;
        $usage->staff_id = Auth::user()->id;
        $usage->remarks = $request->remarks;
        $usage->status_usage = 0;
        $filename       = date('mdYHis') . "-signature.png";
        $usage->file    = $filename;
        $usage->save();
        $data_uri = explode(',', $request->signature);
        // $encoded_image = explode(",", $data_uri)[1];
        // $decoded_image = base64_decode($encoded_image);
        // if(isset($data_uri[0])):
        //     $data_uri = $data_uri[0];
        // endif;
        $encoded_image = $data_uri[1];
        $decoded_image = base64_decode($encoded_image);
        

        file_put_contents(__DIR__. '/../../../storage/app/public/customer/'. $filename, $decoded_image);

        for ($i = 0; $i < count($request->product_id); $i++) {
            $invoice_package = InvoicePackage::find($request->inv_package_id[$i]);
            $usage_detail = new UsageDetail;
            $usage_detail->usage_id = $usage->id_usage;
            $usage_detail->inv_code = $request->inv_code[$i];
            $usage_detail->product_id = $request->product_id[$i];
            $usage_detail->invoice_package_id = $request->inv_package_id[$i];
            $usage_detail->point = $request->point[$i];

            $usage_detail->qty = $invoice_package->qty - $request->usage[$i];
            $usage_detail->used = $request->usage[$i];
            $usage_detail->duration = $request->durasi[$i];
            $usage_detail->value = $invoice_package->nominal;
            $usage_detail->save();

            $invoice_package->used = $invoice_package->used + $request->usage[$i];
            $invoice_package->qty = $invoice_package->qty - $request->usage[$i];
            $invoice_package->update();

            $therapist = "therapist_" . $request->inv_package_id[$i];
            for ($z = 0; $z < count($request->$therapist); $z++) {
                $usage_therapist = new UsageTherapist;
                $usage_therapist->usage_detail_id = $usage_detail->id_usage_detail;
                $usage_therapist->therapist_id = $request->$therapist[$z];
                $usage_therapist->save();
            }
        }

        $queue = Queue::where('customer_id',$usage->customer_id)->count();
        if($queue)
        {
            $updateQueue = Queue::where('customer_id',$usage->customer_id)->latest('created_at')->first();
            $updateQueue->status_queue = 2;
            $updateQueue->update();
        }
        
        $this->createMutations($request, $usage);

        return json_encode($usage->id_usage);
    }

   /**
     * Create mutations.
     *
     * @return void
     **/
    public function createMutations(Request $request, $usage)
    {
        foreach ($request->product_id as $index => $id) {
            $product = ProductTreatment::find($id);
            
            $invoice = Invoice::where('inv_code', $request->inv_code[$index])->first();

            $userOutlets = $request->user()->userOutlets()->pluck('outlet_id');
            $grantedWarehouses = WarehouseOutlet::whereIn('outlet_id', $userOutlets)->pluck('warehouse_id');

            
            if (isset($request->anamnesa_detail_id[$index])) {
                $anamnesaDetail = AnamnesaDetail::find($request->anamnesa_detail_id[$index]);
                if (isset($anamnesaDetail->boms) && count($anamnesaDetail->boms) > 0) {
                    foreach ($anamnesaDetail->boms as $material) {
                        $usageCount = $request->usage[$index];
                        $sort = $material->rawItem->dispense_method == 'lifo' ? 'asc' : 'desc';
    
                        $currentStock = $material->rawItem->stock - ($usageCount * $material->qty);
    
                        $material->rawItem()->update([
                            'stock' => $currentStock
                        ]);
    
                        while ($usageCount > 0) {
                            $mutationLog = Mutation::where('transtype', 'in')
                                                    ->where('transdesc', '<>', 'BEGIN EMPTY')
                                                    ->where('item_id', $material->raw_item_id)
                                                    ->whereIn('warehouse_destination_id', $grantedWarehouses)
                                                    ->orderBy('transdate', $sort)
                                                    ->where('balance', '>', 0)
                                                    ->first();
        
                            if ( ! $mutationLog) break;
            
                            $outQty = 0;
                            
                            // current item at this warehouse has not enough stock.
                            if ($mutationLog->balance < $usageCount) {
                                $usageCount -= $mutationLog->balance;
                                $outQty = $mutationLog->balance;
        
                                $mutationLog->balance = 0;
                            } else {
                                $mutationLog->balance -= $usageCount;
                                $outQty = $usageCount;
        
                                $usageCount = 0;
                            }
        
                            $mutationLog->save();
        
                            $mutation = [
                                'warehouse_origin_id' => $mutationLog->warehouse_destination_id,
                                'rack_origin_id' => $mutationLog->rack_destination_id,
                                'sales_invoice_id' => $usage->id_usage,
                                'item_id' => $material->raw_item_id,
                                'transdate' => Carbon::createFromFormat('d/m/Y', $request->utilisationdate)->format('Y-m-d'),
                                'transtype' => 'out',
                                'cogs'     => $mutationLog->cogs,
                                'price'     => str_replace(',', '', $material->rawItem->sales->price),
                                'ref_id'    => $mutationLog->id,
                                'balance'   => 0,
                                'transdesc' => 'USAGE',
                                'qty' => $outQty * $material->qty * -1
                            ];
                            
                            Mutation::create($mutation);
                        }
                    }
                } else {
                    $usageCount = $request->usage[$index];
                    $sort = $product->item->dispense_method == 'lifo' ? 'asc' : 'desc';
    
                    $currentStock = $product->item->stock - $usageCount;
    
                    $product->item()->update([
                        'stock' => $currentStock
                    ]);
    
                    while ($usageCount > 0) {
                        $mutationLog = Mutation::where('transtype', 'in')
                                                ->where('transdesc', '<>', 'BEGIN EMPTY')
                                                ->where('item_id', $product->item->id)
                                                ->whereIn('warehouse_destination_id', $grantedWarehouses)
                                                ->orderBy('transdate', $sort)
                                                ->where('balance', '>', 0)
                                                ->first();
    
                        if ( ! $mutationLog) break;
        
                        $outQty = 0;
                        
                        // current item at this warehouse has not enough stock.
                        if ($mutationLog->balance < $usageCount) {
                            $usageCount -= $mutationLog->balance;
                            $outQty = $mutationLog->balance;
    
                            $mutationLog->balance = 0;
                        } else {
                            $mutationLog->balance -= $usageCount;
                            $outQty = $usageCount;
    
                            $usageCount = 0;
                        }
    
                        $mutationLog->save();
    
                        $mutation = [
                            'warehouse_origin_id' => $mutationLog->warehouse_destination_id,
                            'rack_origin_id' => $mutationLog->rack_destination_id,
                            'sales_invoice_id' => $usage->id_usage,
                            'item_id' => $product->item->id,
                            'transdate' => Carbon::createFromFormat('d/m/Y', $request->utilisationdate)->format('Y-m-d'),
                            'transtype' => 'out',
                            'cogs'     => $mutationLog->cogs,
                            'price'     => str_replace(',', '', $product->item->sales->price),
                            'ref_id'    => $mutationLog->id,
                            'balance'   => 0,
                            'transdesc' => 'USAGE',
                            'qty' => $outQty * -1
                        ];
                        
                        Mutation::create($mutation);
                    }
                }
            } else {
                // check if current usage is treatment.
                if ($product->item->type == 'service') {
                    foreach ($product->item->boms as $material) {
                        $usageCount = $request->usage[$index];
                        $sort = $material->rawItem->dispense_method == 'lifo' ? 'asc' : 'desc';
    
                        $currentStock = $material->rawItem->stock - ($usageCount * $material->qty);
    
                        $material->rawItem()->update([
                            'stock' => $currentStock
                        ]);
    
                        while ($usageCount > 0) {
                            $mutationLog = Mutation::where('transtype', 'in')
                                                    ->where('transdesc', '<>', 'BEGIN EMPTY')
                                                    ->where('item_id', $material->raw_item_id)
                                                    ->whereIn('warehouse_destination_id', $grantedWarehouses)
                                                    ->orderBy('transdate', $sort)
                                                    ->where('balance', '>', 0)
                                                    ->first();
        
                            if ( ! $mutationLog) break;
            
                            $outQty = 0;
                            
                            // current item at this warehouse has not enough stock.
                            if ($mutationLog->balance < $usageCount) {
                                $usageCount -= $mutationLog->balance;
                                $outQty = $mutationLog->balance;
        
                                $mutationLog->balance = 0;
                            } else {
                                $mutationLog->balance -= $usageCount;
                                $outQty = $usageCount;
        
                                $usageCount = 0;
                            }
        
                            $mutationLog->save();
        
                            $mutation = [
                                'warehouse_origin_id' => $mutationLog->warehouse_destination_id,
                                'rack_origin_id' => $mutationLog->rack_destination_id,
                                'sales_invoice_id' => $usage->id_usage,
                                'item_id' => $material->raw_item_id,
                                'transdate' => Carbon::createFromFormat('d/m/Y', $request->utilisationdate)->format('Y-m-d'),
                                'transtype' => 'out',
                                'cogs'     => $mutationLog->cogs,
                                'price'     => str_replace(',', '', $material->rawItem->sales->price),
                                'ref_id'    => $mutationLog->id,
                                'balance'   => 0,
                                'transdesc' => 'USAGE',
                                'qty' => $outQty * $material->qty * -1
                            ];
                            
                            Mutation::create($mutation);
                        }
                    }
    
                    
                } else {
    
                    $usageCount = $request->usage[$index];
                    $sort = $product->item->dispense_method == 'lifo' ? 'asc' : 'desc';
    
                    $currentStock = $product->item->stock - $usageCount;
    
                    $product->item()->update([
                        'stock' => $currentStock
                    ]);
    
                    while ($usageCount > 0) {
                        $mutationLog = Mutation::where('transtype', 'in')
                                                ->where('transdesc', '<>', 'BEGIN EMPTY')
                                                ->where('item_id', $product->item->id)
                                                ->whereIn('warehouse_destination_id', $grantedWarehouses)
                                                ->orderBy('transdate', $sort)
                                                ->where('balance', '>', 0)
                                                ->first();
    
                        if ( ! $mutationLog) break;
        
                        
                        $outQty = 0;
                        
                        // current item at this warehouse has not enough stock.
                        if ($mutationLog->balance < $usageCount) {
                            $usageCount -= $mutationLog->balance;
                            $outQty = $mutationLog->balance;
    
                            $mutationLog->balance = 0;
                        } else {
                            $mutationLog->balance -= $usageCount;
                            $outQty = $usageCount;
    
                            $usageCount = 0;
                        }
    
                        $mutationLog->save();
    
                        $mutation = [
                            'warehouse_origin_id' => $mutationLog->warehouse_destination_id,
                            'rack_origin_id' => $mutationLog->rack_destination_id,
                            'sales_invoice_id' => $usage->id_usage,
                            'item_id' => $product->item->id,
                            'transdate' => Carbon::createFromFormat('d/m/Y', $request->utilisationdate)->format('Y-m-d'),
                            'transtype' => 'out',
                            'cogs'     => $mutationLog->cogs,
                            'price'     => str_replace(',', '', $product->item->sales->price),
                            'ref_id'    => $mutationLog->id,
                            'balance'   => 0,
                            'transdesc' => 'USAGE',
                            'qty' => $outQty * -1
                        ];
                        
                        Mutation::create($mutation);
                    }
    
                }
            }
        }
    }
    
    public function printUsage($id)
    {
        $usage = Usage::find($id);
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');

        return view('customers.treatment-balance.print', compact('usage'));
    }

    public function voidUsage(Request $request, $id)
    {
        $saveLog = new LogUsage;
        $saveLog->usage_id = $id;
        $saveLog->staff_id = Auth::user()->id;
        $saveLog->remarks = $request->remarks;
        $saveLog->save();

        $usage = Usage::find($id);
        $usage->status_usage = 1;
        $usage->remarks = $request->remarks;
        $usage->update();

        for ($i = 0; $i < count($request->inv_package); $i++) {
            $invoice_package = InvoicePackage::find($request->inv_package[$i]);
            $invoice_package->qty = $request->used[$i] + $invoice_package->qty;
            $invoice_package->used = $invoice_package->used - $request->used[$i];
            $invoice_package->update();
        }

        $this->createVoidAdjustment($request, $usage);

        $getDetail = UsageDetail::where('usage_id', $id)->pluck('id_usage_detail')->toArray();
        UsageTherapist::whereIn('usage_detail_id', $getDetail)->delete();
        UsageDetail::where('usage_id', $id)->delete();
    }

    /**
     * Inventory adjustment for void usage.
     **/
    public function createVoidAdjustment(Request $request, $usage)
    {
        foreach ($request->inv_package as $index => $package_id) {
            $package = InvoicePackage::find($package_id);

            $product = ProductTreatment::find($package->product_id);
            $usageCount = $request->used[$index];

            if ( ! is_null($package->anamnesa_detail_id)) {
                $anamnesaDetail = AnamnesaDetail::find($package->anamnesa_detail_id);
                
                if (count($anamnesaDetail->boms)) {
                    foreach ($anamnesaDetail->boms as $material) {
                        $stockBefore = $material->rawItem->stock;
    
                        $currentStock = $material->rawItem->stock + ($usageCount * $material->qty);
                        $currentSellableStock = $material->rawItem->sellable_stock + ($usageCount * $material->qty);
    
                        $material->rawItem()->update([
                            'stock' => $currentStock,
                            'sellable_stock' => $currentSellableStock
                        ]);
    
                        $mutationRef = Mutation::where('item_id', $material->raw_item_id)
                                                    ->where('sales_invoice_id', $usage->id_usage)
                                                    ->first();
    
                        $mutationRef->balance += ($usageCount * $material->qty);
                        $mutationRef->save();
    
                        $adjustmentData = [
                            'item_id'               => $material->raw_item_id,
                            'warehouse_id'          => $mutationRef->warehouse_origin_id,
                            'created_by'            => $request->user()->id,
                            'adjusted_by'           => $request->user()->id,
                            'transdate'             => now()->format('Y-m-d'),
                            'transdesc'             => 'ADJ VOID USAGE',
                            'stock_before'          => $stockBefore,
                            'stock_after'           => $currentStock,
                            'stock_adjustment'      => ($usageCount * $material->qty),
                            'stock_adjustment_type' => 'in',
                            'ref_id'                => $mutationRef->id,
                            'reason'                => $usage->remarks
                        ];
    
                        $adjustment = new InventoryAdjustment($adjustmentData);
                        $adjustment->code = InventoryAdjustment::generateCode();
                        $adjustment->save();
    
                        $mutationData = [
                            'warehouse_destination_id' => $mutationRef->warehouse_origin_id,
                            'rack_destination_id' => $mutationRef->rack_origin_id,
                            'item_id' => $material->raw_item_id,
                            'transdate' => now()->format('Y-m-d'),
                            'transtype' => 'in',
                            'cogs'     => $mutationRef->cogs,
                            'price'     => str_replace(',', '', $material->rawItem->sales->price),
                            'adjustment_id' => $adjustment->id,
                            'ref_id'    => $mutationRef->id,
                            'balance'   => 0,
                            'transdesc' => 'ADJ VOID USAGE',
                            'qty' => ($usageCount * $material->qty)
                        ];
                        
                        $mutation = Mutation::create($mutationData);
                    }
                }
            } else {
                // check if current usage is treatment.
                if ($product->item->type == 'service') {
                    if (count($product->item->boms)) {
                        foreach ($product->item->boms as $material) {
                            $stockBefore = $material->rawItem->stock;
        
                            $currentStock = $material->rawItem->stock + ($usageCount * $material->qty);
                            $currentSellableStock = $material->rawItem->sellable_stock + ($usageCount * $material->qty);
        
                            $material->rawItem()->update([
                                'stock' => $currentStock,
                                'sellable_stock' => $currentSellableStock
                            ]);
        
                            $mutationRef = Mutation::where('item_id', $material->raw_item_id)
                                                        ->where('sales_invoice_id', $usage->id_usage)
                                                        ->first();
        
                            $mutationRef->balance += ($usageCount * $material->qty);
                            $mutationRef->save();
        
                            $adjustmentData = [
                                'item_id'               => $material->raw_item_id,
                                'warehouse_id'          => $mutationRef->warehouse_origin_id,
                                'created_by'            => $request->user()->id,
                                'adjusted_by'           => $request->user()->id,
                                'transdate'             => now()->format('Y-m-d'),
                                'transdesc'             => 'ADJ VOID USAGE',
                                'stock_before'          => $stockBefore,
                                'stock_after'           => $currentStock,
                                'stock_adjustment'      => ($usageCount * $material->qty),
                                'stock_adjustment_type' => 'in',
                                'ref_id'                => $mutationRef->id,
                                'reason'                => $usage->remarks
                            ];
        
                            $adjustment = new InventoryAdjustment($adjustmentData);
                            $adjustment->code = InventoryAdjustment::generateCode();
                            $adjustment->save();
        
                            $mutationData = [
                                'warehouse_destination_id' => $mutationRef->warehouse_origin_id,
                                'rack_destination_id' => $mutationRef->rack_origin_id,
                                'item_id' => $material->raw_item_id,
                                'transdate' => now()->format('Y-m-d'),
                                'transtype' => 'in',
                                'cogs'     => $mutationRef->cogs,
                                'price'     => str_replace(',', '', $material->rawItem->sales->price),
                                'adjustment_id' => $adjustment->id,
                                'ref_id'    => $mutationRef->id,
                                'balance'   => 0,
                                'transdesc' => 'ADJ VOID USAGE',
                                'qty' => ($usageCount * $material->qty)
                            ];
                            
                            $mutation = Mutation::create($mutationData);
                        }
                    }
                } else {
                    $stockBefore = $product->item->stock;

                    $currentStock = $product->item->stock + $usageCount;
                    $currentSellableStock = $product->item->sellable_stock + $usageCount;

                    $product->item()->update([
                        'stock' => $currentStock,
                        'sellable_stock' => $currentSellableStock
                    ]);

                    $mutationRef = Mutation::where('item_id', $product->item->id)
                                                ->where('sales_invoice_id', $usage->id_usage)
                                                ->first();
                                                
                    $mutationRef->balance += $usageCount;
                    $mutationRef->save();

                    $adjustmentData = [
                        'item_id'               => $product->item->id,
                        'warehouse_id'          => $mutationRef->warehouse_origin_id,
                        'created_by'            => $request->user()->id,
                        'adjusted_by'           => $request->user()->id,
                        'transdate'             => now()->format('Y-m-d'),
                        'transdesc'             => 'ADJ VOID USAGE',
                        'stock_before'          => $stockBefore,
                        'stock_after'           => $currentStock,
                        'stock_adjustment'      => $usageCount,
                        'stock_adjustment_type' => 'in',
                        'ref_id'                => $mutationRef->id,
                        'reason'                => $usage->remarks
                    ];

                    $adjustment = new InventoryAdjustment($adjustmentData);
                    $adjustment->code = InventoryAdjustment::generateCode();
                    $adjustment->save();

                    $mutationData = [
                        'warehouse_destination_id' => $mutationRef->warehouse_origin_id,
                        'rack_destination_id' => $mutationRef->rack_origin_id,
                        'item_id' => $product->item->id,
                        'transdate' => now()->format('Y-m-d'),
                        'transtype' => 'in',
                        'cogs'     => $mutationRef->cogs,
                        'price'     => str_replace(',', '', $product->item->sales->price),
                        'adjustment_id' => $adjustment->id,
                        'ref_id'    => $mutationRef->id,
                        'balance'   => 0,
                        'transdesc' => 'ADJ VOID USAGE',
                        'qty' => $usageCount
                    ];
                    
                    $mutation = Mutation::create($mutationData);
                }
            }

            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usage = Usage::find($id);

        $usage_last = '';
        foreach ($usage->usage_detail as $detail) {
            $usage_last = UsageDetail::where('inv_code', $detail->inv_code)
            ->latest('id_usage_detail')
            ->first();
        }
        // dd($usage_last);

        // $delete = Usage::leftJoin('usage_details', 'usage_details.usage_id', 'usages.id_usage')
        // ->where('customer_id', $usage->customer_id)
        // ->where('status_usage', 0)
        // ->where('usage_details.inv_code', $usage->usage_detail->inv_code)
        // ->get();

        // dd($delete);
        // ->latest('id_usage')
        // ->first();
        $log_usage = LogUsage::where('usage_id', $id)->whereNull('remarks')->get();

        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $therapist_all = Therapist::whereHas('therapist_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();
        return view('customers.past-usage.detail', compact('usage', 'therapist_all', 'usage_last', 'log_usage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Usage::find($id);
        $update->usage_date = Carbon::createFromFormat('d/m/Y', $request->date_usage)->format('Y-m-d');
        $update->remarks = $request->remarks;
        $update->update();

        $saveLog = new LogUsage;
        $saveLog->usage_id = $id;
        $saveLog->staff_id = Auth::user()->id;
        $saveLog->save();

        for ($i = 0; $i < count($request->product_id); $i++) {
            $usage_detail = UsageDetail::find($request->id_usage[$i]);
            $usage_detail->point = $request->point[$i];
            $usage_detail->update();

            $therapist = "therapist_" . $request->inv_package_id[$i];

            $before = UsageTherapist::where('usage_detail_id', $request->id_usage)->pluck('therapist_id')->toArray();
            $shouldDelete = array_diff($before, $request->$therapist);

            if (count($shouldDelete)) {
                UsageTherapist::where('usage_detail_id', $request->id_usage)
                    ->whereIn('therapist_id', $shouldDelete)
                    ->delete();
            }

            for ($z = 0; $z < count($request->$therapist); $z++) {
                UsageTherapist::updateOrCreate([
                    'usage_detail_id' => $request->id_usage[$i],
                    'therapist_id' => $request->$therapist[$z]
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
