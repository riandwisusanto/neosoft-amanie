<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Outlet;
use App\Models\UserOutlet;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (array_intersect(["SUPER_USER"], json_decode(Auth::user()->level))) {
            $outlet = Outlet::all();
        } else {
            $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
            $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        }
        return view('setting.useraccess.index', compact('outlet'));
    }

    public function listData()
    {
        $user = User::all();
        $outlet = UserOutlet::leftJoin('outlets', 'outlets.id_outlet', 'user_outlets.outlet_id')->get();
        $no = 0;
        $data = array();
        foreach ($user as $list) {
            $branch = '';
            foreach ($outlet as $key) {
                if ($key->user_id == $list->id) {
                    $branch .= "&middot;".$key->outlet_name."<br>";
                }
            }

            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = $list->name;
            $row[] = $list->username;
            $row[] = $list->email;
            $role = '';
            foreach (json_decode($list->level) as $level) {
                $role .= "&middot;".$level."<br>";
            }
            $row[] = $role;
            $row[] = $branch;
            if ($list->status_user == 1) {
                $row[] = '&#10004;';
            } else {
                $row[] = '&#10007;';
            }

            $row[] = '<div class="btn-group-vertical">
                    <a onclick="editForm('.$list->id.')" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a>
                    <a onclick="resetPassword('.$list->id.')" class="btn btn-warning btn-sm"><i class="fa fa-retweet"></i> Reset Password</a></div>';
            $data[] = $row;
        }

        $output = array("data" => $data);
        return response()->json($output);
    }

    public function resetPassword(Request $request)
    {
        $reset = User::findOrFail($request->user_id);
        $reset->password = bcrypt('123456789');
        $reset->update();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'unique:users|max:255',
            'username' => 'unique:users',
        ]);

        $user = new User;
        $user->username = $request->username;
        $user->name = $request->fullname;
        $user->email = $request->email;
        $user->password = bcrypt('123456789');
        $user->api_token = str_random(40);
        $user->level = json_encode($request->role);

        if ($request->status_user == null) {
            $user->status_user = 0;
        } else {
            $user->status_user = 1;
        }

        $user->save();

        foreach ($request->outlet as $key => $value) {
            $user_outlet = new UserOutlet;
            $user_outlet->user_id = $user->id;
            $user_outlet->outlet_id = $value;
            $user_outlet->save();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $outlet = UserOutlet::where('user_id', $id)->pluck('outlet_id');

        $level = json_decode($user->level);
        $output = array("user" => $user, "outlet" => $outlet, "level" => $level);
        return json_encode($output);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'unique:users,email,'.$id,
            'username' => 'unique:users,username,'.$id,
        ]);
        $user = User::find($id);
        $user->username = $request->username;
        $user->name = $request->fullname;
        $user->email = $request->email;
        $user->level = json_encode($request->role);
        // if (!array_intersect(["SUPER_USER"], json_decode(Auth::user()->level))) {
        // }

        if ($request->status_user == null) {
            $user->status_user = 0;
        } else {
            $user->status_user = 1;
        }

        $user->update();

        $before = UserOutlet::where('user_id', $id)->pluck('outlet_id')->toArray();
        $shouldDelete = array_diff($before, $request->outlet);

        if (count($shouldDelete)) {
            UserOutlet::where('user_id', $id)
                                ->whereIn('outlet_id', $shouldDelete)
                                ->delete();
        }

        foreach ($request->outlet as $outlet_id) {
            UserOutlet::firstOrCreate([
                'user_id'     => $id,
                'outlet_id'   => $outlet_id
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $user = User::find($id);
        // $user->delete();

        // $before = UserOutlet::where('user_id', $id)->pluck('outlet_id')->toArray();
        // UserOutlet::where('user_id', $id)
        //     ->whereIn('outlet_id', $before)
        //     ->delete();
    }
}
