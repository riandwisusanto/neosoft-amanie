<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnamnesaBom extends Model
{
    /** @var Type $var description */
    protected $fillable = [
        'anamnesa_detail_id',
        'raw_item_id',
        'uom_id',
        'qty',
        'unit_value',
        'price'
    ];

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function rawItem()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'raw_item_id');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function uom()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_id');
    }
}
