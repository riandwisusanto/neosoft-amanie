<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentTherapist extends Model
{
    protected $table = 'appointment_therapists';
    protected $primaryKey = 'id_appointment_therapist';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['therapist_id', 'appointment_id'];
}
