<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BeginBalance extends Model
{
    protected $fillable = [
        'item_id',
        'begin_date',
        'begin_balance',
        'begin_cost',
    ];

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }
}
