<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

use Carbon\Carbon;

class Bom extends Model
{
    use Updater;
    protected $fillable = [
        'code',
        'item_id',
        'description',
        'batch_no',
        'amount',
        'is_active',
        'created_uid',
        'updated_uid',
    ];

    public function bom_details(){
        return $this->hasMany(BomDetail::class);
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public static function generateCode(){
        $now    = Carbon::now();
        $prefix = 'BOM-'.$now->isoFormat('YYMM');
        $num    = 0;
        $data   = self::select('code')
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->orderBy('code', 'desc')
            ->first();

        if($data) $num = (int) substr($data->code, strlen($prefix), 4);

        $num++;
        return $prefix.str_pad($num, 4, '0', STR_PAD_LEFT);
    }
}
