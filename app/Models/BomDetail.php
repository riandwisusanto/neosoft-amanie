<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class BomDetail extends Model
{
    use Updater;
    protected $fillable = [
        'bom_id',
        'item_id',
        'amount',
        'expire_date',
        'uom_id',
        'qty',
        'price',
        'total',
        'created_uid',
        'updated_uid',
    ];

    public function bom(){
        return $this->belongsTo(BomHeader::class);
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }
}
