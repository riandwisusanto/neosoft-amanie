<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerConsignment extends Model
{
    protected $table = 'customer_consignments';
    protected $primaryKey = 'id_customer_consignment';

    protected $fillable = ['customer_id','consigment_date','image'];
    protected $dates = ['consignment_date'];
}
