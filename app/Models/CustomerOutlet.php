<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerOutlet extends Model
{
    protected $table = 'customer_outlets';
    protected $primaryKey = 'id_customer_outlet';
    
    /** @var Type $var description */
    protected $fillable = [
        'id_customer_outlet',
        'customer_id',
        'outlet_id'
    ];
}
