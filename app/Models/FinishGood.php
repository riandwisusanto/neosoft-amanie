<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

class FinishGood extends Model
{
    use Updater;

    protected $fillable = [
        'work_in_process_id',
        'item_id',
        'uom_id',
        'description',
        'mfg_date',
        'expired_date',
        'qty',
        'begin_stock',
        'status',
        'is_active',
        'created_uid',
        'updated_uid'
    ];

    public function work_in_process(){
        return $this->belongsTo(WorkInProcess::class);
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }
}
