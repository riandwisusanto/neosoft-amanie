<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $primaryKey = 'group_id';

    protected $fillable = ['group_name', 'name'];

    public function therapist()
    {
        return $this->belongsTo(Therapist::class, 'group_id', 'group_id');
    }
}
