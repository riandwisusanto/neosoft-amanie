<?php

namespace App\Models;

use Carbon\carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $primaryKey = 'id_invoice';

    public function getInvDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getExpiredDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    // public function setInvDateAttribute($date){
    //     return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    // }

    // public function setExpiredDateAttribute($date){
    //     return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    // }

    public static function generateInvoiceCode($outlet_id)
    {
        $now = Carbon::now();
        $outlet = Outlet::find($outlet_id);
        $array = Invoice::where('outlet_id', $outlet_id)
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('inv_code')->first();

        if ($array) {
            $number = substr($array->inv_code, -4) + 1;
        } else {
            $number = 0001;
        }

        $idinvcode =
        $outlet->outlet_code . "-I-" . $now->isoFormat('YYYY') . $now->isoFormat('MM') . "-" . sprintf('%04d', $number);
        return $idinvcode;
    }

    public function outlet()
    {
        return $this->belongsTo('App\Models\Outlet', 'outlet_id', 'id_outlet');
    }

    public function marketing_source()
    {
        return $this->belongsTo('App\Models\MarketingSource', 'marketing_source_id', 'id_marketing_source');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
    }

    public function consultant()
    {
        return $this->belongsTo('App\Models\Consultant', 'consultant_id', 'id_consultant');
    }

    public function therapist()
    {
        return $this->belongsTo('App\Models\Therapist', 'therapist_id', 'id_therapist');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }

    public function payment()
    {
        return $this->hasMany('App\Models\Payment', 'inv_id', 'id_invoice');
    }

    public function invoice_detail()
    {
        return $this->hasMany('App\Models\InvoiceDetail', 'inv_id', 'id_invoice');
    }

    public function log_convert()
    {
        return $this->hasMany('App\Models\LogConvert', 'to_invoice_id', 'inv_code');
    }

    public function log_void_invoice()
    {
        return $this->hasOne('App\Models\LogVoidInvoice', 'inv_id', 'id_invoice');
    }

    public function getTotalAttribute($price)
    {
        return format_money($price);
    }

    public function getRemainingAttribute($price)
    {
        return format_money($price);
    }

    public function usage_detail()
    {
        return $this->belongsTo('App\Models\UsageDetail', 'inv_code', 'inv_code');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id', 'id_agent');
    }

    public function setTotalAttribute($price)
    {
        $this->attributes['total'] = str_replace(array('.', ','), "", $price);
    }

    public function setRemainingAttribute($price)
    {
        $this->attributes['remaining'] = str_replace(array('.', ','), "", $price);
    }
}
