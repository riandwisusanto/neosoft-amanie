<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicePackage extends Model
{
    protected $table = 'invoice_packages';
    protected $primaryKey = 'id_invoice_package';

    public function package_detail()
    {
        return $this->belongsTo('App\Models\PackageDetail', 'product_id', 'product_id');
    }

    public function product_treatment()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'product_id', 'id_product_treatment');
    }
    
    public function invoice_detail()
    {
        return $this->hasMany('App\Models\InvoiceDetail', 'id_inv_detail', 'inv_detail_id');
    }
}
