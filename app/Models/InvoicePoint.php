<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class InvoicePoint extends Model
{
    //
    protected $table = 'invoice_points';
    protected $primaryKey = 'id_invoice_point';

    public static function generateInvoiceCode($outlet)
    {
        $now = Carbon::now();
        $outlet = Outlet::find($outlet);
        $array = InvoicePoint::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->get()
            ->toArray();

        if ($array == []) {
            $number = 0001;
            $idinvcode =
                $outlet->outlet_code."-P-".$now->isoFormat('YYYY').
                $now->isoFormat('MM')."-".
                sprintf('%04d', $number);
        } else {
            $number = substr(end($array)['inv_point_code'], -4)+1;
            $idinvcode =
                $outlet->outlet_code."-P-".$now->isoFormat('YYYY').
                $now->isoFormat('MM')."-".
                sprintf('%04d', $number);
        }
        return $idinvcode;
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id_customer');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id', 'id_outlet');
    }

    public function staff()
    {
        return $this->belongsTo(User::class, 'staff_id', 'id');
    }

    public function invoice_details()
    {
        return $this->hasMany(InvoicePointDetail::class, 'invoice_point_id', 'id_invoice_point');
    }

    public function void()
    {
        return $this->hasOne(LogVoidInvoicePoint::class, 'inv_point_id', 'id_invoice_point');
    }
}
