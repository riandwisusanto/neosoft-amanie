<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicePointDetail extends Model
{
    //
    protected $table = 'invoice_point_details';
    protected $primaryKey = 'id_invoice_point_detail';

    public function invoice_point()
    {
        return $this->belongsTo(InvoicePoint::class, 'invoice_point_id', 'id_invoice_point');
    }
    public function product_point()
    {
        return $this->belongsTo(ProductPoint::class, 'product_point_id', 'id_product_point');
    }
}
