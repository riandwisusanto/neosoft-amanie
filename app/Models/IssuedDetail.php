<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

class IssuedDetail extends Model
{
    use Updater;

    protected $fillable = [
        'issued_id',
        'item_id',
        'uom_id',
        'description',
        'qty',
        'price',
        'disc',
        'tax',
        'total',
        'created_uid',
        'updated_uid',
    ];

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }
}
