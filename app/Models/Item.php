<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

use Carbon\Carbon;

class Item extends Model
{
    use Updater;
    protected $primaryKey = 'id_item';
    protected $fillable = [
        'code',
        'product_treatment_code',
        'description',
        'brand',
        'category_id',
        'begin_stock',
        'buffer_stock',
        'is_active',
        'created_by',
        'updated_by',
    ];

    public function category(){
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }

    public function product_treatment(){
        return $this->belongsTo(ProductTreatment::class, 'product_treatment_code', 'product_treatment_code');
    }

    public function purchase_order_details(){
        return $this->hasMany(PurchaseOrderDetail::class, 'item_id');
    }

    public function service_details(){
        return $this->hasMany(ServiceDetail::class, 'item_id');
    }

    public function retur_details(){
        return $this->hasMany(ReturDetail::class, 'item_id');
    }

    public function bom_details(){
        return $this->hasMany(BomDetail::class, 'item_id');
    }

    public function issued_details(){
        return $this->hasMany(IssuedDetail::class, 'item_id');
    }

    public function product_locations(){
        return $this->hasMany(ProductLocation::class, 'item_id');
    }

    public function product_mutations(){
        return $this->hasMany(ProductMutation::class, 'item_id');
    }

    public function work_orders(){
        return $this->hasMany(ProductLocation::class, 'item_id');
    }

    public function work_in_process_details(){
        return $this->hasMany(WorkInProcessDetail::class, 'item_id');
    }

    public function finish_goods(){
        return $this->hasMany(FinishGood::class, 'item_id');
    }

    public static function generateCode(){
        $now    = Carbon::now();
        $prefix = 'I-'.$now->isoFormat('YYMM');
        $num    = 0;
        $data   = self::select('code')
            ->where('code', 'like', 'I-')
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->orderBy('code', 'desc')
            ->first();

        if($data) $num = (int) substr($data->code, strlen($prefix), 4);

        $num++;
        return $prefix.str_pad($num, 4, '0', STR_PAD_LEFT);
    }

    protected static function boot(){
        parent::boot();
        static::creating(function($m){
            $m->code = $m->product_treatment_code ? $m->product_treatment_code : self::generateCode();
        });

        static::updating(function($m){
            $m->code = $m->product_treatment_code ? $m->product_treatment_code : self::generateCode();
        });
    }
}
