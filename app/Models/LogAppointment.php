<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogAppointment extends Model
{
    protected $table = 'log_appointments';
    protected $primaryKey = 'id_log_appointment';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
