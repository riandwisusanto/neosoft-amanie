<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogConvert extends Model
{
    protected $table = 'log_converts';
    protected $primaryKey = 'id_log_convert';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }
}
