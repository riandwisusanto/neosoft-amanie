<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUsage extends Model
{
    protected $table = 'log_usages';
    protected $primaryKey = 'id_log_usage';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }

    public function usage()
    {
        return $this->belongsTo('App\Models\Usage', 'usage_id', 'id_usage');
    }
}
