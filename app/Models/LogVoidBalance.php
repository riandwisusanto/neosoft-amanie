<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogVoidBalance extends Model
{
    protected $table = 'log_void_balances';
    protected $primaryKey = 'id_log_balance';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }
}
