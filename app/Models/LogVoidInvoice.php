<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogVoidInvoice extends Model
{
    protected $table = 'log_void_invoices';
    protected $primaryKey = 'id_log_void';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }
}
