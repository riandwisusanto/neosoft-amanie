<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogVoidInvoicePoint extends Model
{
    //
    protected $table = 'log_void_invoice_points';
    protected $primaryKey = 'id_log_void_inv_point';

    public function invoice_point()
    {
        return $this->belongsTo(InvoicePoint::class, 'inv_point_id', 'id_invoice_point');
    }

    public function staff()
    {
        return $this->belongsTo(User::class, 'staff_id', 'id');
    }
}
