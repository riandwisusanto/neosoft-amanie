<?php

namespace App\Models\Logistics;

use Illuminate\Database\Eloquent\Model;

class Bom extends Model
{
    /** @var array $fillable fillable fields. */
    protected $fillable = [
        'raw_item_id',
        'uom_id',
        'qty',
        'unit_value'
    ];

    /**
     * Raw item.
     *
     * @return Model
     **/
    public function rawItem()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'raw_item_id');
    }
}
