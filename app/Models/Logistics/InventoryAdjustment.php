<?php

namespace App\Models\Logistics;

use Illuminate\Database\Eloquent\Model;

class InventoryAdjustment extends Model
{
    /** @var array $fillable fillable fields */
    protected $fillable = [
        'item_id',
        'warehouse_id',
        'created_by',
        'adjusted_by',
        'transdate',
        'transdesc',
        'stock_before',
        'stock_after',
        'code',
        'stock_adjustment',
        'stock_adjustment_type',
        'ref_id',
        'reason'
    ];

    /**
     * Generate Code
     * 
     * @return string
     */
    public static function generateCode()
    {
        $now = now();
        $array = InventoryAdjustment::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('code')->first();

        $number = 0001;
       
        if ($array) {
            $number = substr($array->code, -4) + 1;
        }

        $code = "ADJ-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $code;
    }

    /**
     * Mutation Reference.
     *
     * @return Model
     **/
    public function ref()
    {
        return $this->belongsTo('App\Models\Logistics\Mutation', 'ref_id');
    }

    /**
     * Adjuster.
     *
     * @return Model
     **/
    public function adjuster()
    {
        return $this->belongsTo('App\Models\User', 'adjusted_by');
    }

    /**
     * Adjuster.
     *
     * @return Model
     **/
    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id');
    }

    /**
     * Adjuster.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id');
    }
}
