<?php

namespace App\Models\Logistics;

use Illuminate\Database\Eloquent\Model;

class InventoryTaking extends Model
{
    /** @var Type $var description */
    protected $fillable = ['warehouse_id', 'date', 'remarks'];
}
