<?php

namespace App\Models\Logistics;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /** @var array $fillable fillable fields. */
    protected $fillable = [
        'sales_information_id',
        'category_id',
        'uom_id',
        'name',
        'brand',
        'stock',
        'sellable_stock',
        'unit_value',
        'type',
        'cogs',
        'has_bom',
        'buffer_stock'
    ];

    /**
     * Unit of Measurement.
     *
     * @return Model
     **/
    public function uom()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_id', 'id');
    }

    /**
     * Mutation list.
     *
     * @return Model
     **/
    public function mutations()
    {
        return $this->hasMany('App\Models\Logistics\Mutation', 'item_id');
    }

    /**
     * Bill of materials.
     *
     * @return Model
     **/
    public function boms()
    {
        return $this->hasMany('App\Models\Logistics\Bom', 'item_id');
    }

    /**
     * Sales information.
     *
     * @return Model
     **/
    public function sales()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'sales_information_id', 'id_product_treatment');
    }
}
