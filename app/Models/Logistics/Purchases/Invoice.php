<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /** @var string table name */
    protected $table = 'purchase_invoices';

    /** @var array fillable fields */
    protected $fillable = [
        'supplier_id',
        'transdate',
        'total',
        'outstanding',
        'paidamount',
        'remarks'
    ];

    /**
     * Supplier.
     *
     * @return Model
     **/
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id', 'id_supplier');
    }

    /**
     * Invoice details.
     *
     * @return Model
     **/
    public function details()
    {
        return $this->hasMany('App\Models\Logistics\Purchases\InvoiceDetail', 'invoice_id');
    }
}
