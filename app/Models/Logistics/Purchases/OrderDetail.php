<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'order_id',
        'item_id',
        'qty',
        'price',
        'subtotal'
    ];

    /**
     * Header.
     *
     * @return Model
     **/
    public function order()
    {
        return $this->belongsTo('App\Models\Logistics\Purchases\Order', 'order_id');
    }

   /**
     * Item.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id');
    }
}
