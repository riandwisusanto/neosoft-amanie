<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /** @var string $table table name */
    protected $table = 'purchase_payments';

    /** @var array $fillable fillable fields */
    protected $fillable = [
        'supplier_id',
        'payment_method_id',
        'transdate',
        'remarks'
    ];

    /**
     * Supplier.
     *
     * @return Model
     **/
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id', 'id_supplier');
    }

    /**
     * Payment method.
     *
     * @return Model
     **/
    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\Bank', 'payment_method_id', 'bank_id');
    }

    /**
     * Payment details.
     *
     * @return Model
     **/
    public function details()
    {
        return $this->hasMany('App\Models\Logistics\Purchases\PaymentDetail', 'payment_id');
    }
}
