<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class PurchaseReturn extends Model
{
    /** @var string table name */
    protected $table = 'returns';

    /** @var array fillable fields */
    protected $fillable = [
        'supplier_id',
        'transdate',
        'remarks'
    ];

    /**
     * Supplier.
     *
     * @return Model
     **/
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id', 'id_supplier');
    }

    /**
     * Return details.
     *
     * @return Model
     **/
    public function details()
    {
        return $this->hasMany('App\Models\Logistics\Purchases\ReturnDetail', 'return_id');
    }
}
