<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class ReceiveDetail extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'receive_id',
        'order_detail_id',
        'item_id',
        'warehouse_id',
        'rack_id',
        'received_qty'
    ];

    /**
     * Item.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id');
    }

    /**
     * Order detail.
     *
     * @return Model
     **/
    public function orderDetail()
    {
        return $this->belongsTo('App\Models\Logistics\OrderDetail', 'order_detail_id');
    }

    /**
     * Receive (Header).
     *
     * @return Model
     **/
    public function receive()
    {
        return $this->belongsTo('App\Models\Logistics\Receive', 'receive_id');
    }

    /**
     * Warehouse.
     *
     * @return Model
     **/
    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id', 'id_warehouse');
    }

    /**
     * Rack.
     *
     * @return Model
     **/
    public function rack()
    {
        return $this->belongsTo('App\Models\Rack', 'rack_id', 'id_rack');
    }
}
