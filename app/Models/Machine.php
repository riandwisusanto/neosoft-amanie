<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    //
    protected $table = 'machines';
    protected $primaryKey = 'id_machine';

    protected $fillable = ['machine_name'];
}
