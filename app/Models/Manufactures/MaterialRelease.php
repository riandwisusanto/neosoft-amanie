<?php

namespace App\Models\Manufactures;

use Illuminate\Database\Eloquent\Model;

class MaterialRelease extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'code',
        'material_id',
        'released_by',
        'released_qty',
        'raw_item_id',
        'released_date',
        'created_by',
        'warehouse_origin_id',
    ];

    /**
     * Generate Code
     * 
     * @return string
     */
    public static function generateCode()
    {
        $now = now();
        $array = MaterialRelease::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('code')->first();

        $number = 0001;
       
        if ($array) {
            $number = substr($array->code, -4) + 1;
        }

        $code = "MR-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $code;
    }

    /**
     * Warehouse origin.
     *
     * @return Model
     **/
    public function warehouseOrigin()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_origin_id', 'id_warehouse');
    }

    /**
     * Raw item.
     *
     * @return Model
     **/
    public function rawItem()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'raw_item_id');
    }
    
    /**
     * Material.
     *
     * @return Model
     **/
    public function material()
    {
        return $this->belongsTo('App\Models\Manufactures\Material', 'material_id');
    }
    
    /**
     * Creator.
     *
     * @return Model
     **/
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }

    /**
     * Releaser.
     *
     * @return Model
     **/
    public function releaser()
    {
        return $this->belongsTo('App\Models\User', 'released_by');
    }
}
