<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketingOutlet extends Model
{
    protected $table = 'marketing_outlets';
    protected $primaryKey = 'id_MarketingOutlet';

    protected $fillable = ['outlet_id', 'marketing_id'];

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}
