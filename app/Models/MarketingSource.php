<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketingSource extends Model
{
    //
    protected $table = 'marketing_sources';
    protected $primaryKey = 'id_marketing_source';

    protected $fillable = ['marketing_source_name', 'status_marketing_source'];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'id_marketing_source', 'marketing_source_id');
    }

    public function marketing_outlets()
    {
        return $this->hasMany(MarketingOutlet::class, 'marketing_id');
    }
}
