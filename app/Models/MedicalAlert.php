<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicalAlert extends Model
{
    protected $table = 'medical_alerts';
    protected $primaryKey = 'id_medical';

    protected $fillable = ['customer_id', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'product_id', 'id_product_treatment');
    }
}
