<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    protected $table = 'outlets';
    protected $primaryKey = 'id_outlet';

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'outlet_id', 'id_outlet');
    }

    public function user_outlets()
    {
        return $this->hasMany('App\Models\UserOutlet', 'outlet_id', 'id_outlet');
    }
}
