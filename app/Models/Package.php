<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'packages';
    protected $primaryKey = 'id_package';

    public static function generatePackageCode()
    {
        $now = Carbon::now();
        $array = Package::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('package_code')->first();

        if ($array) {
            $number = substr($array->package_code, -4) + 1;
        } else {
            $number = 0001;
        }

        $idkcs = "PKG" . "-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $idkcs;
    }

    public function package_detail()
    {
        return $this->hasMany('App\Models\PackageDetail', 'package_id', 'id_package');
    }

    public function details()
    {
        return $this->hasMany('App\Models\PackageDetail', 'package_id', 'id_package');
    }

    public function package_outlet()
    {
        return $this->hasMany('App\Models\PackageOutlet', 'package_id', 'id_package');
    }

    public function package_point()
    {
        return $this->hasMany('App\Models\PackagePoint', 'package_id', 'id_package');
    }

    public function invoice_detail()
    {
        return $this->belongsTo('App\Models\InvoiceDetail', 'package_code', 'package_code');
    }

    public function price_logs()
    {
        return $this->hasMany(LogPricePackage::class, 'package_id')->orderBy('created_at', 'desc');
    }

    public function getPriceAttribute($price)
    {
        return format_money($price);
    }
}
