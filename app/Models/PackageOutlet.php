<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageOutlet extends Model
{
    protected $table = 'package_outlets';
    protected $primaryKey = 'id_package_outlet';

    protected $fillable = ['package_id', 'outlet_id'];
}
