<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackagePoint extends Model
{
    protected $table = 'package_points';
    protected $primaryKey = 'id_package_point';

    protected $fillable = ['package_id', 'point'];
}
