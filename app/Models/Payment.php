<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $primaryKey = 'id_payment';

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id', 'id_bank');
    }

    public function getAmountAttribute($price)
    {
        return format_money($price);
    }
}
