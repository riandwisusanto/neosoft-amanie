<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

class ProductLocation extends Model
{
    use Updater;

    protected $fillable = [
        'item_id',
        'description',
        'warehouse_id',
        'rack_id',
        'is_active',
        'created_uid',
        'updated_uid',
    ];

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function rack(){
        return $this->belongsTo(Rack::class, 'rack_id');
    }
}
