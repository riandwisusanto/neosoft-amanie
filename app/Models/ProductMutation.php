<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class ProductMutation extends Model
{
    use Updater;
    protected $fillable = [
        'item_id',
        'qty',
        'warehouse_begin',
        'warehouse_last',
        'rack_begin',
        'rack_last',
        'description',
        'is_active',
    ];

    public function rack_begin_data(){
        return $this->belongsTo(Rack::class, 'rack_begin');
    }

    public function rack_last_data(){
        return $this->belongsTo(Rack::class, 'rack_last');
    }

    public function warehouse_begin_data(){
        return $this->belongsTo(Warehouse::class, 'warehouse_begin');
    }

    public function warehouse_last_data(){
        return $this->belongsTo(Warehouse::class, 'warehouse_last');
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }
}
