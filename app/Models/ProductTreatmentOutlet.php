<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTreatmentOutlet extends Model
{
    protected $primaryKey = 'id_product_treatment_outlet';

    protected $fillable = ['outlet_id', 'product_id'];

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}
