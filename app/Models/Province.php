<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    protected $primaryKey = 'id_province';

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'id_province', 'city_id');
    }
}
