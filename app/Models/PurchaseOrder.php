<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

use Carbon\Carbon;

class PurchaseOrder extends Model
{
    use Updater;

    protected $fillable = [
        'purchase_order_num',
        'purchase_order_date',
        'supplier_id',
        'description',
        'terms',
        'pay_method',
        'due_date',
        'status',
        'created_by',
        'updated_by',
    ];

    public function getStatusDescAttribute(){
        $text = '';
        if($this->status === '1'){
            $text = 'Create/Draft PO';
        }else if($this->status === '2'){
            $text = 'Deliver PO';
        }else if($this->status === '3'){
            $text = 'Receive Product';
        }else if($this->status === '4'){
            $text = 'Canceled';
        }else{
            $text = 'Closed PO';
        }
        return $text;
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function purchase_order_details(){
        return $this->hasMany(PurchaseOrderDetail::class);
    }

    public static function generateCode(){
        $now    = Carbon::now();
        $prefix = 'PO-'.$now->isoFormat('YYMM');
        $num    = 0;
        $data   = self::select('purchase_order_num')
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->orderBy('purchase_order_num', 'desc')
            ->first();

        if($data) $num = (int) substr($data->purchase_order_num, strlen($prefix), 4);

        $num++;
        return $prefix.str_pad($num, 4, '0', STR_PAD_LEFT);
    }

    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            $model->purchase_order_num = self::generateCode();
        });

        static::deleting(function ($model) {
            $model->purchase_order_details()->delete();
        });
    }
}
