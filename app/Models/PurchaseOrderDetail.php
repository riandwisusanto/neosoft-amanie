<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class PurchaseOrderDetail extends Model
{
    use Updater;

    protected $fillable = [
        'purchase_order_id',
        'item_id',
        'uom_id',
        'description',
        'qty',
        'price',
        'disc',
        'tax',
        'total',
    ];

    public function purchase_order(){
        return $this->belongsTo(PurchaseOrder::class, 'purchase_order_num');
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }
}
