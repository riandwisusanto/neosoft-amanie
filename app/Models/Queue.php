<?php
 
namespace App\Models;
 
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
 
class Queue extends Model
{
   protected $table = 'queues';
   protected $primaryKey = 'id_queue';
 
   public static function generateQueueNumber()
   {
 
       $now = Carbon::now();
       $array = Queue::whereDay('created_at', $now->day)
           ->whereMonth('created_at', $now->month)
           ->latest('queue_number')->first();
 
       if ($array) {
           $number = substr($array->queue_number, -4) + 1;
       } else {
           $number = 01;
       }
 
       $idkcs = sprintf('%04d', $number);
       return $idkcs;
 
   }
 
   public function customer()
   {
       return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
   }
}
 
 

