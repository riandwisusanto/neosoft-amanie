<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueueScreen extends Model
{
    protected $table = 'queue_screens';
    protected $primaryKey = 'id_queue_screen';
    protected $fillable = ['queue_id','therapist_id'];


     
   public function queue()
   {
       return $this->belongsTo('App\Models\Queue', 'queue_id', 'id_queue');
   }
   public function therapist()
   {
       return $this->belongsTo('App\Models\Therapist', 'therapist_id', 'id_therapist');
   }

}
