<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class Rack extends Model
{
    use Updater;

    protected $primaryKey = 'id_rack';
    protected $fillable = [
        'main_rack_code',
        'sub_rack_code',
        'description',
        'warehouse_id',
        'is_active',
        'created_by',
        'updated_by',
    ];

    public function receive_detail(){
        return $this->hasMany('App\Models\Logistics\Purchases\ReceiveDetail', 'rack_id');
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }
}
