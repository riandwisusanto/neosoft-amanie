<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

use Carbon\Carbon;

class Receive extends Model
{
    use Updater;

    protected $fillable = [
        'purchase_order_id',
        'receive_date',
        'receive_code',
        'receive_by',
        'supplier_id',
        'invoice_num',
        'description',
        'warehouse_id',
        'status',
        'created_uid',
        'updated_uid',
    ];

    public function getStatusDescAttribute(){
        $text = '';
        if($this->status === '1'){
            $text = 'Fulfilled';
        }else if($this->status === '2'){
            $text = 'Unfulfilled';
        }else if($this->status === '3'){
            $text = 'Canceled';
        }else{
            $text = 'Closed';
        }
        return $text;
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function purchase_order(){
        return $this->belongsTo(PurchaseOrder::class);
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function receive_details(){
        return $this->hasMany(ReceiveDetail::class);
    }

    public static function generateCode(){
        $now    = Carbon::now();
        $prefix = 'RCV-'.$now->isoFormat('YYMM');
        $num    = 0;
        $data   = self::select('receive_code')
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->orderBy('receive_code', 'desc')
            ->first();

        if($data) $num = (int) substr($data->receive_code, strlen($prefix), 4);

        $num++;
        return $prefix.str_pad($num, 4, '0', STR_PAD_LEFT);
    }

    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            $model->receive_code = self::generateCode();
        });

        static::deleting(function ($model) {
            $model->receive_details()->delete();
        });
    }
}
