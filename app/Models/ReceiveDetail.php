<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class ReceiveDetail extends Model
{
    use Updater;

    protected $fillable = [
        'receive_id',
        'uom_id',
        'item_id',
        'rack_id',
        'batch_no',
        'expire_date',
        'qty',
        'price',
        'total',
    ];

    public function rcv(){
        return $this->belongsTo(Rcv::class);
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function rack(){
        return $this->belongsTo(Rack::class, 'rack_id');
    }
}
