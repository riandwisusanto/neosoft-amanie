<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

class ReturDetail extends Model
{
    use Updater;

    protected $fillable = [
        'retur_id',
        'item_id',
        'uom_id',
        'description',
        'qty',
        'price',
        'disc',
        'tax',
        'total',
        'created_uid',
        'updated_uid'
    ];

    public function retur(){
        return $this->hasMany(Retur::class);
    }
}
