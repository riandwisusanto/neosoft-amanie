<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class Service extends Model
{
    use Updater;

    protected $fillable = [
        'name',
        'product_treatment_code',
        'description',
    ];

    public function service_details(){
        return $this->hasMany(ServiceDetail::class);
    }

    public function product_treatment(){
        return $this->belongsTo(ProductTreatment::class, 'product_treatment_code', 'product_treatment_code');
    }

    protected static function boot(){
        parent::boot();
        static::deleting(function($m){
            $m->service_details()->delete();
        });
    }
}
