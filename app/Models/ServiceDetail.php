<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class ServiceDetail extends Model
{
    use Updater;

    protected $fillable = [
        'service_id',
        'item_id',
        'uom_id',
        'expired_date',
        'qty',
        'price',
        'created_uid',
        'update_uid',
    ];

    public function service(){
        return $this->belongsTo(Service::class);
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }
}
