<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Supplier extends Model
{
    protected $primaryKey = 'id_supplier';
    protected $fillable = [
        'code',
        'name',
        'addr',
        'city_id',
        'post_code',
        'is_active',
    ];

    public function province(){
        return $this->belongsTo(Province::class, 'city_id');
    }

    public function purchase_orders(){
        return $this->hasMany('App\Models\Logistics\Purchases\Order', 'supplier_id');
    }

    public function receives(){
        return $this->hasMany(Receive::class, 'supplier_id');
    }

    public static function generateCode(){
        $now    = Carbon::now();
        $prefix = 'S-'.$now->isoFormat('YYMM');
        $num    = 0;
        $data   = self::select('code')
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->orderBy('code', 'desc')
            ->first();

        if($data) $num = (int) substr($data->code, strlen($prefix), 4);

        $num++;
        return $prefix.str_pad($num, 4, '0', STR_PAD_LEFT);
    }

    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            $model->code = self::generateCode();
        });
    }
}
