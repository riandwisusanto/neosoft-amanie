<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TherapistLeave extends Model
{
    protected $table = 'therapist_leaves';
    protected $primaryKey = 'id_therapist_leave';
    protected $fillable = [
        'therapist_id',
        'day',
        'start_date',
        'end_date',
        'description',
    ];

    public function therapist()
    {
        return $this->belongsTo('App\Models\Therapist', 'therapist_id', 'id_therapist');
    }

    public function getStartDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getEndDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}
