<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    // protected $primaryKey = 'id_uom';
    protected $fillable = [
        'name',
        'is_active',
    ];

    public function purchase_order_details(){
        return $this->hasMany(PurchaseOrderDetail::class, 'uom_id');
    }

    public function service_details(){
        return $this->hasMany(ServiceDetail::class, 'uom_id');
    }

    public function retur_details(){
        return $this->hasMany(ReturDetail::class, 'uom_id');
    }

    public function bom_details(){
        return $this->hasMany(BomDetail::class, 'uom_id');
    }

    public function work_in_process_details(){
        return $this->hasMany(WorkInProcessDetail::class, 'uom_id');
    }

    public function finish_goods(){
        return $this->hasMany(FinishGood::class, 'uom_id');
    }
}
