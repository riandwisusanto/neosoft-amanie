<?php
namespace App\Models;

use Carbon\carbon;
use Illuminate\Database\Eloquent\Model;

class Usage extends Model
{
    protected $table = 'usages';
    protected $primaryKey = 'id_usage';

    // public function setUsageDateAttribute($date){
    //     return Carbon::parse($date)->format('Y-m-d');
    // }

    public static function generateUsageCode($outlet_id)
    {
        $now = Carbon::now();
        $outlet = Outlet::find($outlet_id);
        $array = Usage::where('outlet_id', $outlet_id)
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('usage_code')->first();

        if ($array) {
            $number = substr($array->usage_code, -4) + 1;
        } else {
            $number = 0001;
        }
        $idinvcode = $outlet->outlet_code . "-U-" . $now->isoFormat('YYYY') . $now->isoFormat('MM') . "-" . sprintf('%04d', $number);
        return $idinvcode;
    }

    public function outlet()
    {
        return $this->belongsTo('App\Models\Outlet', 'outlet_id', 'id_outlet');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }

    public function usage_detail()
    {
        return $this->hasMany('App\Models\UsageDetail', 'usage_id', 'id_usage');
    }

    public function getUsageDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}
