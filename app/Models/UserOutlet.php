<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOutlet extends Model
{
    protected $table = 'user_outlets';
    protected $primaryKey = 'id_outlet';

    protected $fillable = ['outlet_id', 'user_id'];
}
