<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VItemStock extends Model
{
    protected $table = 'v_item_stock';
}
