<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WarehouseOutlet extends Model
{
    /** @var Type $var description */
    protected $primaryKey = 'id_warehouse_outlet';
    
    /** @var Type $var description */
    protected $fillable = ['warehouse_id', 'outlet_id'];
}
