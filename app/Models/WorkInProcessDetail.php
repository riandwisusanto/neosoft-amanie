<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

class WorkInProcessDetail extends Model
{
    use Updater;

    protected $fillable = [
        'work_in_process_id',
        'item_id',
        'uom_id',
        'bom_qty',
        'real_qty',
        'price',
        'total',
        'created_uid',
        'updated_uid',
    ];

    public function work_in_process(){
        return $this->belongsTo(WorkInProcess::class);
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }
}
