<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class WorkOrder extends Model
{
    use Updater;

    protected $fillable = [
        'wo_date',
        'bom_id',
        'item_id',
        'description',
        'start_date',
        'due_date',
        'uom_id',
        'order_qty',
        'is_active',
        'created_uid',
        'updated_uid',
    ];

    public function bom(){
        return $this->belongsTo(Bom::class);
    }

    public function item(){
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }
}
