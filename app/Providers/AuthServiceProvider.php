<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin', function ($user) {
            return count(array_intersect(["ADMINISTRATOR", "SUPER_USER"], json_decode($user->level)));
        });

        Gate::define('all', function ($user) {
            return count(array_intersect(["FRONTDESK", "FINANCE", "DOKTER", "BRANCH_MANAGER", "CASHIER", "ADMINISTRATOR"], json_decode($user->level)));
        });

        Gate::define('customer', function ($user) {
            return count(array_intersect(["FRONTDESK", "FINANCE", "BRANCH_MANAGER", "DOKTER", "CASHIER", "ADMINISTRATOR"], json_decode($user->level)));
        });

        Gate::define('sales', function ($user) {
            return count(array_intersect(["CASHIER", "FRONTDESK", "FINANCE", "ADMINISTRATOR", "BRANCH_MANAGER"], json_decode($user->level)));
        });

        Gate::define('appointment', function ($user) {
            return count(array_intersect(["FRONTDESK", "DOKTER", "ADMINISTRATOR", "BRANCH_MANAGER", "FINANCE", "OUTLET_SUPERVISOR"], json_decode($user->level)));
        });
        Gate::define('queue', function ($user) {
            return count(array_intersect(["FRONTDESK", "DOKTER", "ADMINISTRATOR", "BRANCH_MANAGER"], json_decode($user->level)));
        });
        Gate::define('doctor', function ($user) {
            return count(array_intersect(["FRONTDESK", "DOKTER", "ADMINISTRATOR", "BRANCH_MANAGER"], json_decode($user->level)));
        });
    }
}
