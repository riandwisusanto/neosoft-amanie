<?php
namespace App\Services;
use GuzzleHttp\Client as Http;
use GuzzleHttp\Psr7;
use Illuminate\Support\Facades\Log;
use stdClass;

class StarSender {
    private $client;

    public function __construct() {
        $this->client = new Http([
            'base_uri'=>'https://starsender.online',
            'headers' => [
                'apikey'=>env('WA_STARSENDER_TOKEN')
            ]
        ]);
    }

    private function endpoint($endpoint, array $params) {
        $encodedParams = array_map(function($e) {
            return rawurlencode($e);
        },$params);
        return vsprintf($endpoint, $encodedParams);        
    }

    private static function res($res) {
        $res = Self::parseRes($res);
        $obj = new stdClass;
        $obj->status = $res->status;
        $obj->id = $res->data->message_id ?? null;
        return $obj;
    }

    private static function parseRes($res) {
        return json_decode((string) $res->getBody());
    }

    private function getMessage($id) {
        $endpoint = 'api/getMessage';
        return Self::parseRes($this->client->post($endpoint, ['json' => ['message_id' => $id]]));
    }


    public function sendText($to, $message) {
        $endpoint = 'api/sendText?message=%s&tujuan=%s@s.whatsapp.net';
        return Self::res($this->client->post($this->endpoint($endpoint, [$message, $to])));
    }

    public function sendTextImage($to, $file, $message) {
        $endpoint = 'api/sendFilesUpload?message=%s&tujuan=%s@s.whatsapp.net';
        $data = [
            'multipart' => [
                [
                    'name'     => 'file',
                    'contents' => Psr7\Utils::tryFopen($file, 'r'),
                ],
            ]
        ];
        return Self::res($this->client->post( $this->endpoint($endpoint, [$message, $to]), $data ));
    }

}

