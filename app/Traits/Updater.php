<?php
namespace App\Traits;
use Auth;

trait Updater{
    protected static function boot(){
        parent::boot();
        static::creating(function ($model) {
            $name = Auth::user() ? Auth::user()->name : '';
        
            $model->created_uid = $name;
        });
    
        static::updating(function ($model) {
            $name = Auth::user() ? Auth::user()->name : '';
        
            $model->updated_uid = $name;
        });
    }
}
