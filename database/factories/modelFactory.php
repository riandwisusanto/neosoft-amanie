<?php
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Therapist::class, function (Faker\Generator $faker) {
    return [
        'therapist_name' => $faker->name,
        'group_id' => 1,
        'join_date' => Carbon::parse('2017-10-30'),
        'status_therapist' => 1
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Room::class, function (Faker\Generator $faker) {
    return [
        'room_name' => 'ESC-'.str_random(5),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\ProductTreatment::class, function (Faker\Generator $faker) {
    return [
        'product_treatment_code' => 'T-'.str_random(10),
        'product_treatment_name' => str_random(5).'PERAWAT',
        'price' => mt_rand(100000, 999999),
        'duration' => mt_rand(100, 300),
        'status_product_treatment' => 1,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'induk_customer' => 'EM-'.mt_rand(10000, 99999),
        'full_name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'birth_place' => str_random(10),
        'birth_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'join_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'gender' => 'F',
        'ktp' => mt_rand(100000000000, 999999999999),
        'address' => $faker->address,
        'occupation' => str_random(7),
        'email' => $faker->unique()->email,
        'staf_id' => 1,
        'religion' => mt_rand(1, 5),
        'country_id' => mt_rand(1, 2),
        'city_id' => mt_rand(1, 35),
        'zip' => 74114,
        'status_customer' => 1,
        'nextofikin' => $faker->name,
        'nextofikin_number' => $faker->phoneNumber,
        'source_id' => 1,
        'source_remarks' => 'nothing',
    ];
});
