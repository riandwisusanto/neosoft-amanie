<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_appointments', function (Blueprint $table) {
            $table->bigIncrements('id_log_appointment');
            $table->integer('appointment_id');
            $table->integer('user_id');
            $table->integer('log_status');
            $table->timestamp('date_time');
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_appointments');
    }
}
