<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use phpDocumentor\Reflection\Types\Boolean;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id_invoice');
            $table->string('inv_code')->unique();
            $table->date('inv_date');
            $table->integer('customer_id');
            $table->integer('outlet_id');
            $table->integer('marketing_source_id');
            $table->integer('consultant_id');
            $table->integer('therapist_id');
            $table->integer('staff_id');
            $table->integer('agent_id');
            $table->date('expired_date');
            $table->bigInteger('total');
            $table->string('customer_sign')->nullable();
            $table->bigInteger('remaining');
            $table->text('remarks')->nullable();
            $table->text('inv_remarks')->nullable();
            $table->boolean('status_inv')->default(0);
            $table->boolean('void_invoice')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
