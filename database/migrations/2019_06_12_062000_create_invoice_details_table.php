<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_details', function (Blueprint $table) {
            $table->bigIncrements('id_inv_detail');
            $table->integer('inv_id')->unsigned();
            $table->unsignedBigInteger('anamnesa_detail_id')->nullable();
            $table->string('package_code', 100);
            $table->bigInteger('current_price');
            $table->smallInteger('qty');
            $table->float('discount_1', 10, 2);
            $table->float('discount_2', 10, 2)->nullable();
            $table->integer('activity_id');
            $table->bigInteger('subtotal');
            $table->boolean('type_line')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_details');
    }
}
