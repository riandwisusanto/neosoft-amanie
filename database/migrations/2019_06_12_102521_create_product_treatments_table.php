<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTreatmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_treatments', function (Blueprint $table) {
            $table->bigIncrements('id_product_treatment');
            $table->string('product_treatment_code')->unique();
            $table->string('product_treatment_name');
            $table->integer('price');
            $table->integer('duration');
            $table->boolean('status_product_treatment')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_treatments');
    }
}
