<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_packages', function (Blueprint $table) {
            $table->bigIncrements('id_invoice_package');
            $table->unsignedBigInteger('anamnesa_detail_id')->nullable();
            $table->integer('inv_detail_id');
            $table->integer('product_id');
            $table->integer('qty');
            $table->integer('used');
            $table->integer('nominal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_packages');
    }
}
