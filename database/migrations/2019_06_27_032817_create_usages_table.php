<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usages', function (Blueprint $table) {
            $table->bigIncrements('id_usage');
            $table->string('usage_code')->unique();
            $table->date('usage_date');
            $table->integer('customer_id');
            $table->integer('outlet_id');
            $table->integer('staff_id');
            $table->string('file')->nullable();
            $table->boolean('status_usage');
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usages');
    }
}
