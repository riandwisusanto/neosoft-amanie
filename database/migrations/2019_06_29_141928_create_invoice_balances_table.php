<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_balances', function (Blueprint $table) {
            $table->bigIncrements('id_balance');
            $table->string('balance_code')->unique();
            $table->date('balance_date');
            $table->integer('customer_id');
            $table->integer('invoice_id');
            $table->integer('outlet_id');
            $table->integer('outstanding');
            $table->integer('staff_id');
            $table->boolean('status_balance');
            $table->text('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_balances');
    }
}
