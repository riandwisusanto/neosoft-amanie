<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_points', function (Blueprint $table) {
            $table->bigIncrements('id_invoice_point');
            $table->string('inv_point_code');
            $table->integer('customer_id');
            $table->integer('outlet_id');
            $table->integer('staff_id')->nullable();
            $table->bigInteger('total');
            $table->text('remarks')->nullable();
            $table->boolean('status_inv_point')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_points');
    }
}
