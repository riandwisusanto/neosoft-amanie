<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicePointDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_point_details', function (Blueprint $table) {
            $table->bigIncrements('id_invoice_point_detail');
            $table->integer('invoice_point_id')->unsigned();
            $table->integer('product_point_id')->unsigned();
            $table->bigInteger('current_point');
            $table->smallInteger('qty');
            $table->bigInteger('subtotal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_point_details');
    }
}
