<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_order_id');
            $table->integer('item_id');
            $table->integer('uom_id');
            $table->string('description', 45);
            $table->integer('qty');
            $table->integer('price');
            $table->integer('disc');
            $table->integer('tax');
            $table->integer('total');
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_details');
    }
}
