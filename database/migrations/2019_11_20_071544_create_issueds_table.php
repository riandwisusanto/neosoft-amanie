<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issueds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('warehouse_id');
            $table->integer('customer_id');
            $table->string('issued_num', 15);
            $table->date('issued_date');
            $table->text('description');
            $table->string('invoice_no', 45);
            $table->date('invoice_date');
            $table->string('pay_method', 45);
            $table->date('due_date');
            $table->integer('status')->comment("1: Create/Draft Retur, 2: Deliver Product, 3 : Receive Product by Customer, 4: Canceled, 5: Closed");
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issueds');
    }
}
