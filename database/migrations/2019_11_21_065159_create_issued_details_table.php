<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuedDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issued_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('issued_id');
            $table->integer('item_id');
            $table->integer('uom_id');
            $table->text('description');
            $table->integer('qty');
            $table->integer('price');
            $table->integer('disc');
            $table->integer('tax');
            $table->integer('total');
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_details');
    }
}
