<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkInProcessDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_in_process_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('work_in_process_id');
            $table->integer('item_id');
            $table->integer('uom_id');
            $table->integer('bom_qty');
            $table->integer('real_qty');
            $table->integer('price');
            $table->integer('total');
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_in_process_details');
    }
}
