<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinishGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finish_goods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('work_in_process_id');
            $table->integer('item_id');
            $table->integer('uom_id');
            $table->text('description');
            $table->date('mfg_date');
            $table->date('expired_date');
            $table->integer('qty');
            $table->integer('price');
            $table->string('status', 1)->default(1);
            $table->string('is_active', 1)->default(1);
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finish_goods');
    }
}
