<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnamnesaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anamnesa_details', function (Blueprint $table) {
            $table->bigIncrements('id_anamnesa_detail');
            $table->integer('anamnesa_id');
            $table->integer('product_id');
            $table->double('adjustment', 11, 2)->nullable();
            $table->integer('scenario_id')->default(1);
            $table->integer('price');
            $table->bigInteger('qty');
            $table->bigInteger('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anamnesa_details');
    }
}
