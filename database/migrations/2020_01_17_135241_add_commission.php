<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_treatments', function (Blueprint $table) {
            $table->float('commissionDr', 10, 2)->after('status_product_treatment')->nullable();
            $table->float('commissionTr', 10, 2)->after('status_product_treatment')->nullable();
            $table->boolean('commtype')->after('commissionDr')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_treatments', function (Blueprint $table) {
            $table->dropColumns('commissionDr');
            $table->dropColumns('commissionTr');
            $table->dropColumns('commtype');

        });
    }
}
