<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMallProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mall_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('weight_uom_id');
            // $table->integer('price')->default(0);
            $table->text('description');
            $table->tinyInteger('display_on_mall')->default(1);
            $table->double('weight')->default(1);
            $table->text('informations');
            $table->double('discount_1', 13, 2)->default(0);
            $table->double('discount_2', 13, 2)->default(0);
            $table->enum('discount_type', ['nominal', 'percent']);
            $table->double('use_dicount_gimmick')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
