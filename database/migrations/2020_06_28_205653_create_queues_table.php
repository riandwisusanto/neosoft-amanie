<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queues', function (Blueprint $table) {
            $table->bigIncrements('id_queue');
            $table->BigInteger('queue_number');
            $table->Integer('customer_id');
            $table->Integer('therapist_id');
            $table->Integer('outlet_id');
            $table->date('come_date');
            $table->boolean('status_queue')->default(0);
            $table->boolean('status_payment')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queues');
    }
}
