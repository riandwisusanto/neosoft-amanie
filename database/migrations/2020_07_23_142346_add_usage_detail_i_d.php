<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsageDetailID extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dr_img', function (Blueprint $table) {
            $table->BigInteger('usage_detail_id')->after('customer_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dr_img', function (Blueprint $table) {
            $table->dropColumns('usage_detail_id');
            
        });
    }
}
