<?php

use Illuminate\Database\Seeder;

class Activity extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activities')->insert(
        array(
        array('id_activity' => '3','activity_code' => 'ACT-001' ,'activity_name' => 'No Activities','activity_group_id' => '1','activity_pic' => 'Eva','start_date' => '2019-07-01','end_date' => '2029-07-31','remarks' => '','discount' => '0', 'disc_type' => '1', 'status_activity' => '1')
      )
    );
    }
}
