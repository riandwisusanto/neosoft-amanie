<?php

use Illuminate\Database\Seeder;

class Bank extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert(
            array(
                array('id_bank' => '1','bank_code' => 'CASH','bank_name' => 'Cash','bank_charge' => '0','status_bank' => '1'),
                array('id_bank' => '3','bank_code' => 'BCA_CC','bank_name' => 'BCA Credit Card','bank_charge' => '2.5','status_bank' => '1'),
                array('id_bank' => '11','bank_code' => 'CICIL-BCA','bank_name' => 'Cicilan BCA','bank_charge' => '0','status_bank' => '1'),
                array('id_bank' => '12','bank_code' => 'EDC-MDR','bank_name' => 'EDC-Mandiri','bank_charge' => '0','status_bank' => '1'),
                array('id_bank' => '13','bank_code' => 'EDC-BCA','bank_name' => 'EDC-BCA','bank_charge' => '0','status_bank' => '1'),
                array('id_bank' => '14','bank_code' => 'CICIL-MDR','bank_name' => 'Cicilan Mandiri','bank_charge' => '0','status_bank' => '1'),
                array('id_bank' => '15','bank_code' => 'EDC-CIMB','bank_name' => 'EDC-CIMB Niaga','bank_charge' => '0','status_bank' => '1'),
                array('id_bank' => '16','bank_code' => 'MDR_CC','bank_name' => 'MANDIRI Credit Card','bank_charge' => '2.5','status_bank' => '1'),
                array('id_bank' => '17','bank_code' => 'BSP','bank_name' => 'BrideStory Pay','bank_charge' => '0','status_bank' => '1'),
                array('id_bank' => '23','bank_code' => 'TRANSFER','bank_name' => 'Transfer','bank_charge' => '0','status_bank' => '1'),
                array('id_bank' => '22','bank_code' => 'CICIL-CIMB','bank_name' => 'Cicilan CIMB Niaga','bank_charge' => '0','status_bank' => '1')
            )
        );
    }
}
