<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'id' => 1, 'name' => 'Food', 'is_active' => 1],
            [ 'id' => 2, 'name' => 'Clothes', 'is_active' => 1],
        ];
        DB::table('categories')->insert($data);
    }
}
