<?php

use Illuminate\Database\Seeder;

class Consultant extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('consultants')->insert(
            array(
                array('id_consultant' => '1','join_date' => '2019-05-23','consultant_name' => 'syifa','status_consultant' => '1'),
                array('id_consultant' => '2','join_date' => '2019-06-27','consultant_name' => 'Rofiqoh','status_consultant' => '1'),
                array('id_consultant' => '3','join_date' => '2019-07-01','consultant_name' => 'Nisa','status_consultant' => '1')
            )
        );
    }
}
