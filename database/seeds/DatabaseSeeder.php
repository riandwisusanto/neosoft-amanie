<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(DrReview::class);
        // $this->call(DrReviewDetail::class);
        // $this->call(Activity::class);
        // $this->call(ProductTreatment::class);
        // $this->call(Package::class);
        // $this->call(PackageDetail::class);
        // $this->call(PackageOutlet::class);
        // $this->call(PackagePoint::class);
        $this->call(Country::class);
        $this->call(ProvinceIndonesia::class);
        $this->call(ProvinceSingapura::class);
        $this->call(Outlet::class);
        // $this->call(Consultant::class);
        // $this->call(MarketingSource::class);
        // $this->call(ActivityGroup::class);
        // $this->call(Therapist::class);
        // $this->call(ConsultantDetailTableSeeder::class);
        // $this->call(GroupTableSeeder::class);
        // $this->call(Room::class);
        //$this->call(Customer::class);
        $this->call(Rack::class);
        $this->call(Warehouse::class);
        $this->call(WarehouseOutletSeeder::class);
        $this->call(Bank::class);
        $this->call(UnitOfMaterialTableSeeder::class);
        $this->call(User::class);
        $this->call(UserOutletSeeder::class);
        $this->call(Rank::class);
    }
}
