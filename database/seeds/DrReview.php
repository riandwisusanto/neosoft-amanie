<?php

use Illuminate\Database\Seeder;

class DrReview extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dr_img')->insert(
            array(
            array('id_img' => '26','customer_id' => '11','tgl_in' => '2019-06-25','ket' => 'Before','id' => '158','remaks' => 'COBA','folder' => 'img/IMG--452062019-06-25'),
            array('id_img' => '27','customer_id' => '11','tgl_in' => '2019-06-25','ket' => 'After','id' => '158','remaks' => 'Uji upload','folder' => 'img/IMG-26-555642019-06-25'),
            array('id_img' => '31','customer_id' => '12','tgl_in' => '2019-06-27','ket' => 'Before','id' => '163','remaks' => 'Nisa','folder' => 'img/IMG-27-189292019-06-27'),
            array('id_img' => '38','customer_id' => '96','tgl_in' => '2019-07-02','ket' => 'After','id' => '158','remaks' => 'Injeksi acne','folder' => 'img/IMG-37-672472019-07-02'),
            array('id_img' => '40','customer_id' => '96','tgl_in' => '2019-07-02','ket' => 'After','id' => '158','remaks' => 'Injeksi Acne','folder' => 'img/IMG-38-142162019-07-02'),
            array('id_img' => '41','customer_id' => '96','tgl_in' => '2019-07-02','ket' => 'After','id' => '158','remaks' => 'Injeksi Acne','folder' => 'img/IMG-40-454742019-07-02'),
            array('id_img' => '42','customer_id' => '797','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Day cream : pagi n-gel
          Sunscreen spf 45
          Krim mlm : diamond glow plus','folder' => 'img/IMG-41-617002019-07-08'),
            array('id_img' => '43','customer_id' => '691','tgl_in' => '2019-07-08','ket' => 'Before','id' => '0','remaks' => 'Before Laser','folder' => 'img/IMG-42-94242019-07-08'),
            array('id_img' => '44','customer_id' => '691','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Laser ','folder' => 'img/IMG-43-353752019-07-08'),
            array('id_img' => '45','customer_id' => '213','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Botox 8 unit','folder' => 'img/IMG-44-502182019-07-08'),
            array('id_img' => '46','customer_id' => '285','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Peeling Obagi','folder' => 'img/IMG-45-933782019-07-08'),
            array('id_img' => '47','customer_id' => '96','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Injeksi acne','folder' => 'img/IMG-46-712312019-07-08'),
            array('id_img' => '48','customer_id' => '791','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Dermapen Hair Growth & PRP','folder' => 'img/IMG-47-253322019-07-08'),
            array('id_img' => '49','customer_id' => '443','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Hifu 15 shot','folder' => 'img/IMG-48-632642019-07-08'),
            array('id_img' => '50','customer_id' => '239','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Peeling Bokong , Microdermabrasi , Laser Bokong , & Laser Wajah','folder' => 'img/IMG-49-974362019-07-08'),
            array('id_img' => '51','customer_id' => '790','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Peeling , Injeksi Acne , & Laser','folder' => 'img/IMG-50-136842019-07-08'),
            array('id_img' => '52','customer_id' => '614','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before LipoLaser & Laser Strech mark','folder' => 'img/IMG-51-273882019-07-08'),
            array('id_img' => '53','customer_id' => '458','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Dermapen','folder' => 'img/IMG-52-447432019-07-08'),
            array('id_img' => '54','customer_id' => '796','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Hifu','folder' => 'img/IMG-53-486942019-07-08'),
            array('id_img' => '55','customer_id' => '411','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Laser & Injeksi Acne','folder' => 'img/IMG-54-572602019-07-08'),
            array('id_img' => '56','customer_id' => '213','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before ','folder' => 'img/IMG-55-736972019-07-08'),
            array('id_img' => '57','customer_id' => '0','tgl_in' => '2019-07-08','ket' => 'Before','id' => '158','remaks' => 'Before Dermapen','folder' => 'img/IMG-56-273022019-07-08'),
            array('id_img' => '58','customer_id' => '567','tgl_in' => '2019-07-10','ket' => 'Before','id' => '161','remaks' => 'Before Peeling acne Scar & Laser ','folder' => 'img/IMG-57-496362019-07-10'),
            array('id_img' => '59','customer_id' => '800','tgl_in' => '2019-07-10','ket' => 'Before','id' => '161','remaks' => 'Before Ekstraksi A + Injeksi anti acne & Laser rajuve','folder' => 'img/IMG-58-954432019-07-10'),
            array('id_img' => '60','customer_id' => '258','tgl_in' => '2019-07-10','ket' => 'Before','id' => '161','remaks' => 'Before Peeling acne plus & Laser rejuve','folder' => 'img/IMG-59-807542019-07-10'),
            array('id_img' => '62','customer_id' => '614','tgl_in' => '2019-07-12','ket' => 'Before','id' => '161','remaks' => 'Before dermapen dan laser ','folder' => 'img/IMG-60-74022019-07-12'),
            array('id_img' => '63','customer_id' => '801','tgl_in' => '2019-07-12','ket' => 'Before','id' => '161','remaks' => 'Before Laser','folder' => 'img/IMG-62-882252019-07-12'),
            array('id_img' => '64','customer_id' => '802','tgl_in' => '2019-07-12','ket' => 'Before','id' => '161','remaks' => 'Before Laser, peeling acne plus , Hifu ','folder' => 'img/IMG-63-499212019-07-12'),
            array('id_img' => '65','customer_id' => '793','tgl_in' => '2019-07-12','ket' => 'Before','id' => '161','remaks' => 'Before peeling acne plus','folder' => 'img/IMG-64-702492019-07-12'),
            array('id_img' => '67','customer_id' => '534','tgl_in' => '2019-07-12','ket' => 'Before','id' => '161','remaks' => 'Before hifu 9 shot','folder' => 'img/IMG-65-756232019-07-12'),
            array('id_img' => '68','customer_id' => '795','tgl_in' => '2019-07-12','ket' => 'Before','id' => '161','remaks' => 'Before dermapen','folder' => 'img/IMG-67-859872019-07-12'),
            array('id_img' => '69','customer_id' => '80','tgl_in' => '2019-07-12','ket' => 'Before','id' => '161','remaks' => 'Before laser surgery A & Laser rejuve','folder' => 'img/IMG-68-242452019-07-12'),
            array('id_img' => '72','customer_id' => '797','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Before botox 50unit','folder' => 'img/IMG-69-272522019-07-15'),
            array('id_img' => '74','customer_id' => '798','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Before Laser Rejuve','folder' => 'img/IMG-73-338322019-07-15'),
            array('id_img' => '75','customer_id' => '799','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Before Laser Surgery A','folder' => 'img/IMG-74-607902019-07-15'),
            array('id_img' => '76','customer_id' => '149','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Before Laser rejuve , peeling acne plus , Injeksi meladerm , botox 6unit , serum glowing','folder' => 'img/IMG-75-950292019-07-15'),
            array('id_img' => '77','customer_id' => '225','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Before Laser','folder' => 'img/IMG-76-347442019-07-15'),
            array('id_img' => '78','customer_id' => '155','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Peeling A , Laser Rejuve','folder' => 'img/IMG-77-498062019-07-15'),
            array('id_img' => '79','customer_id' => '119','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Dermapen stretchmark , Laser Wajah & Leher , Hifu 100shot','folder' => 'img/IMG-78-958592019-07-15'),
            array('id_img' => '80','customer_id' => '189','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Laser Rejuve , SkinBooster 1cc, Hifu 50shot, Botox 12unit','folder' => 'img/IMG-79-689972019-07-15'),
            array('id_img' => '81','customer_id' => '120','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Laser Rejuve, Ekstraksi A , Injeksi Anti Acne','folder' => 'img/IMG-80-308062019-07-15'),
            array('id_img' => '82','customer_id' => '222','tgl_in' => '2019-07-15','ket' => 'Before','id' => '0','remaks' => 'A/ uneven skin tone, atopy skin
          P/ Laser ndyag 8-6/ 2-2,5
               Aft tr C D
               Home tr: mometasone cr utk insect bite hyp
                                   N gel utk maintenance','folder' => 'img/IMG-81-161622019-07-15'),
            array('id_img' => '83','customer_id' => '788','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Avs perbaikan
          Hpi perbaikan
          Home treatment lanjut
          Rencana laser hpi bila tdk perbaikan
          Home tr : nc silver 1 , ddc n gel , dfw a , des 2 , aft tr AD , Acne cream','folder' => 'img/IMG-82-32482019-07-15'),
            array('id_img' => '84','customer_id' => '103','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Laser hpi +ndyag ss 3 11,5
          Atc M
          Home treatment
          Acne Spot , Moisturizer acne niacef 8gr + acuatim 2gr','folder' => 'img/IMG-83-823392019-07-15'),
            array('id_img' => '85','customer_id' => '807','tgl_in' => '2019-07-15','ket' => 'Before','id' => '161','remaks' => 'Botox 38unit','folder' => 'img/IMG-84-751232019-07-15'),
            array('id_img' => '86','customer_id' => '96','tgl_in' => '2019-07-16','ket' => 'Before','id' => '161','remaks' => 'Before Laser rejuve , LLT , ','folder' => 'img/IMG-85-707102019-07-16'),
            array('id_img' => '87','customer_id' => '307','tgl_in' => '2019-07-16','ket' => 'Before','id' => '161','remaks' => 'Hifu 50shot','folder' => 'img/IMG-86-703522019-07-16'),
            array('id_img' => '90','customer_id' => '781','tgl_in' => '2019-07-16','ket' => 'Before','id' => '158','remaks' => 'sebelumnya','folder' => 'img/IMG-87-455322019-07-16'),
            array('id_img' => '91','customer_id' => '781','tgl_in' => '2019-07-16','ket' => 'After','id' => '158','remaks' => 'setelahnya','folder' => 'img/IMG-90-683572019-07-16'),
            array('id_img' => '92','customer_id' => '781','tgl_in' => '2019-07-19','ket' => 'Before','id' => '158','remaks' => 'Gggjh','folder' => 'img/IMG-91-409402019-07-19'),
            array('id_img' => '93','customer_id' => '225','tgl_in' => '2019-07-19','ket' => 'After','id' => '161','remaks' => 'Before Skin booster','folder' => 'img/IMG-92-565372019-07-19'),
            array('id_img' => '94','customer_id' => '285','tgl_in' => '2019-07-19','ket' => 'After','id' => '161','remaks' => 'Before laser Rejuve','folder' => 'img/IMG-93-385222019-07-19'),
            array('id_img' => '95','customer_id' => '478','tgl_in' => '2019-07-19','ket' => 'Before','id' => '161','remaks' => 'Before peeling acne plus','folder' => 'img/IMG-94-877342019-07-19'),
            array('id_img' => '96','customer_id' => '239','tgl_in' => '2019-07-19','ket' => 'After','id' => '161','remaks' => 'Kontrol','folder' => 'img/IMG-95-661992019-07-19'),
            array('id_img' => '97','customer_id' => '711','tgl_in' => '2019-07-19','ket' => 'Before','id' => '161','remaks' => 'Before Botox 18unit','folder' => 'img/IMG-96-373112019-07-19'),
            array('id_img' => '98','customer_id' => '83','tgl_in' => '2019-07-19','ket' => 'Before','id' => '161','remaks' => 'Before botox 54unit ,  Laser bibir , Laser Rejuve','folder' => 'img/IMG-97-770662019-07-19'),
            array('id_img' => '99','customer_id' => '362','tgl_in' => '2019-07-19','ket' => 'Before','id' => '161','remaks' => 'Idung udh bagus , PRP , Laser , Kil - kom','folder' => 'img/IMG-98-578922019-07-19'),
            array('id_img' => '100','customer_id' => '802','tgl_in' => '2019-07-19','ket' => 'After','id' => '161','remaks' => 'Sudah lebih cerah , Laser , Hifu','folder' => 'img/IMG-99-327632019-07-19'),
            array('id_img' => '101','customer_id' => '816','tgl_in' => '2019-07-19','ket' => 'Before','id' => '161','remaks' => 'Ingin mengobati muka kusam
          Hiperplasi sebasea
          Hpi jerawat
          Laser , Hifu','folder' => 'img/IMG-100-938142019-07-19')
          )
        );
    }
}
