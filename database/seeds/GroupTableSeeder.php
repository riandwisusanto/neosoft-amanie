<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create(['group_name' => 'Dermis | Skin', 'name' =>'Dermis | Skin']);
    }
}
