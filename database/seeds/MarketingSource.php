<?php

use Illuminate\Database\Seeder;

class MarketingSource extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marketing_sources')->insert(
            array(
                array('id_marketing_source' => '1','marketing_source_name' => 'iklan TV','extra_field' => '1','status_marketing_source' => '1'),
                array('id_marketing_source' => '2','marketing_source_name' => 'Instagram','extra_field' => '1','status_marketing_source' => '1'),
                array('id_marketing_source' => '3','marketing_source_name' => 'Youtube','extra_field' => '1','status_marketing_source' => '1'),
                array('id_marketing_source' => '4','marketing_source_name' => 'Website','extra_field' => '1','status_marketing_source' => '1'),
                array('id_marketing_source' => '5','marketing_source_name' => 'Referral','extra_field' => '1','status_marketing_source' => '1'),
                array('id_marketing_source' => '6','marketing_source_name' => 'Event','extra_field' => '1','status_marketing_source' => '1'),
                array('id_marketing_source' => '7','marketing_source_name' => 'Walk In','extra_field' => '1','status_marketing_source' => '1'),
                array('id_marketing_source' => '8','marketing_source_name' => 'Pasien Lama','extra_field' => '1','status_marketing_source' => '1')
            )
        );
    }
}
