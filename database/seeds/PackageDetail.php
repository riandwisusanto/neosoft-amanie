<?php

use Illuminate\Database\Seeder;

class PackageDetail extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('package_details')->insert(
            array(
                array('id_package_detail' => '849','package_id' => '353','product_id' => '441','qty' => '1','price' => '750000'),
                array('id_package_detail' => '850','package_id' => '354','product_id' => '441','qty' => '1','price' => '750000'),
                array('id_package_detail' => '856','package_id' => '355','product_id' => '324','qty' => '1','price' => '100000'),
                array('id_package_detail' => '855','package_id' => '355','product_id' => '164','qty' => '1','price' => '40000'),
                array('id_package_detail' => '857','package_id' => '356','product_id' => '284','qty' => '1','price' => '130000'),
                array('id_package_detail' => '858','package_id' => '356','product_id' => '294','qty' => '1','price' => '130000'),
                array('id_package_detail' => '859','package_id' => '356','product_id' => '206','qty' => '1','price' => '130000'),
                array('id_package_detail' => '860','package_id' => '357','product_id' => '442','qty' => '1','price' => '110000'),
                array('id_package_detail' => '861','package_id' => '357','product_id' => '322','qty' => '1','price' => '110000'),
                array('id_package_detail' => '862','package_id' => '358','product_id' => '393','qty' => '1','price' => '160000'),
                array('id_package_detail' => '863','package_id' => '358','product_id' => '442','qty' => '1','price' => '160000'),
                array('id_package_detail' => '864','package_id' => '359','product_id' => '434','qty' => '1','price' => '75000'),
                array('id_package_detail' => '865','package_id' => '360','product_id' => '443','qty' => '1','price' => '290000'),
                array('id_package_detail' => '866','package_id' => '360','product_id' => '444','qty' => '1','price' => '290000'),
                array('id_package_detail' => '867','package_id' => '361','product_id' => '445','qty' => '1','price' => '220000'),
                array('id_package_detail' => '868','package_id' => '362','product_id' => '163','qty' => '1','price' => '50000'),
                array('id_package_detail' => '869','package_id' => '362','product_id' => '330','qty' => '1','price' => '50000'),
                array('id_package_detail' => '870','package_id' => '362','product_id' => '380','qty' => '1','price' => '40000'),
                array('id_package_detail' => '873','package_id' => '363','product_id' => '367','qty' => '1','price' => '75000'),
                array('id_package_detail' => '874','package_id' => '363','product_id' => '205','qty' => '1','price' => '75000'),
                array('id_package_detail' => '875','package_id' => '364','product_id' => '377','qty' => '1','price' => '65000'),
                array('id_package_detail' => '876','package_id' => '364','product_id' => '378','qty' => '1','price' => '60000'),
                array('id_package_detail' => '877','package_id' => '365','product_id' => '446','qty' => '1','price' => '235000'),
                array('id_package_detail' => '878','package_id' => '366','product_id' => '239','qty' => '1','price' => '110000'),
                array('id_package_detail' => '879','package_id' => '366','product_id' => '298','qty' => '1','price' => '110000'),
                array('id_package_detail' => '880','package_id' => '366','product_id' => '418','qty' => '1','price' => '110000'),
                array('id_package_detail' => '881','package_id' => '367','product_id' => '447','qty' => '111','price' => '150000'),
                array('id_package_detail' => '882','package_id' => '368','product_id' => '286','qty' => '1','price' => '210000'),
                array('id_package_detail' => '883','package_id' => '368','product_id' => '395','qty' => '1','price' => '210000'),
                array('id_package_detail' => '888','package_id' => '369','product_id' => '449','qty' => '1','price' => '100000'),
                array('id_package_detail' => '889','package_id' => '369','product_id' => '411','qty' => '1','price' => '100000'),
                array('id_package_detail' => '890','package_id' => '370','product_id' => '450','qty' => '1','price' => '100000'),
                array('id_package_detail' => '891','package_id' => '370','product_id' => '451','qty' => '1','price' => '100000'),
                array('id_package_detail' => '892','package_id' => '370','product_id' => '452','qty' => '1','price' => '120000'),
                array('id_package_detail' => '895','package_id' => '371','product_id' => '322','qty' => '1','price' => '115000'),
                array('id_package_detail' => '896','package_id' => '371','product_id' => '442','qty' => '1','price' => '115000'),
                array('id_package_detail' => '897','package_id' => '372','product_id' => '453','qty' => '1','price' => '150000'),
                array('id_package_detail' => '898','package_id' => '373','product_id' => '454','qty' => '1','price' => '280000'),
                array('id_package_detail' => '899','package_id' => '374','product_id' => '227','qty' => '1','price' => '10000'),
                array('id_package_detail' => '900','package_id' => '375','product_id' => '455','qty' => '1','price' => '14000')
            )
        );
    }
}
