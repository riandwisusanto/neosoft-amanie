<?php

use Illuminate\Database\Seeder;

class PackagePoint extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('package_points')->insert(
            array(
                array('id_package_point' => '528','package_id' => '353','point' => '10'),
                array('id_package_point' => '529','package_id' => '354','point' => '10'),
                array('id_package_point' => '532','package_id' => '355','point' => '5'),
                array('id_package_point' => '533','package_id' => '356','point' => '5'),
                array('id_package_point' => '534','package_id' => '357','point' => '5'),
                array('id_package_point' => '535','package_id' => '358','point' => '5'),
                array('id_package_point' => '536','package_id' => '359','point' => '5'),
                array('id_package_point' => '537','package_id' => '360','point' => '5'),
                array('id_package_point' => '538','package_id' => '361','point' => '5'),
                array('id_package_point' => '539','package_id' => '362','point' => '5'),
                array('id_package_point' => '541','package_id' => '363','point' => '5'),
                array('id_package_point' => '542','package_id' => '364','point' => '5'),
                array('id_package_point' => '543','package_id' => '365','point' => '5'),
                array('id_package_point' => '544','package_id' => '366','point' => '5'),
                array('id_package_point' => '545','package_id' => '367','point' => '5'),
                array('id_package_point' => '546','package_id' => '368','point' => '5'),
                array('id_package_point' => '551','package_id' => '369','point' => '5'),
                array('id_package_point' => '552','package_id' => '370','point' => '5'),
                array('id_package_point' => '555','package_id' => '371','point' => '5'),
                array('id_package_point' => '556','package_id' => '372','point' => '10'),
                array('id_package_point' => '557','package_id' => '373','point' => '5'),
                array('id_package_point' => '558','package_id' => '374','point' => '5'),
                array('id_package_point' => '559','package_id' => '375','point' => '5')
            )
        );
    }
}
