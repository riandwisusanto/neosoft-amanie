<?php

use Illuminate\Database\Seeder;

class Rack extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id_rack'           => 1,
                'main_rack_code'    => 'Lemari A',
                'sub_rack_code'     => 'Loker 1',
                'description'       => 'loker kimia',
                'warehouse_id'      => 1
            ],
            [
                'id_rack'           => 2,
                'main_rack_code'    => 'Lemari A',
                'sub_rack_code'     => 'Loker 2',
                'description'       => 'loker non-kimia',
                'warehouse_id'      => 1
            ],
        ];

        DB::table('racks')->insert($data);
    }
}
