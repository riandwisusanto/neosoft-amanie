<?php

use Illuminate\Database\Seeder;

class Room extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Room::class, 3)->create();
    }
}
