<?php

use Illuminate\Database\Seeder;

use App\Models\Supplier;

class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'id' => 1, 'name' => 'Elvira Rumangu', 'addr' => 'Buyungon-lingkungan IV', 'city_id' => '0', 'post_code' => '0', 'is_active' => 1 ],
            [ 'id' => 2, 'name' => 'Niken Sumarni', 'addr' => 'Buyungon-lingkungan IV', 'city_id' => '0', 'post_code' => '0', 'is_active' => 1 ]
        ];
        Supplier::insert($data);
    }
}
