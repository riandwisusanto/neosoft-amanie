<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users");

        $administrator = new \App\Models\User;
        $administrator->id = 1;
        $administrator->username = "administrator";
        $administrator->name = "Administrator";
        $administrator->email = "neosoft@neosoft.com";
        $administrator->level = json_encode(["ADMINISTRATOR", "SUPER_USER"]);
        $administrator->status_user = 1;
        $administrator->password = \Hash::make("123456789");
        $administrator->api_token = '123456';

        $administrator->save();
    }
}
