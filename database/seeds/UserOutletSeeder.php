<?php

use Illuminate\Database\Seeder;
use App\Models\UserOutlet;

class UserOutletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $outlet = new UserOutlet;
        $outlet->user_id = 1;
        $outlet->outlet_id = 1;
        $outlet->save();
    }
}
