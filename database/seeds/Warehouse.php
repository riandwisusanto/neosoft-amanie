<?php

use Illuminate\Database\Seeder;

class Warehouse extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id_warehouse'      => 1,
                'name'              => 'Gudang Utama',
                'description'       => 'Gudang pusat & utama',
                'addr'              => 'Jl. Kebenaran',
                'is_active'         => 1
            ]
        ];

        DB::table('warehouses')->insert($data);
    }
}
