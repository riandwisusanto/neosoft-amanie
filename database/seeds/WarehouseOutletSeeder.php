<?php

use Illuminate\Database\Seeder;
use App\Models\WarehouseOutlet;

class WarehouseOutletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $outlet = new WarehouseOutlet;
        $outlet->warehouse_id = 1;
        $outlet->outlet_id = 1;
        $outlet->save();
    }
}
