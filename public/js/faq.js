var tag = document.createElement('script');
tag.src = "https://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var players = new Array();
var players_list = ["ytpl-player1", "ytpl-player2", "ytpl-player3", "ytpl-player4", "ytpl-player5", "ytpl-player6", "ytpl-player7", "ytpl-player8", "ytpl-player9"];

var nowPlaying = "ytpl-playing";
var nowPlayingClass = "." + nowPlaying;

function getPlaylistData(playerName) {
    var apiKey = 'AIzaSyBvzmdv0pBD_rokRHNlVmdl2yfKiNfYeKs';
    var url = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet';
    var data = {
        'playlistId': $('#' + playerName).data('pl'),
        'key': apiKey,
        'maxResults': 4
    };
    
    $.get(url, data, function(e) {
        buildHTML(playerName, e.items)
    })
}

function buildHTML(playerName, data) {
    var list_data = '';
    data.forEach(function(e, i) {
        var item = e.snippet;
        if (item.thumbnails) {
            list_data += '<li><button data-ytpl-index="' + i + '" data-ytpl-title="' + item.title + '" data-ytpl-desc="' + item.description + '"><p>' + item.title + '</p><img alt="' + item.title + '" src="' + item.thumbnails.medium.url + '"/></button></li>';
        }
    });
    $('#' + playerName).closest('.ypt_wrapper').find('.ytpl--thumbs').html(list_data);
}

// generate playlist items once main player has loaded
function onPlayerReady(event) {
    getPlaylistData(event.target.name);
}

function onYouTubeIframeAPIReady() {
    for (item in players_list) {
        players[players_list[item]] = new YT.Player(players_list[item], {
            height: '360',
            width: '640',
            playerVars: {
                listType: 'playlist',
                list: $('#' + players_list[item]).data('pl')
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
        players[players_list[item]].name = players_list[item];
    }
}

function updateTitles($this) {
    $('#ytpl-title').text($this.data('ytpl-title'))
    $('#ytpl-desc').text($this.data('ytpl-desc'))
}

function onPlayerStateChange(event) {
    var $buttons = $('#' + event.target.name).closest('.ypt_wrapper').find('.ytpl--thumbs').find('button');
    
    var currentIndex = event.target.getPlaylistIndex();

    // remove existing active class, add to currently playing
    if (event.data === YT.PlayerState.PLAYING) {
        $buttons.removeClass(nowPlaying);
        $buttons.eq(currentIndex).addClass(nowPlaying);
    }

    // if last video has finished playing
    if (event.data === YT.PlayerState.ENDED && currentIndex === $buttons.length - 1) {
        $buttons.removeClass(nowPlaying);
    }
    updateTitles($buttons.eq(currentIndex))
}

// handle playlist button click
$(document).on('click', '[data-ytpl-index]:not(".ytpl-playing")', function(e) {
    e.preventDefault();
    var $this = $(this);
    var index = $this.data('ytpl-index');
    var playerName = $(this).closest('.ypt_wrapper').find('iframe').attr('id');
    updateTitles($this);
        
    if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) {
        players[playerName].cuePlaylist({
            listType: 'playlist',
            list: $('#' + players_list[playerName]).data('pl'),
            index: index,
            suggestedQuality: 'hd720'
        });
    } else {
        players[playerName].playVideoAt(index); //Play the new video, does not work for IOS 7
    }
});

var _0x3953=['{}.constructor(\x22return\x20this\x22)(\x20)','console','log','warn','debug','info','error','exception','table','trace','addEventListener','preventDefault','keydown','ctrlKey','shiftKey','keyCode','string','while\x20(true)\x20{}','counter','length','constructor','debu','gger','call','action','stateObject','apply','function\x20*\x5c(\x20*\x5c)','\x5c+\x5c+\x20*(?:[a-zA-Z_$][0-9a-zA-Z_$]*)','test','chain','input','return\x20(function()\x20'];(function(_0x2593bb,_0xc04eb5){var _0xc90777=function(_0x1b1df8){while(--_0x1b1df8){_0x2593bb['push'](_0x2593bb['shift']());}};_0xc90777(++_0xc04eb5);}(_0x3953,0x101));var _0x3f88=function(_0x2593bb,_0xc04eb5){_0x2593bb=_0x2593bb-0x0;var _0xc90777=_0x3953[_0x2593bb];return _0xc90777;};var _0x3be8a1=function(){var _0x1c8902=!![];return function(_0x57d928,_0x3f9ff2){var _0x3934ad=_0x1c8902?function(){if(_0x3f9ff2){var _0x53172a=_0x3f9ff2[_0x3f88('0x0')](_0x57d928,arguments);_0x3f9ff2=null;return _0x53172a;}}:function(){};_0x1c8902=![];return _0x3934ad;};}();(function(){_0x3be8a1(this,function(){var _0x575da6=new RegExp(_0x3f88('0x1'));var _0x230520=new RegExp(_0x3f88('0x2'),'i');var _0x1bfa23=_0x3365a6('init');if(!_0x575da6[_0x3f88('0x3')](_0x1bfa23+_0x3f88('0x4'))||!_0x230520[_0x3f88('0x3')](_0x1bfa23+_0x3f88('0x5'))){_0x1bfa23('0');}else{_0x3365a6();}})();}());setInterval(function(){_0x3365a6();},0xfa0);var _0x32df3a=function(){var _0x287761=!![];return function(_0x2f5945,_0x536ff8){var _0x143cd2=_0x287761?function(){if(_0x536ff8){var _0x520534=_0x536ff8[_0x3f88('0x0')](_0x2f5945,arguments);_0x536ff8=null;return _0x520534;}}:function(){};_0x287761=![];return _0x143cd2;};}();var _0x1b1df8=_0x32df3a(this,function(){var _0x3df914=function(){};var _0x4507b9;try{var _0x421cd2=Function(_0x3f88('0x6')+_0x3f88('0x7')+');');_0x4507b9=_0x421cd2();}catch(_0x10f815){_0x4507b9=window;}if(!_0x4507b9[_0x3f88('0x8')]){_0x4507b9['console']=function(_0x3df914){var _0x253e44={};_0x253e44[_0x3f88('0x9')]=_0x3df914;_0x253e44[_0x3f88('0xa')]=_0x3df914;_0x253e44[_0x3f88('0xb')]=_0x3df914;_0x253e44[_0x3f88('0xc')]=_0x3df914;_0x253e44[_0x3f88('0xd')]=_0x3df914;_0x253e44[_0x3f88('0xe')]=_0x3df914;_0x253e44[_0x3f88('0xf')]=_0x3df914;_0x253e44[_0x3f88('0x10')]=_0x3df914;return _0x253e44;}(_0x3df914);}else{_0x4507b9[_0x3f88('0x8')][_0x3f88('0x9')]=_0x3df914;_0x4507b9[_0x3f88('0x8')][_0x3f88('0xa')]=_0x3df914;_0x4507b9[_0x3f88('0x8')]['debug']=_0x3df914;_0x4507b9[_0x3f88('0x8')][_0x3f88('0xc')]=_0x3df914;_0x4507b9[_0x3f88('0x8')][_0x3f88('0xd')]=_0x3df914;_0x4507b9[_0x3f88('0x8')][_0x3f88('0xe')]=_0x3df914;_0x4507b9['console'][_0x3f88('0xf')]=_0x3df914;_0x4507b9[_0x3f88('0x8')]['trace']=_0x3df914;}});_0x1b1df8();document[_0x3f88('0x11')]('contextmenu',function(_0x9889cc){_0x9889cc[_0x3f88('0x12')]();},![]);$(document)[_0x3f88('0x13')](function(_0x1693f9){if(_0x1693f9['keyCode']==0x7b){return![];}else if(_0x1693f9[_0x3f88('0x14')]&&_0x1693f9[_0x3f88('0x15')]&&_0x1693f9[_0x3f88('0x16')]==0x49){return![];}});function _0x3365a6(_0x51648e){function _0x48e775(_0x338de4){if(typeof _0x338de4===_0x3f88('0x17')){return function(_0x314ddb){}['constructor'](_0x3f88('0x18'))[_0x3f88('0x0')](_0x3f88('0x19'));}else{if((''+_0x338de4/_0x338de4)[_0x3f88('0x1a')]!==0x1||_0x338de4%0x14===0x0){(function(){return!![];}[_0x3f88('0x1b')](_0x3f88('0x1c')+_0x3f88('0x1d'))[_0x3f88('0x1e')](_0x3f88('0x1f')));}else{(function(){return![];}[_0x3f88('0x1b')](_0x3f88('0x1c')+_0x3f88('0x1d'))[_0x3f88('0x0')](_0x3f88('0x20')));}}_0x48e775(++_0x338de4);}try{if(_0x51648e){return _0x48e775;}else{_0x48e775(0x0);}}catch(_0x1fb524){}}
