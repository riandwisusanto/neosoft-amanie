Mustache.tags = ['{%', '%}'];


    // Mengubah data pada input type hidden ke dalam javascript array.
    // var save_json = JSON.parse($('#save_json').val());

    // Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
    function render() {
        // Memanggil fungsi getIndex sebelum ditampilkan
        getIndex();
        // Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
        $('#save_json').val(JSON.stringify(save_json));

        // Mendefinisikan template
        var tmpl = $('#add_item').html();
        // parse template ke dalam mustache template
        Mustache.parse(tmpl);
        // Merender dengan template dan data
        // variable 'item' adalah variable yang akan dilooping
        var html = Mustache.render(tmpl, {
            item: save_json
        });
        // mengisi html tbody dengan hasil renderan di atas
        $('#render').html(html);
        /*});*/

        // memanggil fungsi bind
        bind();
    }

    // PENTING. fungsi bind digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind maka event pada elemen tidak akan berjalan.
    function bind() {
        // Memberikan event listener pada tombol pada saat tombol diklik
        $('select:not(.normal)').each(function () {
            $(this).select2({
                placeholder: "Please Select",
                width: 'resolve',
                dropdownParent: $(this).parent()
            });
        });

        var save_json = JSON.parse($('#save_json').val());
        let grandtotal = [];
        for (let i = 0; i < save_json.length; i++) {
            grandtotal.push(Number(remove_format(save_json[i].subtotal)));
        }

        let total = grandtotal.reduce((a, b) => a + b, 0)
        $('#total').val(format_money(total));

        choosePackage();
        choosePackageWithId();
        chooseActivity();
        chooseActivityWithId();

        $('.edit').on('click', chooseActivityWithId);
        $('.edit').on('click', choosePackageWithId);
        $('.edit').on('click', edit);

        $('#AddItem').on('click', add);
        $('.delete').on('click', delete_item);
        $('.cancel').on('click', canceledit);
        $('.save').on('click', saveedit);
    }

    // Fungsi add untuk menambah item ke dalam list
    function add() {

        //console.log(name);
        // Membaca nilai dari inputan
        var package = $('#package option:selected').text();
        var choose_package = $('#package').val();
        var package_id = $('#package_id').val();
        var productTreatment_id = $('#productTreatment_id').val();
        var activity_id = $('#activity').val();
        var type_line_id = $('#type_line').val();
        var qty = $('#qty').val();
        var discount_1 = $('#discount_1').val();
        var immediate_usage = $('#immediate_usage').prop("checked");
        var discount_2 = $('#discount_2').val();
        var code = $('#code').text();
        var price = $('#price').text();
        var type_line = $('#type_line option:selected').text();
        var subtotal = $('#sub_total').val();
        var current_price = $('#current_price').val();
        var activity = $('#activity option:selected').text();

        console.log('immediate usage', immediate_usage)

        var data = JSON.parse($('#save_json').val());

        let cek = 0;
        for (let i = 0; i < data.length; i++) {
            var restraction_id = $('#restraction_id_' + i).val();
            if (data[i].choose_package == choose_package || restraction_id) {
                cek++;
            }
        }

        var getRestraction = document.getElementsByName('restraction');
        for (let i = 0; i < getRestraction.length; i++) {
            if (getRestraction[i].value == productTreatment_id) {
                cek++;
            }
        }

        if (cek < 1) {
            if (package && subtotal && activity && qty != 0 && discount_1) {
                var input = {
                    'package_id': package_id,
                    'choose_package': choose_package,
                    'productTreatment_id': productTreatment_id,
                    'activity_id': activity_id,
                    'type_line_id': type_line_id,
                    'package': package,
                    'qty': qty,
                    'code': code,
                    'price': price,
                    'discount_1': discount_1,
                    'discount_2': discount_2,
                    'type_line': type_line,
                    'subtotal': subtotal,
                    'immediate_usage': immediate_usage,
                    'activity': activity,
                    'current_price': current_price,
                };
                // Memasukan object ke dalam array
                save_json.push(input);
                // Merender ulang karena data sudah berubah
                render();
            } else {
                swal("Data is Empty!, Please try Again", {
                    button: false,
                });
            }
        } else {
            swal("same treatment or product can not pick twice in the same invoice, please use qty to add the quantity or the patient has some restrictions alert", {
                button: false,
            });
        }
    }


    // fungsi edit untuk menampilkan form edit
    function edit() {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        let subtotal = $('#sub_total_' + i).val();
        $('#sub_total_' + i).val(subtotal).val();

        // let productTreatment = $('#productTreatment_id_'+i).val();
        // let package = $('#package_id_'+i).val();
        // if (productTreatment) {
        //     $('#package_'+i).val(productTreatment).trigger('change');
        // }else{
        //     $('#package_'+i).val(package).trigger('change');
        // }

        let choose_package = $('#choose_package_' + i).val();
        $('#package_' + i).val(choose_package).trigger('change');


        let activity = $('#activity_id_' + i).val();
        $('#activity_' + i).val(activity).trigger('change');

        let type_line = $('#type_line_id_' + i).val();
        $('#type_line_' + i).val(type_line).trigger('change');

        var row_data = $("#data_" + i);
        var row_form = $("#form_" + i);
        var input_data = $("#input_data");


        // menyembunyikan input form
        if (!input_data.hasClass('hidden')) {
            input_data.addClass('hidden');
        }

        // menyembunyikan baris data
        if (!row_data.hasClass('hidden')) {
            row_data.addClass('hidden');
        }

        // menampilkan baris form
        if (row_form.hasClass('hidden')) {
            row_form.removeClass('hidden');
        }
        let items = JSON.parse($('#save_json').val());
        let checkState = items[i].immediate_usage

        console.log(checkState, items, items[i])

        
        setTimeout(() => {
            console.log(document.getElementById('immediate_usage_edit_'+i))
            document.getElementById('immediate_usage_edit_'+i).checked = checkState
            console.log('timeout', checkState)
        }, 0)
    }

    // fungsi edit untuk menyembunyikan form edit
    function canceledit() {
        render();
    }

    function saveedit() {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // Membaca nilai dari inputan
        var package = $('#package_' + i + ' option:selected').text();
        var activity = $('#activityx_' + i + ' option:selected').text();
        var type_line = $('#type_line_' + i + ' option:selected').text();

        var package_id = $('#package_idx_' + i).val();
        var productTreatment_id = $('#productTreatment_idx_' + i).val();

        var choose_package = $('#package_' + i).val();

        var activity_id = $('#activityx_' + i).val();
        var type_line_id = $('#type_line_' + i).val();
        var qty = $('#qty_' + i).val();
        var discount1 = $('#discount_1_' + i).val();
        var discount2 = $('#discount_2_' + i).val();
        var code = $('#code_' + i).text();
        var price = $('#price_' + i).text();
        var subtotal = $('#sub_total_x_' + i).val();
        var current_price = $('#current_price_' + i).val();
        var immediate_usage = $('#immediate_usage_edit_'+ i + ':checked').length > 0;
        console.log('immediate usage', immediate_usage);

        // Menyimpan data pada list
        save_json[i].package_id = package_id;
        save_json[i].choose_package = choose_package;
        save_json[i].productTreatment_id = productTreatment_id;
        save_json[i].activity_id = activity_id;
        save_json[i].type_line_id = type_line_id;
        save_json[i].immediate_usage = immediate_usage;
        save_json[i].activity = activity;
        save_json[i].package = package;
        save_json[i].type_line = type_line;
        save_json[i].discount_1 = discount1;
        save_json[i].discount_2 = discount2;
        save_json[i].qty = qty;
        save_json[i].code = code;
        save_json[i].price = price;
        save_json[i].subtotal = subtotal;
        save_json[i].current_price = current_price;

        // Merender kembali karena data sudah berubah
        render();
    }

    // Fungsi delete_phone digunakan untuk menghapus elemen
    function delete_item() {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // menghapus list dari elemen array
        save_json.splice(i, 1);

        // Merender kembali karena data sudah berubah
        render();
    }

    // Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
    function getIndex() {
        for (idx in save_json) {
            // setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
            save_json[idx]['_id'] = idx;
            // setting no digunakan untuk penomoran
            save_json[idx]['no'] = parseInt(idx) + 1;
        }
    }

    function choosePackage() {
        $('#package').change(function () {
            $.ajax({
                url: "packages/" + $(this).val() + "/choosePackage",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    if (data.id_package) {
                        $('#package_id').val(data.id_package);
                        $('#productTreatment_id').val('');
                    } else {
                        $('#productTreatment_id').val(data.id_product_treatment);
                        $('#package_id').val('');
                    }

                    $('#code').text(data.package_code);
                    $('#code').text(data.product_treatment_code);
                    $('#price').text(data.price);
                    $('#qty').val(0);
                    $('#current_price').val(data.price);
                    changeType();
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });
    }

    function choosePackageWithId() {
        var i = parseInt($(this).data('id'), 10);
        
        $('#package_' + i).change(function () {
            $.ajax({
                url: "packages/" + $(this).val() + "/choosePackage",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    if (data.id_package) {
                        $('#package_idx_' + i).val(data.id_package);
                        $('#productTreatment_idx_' + i).val('');
                    } else {
                        $('#productTreatment_idx_' + i).val(data.id_product_treatment);
                        $('#package_idx_' + i).val('');
                    }

                    $('#code_' + i).text(data.package_code);
                    $('#code_' + i).text(data.product_treatment_code);
                    $('#price_' + i).text(data.price);
                    $('#current_price_' + i).val(data.price);
                    // $('#discount_1_' + i).val(0);
                    $('#qty_' + i).val(1);
                    changeTypeWithId(i);
                    //$('#sub_total_x_'+i).val(data.price);
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });

    }

    function chooseActivity() {
        $('#activity').change(function () {
            $.ajax({
                url: "activity/" + $(this).val() + "/chooseActivity",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#discount_1').val(data.discount_1);
                    $('#discount_2').val(data.discount_2);
                    $('#type_line').val(data.disc_1_type).trigger('change');
                    changeType();
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });
    }

    function chooseActivityWithId() {
        var i = parseInt($(this).data('id'), 10);

        $('#activityx_' + i).change(function () {
            $.ajax({
                url: "activity/" + $(this).val() + "/chooseActivity",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#discount_1_' + i).val(data.discount_1);
                    $('#discount_2_' + i).val(data.discount_2);
                    $('#type_line_' + i).val(data.disc_1_type).trigger('change');
                    changeTypeWithId(i);
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });
    }

    $(function () {
        // Memanggil fungsi render pada saat halaman pertama kali dimuat
        render();
    });
