function medicalAlert(id) {
    $("#idx").val(id);
    $.ajax({
        url: "medical-alert/" + id + "/edit",
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            $('input[name=_method]').val('POST');
            $("#modal-medical").modal('show');
            $('#save_json').val(JSON.stringify(data.medical));
            //mustache(JSON.stringify(data.medical));
            Mustache.tags = ['{%', '%}'];

            $(function () {
                // Mengubah data pada input type hidden ke dalam javascript array.
                var data = JSON.parse($('#save_json').val());

                // Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
                function render() {
                    // Memanggil fungsi getIndex sebelum ditampilkan
                    getIndex(data);
                    // Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
                    $('#save_json').val(JSON.stringify(data));

                    // Mendefinisikan template
                    var tmpl = $('#add_item').html();
                    // parse template ke dalam mustache template
                    Mustache.parse(tmpl);
                    // Merender dengan template dan data
                    // variable 'item' adalah variable yang akan dilooping
                    var html = Mustache.render(tmpl, {
                        item: data
                    });
                    // mengisi html tbody dengan hasil renderan di atas
                    $('#render').html(html);
                    /*});*/

                    // memanggil fungsi bind
                    bind(data);
                }

                // PENTING. fungsi bind digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind maka event pada elemen tidak akan berjalan.
                function bind() {

                    $('select:not(.normal)').each(function () {
                        $(this).select2({
                            placeholder: "Please Select",
                            width: 'resolve',
                            dropdownParent: $(this).parent()
                        });
                    });

                    // Memberikan event listener pada tombol pada saat tombol diklik
                    $('#AddItem').on('click', add);
                    $('.delete').on('click', delete_item);
                    $('.edit').on('click', edit);
                    $('.cancel').on('click', canceledit);
                    $('.save').on('click', saveedit);
                }

                // Fungsi add untuk menambah item ke dalam list
                function add() {
                    // Membaca nilai dari inputan
                    var medical = $('#medical option:selected').text();
                    var medical_id = $('#medical').val();

                    // if (kode_pasien == '') {
                    //      alert('Silahkan Input Nama');
                    //      return false;
                    // }else if (type == '') {
                    //     alert('Price Not Valid');
                    //      return false;
                    //     alert('Silahkan Input Jumlah');
                    // }else if (quantity == '') {
                    //      return false;
                    // }else if (isNaN(quantity)) {
                    //     alert('Jumlah Not Valid');
                    //      return false;
                    // }

                    // Membuat object yg akan dimasukan kedalam array
                    var input = {
                        'medical': medical,
                        'medical_id': medical_id,
                    };


                    // Memasukan object ke dalam array
                    data.push(input);

                    // Merender ulang karena data sudah berubah

                    render();

                }



                // fungsi edit untuk menampilkan form edit
                function edit() {
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);

                    let medical = $('#medical_id_' + i).val();
                    $('#medicalx_' + i).val(medical).trigger('change');

                    var row_data = $("#data_" + i);
                    var row_form = $("#form_" + i);
                    var input_data = $("#input_data");


                    // menyembunyikan input form
                    if (!input_data.hasClass('hidden')) {
                        input_data.addClass('hidden');
                    }

                    // menyembunyikan baris data
                    if (!row_data.hasClass('hidden')) {
                        row_data.addClass('hidden');
                    }

                    // menampilkan baris form
                    if (row_form.hasClass('hidden')) {
                        row_form.removeClass('hidden');
                    }
                }

                // fungsi edit untuk menyembunyikan form edit
                function canceledit() {
                    render();
                }

                function saveedit() {
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);

                    // Membaca nilai dari inputan
                    var medical = $('#medicalx_' + i + ' option:selected').text();
                    var medical_id = $('#medicalx_' + i).val();

                    // Menyimpan data pada list
                    data[i].medical = medical;
                    data[i].medical_id = medical_id;

                    // Merender kembali karena data sudah berubah
                    render();
                }

                // Fungsi delete_phone digunakan untuk menghapus elemen
                function delete_item() {
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);
                    // menghapus list dari elemen array
                    data.splice(i, 1);

                    // Merender kembali karena data sudah berubah
                    render();
                }

                // Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
                function getIndex(data) {
                    for (idx in data) {
                        // setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
                        data[idx]['_id'] = idx;
                        // setting no digunakan untuk penomoran
                        data[idx]['no'] = parseInt(idx) + 1;
                    }
                }

                // Memanggil fungsi render pada saat halaman pertama kali dimuat
                render();
            });
        },
        error: function () {
            alert("Can not Show the Data!");
        }
    });
}
