Mustache.tags = ['{%', '%}'];

$(function(){
    // Mengubah data pada input type hidden ke dalam javascript array.
    var save_json = JSON.parse($('#save_json_point').val());    

    // Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
    function render_point()
    {
        // Memanggil fungsi getIndex sebelum ditampilkan
        getIndex_point();
        // Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
        $('#save_json_point').val(JSON.stringify(save_json));
        
        // Mendefinisikan template
        var tmpl = $('#add_item_point').html();
        // parse template ke dalam mustache template
        Mustache.parse(tmpl);
        // Merender dengan template dan data
        // variable 'item' adalah variable yang akan dilooping
        var html = Mustache.render(tmpl, { item : save_json });
        // mengisi html tbody dengan hasil renderan di atas
        $('#render_point').html(html);
        /*});*/

        // memanggil fungsi bind_point
        bind_point();        
    }

    // PENTING. fungsi bind_point digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind_point maka event pada elemen tidak akan berjalan.
    function bind_point(){
        // Memberikan event listener pada tombol pada saat tombol diklik       
        $('#AddItem_point').on('click', add_point);
        $('.delete_point').on('click', delete_item_point);
        $('.edit_point').on('click', edit_point);
        $('.cancel_point').on('click', canceledit_point);
        $('.save_point').on('click', saveedit_point);
    }

    // Fungsi add untuk menambah item ke dalam list
    function add_point()
    {
        // Membaca nilai dari inputan        
        
        var point = $('#point').val();

        if (point) {
            var input = {
                'point' : point,
            };
    
    
            // Memasukan object ke dalam array
            save_json.push(input);
    
            // Merender ulang karena data sudah berubah
    
            render_point();
        }else{
            alert('Point is Empty');
        }
        // if (kode_pasien == '') {
        //      alert('Silahkan Input Nama');
        //      return false;
        // }else if (type == '') {
        //     alert('Price Not Valid');
        //      return false;
        //     alert('Silahkan Input Jumlah');
        // }else if (quantity == '') {
        //      return false;
        // }else if (isNaN(quantity)) {
        //     alert('Jumlah Not Valid');
        //      return false;
        // }

        // Membuat object yg akan dimasukan kedalam array
       

    }

    

    // fungsi edit untuk menampilkan form edit
    function edit_point()
    {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        var row_data = $("#data_point_" + i);
        var row_form = $("#form_point_" + i);
        var input_data = $("#input_data_point");        
        

        // menyembunyikan input form
        if(!input_data.hasClass('hidden')){
            input_data.addClass('hidden');
        }

        // menyembunyikan baris data
        if(!row_data.hasClass('hidden')){
            row_data.addClass('hidden');
        }

        // menampilkan baris form
        if(row_form.hasClass('hidden')){
            row_form.removeClass('hidden');
        }
    }

    // fungsi edit untuk menyembunyikan form edit
    function canceledit_point()
    {
        render_point();
    }

    function saveedit_point()
    {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // Membaca nilai dari inputan
        var point = $('#point_' + i).val();

        // Menyimpan data pada list
        save_json[i].point = point;     

        // Merender kembali karena data sudah berubah
        render_point();
    }

    // Fungsi delete_phone digunakan untuk menghapus elemen
    function delete_item_point(){
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // menghapus list dari elemen array
        save_json.splice(i, 1);

        // Merender kembali karena data sudah berubah
        render_point();
    }

    // Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
    function getIndex_point()
    {
        for (idx in save_json) {
            // setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
            save_json[idx]['_id'] = idx;
            // setting no digunakan untuk penomoran
            save_json[idx]['no'] = parseInt(idx) + 1;
        }
    }

    // Memanggil fungsi render pada saat halaman pertama kali dimuat
    render_point();
});
