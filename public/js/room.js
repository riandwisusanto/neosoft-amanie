function changeDate() {
    location.reload();
}

function formatDate(date) {

    var day = date.getDate();
    var monthIndex = date.getMonth() + 1;
    var year = date.getFullYear();

    if (day < 10) {
        day = '0' + day
    }

    if (monthIndex < 10) {
        monthIndex = '0' + monthIndex
    }


    return day + '/' + monthIndex + '/' + year;
}

$('#country').change(function () {
    var countryID = $(this).val();
    if (countryID) {
        getCity(countryID);
    } else {
        // alert('something want wrong');
    }
});

function getCity(country) {
    $.ajax({
        url: 'customers/' + country + '/province',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('select[name="city"]').empty();
            for (let index in data) {
                $('select[name="city"]').append('<option value="' + index + '">' + data[index] + '</option>')
            }
        }
    });
}

function EventClick(id_customer, id_appointment, date) {
    $.ajax({
        url: "/by-room/" + id_customer + "/" + id_appointment + "/" + date,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            let user = '';
            if (data.appointment.user) {
                user = data.appointment.user.name
            }else{
                user = 'Create By Apps'
            }

            $("#modal-action").modal('show');
            $(".modal-title").text('Appointments Usage Detail');
            $('#date').html(data.appointment.come_date);
            $('#outlet_detail').html(data.appointment.outlet.outlet_name);
            $('#name_detail').html(data.appointment.customer.full_name);
            $('#phone').html(data.appointment.customer.phone);
            $('#createBy').html(user);
            $('#time').html(data.appointment.start_time + " - " + data.appointment.end_time);

            let status = '';
            if (data.appointment.status_appointment == 1) {
                status = 'Booking';
            } else if (data.appointment.status_appointment == 2) {
                status = 'Confirmed';
            } else if (data.appointment.status_appointment == 3) {
                status = 'Complete';
            } else if (data.appointment.status_appointment == 4) {
                status = 'Cancelled';
            } else {
                status = 'No Show';
            }

            $('#status_detail').html(status);
            let trHTML = '';
            for (let i = 0; i < data.data.length; i++) {
                let therapist = '';
                for (let n = 0; n < data.data[i].therapist.length; n++) {
                    therapist += data.data[i].therapist[n].therapist_name + ','
                }
                var product = "";
                if (data.data[i].treatment_id) {
                    product = data.data[i].product_treatment.product_treatment_name;
                }else{
                    product = "Consultasi";
                }

                trHTML += '<tr>' +
                    '<td>' + data.data[i].start_time + '</td>' +
                    '<td>' + data.data[i].end_time + '</td>' +
                    '<td>' + product + '</td>' +
                    '<td>' + therapist + '</td>' +
                    '<tr>';
            }
            $('#detail').html(trHTML);

            let button = '<a href="/appointment/' + id_appointment + '" class="btn btn-default"><i class="fa fa-eye"></i> Detail</a>' +
                '<button type="button" class="btn btn-warning" onclick="changeStatus(' + id_appointment + ')">' +
                '<i class="fa fa-exchange"></i> Change Status</button>' +
                '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i>Close</button>';

            $('#action-footer').html(button);
        },
        error: function () {
            alert("Can not Show the Data!");
        }
    });
}

function changeStatus(id) {
    $('#modal-status').modal('show');
    $('.modal-title-status').text('Change Status');
    $('input[name=_method]').val('PATCH');
    $('#modal-status form')[0].reset();
    $('#id_status').val(id);
}

function between(x, min, max) {
    return x >= min && x <= max;
}

function DateClick(title, id, time, date) {
    // if (between(time, '11:0', '12:0')) {
    //     swal({
    //         title: "Break Time",
    //         icon: "success",
    //     })
    // }else{
    // }
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-appointment form')[0].reset();
    $('#id_rooms').val(id).change();
    $('#therapist').val(id).change();
    $("#modal-appointment").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    $(".modal-title").text(title);
    $("#start").val(time);
    $("#datepicker").val(date);
    $("#point_date").val(date);

}

function addCustomer() {
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-customers form')[0].reset();
    $("#modal-customers").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    $('.modal-title').text('New Customers');

    $('#join_date').datepicker('setDate', 'today');
    $('#datepicker').datepicker('setDate', '01/01/1985');
}

function addAppointment() {
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-appointment form')[0].reset();
    $("#modal-appointment").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });

    $('#id_rooms').val(' ').change().attr("disabled", false);
    $('.modal-title').val('').text('New Appointment');
    $("#start").val('08:0');
    $('#point_date').datepicker('setDate', 'today')
}

function detailCustomer(id, induk, name, phone, birth) {
    $('#customer').val(induk);
    $('#suggesstion-box').fadeOut();
    $('#customer_id').val(id);
    $('#full').html(name);
    $('#contact').text(phone);
    $('#birthDate').text(birth);
}

function chooseTreatment() {
    let id = $('#pack_name').val();
    let start = $('#start').val();

    $.ajax({
        url: "/treat/" + id + "/" + start,
        type: "GET",
        dataType: "JSON",
        success: function (data) {

            $('#code').text(data.treatment.product_treatment_code);
            $('#name').text(data.treatment.product_treatment_name);
            $('#duration').text(data.treatment.duration);
            $('#start_time').text(data.start);
            $('#start_input').val(data.start);
            $('#end').text(data.end);
            $('#end_time').val(data.end);

        },
        error: function () {
            alert("Can not Show the Data!");
        }
    });
}

function changeTime() {

    $(document).on('dp.change', 'input.timepicker', function () {
        var sip;
        let id = $('#pack_name').val();

        sip = '';
        if (id) {
            sip = $('#pack_name').val();
            let start = $('#start').val();
            $.ajax({
                url: "/treat/" + sip + "/" + start,
                type: "GET",
                dataType: "JSON",
                success: function (data) {

                    $('#code').text(data.treatment.product_treatment_code);
                    $('#name').text(data.treatment.product_treatment_name);
                    $('#duration').text(data.treatment.duration);
                    $('#start_time').text(data.start);
                    $('#start_input').val(data.start);
                    $('#end').text(data.end);
                    $('#end_time').val(data.end);

                },
                error: function () {
                    alert("Can not Show the Data!");
                }
            });
        }
    });

}
