import LogisticItemIndex from './logistics/items/Index.vue';
import LogisticItemCreate from './logistics/items/Create.vue';
import LogisticItemEdit from './logistics/items/Edit.vue';
import LogisticItemDetail from './logistics/items/Detail.vue';

import LogisticPurchaseOrderIndex from './logistics/purchases/orders/Index.vue';
import LogisticPurchaseOrderCreate from './logistics/purchases/orders/Create.vue';
import LogisticPurchaseOrderDetail from './logistics/purchases/orders/Detail.vue';

import LogisticPurchaseReceiveIndex from './logistics/purchases/receives/Index.vue';
import LogisticPurchaseReceiveCreate from './logistics/purchases/receives/Create.vue';
import LogisticPurchaseReceiveDetail from './logistics/purchases/receives/Detail.vue';

import LogisticReportCard from './logistics/reports/Card.vue';
import LogisticReportOpname from './logistics/reports/Opname.vue';
import LogisticInventoryAdjustment from './logistics/inventories/AdjustmentIndex.vue';
import LogisticInventoryAdjustmentDetail from './logistics/inventories/AdjustmentDetail.vue';

import ManufactureWorkOrderIndex from './manufactures/work_orders/Index.vue';
import ManufactureWorkOrderCreate from './manufactures/work_orders/Create.vue';
import ManufactureWorkOrderDetail from './manufactures/work_orders/Detail.vue';
import ManufactureMaterialDetail from './manufactures/materials/Detail.vue';
import ManufactureMaterialReleaseIndex from './manufactures/materials/ReleaseIndex.vue';
import ManufactureMaterialReleaseCreate from './manufactures/materials/ReleaseCreate.vue';
import ManufactureFinishes from './manufactures/Finishes.vue';

import MallProductIndex from './malls/products/Index.vue';
import MallProductCreate from './malls/products/Create.vue';
import MallOrderIndex from './malls/orders/Index.vue';
import MallOrderDetail from './malls/orders/Detail.vue';
import MallProductDetail from './malls/products/Detail.vue';
import MallProductEdit from './malls/products/Edit.vue';

import AnamnesaCreate from './anamnesas/Create.vue';
import AnamnesaDetail from './anamnesas/Detail.vue';


const routes = [
    { path: '/logistics/items', component: LogisticItemIndex, name: 'LogisticItemIndex' },
    { path: '/logistics/items/create', component: LogisticItemCreate, name: 'LogisticItemCreate' },
    { path: '/logistics/items/:id', component: LogisticItemDetail, name: 'LogisticItemDetail' },
    { path: '/logistics/items/:id/edit', component: LogisticItemEdit, name: 'LogisticItemEdit' },


    { path: '/logistics/purchases/orders', component: LogisticPurchaseOrderIndex, name: 'LogisticPurchaseOrderIndex' },
    { path: '/logistics/purchases/orders/create', component: LogisticPurchaseOrderCreate, name: 'LogisticPurchaseOrderCreate' },
    { path: '/logistics/purchases/orders/:id', component: LogisticPurchaseOrderDetail, name: 'LogisticPurchaseOrderDetail' },

    { path: '/logistics/purchases/receives', component: LogisticPurchaseReceiveIndex, name: 'LogisticPurchaseReceiveIndex' },
    { path: '/logistics/purchases/receives/create', component: LogisticPurchaseReceiveCreate, name: 'LogisticPurchaseReceiveCreate' },
    { path: '/logistics/purchases/receives/:id', component: LogisticPurchaseReceiveDetail, name: 'LogisticPurchaseReceiveDetail' },
    
    { path: '/logistics/reports/cards', component: LogisticReportCard, name: 'LogisticReportCard' },
    { path: '/logistics/reports/opname', component: LogisticReportOpname, name: 'LogisticReportOpname' },
    { path: '/logistics/adjustments', component: LogisticInventoryAdjustment, name: 'LogisticInventoryAdjustment' },
    { path: '/logistics/adjustments/:id', component: LogisticInventoryAdjustmentDetail, name: 'LogisticInventoryAdjustmentDetail' },
    
    { path: '/manufactures/work_orders', component: ManufactureWorkOrderIndex, name: 'ManufactureWorkOrderIndex' },
    { path: '/manufactures/work_orders/create', component: ManufactureWorkOrderCreate, name: 'ManufactureWorkOrderCreate' },
    { path: '/manufactures/work_orders/:id', component: ManufactureWorkOrderDetail, name: 'ManufactureWorkOrderDetail' },
    { path: '/manufactures/materials/releases', component: ManufactureMaterialReleaseIndex, name: 'ManufactureMaterialReleaseIndex' },
    { path: '/manufactures/materials/releases/create', component: ManufactureMaterialReleaseCreate, name: 'ManufactureMaterialReleaseCreate' },
    { path: '/manufactures/materials/:id/wodet', component: ManufactureMaterialDetail, name: 'ManufactureMaterialDetail' },
    { path: '/manufactures/finishes', component: ManufactureFinishes, name: 'ManufactureFinishes' },
    
    { path: '/malls/products', component: MallProductIndex, name: 'MallProductIndex' },
    { path: '/malls/products/create', component: MallProductCreate, name: 'MallProductCreate' },
    { path: '/malls/orders', component: MallOrderIndex, name: 'MallOrderIndex' },
    { path: '/malls/orders/:id', component: MallOrderDetail, name: 'MallOrderDetail' },
    { path: '/malls/products/:id', component: MallProductDetail, name: 'MallProductDetail' },
    { path: '/malls/products/:id/edit', component: MallProductEdit, name: 'MallProductEdit' },

    { path: '/anamnesas/:patient_id/create', component: AnamnesaCreate, name: 'AnamnesaCreate' },
    { path: '/anamnesas/:id', component: AnamnesaDetail, name: 'AnamnesaDetail' },
    
];

export default routes;
