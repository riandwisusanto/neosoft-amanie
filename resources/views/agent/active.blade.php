@extends('base')

@section('title')
Agent List
@endsection

@section('breadcrumb')
@parent
<li>Agent List</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">Active Agent</span>
                <p style="font-size:15px;"><i>List sales agent dengan status aktif.</i></p>
            </div>
            <div class="box-body">
                <table class="activeoke table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th>Reg Date</th>
                            <th>Agent Name</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('agent.detail')
@endsection

@section('script')
<script type="text/javascript">
    var table, table_active, table_detail, save_method;

$(function () {
    table = $('.activeoke').DataTable({
        "processing": true,
        "serverside": true,
        "ajax": {
            "url": "{{ route('agent.activeData') }}",
            "type": "GET"
        }
    });
});

function takeOut(id) {
    swal({
        title: "Are you Sure?",
        text: "Agent Will be Disabled",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : "/agent/"+id+"/takeout",
                type : "POST",
                data : {'_method' : 'patch', '_token' : $('meta[name=csrf-token]').attr('content')},
                success : function(data){
                    swal({
                        icon: "success"
                    })
                    table.ajax.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error deleting!", "Please try again", "error");
                }
            });
        }
    });
}

function detail(id) {
    
    $.ajax({
        url : "/agent/"+id+"/detail",
        type: 'GET',
        contentType: "application/json",                
        success: function(data) {
            $('#modal-form').modal('show');
            $(".detail-oke").dataTable().fnDestroy()                   
            table_detail = $('.detail-oke').DataTable({
                    "ordering" : false
            });
            
            table_detail.clear();
            table_detail.rows.add(data.data);
            table_detail.draw();     
        },
        error: function () {
            alert('error');
        }
    });

}
</script>
@endsection