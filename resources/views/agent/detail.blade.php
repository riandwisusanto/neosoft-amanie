<div class="modal" id="modal-form" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times fa-lg"></i>
                </button>
                <h3 class="modal-title">Detail Agent</h3>
            </div>
            <div class="modal-body">
                <div class="box-body">                
                    <table class="detail-oke table table-striped table-bordered nowrap table-collection">
                        <thead>
                            <tr>
                                <th>Reff Number</th>
                                <th>Full Name</th>
                                <th>Contact</th>
                                <th>KTP ID</th>
                                <th>Email</th>
                                <th>Numb Of Sales</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Reff Number</td>
                                <td>Full Name</td>
                                <td>Contact</td>
                                <td>KTP ID</td>
                                <td>Email</td>
                                <td>Numb Of Sales</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                        class="fa fa-arrow-circle-left"></i> Cancel</button>
            </div> --}}
        </div>
    </div>
</div>