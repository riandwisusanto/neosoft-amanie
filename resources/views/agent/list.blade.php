@extends('base')

@section('title')
Agent List
@endsection

@section('breadcrumb')
@parent
<li>Agent List</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">Agent List</span>
                <p style="font-size:15px;"><i>List data pendaftar sales agent.</i></p>
            </div>
            <div class="box-body">
                <table class="list table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th width="10">No</th>
                            <th>Reg Date</th>
                            <th>Agent Name</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">Rejected List</span>
                <p style="font-size:15px;"><i>List data pendaftar sales agent yang ditolak.</i></p>
            </div>
            <div class="box-body">
                <table id="rejected" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th width="10">No</th>
                            <th>Reg Date</th>
                            <th>Agent Name</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    var table, table_active, save_method;

    $(function () {
        table = $('.list').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('agent.listData') }}",
                "type": "GET"
            }
        });

        table_active = $('#rejected').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('agent.rejectData') }}",
                "type": "GET"
            }
        });
    });

    function deleteData(id) {
        swal({
            title: "Are you Sure?",
            text: "Agent Will be Deleted",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : "/agent/"+id,
                    type : "POST",
                    data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        swal({
                            icon: "success"
                        })
                        table.ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            }
        });
    }

    function approve(id) {
        swal({
            title: "Are you Sure?",
            text: "Agent Will be Approve",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : "/agent/"+id,
                    type : "PATCH",
                    data : {'_method' : 'PATCH', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        swal({
                            icon: "success"
                        })
                        table.ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error approve!", "Please try again", "error");
                    }
                });
            }
        });
    }

    function enableAgent(id) {
        swal({
            title: "Are you Sure?",
            text: "Agent Will be Enabled",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : "/agent/"+id+"/enable",
                    type : "PATCH",
                    data : {'_method' : 'PATCH', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        swal({
                            icon: "success"
                        })
                        table_active.ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error approve!", "Please try again", "error");
                    }
                });
            }
        });
    }
</script>
@endsection