<div class="modal" id="modal-action" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times;
          </span> </button>
        <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body">

        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-3 control-label">Date</label>
            <div class="col-sm-9">
              <p class="form-control-static" id="date"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Start/End</label>
            <div class="col-sm-9">
              <p class="form-control-static" id="time"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Outlet</label>
            <div class="col-sm-9">
              <p class="form-control-static" id="outlet_detail"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Name</label>
            <div class="col-sm-9">
              <p class="form-control-static" id="name_detail"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Phone Number</label>
            <div class="col-sm-9">
              <p class="form-control-static" id="phone"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Create By</label>
            <div class="col-sm-9">
              <p class="form-control-static" id="createBy"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
              <p class="form-control-static" id="status_detail"></p>
            </div>
          </div>
          <div class="hidden-xs">
            <table class="table table-striped table-bordered" style="width:100%">
              <label class="control-label">Item</label>
              <caption>Detail Item.</caption>
              <thead>
                <tr>
                  <th>Start</th>
                  <th>End</th>
                  <th>Treatments</th>
                  <th>Therapist</th>
                </tr>
              </thead>
              <tbody id='detail'>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="modal-footer" id="action-footer">
      </div>
    </div>
  </div>
</div>
