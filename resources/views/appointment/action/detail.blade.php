@extends('base')

@section('title')
Appointments Usage Detail
@endsection

@section('breadcrumb')
@parent
<li>Appointments</li>
<li>Appointments Usage Detail</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                {{-- <h2>{{ $appointment->room->room_name }}</h2> --}}
            </div>
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Date</label>
                        <div class="col-sm-10">
                            <p class="form-control-static" id="date">{{ $appointment->come_date }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Outlet</label>
                        <div class="col-sm-10">
                            <p class="form-control-static" id="outlet_detail">{{ $appointment->outlet->outlet_name }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static" id="name_detail">{{ $appointment->customer->full_name }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Phone Number</label>
                        <div class="col-sm-10">
                            <p class="form-control-static" id="phone">{{ $appointment->customer->phone }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Create By</label>
                        <div class="col-sm-10">
                            <p class="form-control-static" id="createBy">
                                @if ($appointment->user)
                                {{ $appointment->user->name }}
                                @else
                                Create by Apps
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <p class="form-control-static" id="status_detail">
                                @if ($appointment->status_appointment == 1)
                                Booking
                                @elseif ($appointment->status_appointment == 2)
                                Confirmed
                                @elseif ($appointment->status_appointment == 3)
                                Complete
                                @elseif ($appointment->status_appointment == 4)
                                Cancelled
                                @else
                                No Show
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Item</label>
                        <div class="col-sm-10">
                            <table class="table table-striped table-bordered" style="width:100%">
                                <caption>Detail Item.</caption>
                                <thead>
                                    <tr>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Treatments</th>
                                        <th>Therapists</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $item)
                                    @php
                                        if($item->treatment_id){
                                            $product = $item->product_treatment->product_treatment_name;
                                        }else{
                                            $product = "Consultasi";
                                        }
                                    @endphp
                                        @if ($item->come_date == $appointment->come_date)
                                            <tr>
                                                <td>{{ $item->start_time }}</td>
                                                <td>{{ $item->end_time }}</td>
                                                <td>{{ $product }}</td>
                                                @php
                                                $getTherapist='';
                                                foreach ($item->therapist as $therapistx) {
                                                $getTherapist .= $therapistx->therapist_name.',';
                                                }
                                                @endphp
                                                <td>{{ $getTherapist }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Remarks</label>
                        <div class="col-sm-10">
                            <p class="form-control-static" id="status_detail"> {{ $appointment->remarks }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status Change</label>
                        <div class="col-sm-10">
                            <table class="table table-striped table-bordered" style="width:100%">
                                <caption>Log Change</caption>
                                <thead>
                                    <tr>
                                        <th>Date Time</th>
                                        <th>Status</th>
                                        <th>Changed By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($log as $logs)

                                    <tr>
                                        <td>{{ $logs->date_time }}</td>
                                        <td>
                                            @if ($logs->log_status == 1)
                                            Booking
                                            @elseif ($logs->log_status == 2)
                                            Confirmed
                                            @elseif ($logs->log_status == 3)
                                            Complete
                                            @elseif ($logs->log_status == 4)
                                            Cancelled
                                            @else
                                            No Show
                                            @endif
                                        </td>
                                        <td>{{ $logs->name }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-right">
                @if (array_intersect(["ADMINISTRATOR"], json_decode(Auth::user()->level)))
                <a class="btn btn-danger" onclick="deleteAppointment({{ $appointment->id_appointments }})"><i
                        class="fa fa-trash"></i> Delete</a>
                @endif
                <a class="btn btn-success" onclick="updateAppointment({{ $appointment->id_appointments }})"><i
                        class="fa fa-edit"></i> Update</a>
                <a class="btn btn-primary" onclick="rescheduleAppointment({{ $appointment->id_appointments }})"><i
                class="fa fa-edit"></i> Reschedule</a>
                <button type="button" class="btn btn-warning"
                    onclick="changeStatus({{ $appointment->id_appointments }})"><i class="fa fa-exchange"></i> Change
                    Status</button>
            </div>
        </div>
    </div>
</div>
@include('appointment.action.reschedule')
@include('appointment.action.appointment')
@include('appointment.action.status')
@endsection

@section('script')

<script>
    $('#start,#start_new').datetimepicker({
    format: 'HH:mm'
});
$('#datepickerxx').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true
});

$('#datepicker, #join_date, #point_date,#point_date_new').datepicker({
  format: 'dd/mm/yyyy',
  autoclose: true,
  todayHighlight: true
});

$(function () {
    changeTime();
    $('#modal-status form').validator().on('submit', function(e){
        if(!e.isDefaultPrevented()){
            var id_status = $('#id_status').val();

            $.ajax({
                url : "by-room/"+id_status+"/status",
                type : "POST",
                data : $('#modal-status form').serialize(),
                success : function(data){
                    $('#modal-status').modal('hide');
                    location.reload();
                },
                error : function(){
                    alert("Can not Save the Data!");
                }
            });
        return false;
        }
    });

    $('#modal-reschedule form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('appointment.reschedule.storeReschedule',$appointment->id_appointments) }}";

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-reschedule form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-reschdeule').modal('hide');
              window.location.href = "/by-therapist";
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });  

    $('#modal-appointment form').validator().on('submit', function(e){
        if(!e.isDefaultPrevented()){

            $.ajax({
            url : "/appointment/{{ $appointment->id_appointments }}",
            type : "POST",
            data : $('#modal-appointment form').serialize(),
            success : function(data){
                $('#modal-appointment').modal('hide');
                location.reload();
            },
            error : function(xhr){
                swal({
                    title: xhr.responseJSON,
                    icon: "error",
                })
            }
            });
            return false;
        }
    });
});

    function rescheduleAppointment(id)
    {
        $('input[name=_method]').val('POST');
        
        save_method = "add";

        $.ajax({
            url : "/appointment/"+id+"/edit",
            type : "GET",
            dataType : "JSON",
            success : function(data){
                $('#modal-reschedule').modal('show');
                $('.modal-titles').text('Reschedule Appointment');

                $('#customer_id_new').val(data.show.customer_id);
                $('#fulln').html(data.show.full_name);
                $('#fulln').html(data.show.customer.full_name);

                $('#status_new').val(data.show.status_appointment);
                $('#outlet_new').val(data.show.outlet_id).change();
                $('#activity_new').val(data.show.activity_id).change();
                $('#pack_name_new').val(data.show.treatment_id).change();
                var select = document.getElementById( 'therapist_new' );
                for ( var i = 0, l = select.options.length, o; i < l; i++ )
                {
                    o = select.options[i];
                    if ( data.therapist.indexOf( o.value ) != -1 )
                    {
                        o.selected = true;
                    }
                }
                $('#group_therapist').hide();
                
                
            },
            error : function(){
                 alert("Can not Show the Data!");
            }
        });

    }
    function updateAppointment(id) {
        $('input[name=_method]').val('PATCH');
        $('#modal-appointment form')[0].reset();
        $.ajax({
            url : "/appointment/"+id+"/edit",
            type : "GET",
            dataType : "JSON",
            success : function(data){
                $('#modal-appointment').modal('show');
                $('.modal-title').text('Edit Kategori');

                $('#start').val(data.show.start_time);
                var select = document.getElementById( 'therapist' );
                for ( var i = 0, l = select.options.length, o; i < l; i++ )
                {
                    o = select.options[i];
                    if ( data.therapist.indexOf( o.value ) != -1 )
                    {
                        o.selected = true;
                    }
                }

                $('#customer').val(data.show.customer.induk_customer);
                $('#customer_id').val(data.show.customer_id);
                $('#full').html(data.show.full_name);
                $('#contact').text(data.show.phone);
                $('#birthDate').text(data.show.birth_date);
                $('#outlet').val(data.show.outlet_id).change();
                $('#activity').val(data.show.activity_id).change();
                $('#point_date').val(data.show.come_date);
                $('#status').val(data.show.status_appointment).change();
                if (data.show.treatment_id) {
                    $('#pack_name').val(data.show.treatment_id).change();
                }else{
                    $('#pack_name').attr("disabled", true);
                    $('#activity').attr("disabled", true);
                    $('#siap').addClass('hidden');
                    $('#siapOke').removeClass('hidden');
                }
                $('#id_rooms').val(data.show.room_id).change();
                $('#remarks').val(data.show.remarks);

                $('#full').html(data.show.customer.full_name);
                $('#contact').text(data.show.customer.phone);
                $('#birthDate').text(data.show.customer.birth_date);
            },
            error : function(){
                 alert("Can not Show the Data!");
            }
        });
    }

    $(document).ready(function(){
        $('#customer').keyup(function(){
            var query = $(this).val();
            if(query != '')
            {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:"{{ route('customer.keyword') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                $('#suggesstion-box').fadeIn();
                $('#suggesstion-box').html(data);
                }
            });
            }else{
                var _token = $('input[name="_token"]').val();
                $.ajax({
                url:"{{ route('customer.all') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                    $('#suggesstion-box').fadeIn();
                    $('#suggesstion-box').html(data);
                }
                });
            }
        });
    });

    function detailCustomer(id, induk, name, phone, birth) {
        $('#customer').val(induk);
        $('#suggesstion-box').fadeOut();
        $('#customer_id').val(id);
        $('#full').html(name);
        $('#contact').text(phone);
        $('#birthDate').text(birth);
    }

    function deleteAppointment(id){
        swal({
            title: "Are you sure?",
            text: "Appointment will be deleted?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
            $.ajax({
                url : "/appointment/"+id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success : function(data){
                    window.location.href = "{{ route('by-room') }}";
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error deleting!", "Please try again", "error");
                }
            });
            }
        });
    }
</script>
<script src="{{ asset('js/room.js') }}"></script>
@endsection
