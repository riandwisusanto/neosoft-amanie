@extends('base')

@section('title')
Patient's Birthday
@endsection

@section('breadcrumb')
@parent
<li>Patient's Birthday</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">PATIENT'S BIRTHDAY</span>
                        <p style="font-size:15px; font-weight: bold;"><i>List of patients’ birthday for a specific month.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <div class="btn-float-right">
                            <a onclick="showFilter()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                            <a onclick="addWa()" class="btn btn-success"><i class="fa fa-whatsapp"></i> Add WA Blast</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Patient</th>
                            <th>Reference no.</th>
                            <th>Outlet</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Birthday</th>
                            <th>Address</th>
                            <th>Last Transaction</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('customerBirthdays.form')
@include('customerBirthdays.wa')
@endsection

@section('script')
<script type="text/javascript">
    var table, outlets = [], months = [];
    $('#from, #to').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    function selected_all() {
        if ($("#chkall").is(':checked')) {
            $("#outlet > option").prop("selected", "selected");
            $("#outlet").trigger("change");
        } else {
            $("#outlet").find('option').prop("selected", false);
            $("#outlet").trigger("change");
        }
    }
    $('#filter_submit').on('click', function () {
        $("#modal-form").hide();
        table.ajax.url("{{ route('customerBirthdays.data') }}?" + $('#form_data').serialize()).load();
        months = $('#month').val();
        outlets = $('#outlet').val();
    });

    $(function () {
        table = $('.table').DataTable({
            "processing": true,
            "serverside": true,
            "responsive": {
                "details": {
                    "type": 'column',
                    "target": 'tr'
                }
            },
            "columnDefs": [{
                "className": 'control',
                "orderable": false,
                "targets": 0
            }],
            "order": [1, 'asc'],

            "ajax": {
                "url": "{{ route('customerBirthdays.data') }}",
                "type": "GET"
            }
        });

        Filevalidation = () => {
            const fi = document.getElementById('fileName');
            // Check if any file is selected. 
            if (fi.files.length > 0) {
                for (let i = 0; i <= fi.files.length - 1; i++) {

                    const fsize = fi.files.item(i).size;
                    const file = Math.round((fsize / 1024));
                    // The size of the file. 
                    if (file >= 1024) {
                        swal("File too Big, please select a file less than 1mb")
                        // alert( 
                        //   "File too Big, please select a file less than 1mb");
                        fi.value = "";
                    }
                }
            }
        }
        $('#modal-message form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                if (save_method == "add") url = "{{ route('customerBirthdays.store') }}";

                $.ajax({
                    url: url,
                    type: "POST",
                    data: new FormData($('#modal-message form')[0]),
                    processData: false,
                    contentType: false,
                    //  data : $('#modal-message form').serialize(),
                    success: function (data) {
                        swal({
                                title: "Successfully Create Blast",
                                icon: "success",
                                buttons: {
                                    canceled: {
                                        text: 'Cancel',
                                        value: 'cancel',
                                        className: 'swal-button btn-default'
                                    },
                                    sendwa: {
                                        text: 'Send WhatsApp',
                                        value: 'sendwa',
                                        className: 'swal-button btn-success'
                                    }
                                },
                                dangerMode: true,
                            })
                            .then((option) => {
                                switch (option) {
                                    default:
                                        window.location.href = "/customer-birthdays";
                                        break;
                                    case 'sendwa':
                                        $.ajax({
                                            url: "/customer-birthdays/" + data.id_message +
                                                "/wa",
                                            type: "GET",
                                            success: function (x) {
                                                if (data) {
                                                    swal({
                                                        text: 'Send WA Success',
                                                        icon: 'success'
                                                    })
                                                    window.location.href =
                                                        "/customer-birthdays";
                                                }
                                                return false;
                                            },
                                            error: function () {
                                                swal({
                                                    text: 'Send WA Error',
                                                    icon: 'error'
                                                })
                                                return false;
                                            }
                                        });
                                        break;
                                }
                            });
                    },
                    error: function (e) {
                        console.log(e)
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });
    });

    function showFilter() {
        $("#modal-form").show();
        $("#outlet").val('').trigger("change");
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Change Period');
    }
    console.log(months);
    function addWa() {
        if (months != '') {
            $("#save").attr("disabled", false);
        } else {
            $("#save").attr("disabled", true);
        }
        save_method = "add";
        $("#months").val(months);
        $("#outlets").val(outlets);
        $('input[name=_method]').val('POST');
        $("#modal-message").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#modal-message form')[0].reset();
        $('.modal-title').text('Add WA Blast');
    }


    function closeModal() {
        $("#modal-form").hide();
        $("#modal-message").hide();
    }

</script>
@endsection
