<div class="box box-default">
    <div class="box-header">
        <h3>Active Invoice</h3>
    </div>        
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>Invoice</th>
					<th>Invoice.date</th>
					<th>Expired.date</th>
					<th>Amount</th>
					<th>Paid amount</th>
					<th>Remarks</th>
					<th>Paid</th>
					<th>Usage</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
                @php
                    $last = $active_invoice->last();
                @endphp
                @foreach ($active_invoice as $invoice)
                    @php
                        $payment = 0;
                        foreach ($invoice->payment as $payments) {
                            $payment += str_replace(",", "", $payments->amount);
                        }

                        if ($invoice->remaining == 0) {
                            $status_paid = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                        }else {
                            $status_paid = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                        }

                        foreach ($invoice->invoice_detail as $package) {
                            foreach ($package->invoice_package as $qty) {
                                if ($qty->qty == 0) {
                                    $usage = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                                }else {
                                    $usage = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                }
                            }
                        }

                        if ($last->id_invoice == $invoice->id_invoice){
                            $color = '<span class="label label-danger">New</span>';
                        }
                        else{
                            $color = "";
                        }
                        
                    @endphp
                    <tr>
                        <td>{{ $invoice->inv_code }} {!! $color !!}</td>
                        <td>{{ $invoice->inv_date }}</td>
                        <td>{{ $invoice->expired_date }}</td>
                        <td>{{ $invoice->total }}</td>
                        <td>{{ format_money($payment) }}</td>
                        <td>{{ $invoice->remarks }}</td>
                        <td>{!! $status_paid !!}</td>
                        <td>{!! $usage !!}</td>
                        <td>
                            
                            <a href="{{ route('customer.active', $invoice->id_invoice) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a>
                            <a onclick='printWa({{ $invoice->id_invoice }})' class='btn btn-success btn-sm'><i
                                class="fa fa-whatsapp" aria-hidden="true"></i></a>               
                            @if ( ! isset($invoice->customer_sign))
                            <a href="{{ route('invoice.sign', $invoice->id_invoice) }}" class='btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
			</tbody>
		</table>
    </div>
</div>

<script>
// $(function () {
//     printWa();
// })

function printWa(id){
    $.ajax({
        url : "/invoice-create/"+id+"/wa",
        type : "GET",
        success: function (data) {
            if (data) {
                swal({
                    text: 'Sand WA Success',
                    icon: 'success'
                })
            }
            return false;
        },
        error: function (e) {
            alert("Can not Save the Data!");
        }
    });
    return false;
}
</script>

<script>
// $(function () {
//     printWa();
// })

function printWa(id){
    $.ajax({
        url : "/invoice-create/"+id+"/wa",
        type : "GET",
        success: function (data) {
            if (data) {
                swal({
                    text: 'Send WA Success',
                    icon: 'success'
                })
            }
            return false;
        },
        error: function (e) {
            alert("Can not Save the Data!");
        }
    });
    return false;
}
</script>
