@extends('base')

@section('title')
Anamnesa
@endsection

@section('breadcrumb')
@parent
<li>Anamnesa</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">Create Anamnesa</span>
                <p style="font-size:15px;"><i>Create new anamnesa.</i></p>
            </div>

            
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="/hi"></script>
@stack('script')
@endsection
