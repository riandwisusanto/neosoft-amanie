<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">ANAMNESA</span>
    </div>
    <div class="col-xs-12" style="margin-top: 5px;">
        <button onclick="createAnamnesa()" class="btn btn-success" >Create New Anamnesa</button>
        <br><br>
    </div>
</div>
<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-anamnesa">Anamnesa History <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-anamnesa-history">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-capitalize">
                        <th>Anamnesa Code</th>
                        <th>Anamnesa Date</th>
                        <th>Anamnesa</th>
                        <th>Suggestion</th>
                        <th>Product/Treatment</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th class="text-center">Delete</th>
                    </tr>
                </thead>
                    @foreach ($anamnesa as $item)
                    @php
                    $product ='';
                    $qty ='';
                    $price ='';

                    foreach($item->anamnesa_detail as $detail)
                    {
                        @$product .= "&middot;".$detail->product_treatment->product_treatment_name."<br>";
                        @$qty .= "(".$detail->qty.")"."<br>";
                        @$price .=number_format($detail->total / $detail->qty)."<br>";

                    }
                    @endphp
                    <tr>
                        <td>{{ $item->anamnesa_code }}</td>
                        <td>{{ $item->date_anamnesa }}</td>
                        <td>{{ $item->anamnesa }}</td>
                        <td>{{ $item->suggestion }}</td>
                        <td>{!!  @$product !!}</td>
                        <td>{!! @$qty !!}</td>
                        <td>{!! @$price !!}</td>
                        <td>
                        <form
                            action="{{route('customers.anamnesa.destroy',$item->id_anamnesa)}}"
                            method="post">
                    {{csrf_field()}} @csrf @method('patch')
                            <button type="submit" class="btn btn-danger btn-xs">
                                <i class="fa fa-trash"></i>
                            </button>
                            <a href="{{ url('/') }}/new/#/anamnesas/{{ $item->id_anamnesa }}" class="btn btn-warning btn-xs" ><i
                            class="fa fa-credit-card"></i></a>
                        </form>
                    
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>