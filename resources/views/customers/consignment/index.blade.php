<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">CONSIGNMENT FORM</span>
    </div>
    <div class="col-xs-12" style="margin-top: 5px;">
        <button onclick="addConsignment()" class="btn btn-success" >Create New Consignment Form</button>
        <br><br>
    </div>
</div>
<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-consignment-history">Consignment History <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-consignment-history">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-capitalize">
                        <th>Consignment Date</th>
                        <th>Image</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                @foreach($consignment as $con)
                    <tr>
                        <td>{{ $con->consignment_date->format('d/m/Y') }}</td>
                        <td><a class="btn" onclick="showPict({{ $con->id_customer_consignment}})"><img src="{{ asset('storage'.'/'.$con->image) }}" alt="" style="width: 100px;"></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

