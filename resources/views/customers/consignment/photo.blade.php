<div class="modal" id="modal-photo" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
                <form  class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title" id="titlep"></h3>
                </div>
                <div class="modal-body" >
                    <input type="hidden" id="id" name="id_customer">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Image Detail</label>
                                            <div class="tampil-image"></div>
                                        </div>
                                </div>     
                            </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-modal"><i class="fa fa-arrow-circle-left"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
     </div>
</div>
