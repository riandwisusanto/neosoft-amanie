@extends('base')

@section('title')
List Patient
@endsection

@section('breadcrumb')
@parent
<li>Patient</li>
@endsection

@section('content')
<div class="loader hidden"></div>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">PATIENT</span>
                        <p style="font-size:15px; font-weight: bold;"><i>All patient lists.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Patient</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr class="text-capitalize">
                            <th></th>
                            <th class="all">Reff</th>
                            <th>Fullname</th>
                            <th>Phone</th>
                            <th>Membership</th>
                            <th>Join Date</th>
                            <th>Branch</th>
                            {{-- <th>Consultant</th> --}}
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('customers.form')
@include('customers.medicalAlert')
@endsection

@section('script')
<script>
    var table, save_method;

    $('#datepicker, #join_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $(function() {
        chooseCountry();
        validation();

        table = $('.table').DataTable({
            "processing": true,
            "serverSide": true,
            'language': {
                'loadingRecords': '&nbsp;',
                'processing': 'Loading...'
            },
            "responsive": {
                "details": {
                    "type": 'column',
                    "target": 'tr'
                }
            },
            "columns": [
                {
                    "name": "divider",
                    "data": "divider"
                },
                {
                    "name": "induk_customer",
                    "data": "induk_customer"
                },
                {
                    "name": "full_name",
                    "data": "full_name"
                },
                {
                    "name": "phone",
                    "data": "phone"
                },

                {
                    "name": "rank",
                    "data": "rank",
                    "sortable": false,
                },
                {
                    "name": "join_date",
                    "data": "join_date"
                },
                {
                    "name": "outlet",
                    "data": "outlet"
                },
                {
                    "name": "action",
                    "data": "action",
                    "sortable": false,
                    "searchable": false,
                },
            ],
            "columnDefs": [{
                "className": 'control',
                "orderable": false,
                "targets": 0
            }],
            "order": [1, 'asc'],


            "ajax": {
                "url": "{{ route('customers.data') }}",
                "type": "GET"
            }
        });

        $('#modal-customers form').validator().on('submit', function(e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                if (save_method == "add") url = "{{ route('customers.store') }}";
                else url = "customers/" + id;
                $.ajax({
                    url: url,
                    type: "POST",
                    data: $('#modal-customers form').serialize(),
                    success: function(data) {
                        if (data.status == false) {
                            alert('Something Wrong!!, Please Check Message Error!!!');
                            $("#msg_email").html(data.message.email).show();
                            $("#msg_phone").html(data.message.phone).show();
                        } else {
                            $('#gender').val('').trigger('change');
                            $('#religion').val('').trigger('change');
                            $('#status').val('').trigger('change');
                            $('#country').val('').trigger('change');
                            $('#outlet').val('').trigger('change');
                            $('#city').val('').trigger('change');
                            $('#consultant').val('').trigger('change');
                            $('#modal-customers').modal('hide');
                            $("#msg_email").html('');
                            $("#msg_phone").html('');
                            table.ajax.reload();
                            return false;
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });

        $('#modal-medical form').validator().on('submit', function(e) {
            if (!e.isDefaultPrevented()) {
                $.ajax({
                    url: "{{ route('medical-alert.store') }}",
                    type: "POST",
                    data: $('#modal-medical form').serialize(),
                    success: function(data) {
                        $('#modal-medical').modal('hide');
                        //   table.ajax.reload();
                        return false;
                    },
                    error: function(e) {
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });

    });

    function addForm() {
        save_method = "add";
        $('#modal-customers form')[0].reset();
        $('input[name=_method]').val('POST');
        $("#modal-customers").modal('show');
        $('#btn-more').css('margin-left', '0px');
        $('#modal-customers form')[0].reset();
        $('.modal-title').text('ADD PATIENT');
        $('.modal-doc').text('Form for a new patient.');

        $('#kode').attr('readonly', false);
        $('#group_outlet').show();
        $('#group_consultant').show();
        $('#join_date').datepicker('setDate', 'today');
        $('#datepicker').datepicker('setDate', '01/01/1985');
    }

    function getCity(country, city) {
        $.ajax({
            url: 'customers/' + country + '/province',
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {
                $(".loader").removeClass('hidden');
            },
            success: function(data) {
                $('select[name="city"]').empty();
                $(".loader").addClass('hidden');
                for (let index in data) {
                    $('select[name="city"]').append('<option value="' + index + '">' + data[index] + '</option>')
                }
                if (city) {
                    $('select[name="city"]').val(city).change();
                }
                return false;
            }
        });
    }

    function chooseCountry() {
        $('#country').change(function() {
            var countryID = $(this).val();
            if (countryID) {
                getCity(countryID);
            }
            return false;
        });
    }

    function editForm(id) {
        save_method = "edit";
        $('input[name=_method]').val('PATCH');
        $('#modal-customers form')[0].reset();
        $.ajax({
            url: "customers/" + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#modal-customers').modal('show');
                $('.modal-title').text('EDIT PATIENT');
                $('.modal-doc').text('Edit patient’s data.');
                $('#btn-more').css('margin-left', '50%');
                $('#id').val(data.edit.id_customer);
                $('#fullname').val(data.edit.full_name);
                $('#phone').val(data.edit.phone);
                $('#email').val(data.edit.email);
                $('#place').val(data.edit.birth_place);
                $('#datepicker').val(data.edit.birth_date);
                $('#join_date').val(data.edit.join_date);
                $('#gender').val(data.edit.gender).trigger('change');
                $('#religion').val(data.edit.religion).trigger('change');
                $('#status').val(data.edit.status_customer).trigger('change');
                $('#noktp').val(data.edit.ktp);
                $('#address').val(data.edit.address);
                $('#occupation').val(data.edit.occupation);
                $('#zip').val(data.edit.zip);
                $('#country').val(data.edit.country_id).trigger('change');
                getCity(data.edit.country_id, data.edit.city_id);
                $('#outlet').removeAttr('required');
                $('#consultant').removeAttr('required');
                $('#group_outlet').hide();
                $('#group_consultant').hide();
                $('#familyname').val(data.edit.nextofikin);
                $('#familyphone').val(data.edit.nextofikin_number);
                $('#source').val(data.edit.source_id).trigger('change');
                $('#source_remarks').val(data.edit.source_remarks);
                $('#purpose_treatment').val(data.edit.purpose_treatment);

            },
            error: function(e) {
                alert("Can not Show the Data!");
            }
        });
    }

    function verifikasi(id) {
        swal({
                title: "Are you sure?",
                text: "Email verification for mobile apps.",
                icon: "warning",
                buttons: {
                    canceled: {
                        text: 'not Verifikasi',
                        value: 'not',
                        className: 'swal-button btn-warning'
                    },
                    deleted: {
                        text: 'Verifikasi',
                        value: 'verifikasi',
                        className: 'swal-button btn-danger'
                    }
                },
                dangerMode: true,
            })
            .then((willDelete) => {
                switch (willDelete) {
                    default:
                        swal("Nothing is no Change!");
                        break;
                    case 'not':
                        $.ajax({
                            url: "customers/" + id + "?not=true",
                            type: "POST",
                            data: {
                                '_method': 'PUT',
                                '_token': $('meta[name=csrf-token]').attr('content')
                            },
                            success: function(data) {
                                swal("Patient not Verification!", {
                                    icon: "success",
                                });
                                // table.ajax.reload();
                            },
                            error: function() {
                                swal({
                                    text: 'Can not Disabled the Data!',
                                    icon: 'error'
                                })
                            }
                        });
                        break;
                    case 'verifikasi':
                        $.ajax({
                            url: "customers/" + id + "?verifikasi=true",
                            type: "POST",
                            data: {
                                '_method': 'PUT',
                                '_token': $('meta[name=csrf-token]').attr('content')
                            },
                            success: function(data) {
                                swal("Patient has been Verification!", {
                                    icon: "success",
                                });
                                // table.ajax.reload();
                            },
                            error: function() {
                                swal({
                                    text: 'Can not Delete the Data!',
                                    icon: 'error'
                                })
                            }
                        });
                        break;
                }
            });
    }

    function deleteData(id) {
        swal({
            title: "Are you sure?",
            text: "Data pasien bisa di hapus apabila belum pernah melakukan transaksi pembelian.",
            icon: "warning",
            buttons: {
                canceled: {
                    text: 'Cancel',
                    value: 'cancel',
                    className: 'swal-button btn-default'
                },
                deleted: {
                    text: 'Delete',
                    value: 'delete',
                    className: 'swal-button btn-danger'
                }
            },
            dangerMode: true,
        }).then((willDelete) => {
            switch (willDelete) {
                default:
                    swal("Patient is safe!");
                    break;
                case 'delete':
                    $.ajax({
                        url: "customers/" + id,
                        type: "POST",
                        data: {
                            '_method': 'DELETE',
                            '_token': $('meta[name=csrf-token]').attr('content')
                        },
                        success: function(data) {
                            if (data === "error") {
                                swal({
                                    text: 'Customer is Used !',
                                    icon: 'error'
                                })
                            } else {
                                swal("Patient has been deleted!", {
                                    icon: "success",
                                });
                            }
                            location.reload();
                        },
                        error: function() {
                            swal({
                                text: 'Can not Delete the Data!',
                                icon: 'error'
                            })

                        }
                    });
                    break;
            }
        });
    }

    // validas
    function validation() {
        $("#noktp").keypress(function(data) {
            //console.log(data.which);
            if (data.which < 48 || data.which > 57) {
                $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
                return false;
            }
        });

        $("#phone").on('keyup change', function() {
            $("#phone").each(function() {
                this.value = this.value.replace(/[^0-9]/g, '');
            });
        });

        $("#familyphone").keypress(function(data) {
            if (data.which < 48 || data.which > 57) {
                $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
                return false;
            }
        });

        $("#zip").keypress(function(data) {
            //console.log(data.which);
            if (data.which < 48 || data.which > 57) {
                $("#msg_zip").html("Please insert real Zip Code").show().fadeOut(4000);
                return false;
            }
        });
    }
</script>
<script src="{{ asset('js/medicalAlert.js') }}"></script>
@endsection