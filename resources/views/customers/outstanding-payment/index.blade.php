<div class="box box-solid">
    <div class="box-header with-border">
            <span style="font-size:20px;">Outstanding Payment</span>
            <p style="font-size:15px;"><i>Informasi total pembayaran yang belum lunas.</i></p>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Invoice Code</th>
                    <th>Payable</th>
                    <th>Paid</th>
                    <th>Outstanding</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($invoice as $item)
                @if ($item->remaining > 0 )
                    <tr>
                        <td>{{ $item->inv_code }}</td>
                        <td>{{ $item->total }}</td>
                        @php
                            $total_pay = '';
                            foreach($item->payment as $payment){
                                $total_pay .= '<li>'.$payment->amount.'</li>';
                            }
                        @endphp
                        <td>{!! $total_pay !!}</td>
                        <td>{{ $item->remaining }}</td>
                    </tr>
                    
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>