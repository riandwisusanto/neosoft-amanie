<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">PAST TREATMENT USAGE</span>
        <br><br>
    </div>
</div>
<div class="box-body table-responsive no-padding">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr class="text-capitalize">
                    <th></th>
                    <th class="all">Invoice Date</th>
                    <th>Outlet</th>
                    <th>Expired Date</th>
                    <th>Treatment</th>
                    <th>Balance</th>
                    <th class="all" width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                {{ csrf_field() }}
                @foreach ($invoice as $list)
                @if ($list->remaining == 0)
                    @foreach ($list->invoice_detail as $item)
                    @foreach ($item->invoice_package as $package)
                        @if ($package->qty <= 0)
                        <tr>
                            <td>
                                @php
                                $date = str_replace('/', '-', $list->expired_date );
                                $newDate = date("Y-m-d", strtotime($date));
                                @endphp
                                @if ($package->qty <= 0 || $newDate <= now()->toDateString())
                                    <input type="checkbox" name="check_code[]" id="check_code"
                                        value="{{ $package->id_invoice_package }}"
                                        disabled>
                                    @else
                                    <input type="checkbox" name="check_code[]" id="check_code"
                                        value="{{ $package->id_invoice_package }}">
                                    @endif
                            </td>
                            <td>{{ $list->inv_date }}</td>
                            <td>{{ $list->outlet->outlet_name }}</td>
                            <td>{{ $list->expired_date }}</td>
                            <td>{{ $package->product_treatment->product_treatment_name }}</td>
                            <td>{{ $package->qty }}</td>
                            <td>
                                <a href="#" data-title="{{ $package->product_treatment->product_treatment_name }}" data-id="{{ $package->id_invoice_package }}" class="btn show-history btn-warning btn-xs">
                                    <i class="fa fa-credit-card"></i>
                                </a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    @endforeach
                @endif
                @endforeach
            </tbody>
        </table>
</div>