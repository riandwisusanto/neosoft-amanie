<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">PHOTOS</span>
        <br><br>
    </div>
    <!-- 
    <div class="col-xs-12" style="margin-top: 5px;">
        <button class="btn btn-success disabled" aria-disabled="true">Upload New Photos</button>
        <br><br>
    </div>
    -->
</div>

<div class="box box-solid" style="box-shadow: none;">
    <!-- <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-before-after">Before-After Photo <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
        </div>
    </div> -->
    <div class="box-body">

        <div class="nav-tabs-custom" style="border-radius: 0px 0px 3px 3px;">
            <ul class="nav nav-tabs nav-tabs-button" style="padding: 0px !important;">
                <li class="active" style="margin-right:2px; margin-bottom: 0px;"><a href="#tab_1-1" data-toggle="tab" id="nav-tab-1">UPLOAD</a></li>
                <li style="margin-right:2px; margin-bottom: 0px;"><a href="#tab_2-2" data-toggle="tab" id="nav-tab-2">BEFORE-AFTER</a></li>
            </ul>

            <div class="tab-content" style="border:1px solid #d2d6de">

                <div class="tab-pane active" id="tab_1-1">
                <form action="{{ route('dr-review.upload.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="box-body">

                        <div class="row">
                            <!-- <div class="col-sm-4">
                                <select name="customer_id" id="customer_id" class="form-control">
                                    <option value="{{ $overview->id_customer }}">{{ $overview->full_name }} - {{ $overview->induk_customer }}</option>
                                </select>
                            </div> -->
                            <div class="col-xs-12 col-sm-4">
                                <input type="hidden" name="customer_id" id="customer_id" value="{{ $overview->id_customer }}">
                                <select name="treatment" id="treatment_option" class="form-control" style="width: 100%" required>
                                    <option>Choose Treatment</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="" style="font-size:15px;">LEFT</label>
                                    <input type="file" name="image[]" class="form-control dropify">
                                    <span style="font-size:15px;">Format file jpg or png.</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="" style="font-size:15px;">FRONT</label>
                                    <input type="file" name="image[]" class="form-control dropify">
                                    <span style="font-size:15px;">Format file jpg or png.</span>
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="" style="font-size:15px;">RIGHT</label>
                                    <input type="file" name="image[]" class="form-control dropify">
                                    <span style="font-size:15px;">Format file jpg or png.</span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="" style="font-size:15px;">MISCELLANEOUS</label>
                                    <input type="file" name="image[]" class="form-control dropify" multiple>
                                    <span style="font-size:15px;">Format file jpg or png. (Upload multiple photos at once)</span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" style="font-size:15px;">Remark</label>
                                    <textarea name="remarks" rows="4" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
                </div>

                <div class="tab-pane" id="tab_2-2">
                    <div class="box-body">

                        <div class="row">
                           
                            <div class="col-xs-12 col-sm-4">
                                <input type="hidden" name="customer_id2" id="customer_id" value="{{ $overview->id_customer }}">
                                <select name="" id="treatment_options" class="form-control treatment" style="width: 100%">
                                    <option>Choose Treatment</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <select name="before" id="before_option" class="form-control" style="width: 100%">
                                    <option selected disabled>Choose Treatment Date</option>
                                </select>
                                <br>
                                <br>
                                <label for="" style="font-size:15px;">BEFORE</label>
                                <div id="before_img">
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-4">
                                <select name="after" id="after_option" class="form-control" style="width: 100%">
                                    <option selected disabled>Choose Treatment Date</option>
                                </select>
                                <br>
                                <br>
                                <label for="" style="font-size:15px;">AFTER</label>
                                <div id="after_img">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                
            </div>
            
        </div>

    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <label  style="font-size:15px;">Detail Photo</label>
      </div>
      <div class="modal-body text-center">
        <img src="" alt="" id="imgDetail" style="height:550px; width:auto;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>Close</button>
      </div>
    </div>
  </div>
</div>