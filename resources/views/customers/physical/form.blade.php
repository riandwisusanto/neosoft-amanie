<div class="modal" id="modal-physical" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title" id="titles"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id_customer">

                    <div class="row">
                        <div class="col-md-12" id="inner-drawable">
                            <div class="form-group">
                                <label class="control-label">Examination Date</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="examination_date"
                                        name="examination_date" value="<?= date('d/m/Y') ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="description" id="description" cols="30" rows="3"
                                    class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="summary_id" class="control-label">Upload Physical Examination</label>
                                <input type="file" onchange="createPreviewExamination(this)" name="image"
                                    class="form-control" id="image" required accept="image/*">
                                <input type="hidden" name="photodata" value="" id="photodata">
                            </div>
                            <canvas id="drawablePhoto" width="570"></canvas>
                        </div>
                    
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="modal-footer">
                                <a class="btn btn-success" id="clear"><i class="fa fa-trash-o" aria-hidden="true"></i> Clear</a>
                                <a class="btn btn-warning" id="undo"><i class="fa fa-arrow-left" aria-hidden="true"></i> Undo</a>
                                <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i>
                                    Save </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-modal"><i
                                        class="fa fa-arrow-circle-left"></i> Cancel</button>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
