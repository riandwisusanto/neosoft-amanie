<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">PHYSICAL EXAMINATION</span>
    </div>
    <div class="col-xs-12" style="margin-top: 5px;">
        <button onclick="addPhysical()" class="btn btn-success" >Create New Physical Examination</button>
        <br><br>
    </div>
</div>
<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-physical-examination">Physical Examination History <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-physical-examination">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-capitalize">
                        <th>Examination Date</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Delete</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                @foreach($physical as $phy)
                    <tr>
                        <td>{{ $phy->examination_date->format('d/m/Y') }}</td>
                        <td>{{ $phy->description }}</td>
                        <td><a class="btn" onclick="showPictPhysical({{ $phy->id_physical_examination}})"><img src="{{ asset('storage'.'/'.$phy->image) }}" alt="" style="width: 100px;"></a></td>
                        <td>
                            <form action="{{route('customer.physical.destroy', $phy->id_physical_examination)}}" method="post">
                            @csrf
                                <button type="submit" class="btn btn-danger btn-xs">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

