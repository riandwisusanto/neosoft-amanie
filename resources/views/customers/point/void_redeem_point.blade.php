<div class="box box-solid">
    <div class="box-header with-border">
        <span style="font-size:20px;">Void Redeem</span>
        <p style="font-size:15px;"><i>Informasi penukaran point yang dibatalkan.</i></p>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Redeem Invoice</th>
                <th>Created Date</th>
                <th>Void Date</th>
                <th>Point Amount</th>
                <th>Remarks</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $last = $void_redeem_points->last();
            @endphp
            @foreach ($void_redeem_points as $invoice)
                <tr>
                    <td>{{ $invoice->inv_point_code}} @if($last->id_invoice_point == $invoice->id_invoice_point)<span class="label label-danger">New</span>@endif</td>
                    <td>{{ $invoice->created_date }}</td>
                    <td>{{ $invoice->void->created_date }}</td>
                    <td>{{ $invoice->total }}</td>
                    <td>{{ $invoice->remarks }}</td>
                    <td><a href="{{ route('customer.redeem', $invoice->id_invoice_point) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
