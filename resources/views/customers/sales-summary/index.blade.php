<div class="box box-solid">
    <div class="box-header with-border">
        <span style="font-size:20px;">Sales Summary</span>
        <p style="font-size:15px;"><i>Informasi total sales/pembelajaan dari pertama kali menjadi member.</i></p>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Outlet</th>
                    <th>Total Sales</th>
                    <th>Total Payment</th>
                    <th>Total Outstanding</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sales_summary as $item)
                <tr>
                    <td>{{ $item[0]->outlet->outlet_name }}</td>
                    @php
                        $total = 0;
                        $total_payment = 0;
                    @endphp
                    @foreach ($item as $list)
                        @php
                            $total += str_replace(",", "", $list->total);
                            foreach ($list->payment as $payment) {
                                $total_payment += str_replace(",", "", $payment->amount);
                            }
                        @endphp
                    @endforeach
                    <td>{{ format_money($total) }}</td>
                    <td>{{ format_money($total_payment) }}</td>
                    <td>{{ $item[0]->remaining }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>