<div class="box box-solid">      
        <div class="box-header with-border">
            <span style="font-size:20px;">Void Balance</span>
            <p style="font-size:15px;"><i>Informasi pelunasan yang dibatalkan.</i></p>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Balance</th>
                        <th>Balance.date</th>
                        <th>Outstanding</th>
                        <th>Balance By</th>
                        <th>Remarks</th>
                        <th>Void Date</th>
                        <th>Void By</th>
                        <th>Void Remarks</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($void_balance as $balance)
                        <tr>
                            <td>{{ $balance->balance_code }}</td>
                            <td>{{ $balance->balance_date }}</td>
                            <td>{{ $balance->outstanding }}</td>
                            <td>{{ $balance->user->username }}</td>
                            <td>{{ $balance->remarks }}</td>
                            <td>{{ $balance->log_void_balance->created_at }}</td>
                            <td>{{ $balance->log_void_balance->user->username }}</td>
                            <td>{{ $balance->log_void_balance->remarks }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>