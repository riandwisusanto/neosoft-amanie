<div class="box box-solid">
    <div class="box-header with-border">
        <span style="font-size:20px;">Void Usage</span>
        <p style="font-size:15px;"><i>Informasi perawatan yang dibatalkan.</i></p>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Usage Code</th>
                    <th>Usage Date</th>
                    <th>Created By</th>
                    <th>Outlet</th>
                    {{-- <th>Item</th>
                    <th>Invoice Num</th>
                    <th>Therapist</th> --}}
                    <th>Remarks</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($void_usage as $item)
                <tr>
                    <td>{{ $item->usage_code }}</td>
                    <td>{{ $item->usage_date }}</td>
                    <td>{{ $item->user->username }}</td>
                    <td>{{ $item->outlet->outlet_name }}</td>
                    {{-- @php
                    $product = '';
                    $invoice = '';
                    $therapist = '';
                    foreach($item->usage_detail as $data){
                    $invoice .= "<li>".$data->inv_code."</li>";
                    $product .= "<li>".$data->product_treatment->product_treatment_code."</li>";

                    foreach ($data->usage_therapist as $getTherapist){
                    $therapist .= "<li>".$getTherapist->therapist->therapist_name."</li>";
                    }
                    }
                    @endphp

                    <td>{!! $product !!}</td>
                    <td>{!! $invoice !!}</td>
                    <td>{!! $therapist !!}</td> --}}
                    <td>{{ $item->remarks }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>