@extends('base')
@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
</style>
@endsection
@section('content')
<div class="row">
    <!-- /.col -->
    <div class="col-xl-10 col-md-12">
        <div class="box box-solid">
           
        </div>
    </div>
</div>
<div class="nav-tabs-custom">
  <div class="box-header with-border">
    <span style="font-size:20px;">Dashboard</span>
    <p style="font-size:15px;"><i>Manajemen laporan (collections, revenue, sales pasien baru, dan sales pasien lama) ditampilkan dalam bentuk grafik, pelaporan secara real time.</i></p>
  </div>
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs ui-sortable-handle">
      <li class="active"><a href="#dashboard" id="nav-tab-1" data-toggle="tab" aria-expanded="true" >Dashboard</a></li>
      <li class=""><a href="#product-treatment" id="nav-tab-2" data-toggle="tab" aria-expanded="true" >Product Treatment</a></li>
    </ul>

    <form class="form-horizontal" data-toggle="validator" id="form_data" style="margin-bottom: 0px;">
      {{ csrf_field() }}
      <div class="box-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group" style="margin-bottom: 0px;">
              <label class="col-xs-12 col-sm-12 col-md-5 control-label" for="outlets" style="text-align: left !important;">Select Time Period</label>
              <div class="col-xs-12 col-sm-12 col-md-7">
                  <select style="width: 100%;" id="daterange" class="form-control" name="daterange" required>
                    <option value="today">Today</option>
                    <option value="yesterday">Yesterday</option>
                    <option value="thisweek">This Week</option>
                    <option value="lastweek">Last Week</option>
                    <option value="thismonth">This Month</option>
                    <option value="lastmonth">Last Month</option>
                    <option value="thisyear">This Year</option>
                    <option value="lastyear">Last Year</option>
                    <option value="quarter1">Quarter I</option>
                    <option value="quarter2">Quarter II</option>
                    <option value="quarter3">Quarter III</option>
                    <option value="quarter4">Quarter IV</option>
                    <option value="semester1">Semester I</option>
                    <option value="semester2">Semester II</option>
                    <option value="customperiod">Custom Period</option>
                  </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-2" id="date-range-picker">
            <div class="form-group" style="margin-bottom: 0px;">
              <div class="col-xs-12">
                <input style="width: 100%;" type="text" name="daterangepicker" id="daterangepicker" class="form-control" autocomplete="off" value="" required>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="form-group" style="margin-bottom: 0px;">
              <div class="col-xs-12">
                <select style="width: 100%;" id="outlet" class="form-control" name="outlet[]" multiple="multiple" required>
                  @foreach ($outlet as $list)
                    <option value="{{ $list->id_outlet }}" selected="selected">{{ $list->outlet_name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-2">
            <input type="submit" value="Apply" class="btn btn-success">
          </div>
        </div>
      </div>
    </form>

    <div class="tab-content no-padding">
      <div class="chart tab-pane active" id="dashboard" style="position: relative; min-height: 500px;">
        @include('dashboard.dashboard')
      </div>
      <div class="chart tab-pane" id="product-treatment" style="position: relative; min-height: 500px;">
        @include('dashboard.product-treatment')
      </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
<script>
  var chart_product_treatment, chart_collection, customer_chart, table_geografis, table_productranking, table_treatmentranking, first_tab1 = 1, first_tab2 = 0, bars_type = 'vertical', donutChartWidth = 400, barChartWidth = 1000;

  $.getJSON('/dashboard/alldata', function(data) {
    $('#collection').html(data.collection);
    $('#revenue').html(data.revenue);
    $('#patient-total').html(data.customer);
    $('#new-patient').html(data.new_customer);
    $('#old-patient').html(data.old_customer);
    $('#new-patient-percent').html(data.new_customer_percent);
    $('#old-patient-percent').html(data.old_customer_percent);
    customer_chart = data.customer_chart;
    chart_product_treatment = data.chart_product_treatment;
    chart_collection = data.chart_collection;
  });

  function chartResize() {
    if ($(window).width() < 575.98) {
      bars_type = 'horizontal';
      donutChartWidth = 300;
      barChartWidth = 300;
    } else if ($(window).width() < 992) {
      bars_type = 'vertical';
      donutChartWidth = 400;
      barChartWidth = 1000;
    }
    google.charts.setOnLoadCallback(drawVisualization1);
    google.charts.setOnLoadCallback(drawVisualization2);
  }

  $(window).resize(function(){
    chartResize();
  });

  $('#nav-tab-1').click(function(){
    if ($('#nav-tab-2').parent().hasClass('active') && first_tab1 == 0) {
      google.charts.setOnLoadCallback(drawVisualization1);
      table_geografis.load();
      first_tab1 =  1;
    }
  });

  $('#nav-tab-2').click(function(){
    if ($('#nav-tab-1').parent().hasClass('active') && first_tab2 == 0) {
      google.charts.setOnLoadCallback(drawVisualization2);
      table_productranking.load();
      table_treatmentranking.load();
      first_tab2 = 1;
    }
  });

  google.charts.load('current', {'packages':['corechart','bar']});
  google.charts.setOnLoadCallback(drawVisualization1, drawVisualization2);
  function drawVisualization1() {
    var data_collection = google.visualization.arrayToDataTable(chart_collection);
    var options_collection = {
      seriesType: 'bars',
      vAxis: {format: 'decimal'},
      hAxis: {format: 'decimal'},
      bar: {groupWidth: "95%"},
      bars: bars_type,
      // bars: 'horizontal',
      // chartArea: {left:100,right:100,top:25,bottom:25,height:'100%',width:'100%'},
      colors: ['#8b69ae', '#bacfc1'],
      // width: '100%',
      width: barChartWidth,
      height: 500
    };
    var chart_collection2 = new google.charts.Bar(document.getElementById("chart-collection"));
    chart_collection2.draw(data_collection, google.charts.Bar.convertOptions(options_collection));

    var data_patient = google.visualization.arrayToDataTable(customer_chart);
    var options_patient = {
      pieHole: 0.7,
      fontSize: 15,
      pieStartAngle: -90,
      pieSliceText: 'none',
      legend: { position: 'left' },
      chartArea: {left:30,right:30,top:40,bottom:40,height:'100%',width:'100%'},
      // width: '100%',
      colors: ['#8b69ae', '#bacfc1'],
      width: donutChartWidth,
      height: 300
    };
    var chart_patient = new google.visualization.PieChart(document.getElementById('patient-chart'));
    chart_patient.draw(data_patient, options_patient);
  }

  function drawVisualization2() {
    var data_product_treatment = google.visualization.arrayToDataTable(chart_product_treatment);
    var options_product_treatment = {
      seriesType: 'bars',
      vAxis: {format: 'decimal'},
      hAxis: {format: 'decimal'},
      bar: {groupWidth: "95%"},
      bars: bars_type,
      // bars: 'horizontal',
      // chartArea: {left:100,right:100,top:25,bottom:25,height:'100%',width:'100%'},
      colors: ['#8b69ae', '#bacfc1'],
      // width: '100%',
      width: barChartWidth,
      height: 500
    };
    var chart_product_treatment2 = new google.charts.Bar(document.getElementById('chart-product-treatment'));
    chart_product_treatment2.draw(data_product_treatment, google.charts.Bar.convertOptions(options_product_treatment));
  }

  $('#daterangepicker').daterangepicker();

  $("#daterange").on('change',function(){
    if ($(this).val() == "customperiod") {
      $('#date-range-picker').show()
    } else {
      $('#date-range-picker').hide()
    }
  });

  $(function() {
    chartResize();
    $('#date-range-picker').hide();

    table_geografis = $('.table-geografis').DataTable( {
        processing: true,
        serverside: false,
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        "lengthChange": false,
        "searching": false,
        "info": false,
        "columnDefs": [{
          "searchable": false,
          "orderable": false,
          "targets": 0
        }],
        "order": [[ 5, 'desc' ]],
        "ajax" : {
          "url" : "{{ route('dashboard.geografis') }}",
          "type" : "GET"
        }
    });
    table_geografis.on( 'order.dt search.dt', function () {
      table_geografis.column(0, {search:'applied', order:'applied'}).nodes().each(function(cell, i) {
        cell.innerHTML = i+1;
      });
    }).draw();

    table_productranking = $('.table-productranking').DataTable( {
        processing: true,
        serverside: false,
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        "lengthChange": false,
        "searching": false,
        "info": false,
        "order": [[ 2, "desc" ]],
        "columnDefs": [
          {"orderable": false, "targets": [0]}
        ],
        "ajax" : {
          "url" : "{{ route('dashboard.productranking') }}",
          "type" : "GET"
        }
    });
    table_productranking.on( 'order.dt search.dt', function () {
      table_productranking.column(0, {search:'applied', order:'applied'}).nodes().each(function(cell, i) {
        cell.innerHTML = i+1;
      });
    }).draw();

    table_treatmentranking = $('.table-treatmentranking').DataTable( {
        processing: true,
        serverside: false,
        scrollX:        true,
        scrollCollapse: true,
        width: '100%',
        paging:         false,
        "lengthChange": false,
        "searching": false,
        "info": false,
        "order": [[ 2, "desc" ]],
        "columnDefs": [
          {"orderable": false, "targets": [0]}
        ],
        "ajax" : {
          "url" : "{{ route('dashboard.treatmentranking') }}",
          "type" : "GET"
        }
    });
    table_treatmentranking.on( 'order.dt search.dt', function () {
      table_treatmentranking.column(0, {search:'applied', order:'applied'}).nodes().each(function(cell, i) {
        cell.innerHTML = i+1;
      });
    }).draw();
  });

  $('#form_data').validator().on('submit', function(e){
    if(!e.isDefaultPrevented()){
      $.ajax({
        type: 'GET',
        url: '/dashboard/alldata',
        data : $('#form_data').serialize(),
        success: function(data){
          $('#collection').html(data.collection);
          $('#revenue').html(data.revenue);
          $('#patient-total').html(data.customer);
          $('#new-patient').html(data.new_customer);
          $('#old-patient').html(data.old_customer);
          $('#new-patient-percent').html(data.new_customer_percent);
          $('#old-patient-percent').html(data.old_customer_percent);
          customer_chart = data.customer_chart;
          chart_product_treatment = data.chart_product_treatment;
          chart_collection = data.chart_collection;
          if ($('#nav-tab-1').parent().hasClass('active')) {
            google.charts.setOnLoadCallback(drawVisualization1);
          }
          if ($('#nav-tab-2').parent().hasClass('active')) {
            google.charts.setOnLoadCallback(drawVisualization2);
          }
          return false;
        }
      });

      table_geografis.ajax.url("{{ route('dashboard.geografis') }}?"+$('#form_data').serialize());
      table_productranking.ajax.url( "{{ route('dashboard.productranking') }}?"+$('#form_data').serialize());
      table_treatmentranking.ajax.url( "{{ route('dashboard.treatmentranking') }}?"+$('#form_data').serialize());
      if ($('#nav-tab-1').parent().hasClass('active')) {
        first_tab2 = 0;
        table_geografis.load();
      }
      if ($('#nav-tab-2').parent().hasClass('active')) {
        first_tab1 = 0;
        table_productranking.load();
        table_treatmentranking.load();
      }
    }
    return false;
  });

</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
