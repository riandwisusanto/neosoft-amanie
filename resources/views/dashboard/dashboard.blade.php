<div class="box-body">
    <div class="row">
        <h3 class="col-xs-12" style="font-size:20px;">Each of Criterion</h3>
        <div class="col-xs-12 text-center">
            <div id="chart-collection" style="max-width: 1000px; margin: 0px auto;"></div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px;">
        <h3 class="col-xs-12 col-sm-12 col-md-2" style="font-size: 20px;">Main Criteria</h3>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="alert bg-purple text-center" style="background-color: #8b69ae !important;">
                <h4>Collection</h4>
                <h4 id="collection"></h4>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="alert text-center" style="background-color: #bacfc1 !important;">
                <h4>Revenue</h4>
                <h4 id="revenue"></h4>
            </div>
        </div>
    </div>
    <div class="row">
        <h3 class="col-xs-12" style="font-size:20px;">Patient</h3>
        <div class="col-xs-12 col-sm-12 col-md-5">
            <div id="patient-chart" style="max-width: 402px; border: 1px solid #d2d6de; margin: 0px auto;"></div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7">
            <div class="row text-center align-middle" style="min-height: 150px;">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <h4>Patient Total</h4>
                    <h3 id="patient-total"></h3>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <h4>New Patient</h4>
                    <h3 id="new-patient"></h3>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <h4>Existing Patient</h4>
                    <h3 id="old-patient"></h3>
                </div>
            </div>
            <div class="row text-center align-middle" style="min-height: 150px;">
                <div class="col-xs-6">
                    <h4>New Patient</h4>
                    <h3 id="new-patient-percent"></h3>
                </div>
                <div class="col-xs-6">
                    <h4>Existing Patient</h4>
                    <h3 id="old-patient-percent"></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 style="font-size:20px;">Geografis</h3>
            <table class="table table-striped nowrap table-geografis" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>City</th>
                        <th>Patient</th>
                        <th>Invoice</th>
                        <th>Items</th>
                        <th>Collection</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>