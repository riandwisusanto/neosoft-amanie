    <div class="box-body">
        <div class="row">
            <h3 class="col-xs-12" style="font-size:20px;">Product and Treatment Data</h3>
            <div class="col-xs-12">
                <div id="chart-product-treatment" style="max-width: 1000px; margin: 0px auto;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h3 style="font-size:20px;">Product Ranking</h3>
                <table class="table table-striped nowrap table-productranking" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Product Name</th>
                            <th width="20%">Sold</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="col-xs-12 col-md-6">
                <h3 style="font-size:20px;">Treatment Ranking</h3>
                <table class="table table-striped nowrap table-treatmentranking" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Treatment Name</th>
                            <th width="20%">Sold</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>