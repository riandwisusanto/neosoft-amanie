@extends('dr-review.base')
@section('content')
<section class="content" style="margin-top: 30px">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Detail History <strong>{{ $customer->full_name }}</strong></h3>
      <a href="{{ route('dr-review.history') }}" class="btn btn-warning" style="float: right;"><i class="fa fa-arrow-circle-left"></i> Back</a>
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped" id="table">
        <thead>
          <tr>
            <th style="width: 10%">No</th>
            <th>Information</th>
            <th>Remarks</th>
            <th>Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @php($no = 1)
          @foreach ($customer->docter_images as $r)
            <tr>
              <td>{{ $no }}</td>
              <td>{{ $r->desc }}</td>
              <td>{{ $r->remarks }}</td>
              <td>{{ $r->date_in }}</td>
              <td>
                <form action="{{ route('dr-review.history.destroy', $r->id) }}" method="post">
                  @csrf
                  @method('DELETE')
                  <a href="{{ route('dr-review.photo', $r->id) }}" class="btn btn-success">Show Photo</a>
                  <button class="btn btn-danger delete-confirmation">Delete</button>
                </form>
              </td>
            </tr>
            @php($no++)
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="box-footer"></div>
  </div>
</section>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    $('#table').DataTable({
      responsive : true
    })
    $('.select2').select2();
    $('body').on('click', '.delete-confirmation', function(e){
      e.preventDefault();
      swal({
        title             : 'Hapus data tersebut?',
        text              : "Data tidak bisa dikembalikan jika sudah terhapus",
        type              : 'warning',
        showCancelButton  : true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Yes, Hapus!'
      }).then((result) => {
        if (result) {
          $(this).parent().submit();
          // alert('asd');
        }
      })
    });
  });
</script>
@endsection
