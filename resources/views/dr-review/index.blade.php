@extends('base')

@section('style')
<style>
    .nav-tabs-custom .nav-tabs-button li {
        margin-bottom: 0px;
        margin-left: 1px;

    }

    .nav-tabs-custom .nav-tabs-button li a {
        background-color: #f4f4f4 !important;
        border: 1px solid #dddddd !important;
        /*border-radius: 3px !important;*/
        color: #363636 !important;
        padding: 6px 12px !important;
    }

    .nav-tabs-custom .nav-tabs-button {
        border-bottom: 0px !important;
    }

    .nav-tabs-custom .nav-tabs-button li a:hover,
    .nav-tabs-custom .nav-tabs-button li.active a {
        background-color: #c4c4c4 !important;
        border: 1px solid #6a6a6a !important;
        color: #373737 !important;
    }

    .nav-tabs-custom .nav-tabs-button li.active {
        border-top-color: transparent !important;
    }
</style>
@endsection
@section('breadcrumb')
   @parent
   <li>Photo</li>
@endsection
@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <form action="{{ route($route.'upload') }}" method="post" enctype="multipart/form-data">
      @csrf

        <div class="box-header">
          <div class="row">
            <div class="col-sm-8">
              <span style="font-size:20px; font-weight: bold;">PHOTO</span>
              <p style="font-size:15px; font-weight: bold;"><i>Upload patient’s photos and review before-after photos.</i></p>
            </div>
            <div class="col-sm-4">
                <!-- <select name="customer_id" id="customer_id" class="form-control">
                    <option>Choose Patient</option>
                    @foreach ($customers as $r)
                      <option value="{{ $r->id_customer }}">{{ $r->full_name }} - {{ $r->induk_customer }}</option>
                    @endforeach
                </select> -->
            </div>
          </div>
        </div>
        <div class="box-body">
            <div class="nav-tabs-custom" style="border-radius: 0px 0px 3px 3px;">
              <ul class="nav nav-tabs nav-tabs-button">
                  <li class="active" style="margin-right:2px;"><a href="#tab_1-1" data-toggle="tab" id="nav-tab-1">UPLOAD</a></li>
                  <li style="margin-right:2px;"><a href="#tab_2-2" data-toggle="tab" id="nav-tab-2">BEFORE-AFTER</a></li>
              
              </ul>
              <div class="tab-content" style="border:1px solid #d2d6de">
                    <div class="tab-pane active" id="tab_1-1">
                        <div class="box-body">
                          <div class="row">
                              <div class="col-sm-4">
                                <select name="customer_id" id="customer_id" class="form-control">
                                    <option>Choose Patient</option>
                                    @foreach ($customers as $r)
                                      <option value="{{ $r->id_customer }}">{{ $r->full_name }} - {{ $r->induk_customer }}</option>
                                    @endforeach
                                </select>
                              </div>
                              <div class="col-sm-4">
                                  <select name="treatment" id="treatment_option" class="form-control" required>
                                      <option>Choose Treatment</option>
                                    
                                  </select>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-sm-4">
                                  <div class="form-group">
                                      <label for="" style="font-size:15px;">LEFT</label>
                                      <input type="file" name="image[]" class="form-control dropify">
                                      <span style="font-size:15px;">Format file jpg or png.</span>
                                  </div>
                              </div>
                              <div class="col-sm-4">
                                  <div class="form-group">
                                      <label for="" style="font-size:15px;">FRONT</label>
                                      <input type="file" name="image[]" class="form-control dropify">
                                      <span style="font-size:15px;">Format file jpg or png.</span>
                                  </div>
                              </div> 
                              <div class="col-sm-4">
                                  <div class="form-group">
                                      <label for="" style="font-size:15px;">RIGHT</label>
                                      <input type="file" name="image[]" class="form-control dropify">
                                      <span style="font-size:15px;">Format file jpg or png.</span>
                                  </div>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="" style="font-size:15px;">MISCELLANEOUS</label>
                                        <input type="file" name="image[]" class="form-control dropify" multiple>
                                        <span style="font-size:15px;">Format file jpg or png. (Upload multiple photos at once)</span>
                                    </div>
                                </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-sm-12">
                                  <div class="form-group">
                                      <label for="" style="font-size:15px;">Remark</label>
                                      <textarea name="remarks" rows="4" class="form-control" required></textarea>
                                  </div>
                              
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-sm-12">
                                  <div class="form-group">
                                      <button class="btn btn-primary">Submit</button>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2-2">
                        <div class="box-body">
                            <div class="row">
                              <div class="col-sm-4">
                                <select name="customer_id" id="customer_id2" class="form-control">
                                    <option>Choose Patient</option>
                                    @foreach ($customers as $r)
                                      <option value="{{ $r->id_customer }}">{{ $r->full_name }} - {{ $r->induk_customer }}</option>
                                    @endforeach
                                </select>
                              </div>
                                <div class="col-sm-4">
                                    <select name="" id="treatment_options" class="form-control treatment">
                                        <option>Choose Treatment</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="" style="font-size:15px;">BEFORE</label>
                                    <div id="before_img">
                                    </div>

                                    <br>
                                    <select name="before" id="before_option" class="form-control">
                                        <option selected disabled>Choose Treatment Date</option>
                                    </select>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-4">
                                    <label for="" style="font-size:15px;">AFTER</label>
                                    <div id="after_img">
                                    </div>
                                    <br>
                                    <select name="after" id="after_option" class="form-control">
                                        <option selected disabled>Choose Treatment Date</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <label  style="font-size:15px;">Detail Photo</label>
      </div>
      <div class="modal-body">
        <img src="" alt="" id="imgDetail" width="100%" style="height:550px">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>Close</button>
      </div>
    </div>
  </div>
</div>


@endsection
@section('script')
<script>

  // function reset(){
  //   $('#before, #after').css('display', 'none');
  //   $('#before_option, #after_option').html('')
  //   $('#carousel-before, #carousel-after').css('display', 'none');
  // }
  $('#myModal').on('show.bs.modal', function (e) {
      var button = $(e.relatedTarget);
      var img = button.data('img');
      $('#imgDetail').attr('src', img);
  });
  
  $('#customer_id').change(function(){
    if ($("#tab_1-1").hasClass("active")) {
      var thisval = $(this).val();
      $('#customer_id2').val(thisval).trigger('change');
    }
  });
  $('#customer_id2').change(function(){
    if ($("#tab_2-2").hasClass("active")) {
      var thisval = $(this).val();
      $('#customer_id').val(thisval).trigger('change');
    }
  });

  $('#treatment_option').change(function(){
    if ($("#tab_1-1").hasClass("active")) {
      var thisval = $(this).val();
      $('#treatment_options').val(thisval).trigger('change');
    }
  });
  $('#treatment_options').change(function(){
    if ($("#tab_2-2").hasClass("active")) {
      var thisval = $(this).val();
      $('#treatment_option').val(thisval).trigger('change');
    }
  });

  $(document).ready(function () {
    $('#customer_id, #customer_id2').change(function(){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          var treatment = '<option selected disabled>Choose Treatment</option>';
          $.each(res.data, function (i, v) {
              treatment += `<option  value="${v.id_usage_detail}">(${v.usage_code}) ${v.product_treatment_name}</option>`;
          });

          $('#treatment_option').html(treatment);
          $('#treatment_options').html(treatment);
          $('#before_img').html('');
          $('#after_img').html('');
  
        }
      });
    });

    $('.treatment').change(function(e){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan.treatment') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          var treatment = '<option selected disabled>Choose Treatment Date</option>';
          
          $.each(res.data, function (i, v) {
              treatment += `<option  value="${v.id}">${v.date_in}</option>`;
          });

          $('#before_option').html(treatment);
          $('#after_option').html(treatment);

        }
      });
    });

    $('#before_option').change(function(e){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan.photo') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          if(res.success){
            var item = '';
            
            $.each(res.data, function (i, v) {

              item += `
              <a href="!#" data-toggle="modal" data-target="#myModal" data-img="${v}">
              <img src="${v}" alt="..." class="img-thumbnail img-responsive" width="100%" style="height:350px;margin-bottom:15px;">
              </a>
              `;

            });
            $('#before_img').html(item);
            $('#before_img').css('display','block');  

          }
        }
      });
    });

    $('#after_option').change(function(e){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan.photo') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          if(res.success){
            var item = '';
            $.each(res.data, function (i, v) {
              item += `
              <a href="!#" data-toggle="modal" data-target="#myModal" data-img="${v}">
                <img src="${v}" alt="..." class="img-thumbnail img-responsive" width="100%" style="height:350px;margin-bottom:15px;">
              </a>
              `;

            });
            $('#after_img').html(item);  
            $('#after_img').css('display','block');  

           
          }
        }
      });
    });


  });
</script>
@endsection