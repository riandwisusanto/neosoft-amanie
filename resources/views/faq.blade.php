<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name') }} POS</title>
    <link rel="icon" href="{{ asset('img/logo.png') }}" type="image/x-icon">
    <link rel='stylesheet' href='https://cdn.3up.dk/bootstrap@4.1.3/dist/css/bootstrap.min.css'>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}"> 
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/faq.css') }}">
</head>
<body>
<!-- partial:index.partial.html -->
<!-- navbar -->
<div class="rd-navbar-wrap" style="height: 76px;">
    <nav class="rd-navbar rd-navbar-classic rd-navbar-original rd-navbar-static rd-navbar--is-stuck" data-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-lg-stick-up="true" data-lg-stick-up-offset="0px" data-md-device-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-stick-up="true" data-xl-stick-up-offset="0px" data-xxl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-stick-up="true" data-xxl-stick-up-offset="0px">
      <div class="rd-navbar-main">
        <!-- RD Navbar Panel-->
        <div class="rd-navbar-panel">
          <!-- RD Navbar Toggle-->
          <button class="rd-navbar-toggle toggle-original" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
          <!-- RD Navbar Brand-->
          <div class="rd-navbar-brand"><a class="brand" href="index"><img alt="" class="brand-logo-dark" height="32" src="https://www.neosoft.co.id/home/image/logo.png" srcset="https://www.neosoft.co.id/home/image/logo.png 2x" width="96"><img alt="" class="brand-logo-light" height="32" src="https://www.neosoft.co.id/home/image/logo.png" srcset="https://www.neosoft.co.id/home/image/logo.png 2x" width="96"></a></div>
        </div>

        <div class="rd-navbar-nav-wrap toggle-original-elements">
          <p class="rd-navbar-slogan rd-navbar-slogan-2 text-white-50 wow clipInLeft"></p>
          <!-- RD Navbar Nav-->

          <ul class="rd-navbar-nav"></ul>
          <!-- RD Navbar Search-->

          <div class="rd-navbar-element rd-navbar-element_centered">
            <!--<div class="group-xs"><a class="icon icon-sm link-social-2 mdi mdi-facebook" href="#"></a><a class="icon icon-sm link-social-2 mdi mdi-twitter" href="#"></a><a class="icon icon-sm link-social-2 mdi mdi-instagram" href="#"></a></div>-->
          </div>
        </div>

        <div class="rd-navbar-element rd-navbar-element_right">
          <ul class="list-localization">
            <li>
                <a href="{{ url('/') }}"><span class="text-white" style="font-weight: bold;font-size: 1,75rem !important;">DASHBOARD</span><i class="fa fa-sign-in text-yellow" style="font-size: 1,75rem !important;" aria-hidden="true"></i></a>    
            </li>
          </ul>
        </div>

        <div class="rd-navbar-dummy"></div>
      </div>
    </nav>
</div>
<!-- navbar -->
<div class="container">
    <div class="row">
        <div class="col-lg-5">
            <img src="{{ asset("/img/User-Manual_Banner.png") }}" class="banner">
        </div>
         <div class="col-lg-7">
            <h1 class="text">You Need Help ?</h1>
         </div>
    </div>
<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <div class="nav nav-pills faq-nav" id="faq-tabs" role="tablist" aria-orientation="vertical">
                <a href="#appointment" class="nav-link" data-toggle="pill" role="tab" aria-controls="appointment" aria-selected="true">
                    <i class="fa fa-calendar text-red"></i> <span>APPOINTMENT</span>
                </a>
                <a href="#sales" class="nav-link" data-toggle="pill" role="tab" aria-controls="sales" aria-selected="false">
                    <i class="fa fa-money text-yellow"></i> <span>SALES</span>
                </a>
                <a href="#patient" class="nav-link" data-toggle="pill" role="tab" aria-controls="patient" aria-selected="false">
                    <i class="fa fa-address-book-o text-purple"></i> <span>PATIENT</span>
                </a>
                <a href="#management" class="nav-link" data-toggle="pill" role="tab" aria-controls="management" aria-selected="false">
                    <i class="fa fa-laptop text-green"></i> <span>MANAGEMENT</span>
                </a>
                <a href="#logistics" class="nav-link" data-toggle="pill" role="tab" aria-controls="logistics" aria-selected="false">
                    <i class="fa fa-truck text-red"></i> <span>LOGISTICS</span>
                </a>
                <a href="#manufacture" class="nav-link" data-toggle="pill" role="tab" aria-controls="manufacture" aria-selected="false">
                    <i class="fa fa-industry text-purple"></i> <span>MANUFACTURE</span>
                </a>
                <a href="#doctor_review" class="nav-link" data-toggle="pill" role="tab" aria-controls="doctor_review" aria-selected="false">
                    <i class="fa fa-tv text-primary"></i> <span>DOCTOR REVIEW</span>
                </a>
                <a href="#report" class="nav-link" data-toggle="pill" role="tab" aria-controls="report" aria-selected="false">
                    <i class="fa fa-file text-grey"></i> <span>REPORT</span>
                </a>
                <a href="#settings" class="nav-link" data-toggle="pill" role="tab" aria-controls="settings" aria-selected="false">
                    <i class="fa fa-gear text-blue"></i> <span>SETTINGS</span>
                </a>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="tab-content" id="faq-tab-content">
                <div class="tab-pane show active" id="appointment" role="tabpanel" aria-labelledby="appointment">
                    <div class="accordion" id="accordion-tab-1">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-1-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-1" aria-expanded="false" aria-controls="accordion-tab-1-content-1">APPOINTMENT</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-1-content-1" aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                                <div class="card-body" style="padding:0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player6" data-pl="PLOYBardFyS38ARiEd5OYB_xfj-S0xgBUb">
                                            </div>
                                          </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="sales" role="tabpanel" aria-labelledby="sales">
                    <div class="accordion" id="accordion-tab-2">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-2-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-1" aria-expanded="false" aria-controls="accordion-tab-2-content-1">SALES</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-2-content-1" aria-labelledby="accordion-tab-2-heading-1" data-parent="#accordion-tab-2">
                                <div class="card-body" style="padding:0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player1" data-pl="PLOYBardFyS38vBSKGeeuivO8Owa2yqLgK">
                                            </div>
                                          </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="patient" role="tabpanel" aria-labelledby="patient">
                    <div class="accordion" id="accordion-tab-3">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-3-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-3-content-1" aria-expanded="false" aria-controls="accordion-tab-3-content-1">PATIENT</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-3-content-1" aria-labelledby="accordion-tab-3-heading-1" data-parent="#accordion-tab-3">
                                <div class="card-body" style="padding:0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player2" data-pl="PLOYBardFyS3_HpkbCNDCJKB4DNpoQx75C">
                                            </div>
                                          </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="management" role="tabpanel" aria-labelledby="management">
                    <div class="accordion" id="accordion-tab-4">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-4-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-1" aria-expanded="false" aria-controls="accordion-tab-4-content-1">MANAGEMENT</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-4-content-1" aria-labelledby="accordion-tab-4-heading-1" data-parent="#accordion-tab-4">
                                <div class="card-body" style="padding: 0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player3" data-pl="PLOYBardFyS3-y2AHR5B3hM7cEJ_ZgPfMZ">
                                            </div>
                                          </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="logistics" role="tabpanel" aria-labelledby="logistics">
                    <div class="accordion" id="accordion-tab-9">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-9-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-9-content-1" aria-expanded="false" aria-controls="accordion-tab-9-content-1">LOGISTICS</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-9-content-1" aria-labelledby="accordion-tab-9-heading-1" data-parent="#accordion-tab-9">
                                <div class="card-body" style="padding: 0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player9" data-pl="PLOYBardFyS38rHHFZ-8Lnj4ARhKXBwovW">
                                            </div>
                                          </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="manufacture" role="tabpanel" aria-labelledby="manufacture">
                    <div class="accordion" id="accordion-tab-8">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-8-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-8-content-1" aria-expanded="false" aria-controls="accordion-tab-8-content-1">MANUFACTURE</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-8-content-1" aria-labelledby="accordion-tab-8-heading-1" data-parent="#accordion-tab-8">
                                <div class="card-body" style="padding: 0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player8" data-pl="PLOYBardFyS3-iil22OxC9Zgb6-D5dYxBE">
                                            </div>
                                          </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="doctor_review" role="tabpanel" aria-labelledby="doctor_review">
                    <div class="accordion" id="accordion-tab-5">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-5-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-1" aria-expanded="false" aria-controls="accordion-tab-5-content-1">DOCTOR REVIEW</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-5-content-1" aria-labelledby="accordion-tab-5-heading-1" data-parent="#accordion-tab-5">
                                <div class="card-body" style="padding: 0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player4" data-pl="PLOYBardFyS3_EPVNZ6COzI-Aj4jlIiAFb">
                                            </div>
                                          </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="report" role="tabpanel" aria-labelledby="report">
                    <div class="accordion" id="accordion-tab-6">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-6-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-6-content-1" aria-expanded="false" aria-controls="accordion-tab-6-content-1">REPORT</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-6-content-1" aria-labelledby="accordion-tab-6-heading-1" data-parent="#accordion-tab-6">
                                <div class="card-body" style="padding: 0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player5" data-pl="PLOYBardFyS3-zA273dcsXUXwLQQm2tLRj">
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="settings" role="tabpanel" aria-labelledby="settings">
                    <div class="accordion" id="accordion-tab-7">
                        <div class="card" style="margin-bottom: 25%;">
                            <div class="card-header" id="accordion-tab-7-heading-1">
                                <h5>
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-7-content-1" aria-expanded="false" aria-controls="accordion-tab-7-content-1">SETTINGS</button>
                                </h5>
                            </div>
                            <div class="collapse show" id="accordion-tab-7-content-1" aria-labelledby="accordion-tab-7-heading-1" data-parent="#accordion-tab-7">
                                <div class="card-body" style="padding: 0rem;">
                                    <div class="sidebar">
                                        <div class="ypt_wrapper">
                                          <div class="video embed-container">
                                            <div id="ytpl-player7" data-pl="PLOYBardFyS39BS6O_0BfwiqlauskQuO9v">
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-classic-aside">
    <div class="container">
        <p class="text-white-50 rights"><span>©&nbsp; </span><span class="copyright-year">2020</span><span>&nbsp;</span><span>neosoft</span><span>.&nbsp;</span></p>
    </div>
</div>
<!-- partial -->
<script src='https://cdn.3up.dk/jquery@3.3.1/dist/jquery.slim.min.js'></script>
<script src='https://cdn.3up.dk/bootstrap@4.1.3/dist/js/bootstrap.min.js'></script>
<script src="{{ asset('js/faq.js') }}"></script>
</body>
</html>
