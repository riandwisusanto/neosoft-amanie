@extends('base')

@section('title')
Create Invoice
@endsection

@section('breadcrumb')
@parent
<li>Create Invoice</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px; font-weight: bold;">CREATE INVOICE</span>
                <p style="font-size:15px; font-weight: bold;"><i>Create invoice in full payment or down payment.</i></p>
            </div>
            <form class="form form-invoice" data-toggle="validator" method="post">
                <div class="box-body">
                    <div class="form-horizontal">
                        {{ csrf_field() }} {{ method_field('POST') }}
                        <div class="form-group">
                            <label class="col-md-2 control-label">Customer *</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="customer" name="customer" autofocus="on"
                                    autocomplete="off" required>
                                <input type="hidden" name="customer_id" id="customer_id">
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Full Name</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="full"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Contact No</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="contact"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Date of Birth</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="birthDate"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Privilege Member</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="Summary"></p>
                                <p class="form-control-static" id="Priviege"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fullname" class="col-md-2 control-label"></label>
                            <div class="col-md-10">
                                <p id="bonus" class="text-muted text-red well well-md no-shadow"
                                    style="margin-top: 0px;">

                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Outlet</label>
                            <div class="col-md-5">
                                <select id="outlet" name="outlet" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @if (count($outlet) >= 1)
                                    <option selected="selected" value="">-- Select Outlet --</option>
                                    @endif
                                    @foreach ($outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Marketing Source</label>
                            <div class="col-md-5">
                                <select id="marketing_source" name="marketing_source"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    @if (count($marketing_source) >= 1)
                                    <option selected="selected" value="">-- Select Source --</option>
                                    @endif
                                    @foreach ($marketing_source as $list)
                                    <option value="{{ $list->id_marketing_source }}">{{ $list->marketing_source_name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Invoice Date</label>
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                       @if (array_intersect(['SUPER_USER', 'ADMINISTRATOR'], json_decode(Auth::user()->level)))
                                    <input type="text" class="form-control pull-right" id="date_invoice"
                                        name="date_invoice" required>
                                        @else
                                         <input type="text" class="form-control pull-right" id="date_invoice"
                                        name="date_invoice" disabled required>
                                        @endif
                                        
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Expired Date</label>
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date_expired"
                                        name="date_expired" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Served 1 By</label>
                            <div class="col-md-5">
                                <select id="consultant" name="consultant"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="">-- Select Consultant --</option>
                                    @foreach ($consultant as $list)
                                    <option value="{{ $list->id_consultant }}">{{ $list->consultant_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Served 2 By</label>
                            <div class="col-md-5">
                                <select id="therapist" name="therapist"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="">-- Select Therapist --</option>
                                    @foreach ($therapist as $list)
                                    <option value="{{ $list->id_therapist }}">{{ $list->therapist_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Agent</label>
                            <div class="col-md-5">
                                <select id="agent" name="agent" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="0">-- Without Agent --</option>
                                    @foreach ($agent as $list)
                                    <option value="{{ $list->id_agent }}">{{ $list->agent_name }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-md-5">
                                <p id="medical-alert" style="margin-top: 0px;">
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">List Anamnesa</label>
                            <div class="col-md-5">
                                <select id="listAnamnesa" name="listAnamnesa" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>

                                </select>
                                
                            </div>

                        </div>

                        <div class="panel panel-primary">
                            <div class="panel-heading text-center"><strong>Item(s)</strong> </div>
                            <div class="panel-body">
                                <div class="row hidden" id="package_section">
                                    <div id="render"></div>
                                    @include('invoice.package')
                                    <input type="hidden" name="save_json" id="save_json" value="[]">
                                </div>
                                @include('sweet::alert')
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-2 control-label">Total</label>
                            <div class="col-md-5">
                                <input class="form-control" type="text" name="total" id="total" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Payment(s)</label>
                            <div class="col-md-10">
                                <div class="form-horizontal">
                                    <div class="col-md-3">
                                        <label>Payment Method</label>
                                        <select class="form-control" name="payment_type[]" required="on">
                                            @foreach ($bank as $item)
                                            <option value="{{ $item->id_bank }}">{{ $item->bank_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Amount</label>
                                        <input type="text" class="form-control" id="amount" name="amount[]" rel="amount"
                                            placeholder="Amount(s)" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Info</label>
                                        <input type="text" class="form-control" id="payment_info" name="payment_info[]"
                                            placeholder="Info">
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <label style="font-size:12px">Add Other Payment</label>
                                        <p>
                                            <input id="pay" value="1" type="hidden">
                                            <button type="button" id="add" class="btn btn-info btn-xs"
                                                onclick="addPaymant()"><i class="fa fa-plus"></i></button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div id="DivPayment"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Paid Balance </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="paid" name="paid" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Unpaid Balance </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="unpaid" name="unpaid" value="0" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Change</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="kembali" name="kembali" value="0" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Old Invoice </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control input-md" id="old_invoice" name="old_invoice"
                                    placeholder="Migration Invoice">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Remarks </label>
                            <div class="col-md-5">
                                <textarea class="form-control" name="remarks"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary btn-save simpan"><i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
    var table, table_package, outlet, save_json = [], stockInformation = {};

    var allItem = JSON.parse('{!! json_encode($package) !!}')

    $('#date_invoice, #date_expired').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    $('#date_invoice').datepicker('setDate', 'today')
    $('#date_expired').datepicker('setDate', '+1y')

    $(function(){
        chooseOutlet();
        loadAnamnesa();
        findCustomer();
        hitung();
        $('.form-invoice').validator().on('submit', function(e){
            var id_customer = $('#customer_id').val();
            var cek_json = JSON.parse($('#save_json').val());

            if (cek_json.length > 0) {
                if(!e.isDefaultPrevented()){
                    $.ajax({
                        url : "{{ route('invoice-create.store') }}",
                        type : "POST",
                        data : $('.form-invoice').serialize(),
                        success : function(data){
                            if (data === "error") {
                                swal("in the package there are product or treatment restrictions", {
                                    button: false,
                                });
                            }else if (data === "error") {
                                swal("Stock Reached Minimum Quantity", {
                                    button: false,
                                });
                            }else{
                                swal({
                                    title: "Successfully Create Invoice",
                                    icon: "success",
                                    buttons: {
                                        canceled:{
                                            text:'Cancel',
                                            value: 'cancel',
                                            className: 'swal-button btn-default'
                                        },
                                        print:{
                                            text:'Print',
                                            value: 'print',
                                            className: 'swal-button btn-danger'
                                        },
                                        sendwa:{
                                            text:'Send WhatsApp',
                                            value: 'sendwa',
                                            className: 'swal-button btn-success'
                                        }
                                    },
                                    dangerMode: true,
                                })
                                .then((option) => {
                                    switch (option) {
                                        default:
                                            window.location.href = "/customers/"+data.id_invoice+"/detail";
                                            break;
                                        case 'print':
                                            window.open("invoice-create/"+data.id_invoice+"/print")
                                            window.location.href = "/customers/"+data.id_invoice+"/detail";
                                        break;
                                        case 'sendwa':
                                            $.ajax({
                                                url : "invoice-create/"+data.id_invoice+"/wa",
                                                type : "GET",
                                                success : function(x){
                                                    if (x) {
                                                        swal({
                                                            text: 'Send WA Success',
                                                            icon: 'success'
                                                        })
                                                        window.location.href = "/customers/"+data.id_invoice+"/detail";
                                                    }
                                                    return false;
                                                },
                                                error : function(){
                                                    swal({
                                                        text: 'Send WA Error',
                                                        icon: 'error'
                                                    })
                                                    window.location.href = "/customers/"+data.id_invoice+"/detail";
                                                    return false;
                                                }
                                            });
                                        break;
                                    }
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            console.log(jqXHR.responseText);
                            swal("Can not Save the Data ", {
                                button: false,
                            });
                        }
                    });
                    return false;
                }
            }else{
                swal("The Package is Still Empty! Please Add Item", {
                    icon: "error",
                });
                return false;
            }
        });
    });

    function chooseOutlet() {

        $('#outlet').change(function () {
            outlet = $('#outlet').val();
            // $('#package_section').removeClass('hidden');

            // $.ajax({
            //     url : "/invoice-create/"+outlet+"/data/package",
            //     type: 'GET',
            //     dataType: "JSON",
            //     success: function(data) {
            //         $('select[name="package"]').empty();
            //         for (let index in data) {
            //             $('select[name="package"]').append('<option value="'+index+'">'+data[index]+'</option>')
            //         }
            //     },
            //     error: function () {
            //         alert('Can not Save The Data // });
        });
    }
    function addPaymant() {
        hitung();
        var _l = $("input[rel=amount]").length+1;
        var inputbaru = "<div class='form-group' id='row" + _l + "'>"+
                "<label class='col-md-2 control-label'></label>"+
                    "<div class='col-md-10'>"+
                        "<div class='form-horizontal'>"+
                            "<div class='col-md-3'>"+
                                "<label>Payment Method ("+ _l +")</label>"+
                                "<select class='form-control' name='payment_type[]' required='on'>"+
                                    "@foreach ($bank as $item)"+
                                        "<option value='{{ $item->id_bank }}'>{{ $item->bank_name }}</option>"+
                                    "@endforeach"+
                                "</select>"+
                            "</div>"+
                            "<div class='col-md-3'>"+
                                "<label>Amount ("+ _l +")</label>"+
                                "<input type='text' class='form-control' id='amount' name='amount[]' rel='amount' placeholder='Amount(s)' required>"+
                            "</div>"+
                            "<div class='col-md-3'>"+
                                "<label>Info ("+ _l +")</label>"+
                                "<input type='text' class='form-control' id='payment_info' name='payment_info[]' placeholder='Info'>"+
                            "</div>"+
                            "<div class='col-md-3 text-center'>"+
                                "<label style='font-size:12px'>Remove Payment</label>"+
                                "<p>"+
                                "<button class='  btn btn-info btn-xs' onclick='hapusPayment(\"#row" + _l + "\"); return false;'><i class='fa fa-minus'></i></button>"+
                                "</p>"+
                            "</div>"+
                        "</div>"+
                    "</div>";

        $("#DivPayment").append(inputbaru);
        hitung();
        return false;
    }

    function hitung() {
        $("input[rel=amount]").bind('keyup',function(){
            var total = $('#total').val();
            var awal = 0;
            $("input[rel=amount]").each(function(){
                this.value=this.value.replace(/[^0-9]/g,'');
                if(this.value !='') awal += parseInt(this.value,10);

            });

            let totalpaid = remove_format(total)-awal;
            let kembali = awal - remove_format(total);

            $('#paid').val(awal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $('#unpaid').val(totalpaid.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $('#kembali').val(kembali.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));

        });
    }

    function hapusPayment(pay) {
        $(pay).remove();
        hitung();
	}

    var typingTimer, waitTyping = 1250;
    function findCustomer(){
        $('#customer').keyup(function(){
            var query = $(this).val();

            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                lookupCustomer(query)
            }, waitTyping)
        });

        $('#customer').keydown(function() {
            clearTimeout(typingTimer)
        })
    }

        /**
        * Lookup customer data after user finish typing.
        */
        function lookupCustomer(query) {
            if(query != ''){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('customer.keyword') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    dataType: "json",
                    success:function(data){
                        $('#suggesstion-box').fadeIn();
                        $('#suggesstion-box').html(data);
                    }
                });
            }else{
                var _token = $('input[name="_token"]').val();
                $.ajax({
                url:"{{ route('customer.all') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                    $('#suggesstion-box').fadeIn();
                    $('#suggesstion-box').html(data);
                }
                });
            }
    }

    function detailCustomer(id, induk, name, phone, birth) {
        $('#package_section').removeClass('hidden');
        $('#customer').val(induk);
        $('#suggesstion-box').fadeOut();
        $('#customer_id').val(id);
        $('#full').html(name);
        $('#contact').text(phone);
        $('#birthDate').text(birth);
        $.ajax({
        url : "/invoice-create/"+id+"/chooseAnamnesa",
        type: 'GET',
        dataType: "JSON",
        success: function(data) { 
             $('#listAnamnesa').html(data);
        },
        error : function () {
            alert("Can not Show the Data!");
        }
        });

        rank(id);
        star(id);
        medical_alert(id);
        return false;
    }

    function rank(id){
        $.ajax({
            url: "ranks/customer/"+id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                let desc = '';
                if (data) {
                    desc = data.desc.replace(/\n/g, "<br />");
                    $("#bonus").html("<strong>"+ data.name + "</strong>" + "<br />" + desc);
                }
            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function star(id) {
        $.ajax({
            url: "ranks/star/"+id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                let star = 0;
                if (data >= 1 && data < 5000000) {
                    star = 1;
                } else if (data >= 5000000 && data < 9100000) {
                    star = 2;
                } else if (data >= 9100000 && data < 15100000) {
                    star = 3;
                } else if (data > 15100000) {
                    star = 4;
                } else {
                    star = 0;
                }

                let starAfter = '';
                for($i=0; $i < star ;$i++){
                    starAfter += "<i class='fa fa-star text-yellow'></i>";
                }
                $("#Priviege").html(starAfter);
                $("#Summary").html(format_money(data));
            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function ucwords (str) {
        return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
            return $1.toUpperCase();
        });
    }

    function medical_alert(id) {
        $.ajax({
            url: "medical-alert/"+id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $("#medical").remove();

                let loop = '';
                for (let i = 0; i < data.length; i++) {
                    loop += '<i class="fa fa-exclamation-circle text-yellow" aria-hidden="true"></i> '+
                    '<strong>' +
                        ucwords(data[i].product_treatment_name)+
                        '<input type="hidden" name="restraction" id="restraction" value="'+data[i].id_product_treatment+'">'+
                    '</strong><br>'
                }

                let medical_alert = '<div class="col-md-12 col-xs-12" id="medical">'+
                    '<div class="user-block">'+
                        '<img class="img-circle img-bordered-sm" src="{{ asset("/img/medical.ico") }}" alt="user image">'+
                        '<span class="username"> Medical Alert </span>'+
                        '<span class="description">'+
                            loop+
                        '</span>'+
                    '</div>'+
                '</div>';

                $("#medical-alert").append(medical_alert);
            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function remove_format(money) {
        return money.toString().replace(/[^0-9]/g,'');
    }

    function format_money(money){
        if ( ! money) {
            return 0
        }
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function changeType() {
        let type = $('#type_line').val();
        let disc1 = $('#discount_1').val();
        let disc2 = $('#discount_2').val();
        let price = $('#current_price').val();
        let qty = $('#qty').val();

        let stockError, stockValid = true;

        if (stockInformation.type == 'product') {
            if ( stockInformation.sellable <= stockInformation.buffer_stock ) {
                swal(stockInformation.name + ' kurang dari sama dengan ' + stockInformation.buffer_stock + ' unit.', {
                    dangerMode: true,
                    button: true,
                    showCancelButton: false,
                });
            }
        }

        if (stockInformation.type == 'service') {
            let err = [];

            stockInformation.sellable.forEach((product, index) => {
                let min = product.used * qty;
                
                if (min > product.sellable) {
                    let need = min - product.sellable;
                    err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
                    stockValid = false;
                }

                if ( product.sellable <= product.buffer_stock ) {
                    swal(product.name + ' kurang dari sama dengan ' + product.buffer_stock + ' unit.', {
                        dangerMode: true,
                        button: true,
                        showCancelButton: false,
                    });
                }
            });

            err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
            stockError = err.join('<br>');

        }
        
        if (stockInformation.type == 'product') {
            if (qty > stockInformation.sellable) {
                let need = qty - stockInformation.sellable;
                stockError = `${stockInformation.name} hanya tersedia ${stockInformation.sellable} unit.`
                stockValid = false;
            }
        }
        
        if (stockInformation.type == 'package') {
            let grandErr = [];
            stockInformation.sellable.forEach(row => {

            
                if (row.type == 'service') {
                    let err = [];

                    row.sellable.forEach((product, index) => {
                        let min = product.used * qty;
                        
                        if (min > product.sellable) {
                            let need = min - product.sellable;
                            err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
                            stockValid = false;
                        }

                        if ( product.sellable <= product.buffer_stock ) {
                            swal(product.name + ' kurang dari sama dengan ' + product.buffer_stock + ' unit.', {
                                dangerMode: true,
                                button: true,
                                showCancelButton: false,
                            });
                        }
                    });

                    err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
                    grandErr.push(err.join('<br>'));

                }
                
                if (row.type == 'product') {
                    if (qty > row.sellable) {
                        let need = qty - row.sellable;
                        let err = `${row.name} hanya tersedia ${row.sellable} unit.`
                        grandErr.push(err);
                        stockValid = false;
                    }
                    
                    if ( row.sellable <= row.buffer_stock ) {
                        swal(row.name + ' kurang dari sama dengan ' + row.buffer_stock + ' unit.', {
                            dangerMode: true,
                            button: true,
                            showCancelButton: false,
                        });
                    }
                }
            });

            stockError = grandErr.join('<br>');
        }

        if ( ! stockValid) {
            var content = document.createElement("div");
            content.innerHTML = stockError;

            swal({
                title: "Stok Tidak Mencukupi.",
                content,
                icon: "error",
                button: true,
                showCancelButton: false,
            });

            $('#qty').val(0).change();
            return false;
        }


        let subtotal = qty * remove_format(price);

        let total1, grandTotal;
        if (disc2) {
            if (type == 0) {
                total1 = subtotal - (disc1 / 100) * subtotal;
                grandTotal = total1 - (disc2 / 100) * total1;
            } else {
                total1 = subtotal - disc1;
                grandTotal = total1 - disc2;
            }
        }else{
            if (type == 0) {
                grandTotal = subtotal - (disc1 / 100) * subtotal;
            } else {
                grandTotal = subtotal - disc1;
            }
        }

        $('#sub_total').val(format_money(Math.round(grandTotal)));
    }

    function changeTypeWithId(id) {
        let type = $('#type_line_'+id).val();
        //let disc = $('#discount_'+id).val();
        let disc1 = $('#discount_1_'+id).val();
        let disc2 = $('#discount_2_'+id).val();
        let price = $('#current_price_'+id).val();
        let qty = $('#qty_'+id).val();

        let stockError, stockValid = true;

        $('#package').attr('disabled', 'disabled');
        $('#activity').attr('disabled', 'disabled');
        $('#qty').attr('disabled', 'disabled');
        $('#discount_1').attr('readonly', 'readonly');
        $('#discount_2').attr('readonly', 'readonly');

        if (stockInformation.type == 'product') {
            if ( stockInformation.sellable <= stockInformation.buffer_stock ) {
                swal(stockInformation.name + ' kurang dari sama dengan ' + stockInformation.buffer_stock + ' unit.', {
                    dangerMode: true,
                    button: true,
                    showCancelButton: false,
                });
            }
        }

        if (stockInformation.type == 'service') {
            let err = [];

            stockInformation.sellable.forEach((product, index) => {
                let min = product.used * qty;
                
                if (min > product.sellable) {
                    let need = min - product.sellable;
                    err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
                    stockValid = false;
                }
                
                
                if ( product.sellable <= product.buffer_stock ) {
                    swal(product.name + ' kurang dari sama dengan ' + product.buffer_stock + ' unit.', {
                        dangerMode: true,
                        button: true,
                        showCancelButton: false,
                    });
                }
            });

            err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
            stockError = err.join('<br>');

        }
        
        if (stockInformation.type == 'product') {
            if (qty > stockInformation.sellable) {
                let need = qty - stockInformation.sellable;
                stockError = `${stockInformation.name} hanya tersedia ${stockInformation.sellable} unit.`
                stockValid = false;
            }
        }

        if (stockInformation.type == 'package') {
            let grandErr = [];
            stockInformation.sellable.forEach(row => {

            
                if (row.type == 'service') {
                    let err = [];

                    row.sellable.forEach((product, index) => {
                        let min = product.used * qty;
                        
                        if (min > product.sellable) {
                            let need = min - product.sellable;
                            err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
                            stockValid = false;
                        }
                    
                        if ( product.sellable <= product.buffer_stock ) {
                            swal(product.name + ' kurang dari sama dengan ' + product.buffer_stock + ' unit.', {
                                dangerMode: true,
                                button: true,
                                showCancelButton: false,
                            });
                        }
                    });

                    err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
                    grandErr.push(err.join('<br>'));

                }
                
                if (row.type == 'product') {
                    if (qty > row.sellable) {
                        let need = qty - row.sellable;
                        let err = `${row.name} hanya tersedia ${stockInformation.sellable} unit.`
                        grandErr.push(err);
                        stockValid = false;
                    }
                
                    if ( row.sellable <= row.buffer_stock ) {
                        swal(row.name + ' kurang dari sama dengan ' + row.buffer_stock + ' unit.', {
                            dangerMode: true,
                            button: true,
                            showCancelButton: false,
                        });
                    }
                }
            });

            stockError = grandErr.join('<br>');
        }

        if ( ! stockValid) {
            var content = document.createElement("div");
            content.innerHTML = stockError;

            swal({
                title: "Stok Tidak Mencukupi.",
                content,
                icon: "error",
                button: true,
                showCancelButton: false,
            });

            $('#qty_'+id).val(0).change();
            return false;
        }

        let subtotal = qty * remove_format(price);
        let total1, grandTotal;
        if (disc2) {
            if (type == 0) {
                total1 = subtotal - (disc1 / 100) * subtotal;
                grandTotal = total1 - (disc2 / 100) * total1;
            } else {
                total1 = subtotal - disc1;
                grandTotal = total1 - disc2;
            }
        }else{
            if (type == 0) {
                grandTotal = subtotal - (disc1 / 100) * subtotal;
            } else {
                grandTotal = subtotal - disc1;
            }
        }

        $('#sub_total_x_'+id).val(format_money(Math.round(grandTotal)));
    }

    function getItemSellableStock(e) {
        let elementId = $(e).attr('id');
        let $el = $(`#${elementId} option[value="${e.value}"]`);

        let type = $el.attr('data-type').toLowerCase();

        let id = $el.attr('data-id');

        if (type == 'package') {
            id = $el.attr('data-package-id');
        }
        let endpoint = `/api/v1/logistics/items/sales_informations/${type}/${id}/sellables`;
        let token = '{{ auth()->user()->api_token }}';

        $('input[id^="qty"]').attr('readonly', 'readonly');

        $.ajax({
            url: endpoint,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${token}`);
            },
            success: function (res) {
                stockInformation = res.sellable;

                $('input[id^="qty"').removeAttr('readonly');

            
                if (stockInformation.sellable > 0) {
                    if (elementId == 'package') {
                        $('#qty').val(1);
                    } else {
                        let parts = elementId.split('_');
                        $('#qty_'+parts[1]).val(1);
                    }
                }
            }
        });
    }

    function changeCountWithId(id) {
        changeTypeWithId(id);
    }

    function changeCount(){
        changeType();
    }

    function loadAnamnesa() {
        $('#listAnamnesa').change(function () {
            $.ajax({
                url: "invoice-create/"+$(this).val()+"/loadAnamnesa",
                type: "GET",
                dataType: "JSON",
                success: function (data) { 
                    save_json = JSON.parse($('#save_json').val());

                    data.load.forEach((row, index) => {
                        let computedPrice = (row.total / row.qty);
                        let choosePackage = allItem.findIndex(itm => {
                            return itm.product_treatment_code == row.product_treatment_code
                        });

                        var immediate = false;

                        var input = {
                            _id: index,
                            activity: "No Activity",
                            activity_id: "4",
                            current_price: format_money(row.computedPrice),
                            discount_1: 0,
                            discount_2: 0,
                            no: (index+1),
                            code: row.product_treatment_code,
                            package: row.product_treatment_name,
                            package_id: '',
                            price: computedPrice,
                            qty: row.qty,
                            subtotal: format_money(row.total),
                            productTreatment_id: row.product_id,
                            anamnesa_detail_id: row.id_anamnesa_detail,
                            choose_package : choosePackage,
                            type_line: '%',
                            editable: false,
                            type_line_id: '0',
                            isAnamnesa : "disabled",
                            immediate_usage: immediate
                        };

                        save_json.push(input);
                    });
                    
                    render();
                },
                error: function () {
                    alert("Can not Show the Data!");
                }
            });
        });
    }

</script>
<script src="{{ asset('js/invoice.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />

@endsection
