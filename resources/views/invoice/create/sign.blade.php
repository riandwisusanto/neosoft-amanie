<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Invoice Print</title>
    <style>
        * {
            font-size: 8pt;
            color: #000000;
            font-family: "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
        }

        @page {
            margin: 0.3cm;
        }

        ul {
            margin: 0 -20;
            list-style: none;
        }

        ul li:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }
        
        .border {
            border: 1px solid #e4e4ed;
        }

        .normal-header-td {
            border-bottom: 1px solid black;
        }

        .last-header-td {
            border-bottom: 1px solid black;
        }

        .block_center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .col-0_5,
        .col-1,
        .col-1_5,
        .col-2,
        .col-3,
        .col-4,
        .col-5,
        .col-6,
        .col-7,
        .col-8,
        .col-9,
        .col-10,
        .row {
            float: left;
            position: relative;
            min-height: 1px;
        }

        .col-0_5 {
            width: 5%;
        }

        .col-1 {
            width: 10%;
        }

        .col-1_5 {
            width: 15%;
        }

        .col-2 {
            width: 20%;
        }

        .col-3 {
            width: 30%;
        }

        .col-4 {
            width: 40%;
        }

        .col-5 {
            width: 50%;
        }

        .col-6 {
            width: 60%;
        }

        .col-7 {
            width: 70%;
        }
        
        .btn {
            background-color: #3c8dbc;
            color: white;
            padding: 8px 12px;
            border: none;
            border-radius: 4px;
        }

        .col-8 {
            width: 80%;
        }

        .col-9 {
            width: 90%;
        }

        .col-10 {
            width: 100%;
        }

        .floatRight {
            float: right;
        }

        .indentConditionParagraph {
            margin-left: 5px;
            width: 100%;
        }

        .indentConditions {
            float: left;
            width: 100%;
            margin-left: 15px;
        }

        .indentConditions-numbering {
            float: left;
            font-size: 9pt;
            vertical-align: top;
        }

        .indentConditions-bullets:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }

        .indentConditions-nestedConditions {
            float: left;
            width: 95%;
            margin-left: 5px;
        }

        .invoiceDetail-tbl {
            width: 100%;
            border: 1px solid black;
            min-height: 10em;
        }

        .invoiceDetail-tbl td {
            vertical-align: top;
        }

        .invoicePayment-tbl {
            width: 100%;
        }

        tr.lastRow {
            height: 30%;
        }

        .leftHeader {
            white-space: nowrap;
            text-align: right;
            width: 8%;
        }

        .leftHeaderData {
            width: 42%;
            padding-left: 10px;
        }

        .rightHeader {
            white-space: nowrap;
            text-align: right;
            width: 8%;
        }

        .rightHeaderData {
            width: 42%;
            padding-left: 10px;
        }

        .row {
            width: 100%;
        }

        .signature-title {
            height: 3em;
            font-weight: bold;
            font-size: 12px;
        }

        .signature-underline {
            height: 2em;
            font-weight: bold;
            font-size: 11px;
            border-bottom: black 2px solid;
        }

        .termsConditions {
            font-size: 10px;
            float: left;
            width: 100%;
        }

        .termsConditions-grandTitle {
            font-size: 9px;
            font-weight: bold;
            margin-top: 3px;
        }

        .termsConditions-subTitle {
            font-weight: bold;
            margin-top: 3px;
            font-size: 8px;
        }

        .termsConditions>h3 {
            font-size: 9px;
            margin: 0;
        }

        .termsConditions>ul>li,
        .termsConditions>div>ul>li,
        .indentConditions-nestedConditions>ul>li,
        .smallFont {
            font-size: 7pt;
        }

        .textRightAlign {
            text-align: right;
        }

        .pageBreakFooter {
            page-break-after: always;
        }

        .itembalance {
            font-size: 8px;
        }

        .itembalance-title {
            font-weight: bold;
        }

        .customHeight {
            height: 5px;
        }

        hr {
            border: black thin solid;
        }

        .taxInvoiceTextHeader {
            font-size: 16pt;
            font-weight: bold;
        }

        .taxInvoiceNumber {
            font-size: 12pt;
        }

        #termsId {
            width: 95%;
            position: absolute;
            bottom: 10;
        }

        thead {
            display: table-header-group;
        }

        tfoot {
            display: table-footer-group;
        }
    </style>
</head>

<body>
    <table id="mainTbl" style="width: 700px;" border="0" align="center">
        <thead>
            <tr>
                <td>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td valign="top" width="33%">
                                    {{ $invoice->outlet->address }}<br /><br />
                                    <strong>T.</strong> {{ $invoice->outlet->outlet_phone }}<br />
                                    <strong>F.</strong> {{ $invoice->outlet->outlet_fax }}<br />
                                    <strong>GST Reg. No.</strong> <br />
                                    <strong>ACRA No.</strong>
                                </td>
                                <td valign="top" width="33%">
                                    <span class="block_center" style="text-align:center;">
                                        <span class="taxInvoiceTextHeader">INVOICE</span><br />
                                        <span class="taxInvoiceNumber">{{ $invoice->inv_code }}</span>
                                    </span>
                                </td>
                                <td valign="top" width="33%">
                                <img src="/storage/{{ $invoice->outlet->logo }}" style="float: right; width: 150px;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0">
                        <tbody>
                            <tr>
                                <td class="leftHeader"><strong>Outlet Code No.:</strong></td>
                                <td class="leftHeaderData">{{ $invoice->outlet->outlet_code }}</td>
                                <td class="rightHeader"><strong>Date :</strong></td>
                                <td class="rightHeaderData">{{ $invoice->inv_date }}</td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong>Customer Name :</strong></td>
                                <td class="leftHeaderData">{{ $invoice->customer->full_name }}
                                    ({{ $invoice->customer->induk_customer }})</td>
                                
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong>Served by :</strong></td>
                                <td class="leftHeaderData">{{ $invoice->consultant->consultant_name }}</td>
                                <td class="rightHeader"><strong>Doctor :</strong></td>
                                <td class="rightHeaderData">{{ $invoice->therapist->therapist_name }}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table class="invoiceDetail-tbl" border="0" cellspacing="0" cellpadding="3px">
                        <thead>
                            <tr>
                                <td class="normal-header-td" width="5%">No.</td>
                                <td class="normal-header-td">SKU</td>
                                <td class="normal-header-td">Description</td>
                                <td class="normal-header-td">Price</td>
                                <td class="normal-header-td">Qty</td>
                                <td class="normal-header-td">Discount</td>
                                <td class="last-header-td">Sub Total</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no=1;
                            @endphp
                            @foreach ($inv_detail as $list)
                            <tr style="height:8px;">
                                <td>{{ $no }}</td>
                                <td>{{ $list->package_code }}</td>
                                @if ($list->package)
                                    <td>{{ $list->package->package_name }}</td>
                                @else
                                    <td>{{ $list->product_treatment->product_treatment_name }}</td>
                                @endif
                                <td>{{ $list->current_price }}</td>
                                <td>{{ $list->qty }}</td>
                                @php
                                    if($list->type_line == 0){
                                        $type = '%';
                                    }else{
                                        $type = '';
                                    }
                                @endphp
                                @if ($list->discount_2)
                                    <td>{{ $list->discount_1 }} {{ $type }} | {{ $list->discount_2 }} {{ $type }}</td>
                                @else
                                    <td>{{ $list->discount_1 }} {{ $type }}</td>
                                @endif
                                <td>{{ $list->subtotal }}</td>
                            </tr>
                            @php
                            $no++;
                            @endphp
                            @endforeach
                            <tr class="">
                            </tr>
                            <tr class="lastRow">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td colspan="3"><strong>Grand Total</strong></td>                                
                                <td><strong>{{ $invoice->total }}</strong></td>
                            </tr>
                        </tbody>
                    </table>
            <tr style="height:30px;">
                <td valign="top">Remarks: {{ $invoice->remarks }} </td>
            </tr>
            <tr>
                <td>
                    <div class="col-6">
                        <div class="row">
                            <table class="invoicePayment-tbl" style="border:1px solid black; margin-top: 10px;"
                                border="0" cellspacing="0">
                                <tr>
                                    <td class="normal-header-td">Pay Mode</td>
                                    <td class="normal-header-td"></td>
                                    <td class="normal-header-td">Amount</td>
                                </tr>
                                @php
                                $total_amount = 0;
                                @endphp
                                @foreach ($payment as $list)
                                <tr>
                                    <td>{{ $list->bank->bank_code }}</td>
                                    <td></td>
                                    <td>{{ $list->amount }}</td>
                                </tr>
                                @php
                                $total_amount += str_replace(",", "", $list->amount);
                                @endphp
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row">
                            <table cellspacing="0">
                                <tr>
                                    <td class="rightHeader">Total :</td>
                                    <td class="leftHeaderData">{{ format_money($total_amount) }}</td>
                                </tr>
                                <tr>
                                    <td class="rightHeader">Outstanding :</td>
                                    <td class="leftHeaderData">{{ $invoice->remaining }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="row">
                        <div class="col-2">
                            <div class="row">
                                <div class="col-10 floatRight signature-title">Sign by staff :</div>
                                <div class="col-10 floatRight signature-underline"></div>
                                <div class="col-10 floatRight signature-title">{{ $invoice->user->username }}</div>
                            </div>
                        </div>
                        <div class="col-4"></div>
                        <div class="col-4">
                            <div class="row">
                                <label class="col-10 signature-title">Sign Customer Here</label>
                                <div class="col-10 border">
                                    <canvas id="signature-pad" class="signature-pad" height="150"></canvas>
                                </div>
                                <div class="col-10 floatRight signature-title">{{ $invoice->customer->name }}</div>
                                
                                <form id="sign-form">
                                    @csrf
                                    <br>
                                    <input type="hidden" name="signature" id="signature" value="">
                                    <a class="btn btn-danger" type="button" id="clear"><i class="fa fa-trash-o" aria-hidden="true"></i> Clear</a>
                                    <a class="btn btn-warning" type="button" id="undo"><i class="fa fa-arrow-left" aria-hidden="true"></i> Undo</a>
                                    <button type="submit" class="btn save">Simpan</button>    
                                </form>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-10 floatRight signature-underline"></div>
                </td>
            </tr>
            </td>
            </tr>
        </tbody>
        <tfoot id="termsId"></tfoot>
    </table>
    <script src="/plugins/jQuery/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script>
        var canvas = document.getElementById('signature-pad');
        var signaturePad = new SignaturePad(canvas, {});
        
        $('#sign-form').submit(function (e) {
            
            e.preventDefault();
            
            $('#signature').val(signaturePad.toDataURL('image/png'))
            
            $.ajax({
                url : "{{ route('invoice.update-sign', $invoice->id_invoice) }}",
                type : "POST",
                data : $('#sign-form').serialize(),
                success : function(data){
                    window.location.href = "{{ route('invoice.print', $invoice->id_invoice) }}";
                },
                error : function(){
                    alert("Can not Save the Data!");
                }
            });
        });
        
        //clearSignature
        document.getElementById('clear').addEventListener('click', function () {
        signaturePad.clear();
        });

        //undoSignature
        document.getElementById('undo').addEventListener('click', function () {
            var data = signaturePad.toData();
        if (data) {
            data.pop(); // remove the last dot or line
            signaturePad.fromData(data);
        }
        });
    </script>
</body>

</html>