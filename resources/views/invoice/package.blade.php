<script type="x-tmpl-mustache" id="add_item">
    <div class="container-fluid">
            <div class="form-group">
                <label class="col-md-2 control-label">Items Name</label>
                <div class="col-md-5">
                    <input type="hidden" name="package_id" id="package_id">
                    <input type="hidden" name="productTreatment_id" id="productTreatment_id">
                    <select class="form-control input-sm" onChange="getItemSellableStock(this)" name="package" id="package" style="width: 100%">
                        <option></option>
                        @for ($i = 0; $i < $count; $i++)
                            @php
                                if (substr($package[$i]->product_treatment_code, 0, 1) == 'T') {
                                    $type = 'Treatment';
                                }elseif(substr($package[$i]->product_treatment_code, 0, 1) == 'P'){
                                    $type = 'Product';
                                }else{
                                    $type = 'Package';
                                }

                                if($package[$i]->package_name){
                                    $name = $package[$i]->package_name;
                                }else{
                                    $name = $package[$i]->product_treatment_name;
                                }

                            @endphp
                            <option data-id="{{ $package[$i]->id_product_treatment }}" data-package-id="{{ $package[$i]->id_package }}" data-type="{{ $type }}" value="{{ $i }}">{{ $name }} ({{ $type }})</option>
                        @endfor
                    </select>
                </div>

                <label class="col-md-1 control-label">Activity</label>
                <div class="col-md-4">
                    <select class="form-control input-sm" name="activity" id="activity" style="width: 100%">
                        <option></option>
                        @foreach ($activity as $list)
                            <option value="{{ $list->id_activity }}">{{ $list->activity_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 col-sm-12 col-xs-12 control-label"></label>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <label>Code</label>
                    <strong><p id="code">0</p></strong>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <label>Price</label>
                    <strong><p id="price">0</p></strong>
                </div>
                <div class="col-md-1 col-sm-4 col-xs-12">
                    <label>Qty</label>
                    <input type="hidden" id="current_price" name="current_price">
                    <input type="number" class="form-control input-sm" id="qty" readonly name="qty" onChange="changeCount()" value="0" min="1" required>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <label>Discount(1)</label>
                    <input type="number" min="0" max="100" class="form-control input-sm" id="discount_1" name="discount_1" onChange="changeType()">
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <label>Discount(2)</label>
                    <input type="number" min="0" max="100" class="form-control input-sm" id="discount_2" name="discount_2" onChange="changeType()">
                </div>
                <div class="col-md-1 col-sm-4 col-xs-12">
                    <label>Type</label>
                    <p>
                        <select class="form-control input-sm" name="type_line" id="type_line" onChange="changeType()">
                            <option value="0">%</option>
                            <option value="1">Rp</option>
                        </select>
                    </p>
                </div>
                <div class="col-md-3 col-md-offset-2 col-sm-4 col-xs-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="immediate_usage" value="1"> Immediate Usage
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-4 text-center pull-right" style="font-size:12px">
                    <label></label>
                    <p>
                    <button type="button" class="btn btn-success" id="AddItem"><i class="fa fa-check"></i> ADD ITEM</button>
                    </p>
                </div>
                <div class="col-md-3 col-sm-8 col-xs-8 pull-right">
                    <label>Sub Total</label>
                    <input type="text" id="sub_total" class="form-control input-sm" name="sub_total" readonly>
                </div>

            </div>
            <hr>
           {% #item %}
            <div id="data_{% _id %}">
                <div class="form-group">
                    <label class="col-md-2 control-label">Item Name</label>
                    <div class="col-md-5">
                        <input type="hidden" value="{% package_id %}" id="package_id_{% _id %}" name="package_id">
                        <input type="hidden" value="{% productTreatment_id %}" id="productTreatment_id_{% _id %}" name="productTreatment_id">
                        <input type="hidden" value="{% id_anamnesa_detail %}" id="id_anamnesa_detail_{% _id %}" name="id_anamnesa_detail">
                        <input type="hidden" name="choose_package" id="choose_package_{% _id %}" value="{% choose_package %}">
                        <input type="text" value="{% package %}" class="form-control input-sm" disabled>
                    </div>

                    <label class="col-md-1 control-label">Activity</label>
                    <div class="col-md-4">
                        <input type="hidden" value="{% activity_id %}" id="activity_id_{% _id %}" name="activity_id">
                        <input type="text" disabled value="{% activity %}" class="form-control input-sm">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 col-sm-12 col-xs-12 control-label"></label>
                    <div class="col-md-1 col-sm-4 col-xs-12">
                        <label>Code</label>
                        <strong><p id="code">{% code %}</p></strong>
                    </div>
                    <div class="col-md-1 col-sm-4 col-xs-12">
                        <label>Price</label>
                        <strong><p id="price">{% price %}</p></strong>
                    </div>
                    <div class="col-md-1 col-sm-4 col-xs-12">
                        <label>Qty</label>
                        <input type="hidden" id="current_price" name="current_price" value="{% current_price %}">
                        <input type="number" class="form-control input-sm" id="qty" name="qty" onChange="changeCount()" value="{% qty %}" disabled>
                    </div>
                    <div class="col-md-1 col-sm-4 col-xs-12">
                        <label>Discount(1)</label>
                        <input type="text" min="0" max="100" class="form-control input-sm input-sm" id="discount_1" name="discount_1" value="{% discount_1 %}" disabled>
                    </div>
                    <div class="col-md-1 col-sm-4 col-xs-12">
                        <label>Discount(2)</label>
                        <input type="text" min="0" max="100" class="form-control input-sm input-sm" id="discount_2" name="discount_2" value="{% discount_2 %}" disabled>
                    </div>
                    <div class="col-md-1 col-sm-4 col-xs-12">
                        <label>Type</label>
                        <input type="hidden" id="type_line_id_{% _id %}" name="type_line" value="{% type_line_id %}">
                        <input type="text" class="form-control input-sm input-sm" id="type_line" name="type_line" value="{% type_line %}" disabled>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <label>Sub Total</label>
                        <input type="text" id="sub_total_{% _id %}" class="form-control input-sm" name="sub_total" value="{% subtotal %}" readonly>
                    </div>

                    <div class="col-md-1 col-sm-4 col-xs-12 text-right">
                        <label>Action</label>
                        <p>
                            <button type="button" id="edit_{% _id %}" class="btn edit btn-success btn-xs" data-id="{% _id %}"><i class="fa fa-edit"></i></button>
                            <button type="button" id="delete_{% _id %}" class="btn delete btn-danger btn-xs" data-id="{% _id %}"><i class="fa fa-trash"></i></button>
                        </p>
                    </div>
                    <div class="col-md-12 col-md-offset-2">
                        {% #immediate_usage %}
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" disabled checked id="immediate_usage_{% _id %}" value="1"> Immediate Usage
                            </label>
                        </div>
                        {% /immediate_usage %}

                        {% ^immediate_usage %}
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" disabled  id="immediate_usage_{% _id %}" value="1"> Immediate Usage
                            </label>
                        </div>
                        {% /immediate_usage %}
                    </div>
                </div>
            </div>

            <div class="form hidden" id="form_{% _id %}">
                <div id="data_{% _id %}">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Item Name</label>
                        <input type="hidden" name="package_id" id="package_idx_{% _id %}" value="{% package_id %}">
                        <input type="hidden" name="productTreatment_id" id="productTreatment_idx_{% _id %}" value="{% productTreatment_id %}">
                        <div class="col-md-5">
                            <select class="form-control input-sm" name="package" onChange="getItemSellableStock(this)" id="package_{% _id %}" style="width: 100%" {% isAnamnesa %}>
                                <option></option>
                                @for ($i = 0; $i < $count; $i++)
                                    @php
                                        if (substr($package[$i]->product_treatment_code, 0, 1) == 'T') {
                                            $type = 'Treatment';
                                        }elseif(substr($package[$i]->product_treatment_code, 0, 1) == 'P'){
                                            $type = 'Product';
                                        }else{
                                            $type = 'Package';
                                        }

                                        if($package[$i]->package_name){
                                            $name = $package[$i]->package_name;
                                        }else{
                                            $name = $package[$i]->product_treatment_name;
                                        }

                                    @endphp
                                    <option data-id="{{ $package[$i]->id_product_treatment }}" data-package-id="{{ $package[$i]->id_package }}" data-type="{{ $type }}" value="{{ $i }}">{{ $name }} ({{ $type }})</option>
                                @endfor
                            </select>
                        </div>

                        <label class="col-md-1 control-label">Activity</label>
                        <div class="col-md-4">
                            <select class="form-control input-sm" name="activity" id="activityx_{% _id %}" style="width: 100%">
                                @foreach ($activity as $list)
                                    <option value="{{ $list->id_activity }}">{{ $list->activity_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 col-sm-12 col-xs-12 control-label"></label>
                        <div class="col-md-1 col-sm-4 col-xs-12">
                            <label>Code</label>
                            <strong><p id="code_{% _id %}">{% code %}</p></strong>
                        </div>
                        <div class="col-md-1 col-sm-4 col-xs-12">
                            <label>Price</label>
                            <strong><p id="price_{% _id %}">{% price %}</p></strong>
                        </div>
                        <div class="col-md-1 col-sm-4 col-xs-12">
                            <label>Qty</label>
                            <input type="hidden" id="current_price_{% _id %}" name="current_price" value="{% current_price %}">
                            <input type="number" class="form-control input-sm" id="qty_{% _id %}" name="qty" onChange="changeCountWithId({% _id %})" value="{% qty %}" required>
                        </div>
                        <div class="col-md-1 col-sm-4 col-xs-12">
                            <label>Discount(1)</label>
                            <input type="number" min="0" max="100" class="form-control input-sm input-sm" id="discount_1_{% _id %}" name="discount" value="{% discount_1 %}" onChange="changeTypeWithId({% _id %})">
                        </div>
                        <div class="col-md-1 col-sm-4 col-xs-12">
                            <label>Discount(2)</label>
                            <input type="number" min="0" max="100" class="form-control input-sm input-sm" id="discount_2_{% _id %}" name="discount" value="{% discount_2 %}" onChange="changeTypeWithId({% _id %})">
                        </div>
                        <div class="col-md-1 col-sm-4 col-xs-12">
                            <label>Type</label>
                            <p>
                                <select class="form-control input-sm" name="type_line" id="type_line_{% _id %}" onChange="changeTypeWithId({% _id %})">
                                    <option value="0">%</option>
                                    <option value="1">Rp</option>
                                </select>
                            </p>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <label>Sub Total</label>
                            <input type="text" id="sub_total_x_{% _id %}" class="form-control input-sm" name="sub_total" value="{% subtotal %}" readonly>
                        </div>

                        <div class="col-md-1 col-sm-4 col-xs-12 text-right">
                            <label>Action</label>
                            <p>
                                <button type="button" id="save_{% _id %}" class="btn save btn-primary btn-xs" data-id="{% _id %}"><i class="fa fa-floppy-o"></i></button>
                                <button type="button" id="cancel_{% _id %}" class="btn cancel btn-warning btn-xs" data-id="{% _id %}"><i class="fa fa-ban"></i></button>
                            </p>
                        </div>
                        <div class="col-md-12 col-md-offset-2">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="immediate_usage_edit_{% _id %}" value="1"> Immediate Usage
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           {% /item %}
    </div>
</script>
