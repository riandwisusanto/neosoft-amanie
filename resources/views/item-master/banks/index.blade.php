@extends('base')

@section('title')
  List Bank
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection

@section('breadcrumb')
   @parent
   <li>Banks</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-8">
            <span style="font-size:20px; font-weight: bold;">BANK MANAGEMENT</span>
            <p style="font-size:15px; font-weight: bold;"><i>Manage all payment methods.</i></p>
          </div>
          <div class="col-sm-4">
              <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Banks</a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-bank" style="width:100%">
        <thead>
          <tr>
              <th>Code</th>
              <th>Name</th>
              <th>Enable</th>
              <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.banks.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
  table = $('.table-bank').DataTable({

     "processing" : true,
    "serverside" : true,
    "ajax" : {
      "url" : "{{ route('banks.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-banks form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('banks.store') }}";
         else url = "banks/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-banks form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-banks').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
    save_method = "add";
    $('#outlet').val('');
    $("#outlet").trigger("change");
    $('input[name=_method]').val('POST');
    $("#modal-banks").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
    $('#modal-banks form')[0].reset();
        document.getElementById('status_bank_modal').style.visibility = 'hidden';

    $('.modal-title').text('Add Bank');
}

function detailInfo(id) {

}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-banks form')[0].reset();
   $.ajax({
     url : "banks/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-banks').modal('show');
       $('.modal-title').text('Edit Bank');

       $('#id').val(data.id_bank);
       $('#bank_code').val(data.bank_code);
       $('#bank_name').val(data.bank_name);
       $('#bank_charge').val(data.bank_charge);
       $('#outlet').val(data.outlet).change();

       $('#status_bank').prop('checked', data.status_bank);
       document.getElementById('status_bank_modal').style.visibility = 'visible';


     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Bank!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            disabled:{
                text:'Disable',
                value: 'disable',
                className: 'swal-button btn-warning'
            },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        switch (willDelete) {
            default:
                swal("Bank is safe!");
                break;
            case 'disable':
                $.ajax({
                    url : "/banks/"+id+"?disable=true",
                    type : "POST",
                    data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        swal("Bank has been disabled!", {
                            icon: "success",
                        });
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Disabled the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
            case 'delete':
                $.ajax({
                    url : "/banks/"+id,
                    type : "POST",
                    data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        if (data === "error") {
                            swal({
                                text: 'Bank is Used !',
                                icon: 'error'
                            })
                        } else {
                            swal("Bank has been deleted!", {
                                icon: "success",
                            });
                        }
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Delete the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
        }
    });
}


// validasi
  $(document).ready(function(){
    // $("#noktp").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#contact").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_phone").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#familyphone").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });
  });
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
