<div class="modal" id="modal-cities" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>   
         </button>
         <h3 class="modal-title"></h3>
     </div>
         <div class="modal-body" >
             <input type="hidden" id="id" name="id">
             <div class="form-group">
                <label for="group_id" class="col-md-3 control-label">Country *</label>
                <div class="col-md-8">
                    <select class="form-control select2" style="width: 100%" id="country_id" name="country_id">
                        @foreach ($countries as $country)
                           <option value="{{ $country->id_country }}">{{ $country->country }}</option>
                        @endforeach
                    </select>
                    <span class="help-block text-red" id="msg_group"></span>
                </div>
            </div>
             <div class="form-group">
                 <label for="province" class="col-md-3 control-label">Province *</label>
                 <div class="col-md-8">
                     <input type="text" class="form-control" id="province" name="province" placeholder="Province" autocomplete="off" required>
                 </div>
             </div>
         </div>
         
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
    </form>
 
          </div>
       </div>
 </div>
