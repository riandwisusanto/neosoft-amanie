@extends('base')

@section('title')
  List City
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection

@section('breadcrumb')
   @parent
   <li>City</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-8">
            <span style="font-size:20px; font-weight: bold;">CITY MANAGEMENT</span>
            <p style="font-size:15px; font-weight: bold;"><i>Manage all cities.</i></p>
          </div>
          <div class="col-sm-4">
            <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Cities</a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
              <th>Country</th>
              <th>City</th>
              <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.cities.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
  table = $('.table-city').DataTable({

     "processing" : true,
    "serverside" : true,
    "ajax" : {
      "url" : "{{ route('cities.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-cities form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('cities.store') }}";
         else url = "cities/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-cities form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-cities').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
    save_method = "add";
    $('#outlet').val('');
    $("#outlet").trigger("change");
    $('input[name=_method]').val('POST');
    $("#modal-cities").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
    $('#modal-cities form')[0].reset();
        // document.getElementById('status_bank_modal').style.visibility = 'hidden';

    $('.modal-title').text('Add City');
}

function detailInfo(id) {

}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-cities form')[0].reset();
   $.ajax({
     url : "cities/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-cities').modal('show');
       $('.modal-title').text('Edit City');

       $('#id').val(data.id_province);
       $('#country_id').val(data.country_id).change();
       $('#province').val(data.province);


     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this City!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            // disabled:{
            //     text:'Disable',
            //     value: 'disable',
            //     className: 'swal-button btn-warning'
            // },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        switch (willDelete) {
            default:
                swal("City is safe!");
                break;
            case 'disable':
                $.ajax({
                    url : "/cities/"+id+"?disable=true",
                    type : "POST",
                    data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        swal("City has been disabled!", {
                            icon: "success",
                        });
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Disabled the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
            case 'delete':
                $.ajax({
                    url : "/cities/"+id,
                    type : "POST",
                    data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        if (data === "error") {
                            swal({
                                text: 'City is Used !',
                                icon: 'error'
                            })
                        } else {
                            swal("City has been deleted!", {
                                icon: "success",
                            });
                        }
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Delete the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
        }
    });
}


// validasi
  $(document).ready(function(){
    // $("#noktp").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#contact").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_phone").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#familyphone").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });
  });
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
