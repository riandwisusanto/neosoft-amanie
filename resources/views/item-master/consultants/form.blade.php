<div class="modal" id="modal-consultants" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>   
         </button>
         <h3 class="modal-title"></h3>
     </div>
         <div class="modal-body" >
             <input type="hidden" id="id" name="id">
             <div class="form-group">
                 <label for="consultant_name" class="col-md-3 control-label">Full Name *</label>
                 <div class="col-md-8">
                     <input type="text" class="form-control" id="consultant_name" name="consultant_name" placeholder="Full Name" autocomplete="off" required>
                 </div>
             </div>
             <div class="form-group">
                 <label for="branch" class="col-md-3 control-label">Branch *</label>
                 <div class="col-md-8">
                     <select class="form-control select2" style="width: 100%" id="branch" name="branch[]" multiple="multiple">
                         @foreach ($branch as $item)
                             <option value="{{ $item->id_outlet }}">{{ $item->outlet_name }}</option>
                         @endforeach
                     </select>
                 </div>
             </div>
             <div class="form-check" id="status_consultant_modal">
                 <label for="consultant_status" class="col-md-3 control-label"></label>
                 <div class="col-md-8">
                     <input type="checkbox" class="form-check-input" id="status_consultant" name="status_consultant" value="1">
                     <label for="consultant_status" class="form-check-label">Enable</label>
                 </div>
             </div>
         </div>
         
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
    </form>
 
          </div>
       </div>
 </div>