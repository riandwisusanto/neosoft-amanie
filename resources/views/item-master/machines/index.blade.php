@extends('base')

@section('title')
  List Machine
@endsection

@section('breadcrumb')
   @parent
   <li>Machines</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Machines</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
          <tr>
              <th>Machine Name</th>
              <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.machines.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
  table = $('.table').DataTable({
    "serverside" : true,
    "ajax" : {
      "url" : "{{ route('machines.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-machines form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('machines.store') }}";
         else url = "machines/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-machines form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-machines').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('#outlet').val('');
   $("#outlet").trigger("change");
   $('input[name=_method]').val('POST');
   $("#modal-machines").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
   $('#modal-machines form')[0].reset();
   $('#group_id').val('').change()
   $('.modal-title').text('Add Machine');
}

function detailInfo(id) {

}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-machines form')[0].reset();
   $.ajax({
     url : "machines/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-machines').modal('show');
       $('.modal-title').text('Edit Machine');

       $('#id').val(data.id_machine);
       $('#machine_name').val(data.machine_name);


     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
  if(confirm("Are you sure the data will be Delete?")){
     $.ajax({
       url : "machines/"+id,
       type : "POST",
       data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
       success : function(data){
         console.log(data)
         table.ajax.reload();
       },
       error : function(){
         alert("Can not Delete the Data!");
       }
     });
   }
}


// validasi
  $(document).ready(function(){
    // $("#noktp").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#contact").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_phone").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#familyphone").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });
  });
</script>
@endsection
