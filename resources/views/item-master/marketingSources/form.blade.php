<div class="modal" id="modal-marketingSources" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>   
         </button>
         <h3 class="modal-title"></h3>
     </div>
         <div class="modal-body" >
             <input type="hidden" id="id_marketing_source" name="id_marketing_source">
             <div class="form-group">
                 <label for="marketing_source_name" class="col-md-3 control-label">Source Name</label>
                 <div class="col-md-8">
                     <input type="text" class="form-control" id="marketing_source_name" name="marketing_source_name" placeholder="Source Name" autocomplete="off" required>
                 </div>
             </div>
              <div class="form-group" >
                <div class="form-check" id="group_outlet">
                         <label for="outlet" class="col-md-3 control-label">Branch *</label>
                        <div class="col-md-8">
                               <select  class="form-control select2" data-placeholder="Select a Outlet" multiple="multiple"  name="outlet[]" id="outlet" required>
                                @foreach ($outlet as $list)
                                <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                            <span class="help-block text-red" id="msg_outlet"></span>
                        </div>
                </div>
            </div>
             <div class="form-check" id="status_marketing_source_modal">
                 <label for="marketing_source_name" class="col-md-3 control-label"></label>
                 <div class="col-md-8">
                    <input type="checkbox" class="form-check-input" id="status_marketing_source" name="status_marketing_source" value="1">
                    <label for="marketing_source_name" class="form-check-label">Enable</label>
                 </div>
             </div>
         </div>
         
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
    </form>
 
          </div>
       </div>
 </div>