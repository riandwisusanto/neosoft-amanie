@extends('base')

@section('title')
  Marketing Sources
@endsection

@section('breadcrumb')
   @parent
   <li>Marketing Sources</li>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-8">
            <span style="font-size:20px; font-weight: bold;">MARKETING SOURCES</span>
            <p style="font-size:15px; font-weight: bold;"><i>Manage all referrals, digital advertisement, social media, or printed advertisement.</i></p>
          </div>
          <div class="col-sm-4">
            <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Marketing Source</a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered nowrap dt-responsive table-mark" style="width:100%">
        <thead>
          <tr>
              <th>Marketing Source Name</th>
              <th>Enable</th>
              <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.marketingSources.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
  table = $('.table-mark').DataTable({

    "processing" : true,
     "serverside" : true,
    "ajax" : {
      "url" : "{{ route('marketingSources.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-marketingSources form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id_marketing_source').val();
         if(save_method == "add") url = "{{ route('marketingSources.store') }}";
         else url = "marketingSources/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-marketingSources form').serialize(),
           success : function(data){
              $('#modal-marketingSources').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('#outlet').val('');
   $("#outlet").trigger("change");
   $('input[name=_method]').val('POST');
   $("#modal-marketingSources").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
   $('#modal-marketingSources form')[0].reset();
   $('#group_id').val('').change();
   $('#status_marketing_source_modal').hide();

   $('.modal-title').text('Add Marketing Source');

   document.getElementById('status_marketing_source').style.visibility = 'hidden';
}

function detailInfo(id) {

}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-marketingSources form')[0].reset();
   $.ajax({
     url : "marketingSources/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-marketingSources').modal('show');
       $('.modal-title').text('Edit Marketing Source');

       $('#id_marketing_source').val(data.id_marketing_source);
       $('#marketing_source_name').val(data.marketing_source_name);
       $('#status_marketing_source_modal').show();
       $('#outlet').val(data.outlet).change();

       $('#status_marketing_source').prop('checked', data.status_marketing_source);
       document.getElementById('status_marketing_source').style.visibility = 'visible';


     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Marketing Source!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            disabled:{
                text:'Disable',
                value: 'disable',
                className: 'swal-button btn-warning'
            },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        switch (willDelete) {
            default:
                swal("Marketing Source is safe!");
                break;
            case 'disable':
                $.ajax({
                    url : "/marketingSources/"+id+"?disable=true",
                    type : "POST",
                    data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        swal("Marketing Source has been disabled!", {
                            icon: "success",
                        });
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Disabled the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
            case 'delete':
                $.ajax({
                    url : "/marketingSources/"+id,
                    type : "POST",
                    data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        if (data === "error") {
                            swal({
                                text: 'Marketing Source is Used !',
                                icon: 'error'
                            })
                        } else {
                            swal("Marketing Source has been deleted!", {
                                icon: "success",
                            });
                        }
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Delete the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
        }
    });
}
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
