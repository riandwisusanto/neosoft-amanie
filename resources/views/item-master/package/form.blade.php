<div class="modal" id="modal-packages" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Outlet</label>
                                    <select id="outlet" name="outlet[]"
                                        class="form-control select2 select2-hidden-accessible" multiple=""
                                        data-placeholder="Select a Outlet" style="width: 100%;" tabindex="-1"
                                        aria-hidden="true" required>
                                        @foreach ($outlets as $list)
                                        <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Package Type</label>
                                    <input type="text" class="form-control" placeholder="Package Name" autocomplete="off" value="Package" required disabled>
                                    <span class="help-block text-red" id="type"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Package Name</label>
                                    <input type="text" class="form-control" id="package_name" name="package_name"
                                        placeholder="Package Name" autocomplete="off" required>
                                </div>
                            </div>
                            <!-- Commission Package -->
                            <div class="col-md-3">
                                <label>Commission (%) or (Rp)</label>
                                <input type="text" class="form-control col-md-3" id="commission" name="commission"
                                    placeholder="Commission (%) or (Rp)" autocomplete="off">
                            </div>
                            <div class="col-md-3" style="padding-top: 1.8em;">
                                <select id="commissionType" name="commissionType" class="form-control" style="width: 100% !important">
                                    <option value="0">Percent</option>
                                    <option value="1">Rp</option>
                                </select>
                            </div>
                            <!--  -->
                        </div>
                    </div><br>
                    <div class="row">
                        <div id="render"></div>
                        @include('item-master.package.package')
                        <input type="hidden" name="save_json" id="save_json" value="[]" required>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 ">
                            <div class="form-group">
                                <div class="col-md-3 col-md-offset-8">
                                    <label class="control-label">Total</label>
                                    <input type="text" class="form-control" id="packprice" name="packprice" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="price_log_group">
                        <div class="form-group">
                            <label for="price_log" class="col-md-3 control-label">Price Log</label>
                            <div class="col-md-12">
                                <table id="price_log_table" class="table table-striped table-bordered nowrap"       >
                                    <thead>
                                    <tr>
                                        <th>Edited At</th>
                                        <th>Edited By</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div id="render_point"></div>
                        @include('item-master.package.point')
                        <input type="hidden" name="save_json_point" id="save_json_point" value="[]">
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 ">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Remarks</label>
                                    <textarea class="form-control" id="remarks" name="remarks"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" onclick="window.location.href = '/packages'" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>

        </div>
    </div>
</div>
