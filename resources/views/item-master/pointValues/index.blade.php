@extends('base')

@section('title')
  List Point
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection

@section('breadcrumb')
   @parent
   <li>Point</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Point</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-point" style="width:100%">
        <thead>
          <tr>
              <th>Point Value</th>
              <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.pointValues.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
    table = $('.table-point').DataTable({

        // "processing" : true,
        "serverside" : true,
        "ajax" : {
        "url" : "{{ route('pointValues.data') }}",
        "type" : "GET"
        }
    });

  $('#modal-point form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('pointValues.store') }}";
         else url = "pointValues/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-point form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-point').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('#outlet').val('');
   $("#outlet").trigger("change");
   $('input[name=_method]').val('POST');
   $("#modal-point").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
   $('#modal-point form')[0].reset();
   $('.modal-title').text('Add Point');
}


function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-point form')[0].reset();
   $.ajax({
     url : "pointValues/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-point').modal('show');
       $('.modal-title').text('Edit Bank');

       $('#id').val(data.id_point_value);
       $('#point').val(data.point);


     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Point!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        switch (willDelete) {
            default:
                swal("Point is safe!");
                break;
            case 'delete':
                $.ajax({
                    url : "/pointValues/"+id,
                    type : "POST",
                    data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){

                            swal("Point has been deleted!", {
                                icon: "success",
                            });
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Delete the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
        }
    });
}

</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
