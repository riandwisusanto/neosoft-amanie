


<div class="modal" id="modal-ranks" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body" >
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="name" class="col-md-3 control-label">Name </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-md-3 control-label">Desc</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" id="desc" name="desc" placeholder="Description" autocomplete="off" rows="5" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="value_1" class="col-md-3 control-label">Value</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="value_1" name="value_1" placeholder="Value 1" rel="value_1" required>
                            <span class="help-block text-red" id="msg_value1"></span>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="value_2" name="value_2" placeholder="Value 2" rel="value_2" required>
                            <span class="help-block text-red" id="msg_value2"></span>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
 </div>
