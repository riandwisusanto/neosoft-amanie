@extends('base')

@section('title')
  List Loyalty Ranks
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection

@section('breadcrumb')
   @parent
   <li>Loyalty Ranks</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-8">
            <span style="font-size:20px; font-weight: bold;">LOYALTY RANKS</span>
            <p style="font-size:15px; font-weight: bold;"><i>Manage all loyalty membership.</i></p>
          </div>
          <div class="col-sm-4">
              <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add ranks</a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-rank" style="width:100%">
        <thead>
          <tr>
              <th>Name</th>
              <th>Value 1</th>
              <th>Value 2</th>
              <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.ranks.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $(function(){

  format_money();

  table = $('.table-rank').DataTable({

    "processing" : true,
    "serverside" : true,
    "order": [[ 2, "asc" ]],
    "ajax" : {
      "url" : "{{ route('ranks.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-ranks form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('ranks.store') }}";
         else url = "ranks/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-ranks form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-ranks').modal('hide');
              $("#msg_value1").html('');
              $("#msg_value2").html('');

              table.ajax.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function format_money(){
    var rupiah = document.getElementById('value_1');
    var rupiah2 = document.getElementById('value_2');

    rupiah.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value);
    });
    rupiah2.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah2.value = formatRupiah(this.value);
    });
  }

function formatRupiah(angka){
    var number_string = angka.replace(/[^.\d]/g, '').toString(),
        split   		= number_string.split('.'),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);


    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? ',' : '';
        console.log(separator);

        rupiah += separator + ribuan.join(',');
    }

    return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function addForm(){
   save_method = "add";
   $('input[name=_method]').val('POST');
   $("#modal-ranks").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
   $('#modal-ranks form')[0].reset();

   $('.modal-title').text('Add Rank');
}


function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-ranks form')[0].reset();
   $.ajax({
     url : "ranks/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-ranks').modal('show');
       $('.modal-title').text('Edit Bank');

       $('#id').val(data.id_rank);
       $('#name').val(data.name);
       $('#desc').val(data.desc);
       $('#value_1').val(data.value_1);
       $('#value_2').val(data.value_2);

     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Rank!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        switch (willDelete) {
            default:
                swal("Rank is safe!");
                break;
            case 'delete':
                $.ajax({
                    url : "/ranks/"+id,
                    type : "POST",
                    data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){

                        swal("Rank has been deleted!", {
                                icon: "success",
                        });
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Delete the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
        }
    });

}

</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
