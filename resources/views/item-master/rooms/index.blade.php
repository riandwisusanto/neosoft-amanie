@extends('base')

@section('title')
  List Room
@endsection

@section('breadcrumb')
   @parent
   <li>Rooms</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-8">
            <span style="font-size:20px; font-weight: bold;">ROOM MANAGEMENT</span>
            <p style="font-size:15px; font-weight: bold;"><i>Manage all beds or rooms in the clinic.</i></p>
          </div>
          <div class="col-sm-4">
            <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Rooms</a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
          <tr>
              <th>Room Name</th>
              <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.rooms.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
  table = $('.table').DataTable({
    "serverside" : true,
    "ajax" : {
      "url" : "{{ route('rooms.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-rooms form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('rooms.store') }}";
         else url = "rooms/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-rooms form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-rooms').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('#outlet').val('');
   $("#outlet").trigger("change");
   $('input[name=_method]').val('POST');
   $("#modal-rooms").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
   $('#modal-rooms form')[0].reset();
   $('#group_id').val('').change()
   $('.modal-title').text('Add Room');
}

function detailInfo(id) {

}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-rooms form')[0].reset();
   $.ajax({
     url : "rooms/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-rooms').modal('show');
       $('.modal-title').text('Edit Room');

       $('#id').val(data.id_room);
       $('#room_name').val(data.room_name);


     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
   swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Rooms!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
        }).then((willDelete) => {
                switch (willDelete) {
                    default:
                        swal("Rooms is safe!");
                        break;

                    case 'delete':
                        $.ajax({
                            url : "/rooms/"+id,
                            type : "POST",
                            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                            success : function(data){
                                console.log(data);
                                if (data === "error") {
                                    swal({
                                        text: 'Rooms Is Using Client!',
                                        icon: 'error'
                                    })
                                }else{
                                    swal("Rooms has been deleted!", {
                                        icon: "success",
                                    });
                                }
                                table.ajax.reload();
                            },
                            error : function(){
                                swal({
                                    text: 'Can not Delete the Data!',
                                    icon: 'error'
                                })
                            }
                        });
                        break;
                }
          });

}


// validasi
  $(document).ready(function(){
    // $("#noktp").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#contact").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_phone").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#familyphone").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });
  });
</script>
@endsection
