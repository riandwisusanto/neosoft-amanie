<div class="modal" id="modal-therapist-schedules" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>   
         </button>
         <h3 class="modal-title"></h3>
     </div>
         <div class="modal-body" >
             <input type="hidden" id="id" name="id">
             <input type="hidden" name="therapist_id" value="{{ $id }}">
             <div class="form-group">
                <label class="col-md-3 control-label">Hari</label>
                <div class="col-md-8">
                    <select id="day" name="day" class="form-control select2 select2-hidden-accessible"
                        data-placeholder="Select a Outlet" style="width: 100%;"
                        tabindex="-1" aria-hidden="true" required>
                        @foreach ($days as $k => $v)
                        <option value="{{ $v }}">{{ $v }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Start Time:</label>
                <div class="col-md-8">
                    <div class="input-group">
                        <input type="text" name="start_time" id="start_time" class="form-control timepicker" required="" autocomplete="off">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">End Time:</label>
                <div class="col-md-8">
                    <div class="input-group">
                        <input type="text" name="end_time" id="end_time" class="form-control timepicker" required="" autocomplete="off">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                    </div>
                </div>
            </div>
         </div>
         
         <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
    </form>
 
          </div>
       </div>
 </div>