@extends('base')

@section('title')
  List {{ $therapist->therapist_name }} Schedule
@endsection

@section('breadcrumb')
   @parent
   <li>Therapist Schedules</li>
@endsection
@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection
@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-8">
            <span style="font-size:20px;">Schedules</span>
            <p style="font-size:15px;"><i>Penjadwalan piket terapis/bc/dokter</i></p>
          </div>
          <div class="col-sm-4">
              <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Therapist Schedules</a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered nowrap" style="width:100%">
        <thead>
          <tr>
              <th>Therapist Name</th>
              <th>Day</th>
              <th>Start time</th>
              <th>End time</th>
              <th class="all" width="14%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.therapistSchdeules.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $('.timepicker').datetimepicker({
    format: 'HH:mm'
  });

  $(function(){
  table = $('.table').DataTable({
    //scrollY:        "500px",
    scrollX:        true,
    scrollCollapse: true,
    paging:         true,
    fixedColumns:   {
      leftColumns: 1,
      rightColumns: 1
    },
    "ajax" : {
      "url" : "{{ route('therapist-schedules.data', $id) }}",
      "type" : "GET"
    }
  });

  $('#modal-therapist-schedules form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('therapist-schedules.store') }}";
         else url = "therapist-schedules/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : new FormData($('#modal-therapist-schedules form')[0]),
            cache:false,
            contentType: false,
            processData: false,
           success : function(data){
              $('#modal-therapist-schedules').modal('hide');
              table.ajax.reload();
           },
           error : function(){
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('#outlet').val('');
   $("#outlet").trigger("change");
   $('input[name=_method]').val('POST');
   $("#modal-therapist-schedules").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
   $('#modal-therapist-schedules form')[0].reset();
   $('#group_id').val('').change()
   $('.modal-title').text('Add Therapist');
   $('#kode').attr('readonly', false);

}

function detailInfo(id) {

}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-therapist-schedules form')[0].reset();
   $.ajax({
     url : "therapist-schedules/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-therapist-schedules').modal('show');
       $('.modal-title').text('Edit Therapist');

       $('#id').val(data.id_therapist_schedule);
       $('#start_time').val(data.start_time);
       $('#end_time').val(data.end_time);
       $('#day').val(data.day).change();
     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id) {
  //sweet alert
  swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Therapist!",
      icon: "warning",
      buttons: {
          canceled:{
              text:'Cancel',
              value: 'cancel',
              className: 'swal-button btn-default'
          },
          deleted:{
              text:'Delete',
              value: 'delete',
              className: 'swal-button btn-danger'
          }
      },
      dangerMode: true,
  }).then((willDelete) => {
      switch (willDelete) {
          default:
              swal("Therapist is safe!");
              break;
          case 'delete':
              $.ajax({
                  url : "/therapist-schedules/"+id,
                  type : "POST",
                  data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                  success : function(data){
                      if (data === "error") {
                          swal({
                              text: 'Therapist Is Using Client!',
                              icon: 'error'
                          })
                      }else{
                          swal("Therapist has been deleted!", {
                              icon: "success",
                          });
                      }
                      table.ajax.reload();
                  },
                  error : function(){
                      swal({
                          text: 'Can not Delete the Data!',
                          icon: 'error'
                      })
                  }
              });
              break;
      }
  });
}


// validasi
  $(document).ready(function(){
    $("#noktp").keypress(function(data){
      //console.log(data.which);
    if(data.which < 48 || data.which > 57)
      {
        $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
        return false;
      }
    });

    $("#contact").keypress(function(data){
      //console.log(data.which);
    if(data.which < 48 || data.which > 57)
      {
        $("#msg_phone").html("Please insert real numbers phone").show().fadeOut(4000);
        return false;
      }
    });

    $("#familyphone").keypress(function(data){
      //console.log(data.which);
    if(data.which < 48 || data.which > 57)
      {
        $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
        return false;
      }
    });
  });
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

@endsection
