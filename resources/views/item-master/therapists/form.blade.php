<div class="modal" id="modal-therapists" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>   
         </button>
         <h3 class="modal-title"></h3>
     </div>
         <div class="modal-body" >
             <input type="hidden" id="id" name="id">
             <div class="form-group">
                 <label for="therapist_name" class="col-md-3 control-label">Therapist Name *</label>
                 <div class="col-md-8">
                     <input type="text" class="form-control" id="therapist_name" name="therapist_name" placeholder="Therapist Name" autocomplete="off" required>
                 </div>
             </div>
             <div class="form-group">
                 <label for="group_id" class="col-md-3 control-label">Group *</label>
                 <div class="col-md-8">
                     <select class="form-control select2" style="width: 100%" id="group_id" name="group_id">
                        <option selected="selected" value="0">Without Group</option>
                         @foreach ($groups as $item)
                            <option value="{{ $item->group_id }}">{{ $item->group_name }}</option>
                         @endforeach
                     </select>
                     <span class="help-block text-red" id="msg_group"></span>
                 </div>
             </div>
             <div class="form-group">
                <label for="type" class="col-md-3 control-label">Type</label>
                <div class="col-md-8">
                    <select type="text" class="form-control" style="width: 100%" id="type" name="type">
                       <option></option>
                       <option value="0">Doctor</option>
                       <option value="1">Therapist</option>

                    </select>
                    <span class="help-block text-red" id="msg_group"></span>
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-3 control-label">Branch</label>
                <div class="col-md-8">
                    <select id="outlet" name="outlet[]" class="form-control select2 select2-hidden-accessible"
                        multiple="multiple" data-placeholder="Select a Outlet" style="width: 100%;"
                        tabindex="-1" aria-hidden="true" required>
                        @foreach ($outlets as $list)
                        <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                        @endforeach
                    </select>
                </div>
             </div>
             <div class="form-group">
                <label class="col-md-3 control-label">Description</label>
                <div class="col-md-8">
                    <textarea name="description" id="description" placeholder="Enter..." class="form-control" required></textarea>    
                </div>
             </div>
             <div class="form-group">
                <label class="col-md-3 control-label">Image</label>
                <div class="col-md-8">
                    <input type="file" name="img" class="form-control" id="img" >
                    <div class="tampil-image"></div>
                </div>
             </div>
             <div class="form-check" id="status_therapist_modal">
                    <label for="status_therapist" class="col-md-3 control-label"></label>
                    <div class="col-md-8">
                      <input type="checkbox" class="form-check-input" id="status_therapist"
                        name="status_therapist" value="1">
                      <label for="status_therapist" class="form-check-label">Enable</label>
                    </div>
                </div>
         </div>
         
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
    </form>
 
          </div>
       </div>
 </div>