@extends('base')

@section('title')
  List Therapist
@endsection

@section('breadcrumb')
   @parent
   <li>Therapist</li>
@endsection
@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
  .table img {
    height: 60px !important;
    width: auto !important;
  }
</style>
@endsection
@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-8">
            <span style="font-size:20px; font-weight: bold;">THERAPISTS/DOCTORS</span>
            <p style="font-size:15px; font-weight: bold;"><i>Manage all therapists/doctors.</i></p>
          </div>
          <div class="col-sm-4">
              <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Therapists/Doctors</a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered nowrap table-therapists" style="width:100%">
        <thead>
          <tr>
              <th>Therapist Name</th>
              <th>Group</th>
              <th>Join Date</th>
              <th>Image</th>
              <th>Enable</th>
              <th class="all" style="max-width: 28%;">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.therapists.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
  table = $('.table').DataTable({
    scrollX:        true,
    scrollCollapse: true,
    paging:         true,
    fixedColumns:   {
        leftColumns: 1,
        rightColumns: 1
    },
    "ajax" : {
      "url" : "{{ route('therapists.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-therapists form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('therapists.store') }}";
         else url = "therapists/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : new FormData($('#modal-therapists form')[0]),
            cache:false,
            contentType: false,
            processData: false,
           success : function(data){
              $('#modal-therapists').modal('hide');
              table.ajax.reload();
           },
           error : function(xhr, status, error){
              var text = "";
              $.each(xhr.responseJSON.errors, function (key, item) {
                text += item;
              });
              swal({
                  text: text,
                  icon: 'error'
              })
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('#outlet').val('');
   $("#outlet").trigger("change");
   $('input[name=_method]').val('POST');
   $("#modal-therapists").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
   $('#modal-therapists form')[0].reset();
   $('#group_id').val('').change()
   $('.modal-title').text('Add Therapists/Doctors');
   $('#kode').attr('readonly', false);
   document.getElementById('status_therapist_modal').style.visibility = 'hidden';

}

function detailInfo(id) {

}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-therapists form')[0].reset();
   $.ajax({
     url : "therapists/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-therapists').modal('show');
       $('.modal-title').text('Edit Therapists/Doctors');

       $('#id').val(data.id_therapist);
       $('#therapist_name').val(data.therapist_name);
       $('#group_id').val(data.group_id).change();
       $('#outlet').val(data.outlet).change();
       $('#type').val(data.type).change();
       $('#description').text(data.description);

       $('.tampil-image').html('<img src="storage/'+data.img+'" class="img-responsive">');

       $('#status_therapist').prop('checked', data.status_therapist);
       document.getElementById('status_therapist_modal').style.visibility = 'visible';

     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id) {
  //sweet alert
  swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Therapist!",
      icon: "warning",
      buttons: {
          canceled:{
              text:'Cancel',
              value: 'cancel',
              className: 'swal-button btn-default'
          },
          disabled:{
              text:'Disable',
              value: 'disable',
              className: 'swal-button btn-warning'
          },
          deleted:{
              text:'Delete',
              value: 'delete',
              className: 'swal-button btn-danger'
          }
      },
      dangerMode: true,
  }).then((willDelete) => {
      switch (willDelete) {
          default:
              swal("Therapist is safe!");
              break;
          case 'disable':
              $.ajax({
                  url : "/therapists/"+id+"?disable=true",
                  type : "POST",
                  data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                  success : function(data){
                      swal("Therapist has been disabled!", {
                          icon: "success",
                      });
                      table.ajax.reload();
                  },
                  error : function(){
                      swal({
                          text: 'Can not Disabled the Data!',
                          icon: 'error'
                      })
                  }
              });
              break;
          case 'delete':
              $.ajax({
                  url : "/therapists/"+id,
                  type : "POST",
                  data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                  success : function(data){
                      if (data === "error") {
                          swal({
                              text: 'Therapist Is Using Client!',
                              icon: 'error'
                          })
                      }else{
                          swal("Therapist has been deleted!", {
                              icon: "success",
                          });
                      }
                      table.ajax.reload();
                  },
                  error : function(){
                      swal({
                          text: 'Can not Delete the Data!',
                          icon: 'error'
                      })
                  }
              });
              break;
      }
  });
}


// validasi
  $(document).ready(function(){
    $("#noktp").keypress(function(data){
      //console.log(data.which);
    if(data.which < 48 || data.which > 57)
      {
        $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
        return false;
      }
    });

    $("#contact").keypress(function(data){
      //console.log(data.which);
    if(data.which < 48 || data.which > 57)
      {
        $("#msg_phone").html("Please insert real numbers phone").show().fadeOut(4000);
        return false;
      }
    });

    $("#familyphone").keypress(function(data){
      //console.log(data.which);
    if(data.which < 48 || data.which > 57)
      {
        $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
        return false;
      }
    });
  });
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

@endsection
