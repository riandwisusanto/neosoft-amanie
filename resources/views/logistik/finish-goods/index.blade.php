@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Item</th>
            <th>Mfg date </th>
            <th>Expire date</th>
            <th>Description</th>
            <th>QTY</th>
            <th>Status</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include($view.'form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;
  var items = {!! json_encode($items) !!};
  var i = 1;
  var rack_data = [];

  function loadAjaxRack(warehouse_id){
    return new Promise((resolve, reject) => {
      $.ajax({
        url: '{{ route('product-mutations.get-rack') }}',
        data: {warehouse_id: warehouse_id},
        dataType: 'json',
        success: function (res) {
          resolve(res);
        },
        error: function(res){
          reject(res);
        }
      });
    })
  }

  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
    table = $('.table-city').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax" : {
        "url" : "{{ route($route.'.data') }}",
        "type" : "GET"
      }
    });

    $('#modal-{{ $route }} form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route($route.'.store') }}";
        else url = "/{{ $route }}/" + id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-{{ $route }} form').serialize(),
          success : function(data){
            console.log(data)
            $('#modal-{{ $route }}').modal('hide');
            table.ajax.reload();
          },
          error : function(e){
            console.log(e)
            alert("Can not Save the Data!");
          }
        });
        return false;
      }
    });

    $('#work_in_process_id').change(function(){
      let id = $(this).val();
      if(id != null){
        $.ajax({
          url: `{{ url('finish-goods/get-work-in-process') }}/${id}`,
          dataType: "json",
          success: function (res) {
            $('#mfg_date').val(moment(res.wip_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('#uom_id').val(res.uom_id);
            $('#uom_name').val(res.uom ? res.uom.name : '-');
            $('#qty').val(res.order_qty);

            let price = 0;
            if(res.work_in_process_details){
              res.work_in_process_details.map(r => {
                price += parseInt(r.total);
              });
            }

            $('#price').val(price);

            if(res.work_order){
              $('#item_id').val(res.work_order.item_id);
              $('#item_name').val(res.work_order.item ? res.work_order.item.code : '-');
            }
          }
        });
      }
    })
  });

  $('body').on('click', '.delete-item', function(){
    let id = $(this).data('id');
    if(id){
      let delId = JSON.parse($('#del-po-item-id').val());
      delId = [...delId, id];
      $('#del-po-item-id').val(JSON.stringify(delId));
    }
    $(this).parent().parent().remove();
  })

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-{{ $route }}").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-{{ $route }} form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
    $('#receive_details').html('');
    $('#del-po-item-id').val('[]');
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-{{ $route }} form')[0].reset();
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
    $('#del-po-item-id').val('[]');

    $.ajax({
      url : "/{{ $route }}/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-{{ $route }}').modal('show');
        $('.modal-title').text('Edit {{ $title }}');
      
        $('#id').val(data.id);
        $('#begin_stock').val(data.begin_stock);
        $('#price').val(data.price);
        $('#description').val(data.description);
        $('#qty').val(data.qty);
        $('#mfg_date').val(data.mfg_date);
        $('#expired_date').val(moment(data.expired_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#work_in_process_id').val(data.work_in_process_id).trigger('change');
        $('#uom_id').val(data.uom_id).trigger('change');
        $('#item_id').val(data.item_id).trigger('change');

        $('#status-1, #status-2, #status-3').removeAttr('checked');
        if(data.status == 1){
          $('#status-1').attr('checked', true);
        }else if(data.status == 2){
          $('#status-2').attr('checked', true);
        }else{
          $('#status-3').attr('checked', true);
        }
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this {{ $title }}!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/{{ $route }}/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
              }                        
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
