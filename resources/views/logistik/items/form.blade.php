<div class="modal" id="modal-items" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="product_treatment_code" class="col-md-3 control-label">Product *</label>
                        <div class="col-md-8">
                            <select name="product_treatment_code" id="product_treatment_code" class="form-control select2">
                                <option value="-">Choose product</option>
                                @foreach ($product as $r)
                                <option value="{{ $r->product_treatment_code }}">{{ $r->product_treatment_code }} - {{ $r->product_treatment_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="product_treatment_code" class="col-md-3 control-label">Unit of Material *</label>
                        <div class="col-md-8">
                            <select name="uom_id" id="uom_id" class="form-control select2">
                                @foreach ($uoms as $r)
                                <option value="{{ $r->id_uom }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="brand" class="col-md-3 control-label">Brand *</label>
                        <div class="col-md-8">
                            <input type="text" name="brand" id="brand" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-3 control-label">Description *</label>
                        <div class="col-md-8">
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category_id" class="col-md-3 control-label">Category *</label>
                        <div class="col-md-8">
                            <select name="category_id" id="category_id" class="form-control select2">
                                @foreach ($categories as $r)
                                <option value="{{ $r->id_category }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="begin_stock" class="col-md-3 control-label">Begin Stock *</label>
                        <div class="col-md-8">
                            <input type="number" name="begin_stock" id="begin_stock" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="buffer_stock" class="col-md-3 control-label">Buffer Stock *</label>
                        <div class="col-md-8">
                            <input type="number" name="buffer_stock" id="buffer_stock" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="begin_price" class="col-md-3 control-label">Begin Price *</label>
                        <div class="col-md-8">
                            <input type="number" name="begin_price" id="begin_price" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="post_code" class="col-md-3 control-label">Status *</label>
                        <div class="col-md-8">
                            <label><input type="checkbox" name="is_active" value="1" id="active" checked> Enable</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>
