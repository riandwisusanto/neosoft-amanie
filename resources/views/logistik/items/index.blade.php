@extends('base')

@section('title')
  List Items
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>Items</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Items</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Code</th>
            <th>Product</th>
            <th>Description</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Begin Stock</th>
            <th>Status</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('logistik.items.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });    

  $(function(){
    table = $('.table-city').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax" : {
        "url" : "{{ route('items.data') }}",
        "type" : "GET"
      }
    });

    $('#modal-items form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route('items.store') }}";
        else url = "/items/"+id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-items form').serialize(),
          success : function(data){
            console.log(data)
            $('#modal-items').modal('hide');
            table.ajax.reload();
          },
          error : function(e){
            console.log(e)
            alert("Can not Save the Data!");
          }
        });
        return false;
      }
    });
  });

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-items").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-items form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#active').attr('checked', true);
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-items form')[0].reset();
    $('#active').removeAttr('checked');

    $.ajax({
      url : "/items/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-items').modal('show');
        $('.modal-title').text('Edit {{ $title }}');

        $('#id').val(data.id_item);
        $('#code').val(data.code);
        $('#brand').val(data.brand);
        $('#begin_stock').val(data.begin_stock);
        $('#buffer_stock').val(data.buffer_stock);
        $('#begin_price').val(data.begin_price);
        $('#description').val(data.description);

        $('#category_id').val(data.category_id).trigger('change');
        $('#uom_id').val(data.uom_id).trigger('change');
        $('#product_treatment_code').val(data.product_treatment_code ? data.product_treatment_code : '-' ).trigger('change');        

        if(data.is_active == 1){
          $('#active').attr('checked', true);
        }
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this {{ $title }}!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/items/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
              }                        
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
