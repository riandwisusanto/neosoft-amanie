<div class="modal" id="modal-product-locations" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>
         </button>
         <h3 class="modal-title">Add {{ $title }}</h3>
     </div>
         <div class="modal-body" >
            <input type="hidden" id="id" name="id">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="item_id">Item</label>
                        <select name="item_id" id="item_id" class="form-control select2">
                            @foreach ($items as $r)
                            <option value="{{ $r->id_item }}">{{ $r->description }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="warehouse_id">Warehouse</label>
                        <select name="warehouse_id" class="form-control select2" id="warehouse_id" data-rack="rack_id">
                            @foreach ($warehouses as $r)
                            <option value="{{ $r->id_warehouse }}">{{ $r->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="rack_id">Rack</label>
                        <select name="rack_id" id="rack_id" class="form-control select2"></select>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="addr">Description *</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label>Status *</label>
                        <p>
                            <label><input type="checkbox" name="is_active" value="1" id="is_active" checked=checked> Enable</label>
                        </p>
                    </div>
                </div>
            </div>
         </div>
         
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
            </form>
        </div>
    </div>
 </div>
