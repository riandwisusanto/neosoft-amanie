<div class="modal" id="modal-purchase-orders" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="purchase_order_num" class="control-label">Purchase Order Num *</label>
                            <input type="text" id="purchase_order_num" class="form-control" disabled>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="purchase_order_date" class="control-label">Purchase Order Date *</label>
                            <input type="text" name="purchase_order_date" id="purchase_order_date" class="form-control datepicker" value="<?= date('d/m/Y') ?>" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="id_supplier" class="control-label">Supplier *</label>
                            <select name="id_supplier" id="id_supplier" class="form-control select2">
                                @foreach ($suppliers as $r)
                                <option value="{{ $r->id_supplier }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Description *</label>
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="due_date" class="control-label">Due date *</label>
                            <input type="text" name="due_date" id="due_date" class="form-control datepicker" value="<?= date('d/m/Y') ?>" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="terms" class="control-label">Terms *</label>
                            <input type="number" name="terms" id="terms" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="payment_method" class="control-label">Payment Method *</label>
                            <select name="payment_method" id="payment_method" class="form-control select2">
                                    <option value="Cash">Cash</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="Bank Transfer">Bank Transfer</option>
                                    <option value="Debit Card">Debit Card</option>
                                    <option value="Credit Card">Credit Card</option>
                            </select>
                        </div>
                        <style>
                            .table-custom th,
                            .table-custom td{
                                font-size: .9rem;
                            }
                        </style>
                        <div class="col-sm-12">
                            <input type="hidden" name="delId" id="del-po-item-id" value="[]">
                            <table class="table table-bordered table-custom">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Description</th>
                                        <th>Qty</th>
                                        <th>Uom</th>
                                        <th>Price</th>
                                        <th>Disc</th>
                                        <th>Tax</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="purchase_order_items"></tbody>
                                <tfoot id="tfoot-details">
                                    <tr>
                                        <td>
                                            <select id="item_id" class="form-control select2">
                                                @foreach ($items as $r)
                                                <option value="{{ $r->id_item }}">{{ $r->description }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" id="description_item" class="form-control">
                                        </td>
                                        <td>
                                            <input type="number" id="qty" class="form-control">
                                        </td>
                                        <td>
                                            <select id="uom_id" class="form-control select2">
                                                @foreach ($uom as $r)
                                                <option value="{{ $r->id_uom }}">{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" id="price" class="form-control">
                                        </td>
                                        <td>
                                            <input type="number" id="disc" class="form-control">
                                        </td>
                                        <td>
                                            <input type="number" id="tax" class="form-control">
                                        </td>
                                        <td>
                                            <input type="number" id="total" class="form-control" readonly>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button" role="button" id="add-table-item"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>
