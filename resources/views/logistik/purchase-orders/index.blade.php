@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Po num</th>
            <th>Po date</th>
            <th>Description</th>
            <th>Terms</th>
            <th>Status</th>
            <th>Due date</th>
            <th>Supplier</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('logistik.purchase-orders.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method, code = '{{ $code }}';
  var items = {!! json_encode($items) !!};
  var uom   = {!! json_encode($uom) !!};
  var i = 1;

  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
    table = $('.table-city').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax" : {
        "url" : "{{ route('purchase-orders.data', ($status ? ['status' => $status] : [])) }}",
        "type" : "GET"
      }
    });

    $('#modal-purchase-orders form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route('purchase-orders.store') }}";
        else url = "purchase-orders/"+id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-purchase-orders form').serialize(),
          success : function(data){
            $('#modal-purchase-orders').modal('hide');
            table.ajax.reload();
            if(save_method == "add") code = data;
          },
          error : function(e){
            swal({
              title : 'Can not save the data',
              icon  : "warning",
            });
          }
        });
        return false;
      }
    });

    $('#qty, #tax, #disc, #price').on('change keyup', function(){
      let price = $('#price').val() ? parseInt($('#price').val()) : 0;
      let disc = $('#disc').val() ? parseInt($('#disc').val()) : 0;
      let tax = $('#tax').val() ? parseInt($('#tax').val()) : 0;
      let qty = $('#qty').val() ? parseInt($('#qty').val()) : 0;
      let subtotal = (price * qty) + tax - disc;

      $('#total').val(subtotal);
    })

    $('#add-table-item').click(function(){
      let description_item = $('#description_item').val();
      let price = $('#price').val();
      let item_id = $('#item_id').val();
      let uom_id = $('#uom_id').val();
      let total = $('#total').val();
      let disc = $('#disc').val();
      let qty = $('#qty').val();
      let tax = $('#tax').val();

      let html = `<tr>
        <td>
          <select id="item_id_${i}" name="items[${i}][item_id]" class="form-control select2">${ itemsOption(item_id) }</select>
        </td>
        <td>
          <input type="text" id="description_item_${i}" name="items[${i}][description]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="qty_${i}" name="items[${i}][qty]" class="form-control" required>
        </td>
        <td>
          <select id="item_id_${i}" name="items[${i}][uom_id]" class="form-control select2">${ uomOption(uom_id) }</select>
        </td>
        <td>
          <input type="number" id="price_${i}" name="items[${i}][price]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="disc_${i}" name="items[${i}][disc]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="tax_${i}" name="items[${i}][tax]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="total_${i}" name="items[${i}][total]" class="form-control" required>
        </td>
        <td>
          <button class="btn btn-danger btn-sm delete-item" type="button" role="button"><i class="fa fa-times"></i></button>
        </td>
      </tr>`;

      var purchase_order = $('#purchase_order_items').append(html);
      $('.select2').select2();

      var qty_aa = $(`#qty_${i}`).val(qty);
      var tax_aa = $(`#tax_${i}`).val(tax);
      var disc_aa = $(`#disc_${i}`).val(disc);
      var total_aa = $(`#total_${i}`).val(total);
      var price_aa = $(`#price_${i}`).val(price);
      var description_item_aa = $(`#description_item_${i}`).val(description_item);
      
      subtotal();
      function subtotal () {
        price = price_aa.val() ? parseInt(price_aa.val()) : 0;
        disc = disc_aa.val() ? parseInt(disc_aa.val()) : 0;
        tax = tax_aa.val() ? parseInt(tax_aa.val()) : 0;
        qty = qty_aa.val() ? parseInt(qty_aa.val()) : 0;
        total = (price * qty) + tax - disc;

        total_aa.val(total);
      }
      $(`#price_${i}, #qty_${i}, #disc_${i}, #tax_${i}`).on('change keyup', function(){
        subtotal();
      })
      i++;

      $('#tfoot-details').find('input').val('');
    })
  });

  $('body').on('click', '.delete-item', function(){
    let id = $(this).data('id');
    if(id){
      let delId = JSON.parse($('#del-po-item-id').val());
      delId = [...delId, id];
      $('#del-po-item-id').val(JSON.stringify(delId));
      console.log(delId)
    }
    $(this).parent().parent().remove();
  })

  function itemsOption(id){
    let html = '';
    items.map(r => {
      html += `<option value="${r.id_item}" ${id == r.id_item ? 'selected' : ''}>${r.description}</option>`;
    })
    return html;
  }

  function uomOption(id){
    let html = '';
    uom.map(r => {
      html += `<option value="${r.id_uom}" ${id == r.id_uom ? 'selected' : ''}>${r.name}</option>`;
    })
    return html;
  }

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-purchase-orders").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-purchase-orders form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#active').attr('checked', true);
    $('#purchase_order_items').html('');
    $('#del-po-item-id').val('[]');
    $('#purchase_order_num').val(code);
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-purchase-orders form')[0].reset();
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
    $('#del-po-item-id').val('[]');

    $.ajax({
      url : "purchase-orders/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-purchase-orders').modal('show');
        $('.modal-title').text('Edit {{ $title }}');
      
        $('#id').val(data.id);
        $('#purchase_order_num').val(data.purchase_order_num);
        $('#purchase_order_date').val(data.purchase_order_date);
        $('#due_date').val(data.due_date);
        $('#terms').val(data.terms);
        $('#payment_method').val(data.payment_method);
        $('#description').val(data.description);

        $('#id_supplier').val(data.supplier_id).trigger('change');
        $('#purchase_order_items').html('');

        if(data.is_active == 1){
          $('#active').attr('checked', true);
        }else{
          $('#notactive').attr('checked', true);
        }
        let html = ''
        data.purchase_order_details.map(r => {
           html = `<tr>
            <td>
              <input type="hidden" id="id_${i}" name="items[${i}][id]">
              <select id="item_id_${i}" name="items[${i}][item_id]" class="form-control select2">${ itemsOption(r.item_id) }</select>
            </td>
            <td>
              <input type="text" id="description_item_${i}" name="items[${i}][description]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="qty_${i}" name="items[${i}][qty]" class="form-control" required>
            </td>
            <td>
              <select id="item_id_${i}" name="items[${i}][uom_id]" class="form-control select2">${ uomOption(uom_id) }</select>
            </td>
            <td>
              <input type="number" id="price_${i}" name="items[${i}][price]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="disc_${i}" name="items[${i}][disc]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="tax_${i}" name="items[${i}][tax]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="total_${i}" name="items[${i}][total]" class="form-control" required>
            </td>
            <td>
              <button class="btn btn-danger btn-sm delete-item" type="button" role="button" data-id="${r.id}"><i class="fa fa-times"></i></button>
            </td>
          </tr>`;

          var purchase_order = $('#purchase_order_items').append(html);
          $('.select2').select2();

          // aa : after append
          var id_aa = $(`#id_${i}`).val(r.id);
          var qty_aa = $(`#qty_${i}`).val(r.qty);
          var tax_aa = $(`#tax_${i}`).val(r.tax);
          var disc_aa = $(`#disc_${i}`).val(r.disc);
          var total_aa = $(`#total_${i}`).val(r.total);
          var price_aa = $(`#price_${i}`).val(r.price);
          var description_item_aa = $(`#description_item_${i}`).val(r.description);
          
          subtotal();
          function subtotal () {
            price = price_aa.val() ? parseInt(price_aa.val()) : 0;
            disc = disc_aa.val() ? parseInt(disc_aa.val()) : 0;
            tax = tax_aa.val() ? parseInt(tax_aa.val()) : 0;
            qty = qty_aa.val() ? parseInt(qty_aa.val()) : 0;
            total = (price * qty) + tax - disc;

            total_aa.val(total);
          }
          $(`#price_${i}, #qty_${i}, #disc_${i}, #tax_${i}`).on('change keyup', function(){
            subtotal();
          })
          i++;
        })
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this {{ $title }}!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/purchase-orders/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
                code = data;
              }
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }

  function updateStatus(id, status){
    var statusDesc = [
      'Create/Draft PO',
      'Deliver PO',
      'Receive Product',
      'Canceled',
      'Closed PO'
    ];

    swal({
      title: "Are you sure?",
      text: `change status to ${statusDesc[status]}!!`,
      icon: "info",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        updated:{
          text:'Update status',
          value: 'update-status',
          className: 'swal-button btn-primary'
        }
      },
      dangerMode: true,
    }).then((action) => {
      switch (action) {
        case 'update-status':
        $.ajax({
          type     : "post",
          url      : `/purchase-orders/update-status/${id}`,
          data     : {'_token' : $('meta[name=csrf-token]').attr('content')},
          success  : function (res) {
            if(res == 'success'){
              swal("Status has been updated!", {
                icon: "success",
              });
              table.ajax.reload();
            }else{
              swal("Something wrong!", {
                icon: "error",
              });
            }
          }
        });
        break;
        default:
        
        break;
      }
    });
  }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
