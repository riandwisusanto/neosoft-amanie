<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print {{ $title }}</title>
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <style>
    .table-custom td{
      padding: 2px 10px;
    }
  </style>
</head>
<body>
  <div class="container">
    <table class="table-custom">
      <p align="center"><b>PURCHASE ORDER</b></p>
      <tr>
        <td>PO# </td>
        <td>:</td>
        <td>{{ $data->purchase_order_num }}</td>
      </tr>
      <tr>
        <td>PO Date</td>
        <td>:</td>
        <td>{{ date('d M Y', strtotime($data->purchase_order_date)) }}</td>
      </tr>
      <tr>
        <td>Supplier </td>
        <td>:</td>
        <td>{{ $data->supplier ? $data->supplier->name : '-' }}</td>
      </tr>
      <tr>
        <td>Due date </td>
        <td>:</td>
        <td>{{ date('d M Y', strtotime($data->due_date)) }}</td>
      </tr>
      <tr>
        <td>Terms </td>
        <td>:</td>
        <td>{{ $data->terms }}</td>
      </tr>
      <tr>
        <td>Payment method </td>
        <td>:</td>
        <td>{{ $data->payment_method }}</td>
      </tr>
      <tr>
        <td>Description </td>
        <td>:</td>
        <td>{{ $data->description }}</td>
      </tr>
    </table>
    <br />
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Item</th>
          <th>Uom</th>
          <th>Description</th>
          <th>Qty</th>
          <th>Price</th>
          <th>Disc</th>
          <th>Tax</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        @php($no = 1)
        @foreach ($data->purchase_order_details as $r)
        <tr>
          <td>{{ $no }}</td>
          <td>{{ $r->item ? $r->item->description : '-' }}</td>
          <td>{{ $r->uom ? $r->uom->name : '-' }}</td>
          <td>{{ $r->description }}</td>
          <td>{{ number_format($r->qty) }}</td>
          <td>{{ number_format($r->price) }}</td>
          <td>{{ number_format($r->disc) }}</td>
          <td>{{ number_format($r->tax) }}</td>
          <td>{{ number_format($r->total) }}</td>
        </tr>
        @php($no++)
        @endforeach
      </tbody>
    </table>
  </div>
  <script>
    window.print();
  </script>
</body>
</html>