<div class="modal" id="modal-racks" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="main_rack_code" class="col-md-3 control-label">Main Rack Code *</label>
                        <div class="col-md-8">
                            <input type="text" name="main_rack_code" id="main_rack_code" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub_rack_code" class="col-md-3 control-label">Sub Rack Code *</label>
                        <div class="col-md-8">
                            <input type="text" name="sub_rack_code" id="sub_rack_code" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-3 control-label">Description *</label>
                        <div class="col-md-8">
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="warehouse_id" class="col-md-3 control-label">Warehouse *</label>
                        <div class="col-md-8">
                            <select name="warehouse_id" id="warehouse_id" class="form-control select2">
                                @foreach ($warehouses as $r)
                                <option value="{{ $r->id_warehouse }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="post_code" class="col-md-3 control-label">Status *</label>
                        <div class="col-md-8">
                            <label><input type="checkbox" name="is_active" value="1" id="is_active" checked=checked> Enable</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>
