@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Main racks code</th>
            <th>Sub racks code</th>
            <th>Description</th>
            <th>Warehouse</th>
            <th>Status</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('logistik.racks.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });    

  $(function(){
    table = $('.table-city').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax" : {
        "url" : "{{ route('racks.data') }}",
        "type" : "GET"
      }
    });

    $('#modal-racks form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route('racks.store') }}";
        else url = "racks/"+id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-racks form').serialize(),
          success : function(data){
            console.log(data)
            $('#modal-racks').modal('hide');
            table.ajax.reload();
          },
          error : function(e){
            console.log(e)
            alert("Can not Save the Data!");
          }
        });
        return false;
      }
    });
  });

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-racks").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-racks form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-racks form')[0].reset();
    $('#active').removeAttr('checked');

    $.ajax({
      url : "racks/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-racks').modal('show');
        $('.modal-title').text('Edit {{ $title }}');
      
        $('#id').val(data.id_rack);
        $('#main_rack_code').val(data.main_rack_code);
        $('#sub_rack_code').val(data.sub_rack_code);
        $('#brand').val(data.brand);
        $('#begin_stock').val(data.begin_stock);
        $('#description').val(data.description);

        $('#category_id').val(data.category_id).trigger('change');
        

        if(data.is_active == 1){
          $('#active').attr('checked', true);
        }
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this {{ $title }}!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/racks/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
              }                        
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
