@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Code</th>
            <th>Po#</th>
            <th>Date</th>
            <th>Supplier</th>
            <th>Invoice#</th>
            <th>Warehouse</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include($view.'form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method, code = '{{ $code }}';
  var items = {!! json_encode($items) !!};
  var uom   = {!! json_encode($uom) !!};
  var i = 1;
  var rack_data = [];

  function loadAjaxRack(warehouse_id){
    return new Promise((resolve, reject) => {
      $.ajax({
        url: '{{ route('product-mutations.get-rack') }}',
        data: {warehouse_id: warehouse_id},
        dataType: 'json',
        success: function (res) {
          resolve(res);
        },
        error: function(res){
          reject(res);
        }
      });
    })
  }

  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
    table = $('.table-city').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax" : {
        "url" : "{{ route($route.'.data') }}",
        "type" : "GET"
      }
    });

    $('#modal-{{ $route }} form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route($route.'.store') }}";
        else url = "/{{ $route }}/" + id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-{{ $route }} form').serialize(),
          success : function(data){
            console.log(data)
            $('#modal-{{ $route }}').modal('hide');
            table.ajax.reload();
            if(save_method == "add") code = data;
          },
          error : function(e){
            swal({
              title : 'Can not save the data',
              icon  : "warning",
            });
          }
        });
        return false;
      }
    });

    $('#qty, #price').on('change keyup', function(){
      let price = $('#price').val() ? parseInt($('#price').val()) : 0;
      let qty = $('#qty').val() ? parseInt($('#qty').val()) : 0;
      let subtotal = (price * qty);

      $('#total').val(subtotal);
    })

    $('#warehouse_id').change(function(){
      var id = $(this).val();
      let rack = loadAjaxRack(id);
      rack.then(res => {
        rack_data = res;
        let html = rackOption();
        $('#rack_id').html(html);
        $('body').find('.select-rack').each((k, v) => {
          let elem = $($('body').find('.select-rack').get(k));
          if(elem.data('id')){
            elem.html(rackOption(elem.data('id')));
          }else{
            elem.html(rackOption());
          }
        });
      }).catch((msg) => console.log('error ', msg));
    });

    $('#add-table-item').click(function(){
      let expire_date = $('#expire_date').val();
      let batch_no = $('#batch_no').val();
      let item_id = $('#item_id').val();
      let rack_id = $('#rack_id').val();
      let uom_id = $('#uom_id').val();
      let price = $('#price').val();
      let total = $('#total').val();
      let qty = $('#qty').val();

      let html = `<tr>
        <td>
          <select id="item_id_${i}" name="items[${i}][item_id]" class="form-control select2">${ itemsOption(item_id) }</select>
        </td>
        <td>
          <select id="uom_id_${i}" name="items[${i}][uom_id]" class="form-control select2">${ uomOption(uom_id) }</select>
        </td>
        <td>
          <select id="rack_id_${i}" name="items[${i}][rack_id]" class="form-control select2">${ rackOption(rack_id) }</select>
        </td>
        <td>
          <input type="text" id="batch_no_${i}" name="items[${i}][batch_no]" class="form-control" required>
        </td>
        <td>
          <input type="text" id="expire_date_${i}" name="items[${i}][expire_date]" class="form-control datepicker" required>
        </td>
        <td>
          <input type="number" id="qty_${i}" name="items[${i}][qty]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="price_${i}" name="items[${i}][price]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="total_${i}" name="items[${i}][total]" class="form-control" readonly required>
        </td>
        <td>
          <button class="btn btn-danger btn-sm delete-item" type="button" role="button"><i class="fa fa-times"></i></button>
        </td>
      </tr>`;

      var receive_details = $('#receive_details').append(html);
      $(`#item_id_${i}, #uom_id_${i}, #rack_id_${i}`).select2();
      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
      });

      var expire_date_aa = $(`#expire_date_${i}`).val(expire_date);
      var batch_no_aa = $(`#batch_no_${i}`).val(batch_no);
      var total_aa = $(`#total_${i}`).val(total);
      var price_aa = $(`#price_${i}`).val(price);
      var qty_aa = $(`#qty_${i}`).val(qty);
      
      subtotal();
      function subtotal () {
        price = price_aa.val() ? parseInt(price_aa.val()) : 0;
        qty = qty_aa.val() ? parseInt(qty_aa.val()) : 0;
        total = (price * qty);

        total_aa.val(total);
      }
      $(`#price_${i}, #qty_${i}`).on('change keyup', function(){
        subtotal();
      })
      i++;
    })
  });

  $('body').on('click', '.delete-item', function(){
    let id = $(this).data('id');
    if(id){
      let delId = JSON.parse($('#del-po-item-id').val());
      delId = [...delId, id];
      $('#del-po-item-id').val(JSON.stringify(delId));
    }
    $(this).parent().parent().remove();
  })

  function itemsOption(id){
    let html = '';
    items.map(r => {
      html += `<option value="${r.id_item}" ${id == r.id_item ? 'selected' : ''}>${r.description}</option>`;
    })
    return html;
  }

  function uomOption(id){
    let html = '';
    uom.map(r => {
      html += `<option value="${r.id_uom}" ${id == r.id_uom ? 'selected' : ''}>${r.name}</option>`;
    })
    return html;
  }

  function rackOption(id){
    let html = '';
    rack_data.map(r => {
      html += `<option value="${r.id_rack}" ${id == r.id_rack ? 'selected' : ''}>${r.main_rack_code} - ${r.sub_rack_code}</option>`;
    })
    return html;
  }

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-{{ $route }}").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-{{ $route }} form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#receive_details').html('');
    $('#del-po-item-id').val('[]');
    $('#receive_code').val(code);
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-{{ $route }} form')[0].reset();
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
    $('#del-po-item-id').val('[]');

    $.ajax({
      url : "/{{ $route }}/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-{{ $route }}').modal('show');
        $('.modal-title').text('Edit {{ $title }}');
      
        $('#id').val(data.id);
        $('#receive_code').val(data.receive_code);
        $('#receive_date').val(data.receive_date);
        $('#invoice_num').val(data.invoice_num);
        $('#receive_by').val(data.receive_by);
        $('#warehouse_id').val(data.warehouse_id).trigger('change');
        $('#purchase_order_id').val(data.purchase_order_id).trigger('change');
        $('#supplier_id').val(data.supplier_id).trigger('change');
        $('#description').val(data.description);
        $('#receive_details').html('');

        $('#status-1, #status-2, #status-3, #status-4').removeAttr('checked');
        if(data.status === '1'){
          $('#status-1').attr('checked', true);
        }else if(data.status === '2'){
          $('#status-2').attr('checked', true);
        }else if(data.status === '3'){
          $('#status-3').attr('checked', true);
        }else{
          $('#status-4').attr('checked', true);
        }
        let html = ''
        data.receive_details.map(r => {
           html = `<tr>
            <td>
              <input type="hidden" id="id_${i}" name="items[${i}][id]">
              <select id="item_id_${i}" name="items[${i}][item_id]" class="form-control select2append">${ itemsOption(r.item_id) }</select>
            </td>
            <td>
              <select id="uom_id_${i}" name="items[${i}][uom_id]" class="form-control select2append">${ uomOption(r.uom_id) }</select>
            </td>
            <td>
              <select id="rack_id_${i}" name="items[${i}][rack_id]" class="form-control select2append select-rack" data-id="${r.rack_id}"></select>
            </td>
            <td>
              <input type="text" id="batch_no_${i}" name="items[${i}][batch_no]" class="form-control" required>
            </td>
            <td>
              <input type="text" id="expire_date_${i}" name="items[${i}][expire_date]" class="form-control datepicker" required>
            </td>
            <td>
              <input type="number" id="qty_${i}" name="items[${i}][qty]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="price_${i}" name="items[${i}][price]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="total_${i}" name="items[${i}][total]" class="form-control" readonly required>
            </td>
            <td>
              <button class="btn btn-danger btn-sm delete-item" type="button" role="button" data-id="${r.id}"><i class="fa fa-times"></i></button>
            </td>
          </tr>`;

          var receive_details = $('#receive_details').append(html);
          $('.select2append').select2();
          $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
          });

          // aa : after append
          var id_aa = $(`#id_${i}`).val(r.id);
          var qty_aa = $(`#qty_${i}`).val(r.qty);
          var total_aa = $(`#total_${i}`).val(r.total);
          var price_aa = $(`#price_${i}`).val(r.price);
          var batch_no_aa = $(`#batch_no_${i}`).val(r.batch_no);
          var expire_date_aa = $(`#expire_date_${i}`).val(r.expire_date);
          
          subtotal();
          function subtotal () {
            price = price_aa.val() ? parseInt(price_aa.val()) : 0;
            qty = qty_aa.val() ? parseInt(qty_aa.val()) : 0;
            total = (price * qty);

            total_aa.val(total);
          }
          $(`#price_${i}, #qty_${i}`).on('change keyup', function(){
            subtotal();
          })
          i++;
          $('#tfoot-details').find('input').val('');
        })
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this {{ $title }}!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/{{ $route }}/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
                code = data;
              }                        
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
