<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print {{ $title }}</title>
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <style>
    .table-custom td{
      padding: 2px 10px;
    }
  </style>
</head>
<body>
  <div class="container">
    <table class="table-custom">
        <p align="center"><b>RECEIPT OF GOODS</b></p>
      <tr>
        <td>Receive code </td>
        <td>:</td>
        <td>{{ $data->receive_code }}</td>
      </tr>
      <tr>
        <td>Receive date </td>
        <td>:</td>
        <td>{{ date('d M Y', strtotime($data->receive_date)) }}</td>
      </tr>
      <tr>
        <td>Receive by</td>
        <td>:</td>
        <td>{{ $data->receive_by }}</td>
      </tr>
      <tr>
        <td>Receive by</td>
        <td>:</td>
        <td>{{ $data->receive_by }}</td>
      </tr>
      <tr>
        <td>Supplier </td>
        <td>:</td>
        <td>{{ $data->supplier ? $data->supplier->name : '-' }}</td>
      </tr>
      <tr>
        <td>Warehouse </td>
        <td>:</td>
        <td>{{ $data->warehouse ? $data->warehouse->addr : '-' }}</td>
      </tr>
      <tr>
        <td>Invoice num</td>
        <td>:</td>
        <td>{{ $data->invoice_num }}</td>
      </tr>
      <tr>
        <td>Description </td>
        <td>:</td>
        <td>{{ $data->description }}</td>
      </tr>
      <br />
    </table>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Item</th>
          <th>Uom</th>
          <th>Rack</th>
          <th>Batch no</th>
          <th>Expire date</th>
          <th>Qty</th>
          <th>Price</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        @php($no = 1)
        @foreach ($data->receive_details as $r)
        <tr>
          <td>{{ $no }}</td>
          <td>{{ $r->item ? $r->item->description : '-' }}</td>
          <td>{{ $r->uom ? $r->uom->name : '-' }}</td>
          <td>{{ $r->rack ? $r->rack->main_rack_code.' - '.$r->rack->sub_rack_code : '-' }}</td>
          <td>{{ $r->batch_no }}</td>
          <td>{{ $r->expire_date }}</td>
          <td>{{ number_format($r->qty) }}</td>
          <td>{{ number_format($r->price) }}</td>
          <td>{{ number_format($r->total) }}</td>
        </tr>
        @php($no++)
        @endforeach
      </tbody>
    </table>
  </div>
  <script>
    window.print();
  </script>
</body>
</html>