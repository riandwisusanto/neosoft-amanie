@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-stock-card" style="width:100%">
          <thead>
            <tr>
              <th>Transdate</th>
              <th>Item&nbsp;code </th>
              <th>Item&nbsp;name</th>
              <th>Uom</th>
              <th>In</th>
              <th>Const in</th>
              <th>Value in</th>
              <th>Out</th>
              <th>Const out</th>
              <th>Value out</th>
              <th>Balance</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

<script type="text/javascript">
  $(function(){
    table = $('.table-stock-card').DataTable({
      processing    : true,
      serverside    : true,
      scrollY       : "300px",
      scrollX       : true,
      scrollCollapse: true,
      fixedColumns  : {
        leftColumns: 1
      },
      ajax : {
        url : "{{ route($route.'.data') }}",
        type : "GET"
      }
    });
  });
</script>
@endsection
