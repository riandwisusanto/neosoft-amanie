@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Bill&nbsp;Of&nbsp;Materials</th>
            <th>Date</th>
            <th>Item</th>
            <th>Description</th>
            <th>Start Date</th>
            <th>Due Date</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include($view.'form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;
  var items = {!! json_encode($items) !!};
  var i = 1;
  var rack_data = [];

  function loadAjaxRack(warehouse_id){
    return new Promise((resolve, reject) => {
      $.ajax({
        url: '{{ route('product-mutations.get-rack') }}',
        data: {warehouse_id: warehouse_id},
        dataType: 'json',
        success: function (res) {
          resolve(res);
        },
        error: function(res){
          reject(res);
        }
      });
    })
  }

  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
    table = $('.table-city').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax" : {
        "url" : "{{ route($route.'.data') }}",
        "type" : "GET"
      }
    });

    $('#modal-{{ $route }} form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route($route.'.store') }}";
        else url = "/{{ $route }}/" + id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-{{ $route }} form').serialize(),
          success : function(data){
            console.log(data)
            $('#modal-{{ $route }}').modal('hide');
            table.ajax.reload();
          },
          error : function(e){
            console.log(e)
            alert("Can not Save the Data!");
          }
        });
        return false;
      }
    });

    $('#bom_id').change(function(e){
      let id = $(this).val();
      $.ajax({
        url: `/work-orders/get-bom/${id}`,
        dataType: "json",
        success: function (res) {
          $('#item_id').val(res.item_id);
          $('#item_name').val(res.item ? res.item.code : '-');
        }
      });
    })
  });

  $('body').on('click', '.delete-item', function(){
    let id = $(this).data('id');
    if(id){
      let delId = JSON.parse($('#del-po-item-id').val());
      delId = [...delId, id];
      $('#del-po-item-id').val(JSON.stringify(delId));
    }
    $(this).parent().parent().remove();
  })

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-{{ $route }}").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-{{ $route }} form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#active').attr('checked', true);
    $('#receive_details').html('');
    $('#del-po-item-id').val('[]');
    $('#item_id').val('');
    $('#item_name').val('');
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-{{ $route }} form')[0].reset();
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
    $('#del-po-item-id').val('[]');

    $.ajax({
      url : "/{{ $route }}/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-{{ $route }}').modal('show');
        $('.modal-title').text('Edit {{ $title }}');
      
        $('#id').val(data.id);
        $('#wo_date').val(data.wo_date);
        $('#order_uom').val(data.order_uom);
        $('#order_qty').val(data.order_qty);
        $('#start_date').val(data.start_date);
        $('#due_date').val(data.due_date);
        $('#bom_id').val(data.bom_id).trigger('change');
        $('#item_id').val(data.item_id).trigger('change');
        $('#description').val(data.description);
        $('#receive_details').html('');

        if(data.is_active == 1){
          $('#active').attr('checked', true);
        }else{
          $('#active').removeAttr('checked');
        }
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this {{ $title }}!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/{{ $route }}/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
              }                        
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
