@extends('base')

@section('title')
List Wa Blast
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
    .truncate {
        max-width: 50px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
</style>

@endsection

@section('breadcrumb')
@parent
<li>WA Blast</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">WA Blast History</span>
            </div>
            <div class="box-header with-border">
                <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add WA Blast</a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered  table-message" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th style="width: 40%">Message</th>
                            <th>Phone Number</th>
                            <th style="width: 20%">Image</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('message.form')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method;

    $(function () {


        table = $('.table-message').DataTable({
            columnDefs: [{
                targets: 0,
                className: "truncate"
            }],
            createdRow: function (row) {
                var td = $(row).find(".truncate");
                td.attr("title", td.html());
            },
            "ajax": {
                "url": "{{ route('message.data') }}",
                "type": "GET"
            }
        });

        Filevalidation = () => {
            const fi = document.getElementById('fileName');
            // Check if any file is selected. 
            if (fi.files.length > 0) {
                for (let i = 0; i <= fi.files.length - 1; i++) {

                    const fsize = fi.files.item(i).size;
                    const file = Math.round((fsize / 1024));
                    // The size of the file. 
                    if (file >= 1024) {
                        swal("File too Big, please select a file less than 1mb")
                        // alert( 
                        //   "File too Big, please select a file less than 1mb");
                        fi.value = "";
                    }
                }
            }
        }
        $('#modal-message form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                if (save_method == "add") url = "{{ route('message.store') }}";

                $.ajax({
                    url: url,
                    type: "POST",
                    data: new FormData($('#modal-message form')[0]),
                    //  data : $('#modal-message form').serialize(),
                    success: function (data) {
                        swal({
                                title: "Successfully Create Blast",
                                icon: "success",
                                buttons: {
                                    canceled: {
                                        text: 'Cancel',
                                        value: 'cancel',
                                        className: 'swal-button btn-default'
                                    },
                                    sendwa: {
                                        text: 'Send WhatsApp',
                                        value: 'sendwa',
                                        className: 'swal-button btn-success'
                                    }
                                },
                                dangerMode: true,
                            })
                            .then((option) => {
                                switch (option) {
                                    default:
                                        window.location.href = "/message";
                                        break;
                                    case 'sendwa':
                                        $.ajax({
                                            url: "/message/" + data.id_message +
                                                "/wa",
                                            type: "GET",
                                            success: function (x) {
                                                if (data) {
                                                    swal({
                                                        text: 'Send WA Success',
                                                        icon: 'success'
                                                    })
                                                    window.location.href =
                                                        "/message";
                                                }
                                                return false;
                                            },
                                            error: function () {
                                                swal({
                                                    text: 'Send WA Error',
                                                    icon: 'error'
                                                })
                                                return false;
                                            }
                                        });
                                        break;
                                }
                            });
                    },
                    error: function (e) {
                        console.log(e)
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });

    });

    function addForm() {
        save_method = "add";
        $('#customer_id').val('');
        $("#customer_id").trigger("change");
        $('input[name=_method]').val('POST');
        $("#modal-message").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#modal-message form')[0].reset();

        $('.modal-title').text('Add WA Blast');
    }


    function selected_all() {
        if ($("#chkall").is(':checked')) {
            $("#customer_id > option").prop("selected", "selected");
            $("#customer_id").trigger("change");
        } else {
            $("#customer_id").find('option').prop("selected", false);
            $("#customer_id").trigger("change");
        }
    }
    $('#customer_id').on('select2:select', function (e) {
        var data = e.params.data;
        console.log(data);
    });
    function select_1() {
        if ($("#check1").is(':checked')) {
            $("#customer_id > #option1").prop("selected", "selected");
            $("#customer_id").trigger("change");
            $('#check2').prop('checked', false);
            $("#customer_id > #option2").prop("selected", false);
            $("#customer_id").trigger("change");
        } else {
            $("#customer_id").find('option').prop("selected", false);
            $("#customer_id").trigger("change");
        }
    }

    function select_2() {
        if ($("#check2").is(':checked')) {
            $("#customer_id > #option2").prop("selected", "selected");
            $("#customer_id").trigger("change");
            $('#check1').prop('checked', false);
            $("#customer_id > #option1").prop("selected", false);
            $("#customer_id").trigger("change");
        } else {
            $("#customer_id").find('option').prop("selected", false);
            $("#customer_id").trigger("change");
        }
    }

    function printWa(id) {
      window.swal({
        text: "please wait...",
        buttons: false,
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
      })
        $.ajax({
            url: "/message/" + id + "/wa",
            type: "GET",
            success: function (data) {
                if (data) {
                    swal({
                        text: 'Send WA Success',
                        icon: 'success'
                    })
                }
                return false;
            },
            error: function (e) {
                alert("Can not Save the Data!");
            }
        });
        return false;
    }

</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection
