@php
$outstanding_invoice = outstanding_invoice();
$buffer_stock = buffer_stock();
$expired = expired();
@endphp

<!-- Notifications: style can be found in dropdown.less -->
<!-- Reminder Buffer Stock -->
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle text-neowhite neoborder-left" data-toggle="dropdown">
        <i class="fa fa-truck" style="color: white"></i>
        <span class="label label-danger">{{ count($buffer_stock) }}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <strong>{{ count($buffer_stock) }}</strong> buffer stock</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                        @foreach ($buffer_stock as $item)
                        <li>
                            <i class="fa fa-exclamation text-danger"></i> {{ $item->name }}
                            <strong>( {{ $item->sellable_stock }} unit )</strong>
                        </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
</li>


<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle text-neowhite neoborder-left" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        <span class="label label-success">{{ count($outstanding_invoice) }}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <strong>{{ count($outstanding_invoice) }}</strong> Outstanding Invoice</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                        @foreach ($outstanding_invoice as $item)
                        <li>
                            <a href="{{ url('customers/'.$item->customer_id.'') }}">
                                <i class="fa fa-exclamation text-danger"></i> {{ $item->customer->full_name }}
                                <strong>( ${{ $item->remaining }} )</strong>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </li>
        {{-- <li class="footer"><a href="#">View all</a></li> --}}
    </ul>
</li>


<!-- Tasks: style can be found in dropdown.less -->
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle neoborder-left" data-toggle="dropdown">
        <i class="fa fa-flag-o text-neowhite"></i>
        <span class="label label-danger">{{ count($expired) }}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <strong>{{ count($expired) }}</strong> which going expired invoice</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                @foreach ($expired as $item)
                <li>
                    <a href="{{ url('customers/'.$item->customer_id.'') }}">
                        <i class="fa fa-exclamation text-danger"></i> {{ $item->customer->full_name }}
                        ({{ $item->expired_date }})
                    </a>
                </li>
                @endforeach
            </ul>
        </li>
        {{-- <li class="footer">
            <a href="#">View all tasks</a>
        </li> --}}
    </ul>
</li>
<!-- User Account: style can be found in dropdown.less -->

<li class="dropdown">
    <a href="#" class="dropdown-toggle text-neowhite neoborder-left" data-toggle="dropdown">
        {{app()->getLocale()}}
    </a>
    <ul class="dropdown-menu">
        <li><a href="locale/en"> EN (English)</a></li>
        <li><a href="locale/id"> ID (Indonesia)</a></li>
    </ul>
</li>