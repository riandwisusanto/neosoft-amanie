<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ config('app.name') }} POS</title>
<link rel="icon" href="{{ asset('img/logo.png') }}" type="image/x-icon">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
{{-- select --}}
<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">
<!-- Morris chart -->
{{-- <link rel="stylesheet" href="{{ asset('plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}"> --}}
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
<link rel='stylesheet' href="{{ asset('datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
<link href='{{ asset('css/style.css') }}' rel='stylesheet' />
<link href='{{ asset('css/fontGoogle.css') }}' rel='stylesheet' />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<style>
.box{
   box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
   border-radius:5px;
   transition: 0.3s;
}

</style>
</head>
<body class="hold-transition skin-black-light sidebar-mini " onload="startTime()">
<div class="wrapper" id="neosoft-app" style="background-color:white;">
    <div class="row">
        <div class="col-xs-12">
               <div class="box-body">
                   <div class="body">
                                <div class="col-md-8">
                                    <video src="{{ asset('video/Testing1.mp4')}}" autoplay controls playsinline style="width: 100%; height: auto;pointer-events: none; "></video>
                                </div>
                                <div class="col-md-4">
                                    <h2 class="text-center" style="font-family:sans-serif;margin-bottom:40px;margin-top:-2px;"><b>{{$day}}<b id="txt"></b></b></h2>
                                </div>
                                <center>
                                <h1 style="margin-bottom:23px;">NEXT</h1>
                                </center>
                                <div class="col-md-4">
                                        <div class="box box-solid">
                                            <div class="box-body">
                                                <div class="row">
                                                        <div class="col-md-12">
                                                                <div class="col-md-12">
                                                                    <center>
                                                                        <h1 style="font-family:sans-serif;font-size:40px;font-weight:bold;">QUEUE NO</h1>
                                                                        @if(@$queueScreen->queue->queue_number && @$queueScreen->queue->come_date == $date)
                                                                        <h1 style="font-family:sans-serif;font-size:150px;font-weight:bold;color:#347ab8;margin-top:-10px;">{{@$queueScreen->queue->queue_number}}</h1>
                                                                        @else
                                                                        <h1 style="font-family:sans-serif;font-size:85px;font-weight:bold;color:#347ab8;">...</h1>
                                                                        @endif
                                                                    </center>
                                                                </div>
                                                                <hr>
                                                                <div class="col-md-12" style="background-color:#347ab8;color:white">
                                                                    <center>
                                                                        @if(@$queueScreen->therapist->therapist_name && @$queueScreen->queue->come_date == $date)
                                                                        <h2 style="font-family:sans-serif;font-weight:bold;margin-top:10px;">{{@$queueScreen->therapist->therapist_name}}</h2>
                                                                        @else
                                                                        <h2 style="font-family:sans-serif;font-weight:bold;margin-top:10px;">...</h2>
                                                                        @endif
                                                                    </center>
                                                                </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                   </div>
               </div>
               <div class="box-body">
                    <div class="body autoplay">
                                    @foreach($therapist as $app)
                                        @foreach($app->therapist_schedule as $sche)
                                            @if($days == $sche->day)
                                            <div class="col-md-3">
                                                    <div class="box box-solid">
                                                        <div class="box-body">
                                                            <div class="row">
                                                                    <div class="col-md-12">
                                                                    @php
                                                                    $color = array('#347ab8','#33b883','#b73398','#b66f34');

                                                                    $count = count($color) - 1;

                                                                    $i = rand(0, $count);

                                                                    $rand_background = $color[$i];     

                                                                    @endphp
                                                                            <div class="col-md-12">
                                                                                <center>
                                                                                    <h1 style="font-family:sans-serif;font-size:30px;font-weight:bold;">QUEUE NO</h1>
                                                                                    @if(@$app->queueScreen->queue->queue_number && @$app->queueScreen->queue->come_date == $date)
                                                                                    <h1 style="font-family:sans-serif;font-size:85px;font-weight:bold;color:{{$rand_background}}; margin-top:-10px;" id="no">{{@$app->queueScreen->queue->queue_number}}</h1>
                                                                                    @else
                                                                                    <h1 style="font-family:sans-serif;font-size:30px;font-weight:bold;color:{{$rand_background}};">...</h1>
                                                                                    @endif
                                                                                </center>
                                                                            </div> 
                                                                            <div class="col-md-12" style="background-color:{{$rand_background}};color:white;">
                                                                                <center>
                                                                                    <h2 style="font-family:sans-serif;font-size:30px;font-weight:bold;margin-top:10px;">{{ @$app->therapist_name}}</h2>
                                                                                </center>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    @endforeach
                    </div>
                </div>
    
                <div class="box-footer">
                    <center>
                        <h1 style="font-weight:bold;">WELCOME TO NEOSOFT CLINIC</h1>
                    </center>
                </div>
        </div>
    </div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="{{ asset('plugins/jQuery/jquery.min.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

{{-- DataTables --}}
<script src="{{ asset('plugins/DataTables/DataTables/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/DataTables/DataTables/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/DataTables/responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/DataTables/responsive/js/responsive.bootstrap.min.js') }}"></script>
{{-- <script src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script> --}}
{{-- <script src="{{ asset('plugins/morris/morris.min.js') }}"></script> --}}
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
{{-- <script>
$.widget.bridge('uibutton', $.ui.button);
</script> --}}
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Sparkline -->
{{-- <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> --}}
<!-- jQuery Knob Chart -->
{{-- <script src="{{ asset('plugins/knob/jquery.knob.js') }}"></script> --}}
<!-- daterangepicker -->
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
{{-- <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script> --}}
<!-- datepicker -->
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('js/validator.js') }}"></script>
<script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/mustache.js') }}"></script>
<script>
// Carousel Auto-Cycle
// $(document).ready(function() {
// $('.carousel').carousel({
//   interval: 4000
// })
// });
$(document).ready(function() {
    $('.autoplay').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        button:false,
        prevArrow: false,
        nextArrow: false
    })
});
	
setInterval('window.location.reload()', 20000);
</script>
<script>
    function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
    }
</script>
</body>
</html>
 
 
 

