@extends('base')
@section('title')
Queue
@endsection
@section('style')
<style>
  #box{
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      border-radius:5px;
      transition: 0.3s;
  }
  #box:hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
      transition: 0.3s;
  }
  .btn{
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      font-weight:bold;
      font-family:sans-serif;
  }
  .btn:hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
      transition: 0.3s;
  }
  .img{
      height:120px;
  }
</style>
@endsection
@section('breadcrumb')
@parent
<li>Queue</li>
@endsection
@section('content')
<div class="row">
<div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">Queue List</span>
                <p style="font-size:15px;"><i>Menampilkan Daftar Antrian.</i></p>
            </div>
            <div class="box-header with-border">
                <div class="col-md-12">
            
                    <div class="col-md-1">
                        <label for="">Date</label>
                    </div>
                    <div class="body" id="goDate">
                      <form class="form form-horizontal" data-toggle="validator">
                          {{ csrf_field() }}
                          <div class="col-md-3">
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" name="date" class="form-control pull-right" id="datepickerxx"
                                          placeholder="Go To Date ...">
                                  </div>
                          </div>
                          <div class="col-md-1">
                              <button type="submit" class="btn btn-sm btn-primary"> Go...</button>
                          </div>
                      </form>
                    </div>
                    <div class="col-md-1">
                        <label for="">Search Patient</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="search" id="searchbox" class="form-control" placeholder="Name/No">
                    </div>
                    <div class="col-md-3">
                      <label>Today's Total Queue : <span class="text-red">{{$queue}}</span></label>
                    </div>
                </div>
            </div>
            <div class="box-header">
                <div class="col-md-12">
                    <div class="body check">
                        <div class="col-md-10">
                            <div class="checkbox">
                                <label>
                                <input type="checkbox" value="0" checked name="check">
                                    Staff On Duty
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        @if($therapist->count())
                            {{ $therapist->links() }}
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="body action">
                    @foreach($therapist as $app)
                        @foreach($app->therapist_schedule as $sche)
                            @if($day == $sche->day)
                            <div class="col-md-3">
                                    <div class="box box-solid" id="box">
                                        @php
                                        $color = array('#347ab8','#33b883','#b73398','#b66f34');

                                        $count = count($color) - 1;

                                        $i = rand(0, $count);

                                        $rand_background = $color[$i];     

                                        @endphp
                                        <div class="box-header" style="background-color:{{$rand_background}};">
                                                <center>
                                                <h4 class="box-title" style="color:white;font-weight:bold;font-family:sans-serif">{{ $app->therapist_name}}</h4>
                                                </center>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                            <div class="col-md-6" style="margin-left:-15px;margin-right:10px;">
                                                                <a href="#" onclick="queue({{ $app->id_therapist}})" >
                                                                    @if($app->img)
                                                                    <img class="img img-bordered-sm" src="/storage/{{$app->img}}" alt="man image">
                                                                    @else
                                                                    <img class="img img-bordered-sm" src="{{ asset('dist/img/doctor.png') }}" alt="user image">
                                                                    @endif
                                                                </a>
                                                            </div>
                                                            <div class="col-md-6">
                                                            @php
                                                            if($app->type == 0)
                                                            {
                                                                $type = "Doctor";
                                                            }else{
                                                                $type = "Therapist";
                                                            }
                                                        @endphp
                                                            <h4>{{$type}}</h4>
                                                            @foreach($app->therapist_schedule as $sche)
                                                            @if($day == $sche->day)
                                                                    <h4 style="margin-top:-4px;margin-right:-10px;">{{ date('H:i',strtotime($sche->start_time))." - ".date('H:i',strtotime($sche->end_time)) }}</h4>
                                                            @endif
                                                            @endforeach
                                                            <button onclick="queue({{ $app->id_therapist}})" class="btn btn-danger btn-sm " style="margin-top:14px;"><i class="fa fa-plus-circle"></i> Add List</button>
                                                            </div>
                                        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="body" id="recipe-card">
                                    @foreach($app->queue as $queue)
                                    @if($queue->come_date == $date)
                                    <div class="box" id="box">
                                            <div class="box-body">
                                                <div class="row">
                                                <div class="col-md-12">
                                                                <div class="col-md-6 pic" style="margin-left:-15px;margin-right:10px;">
                                                                    <img class="img img-bordered-sm" src="{{ asset('dist/img/man.png') }}" alt="user image">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group" style="margin-top:-10px;">
                                                                        <h4 style="margin-right:-15px;">{{ $queue->customer->full_name}}</h4>
                                                                        <h4 style="color:#666;margin-top:-5px;">NO : #{{$queue->queue_number}}</h4>
                                                                    </div>         
                                                                    <div class="form-group">
                                                                        @if($queue->status_queue == 0)
                                                                        @else
                                                                        @php
                                                                            if($queue->status_payment == 1)
                                                                            {
                                                                                $status = "PAY";
                                                                            }else{
                                                                                $status = "PAID";
                                                                            }
                                                                        @endphp
                                                                        <h5>
                                                                            <i class="fa fa-money text-green"></i>
                                                                            <a href="#!" class="text-green"  onclick="status_queue({{ $queue->id_queue}})"> {{$status}}</a>
                                                                        </h5>
                                                                        @endif
                                                                        @if($queue->status_queue == 0)
                                                                        <h5>
                                                                            <a href="#!" onclick="status_queue({{ $queue->id_queue}})"> Waiting...</a>
                                                                        </h5>
                                                                        @else
                                                                        @php
                                                                            if($queue->status_queue == 1)
                                                                            {
                                                                                $status = "On";
                                                                            }else{
                                                                                $status = "Done";
                                                                            }
                                                                        @endphp
                                                                        <h5>
                                                                            <i class="fa fa-stethoscope text-blue"></i>
                                                                            <a href="#!" onclick="status_queue({{ $queue->id_queue}})"> {{$status}}</a>
                                                                        </h5>
                                                                        @endif
                                                                    </div>   
                                                                    <div class="form-group" id="screen">
                                                                        <form  method="POST" action="{{ route('queue.screen',$queue->id_queue)}}" >
                                                                            {{ @csrf_field() }}
                                                                            @method('POST')
                                                                            <input type="hidden" name="queue_id" id="id_queue" value="{{ @$queue->id_queue}}">  
                                                                            <input type="hidden" name="therapist_id" value="{{ @$queue->therapist_id}}">
                                                                            <div class="col-md-12">
                                                                                <center>
                                                                                <button type="submit" class="btn btn-success" style="border-radius:40px;margin-left:-10px;"><i class="fa fa-phone"></i> Call Up</button>
                                                                                </center>
                                                                            </div>
                                                                        </form>
                                                                    </div>      
                                                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    @else
                                    @endif
                                    @endforeach
                                    </div>
                            </div>
                            @endif
                         @endforeach
                    @endforeach
                </div>
            </div>
        </div>
</div>
</div>
@include('queue.form')
@include('queue.status')
@endsection
@section('script')
<script>
$(document).ready(function() {
  $("#searchbox").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#recipe-card div").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          $(".form-group").removeAttr("style");
          $(".pic").css('display','block');
          $(".col-md-12").removeAttr("style");
      });
  });
});
</script>
<script type = "text/javascript" >
$('#datepicker,#datepickerxx, #come_date').datepicker({
format: 'dd/mm/yyyy',
autoclose: true,
todayHighlight: true
});
$(function(){
  $('#goDate form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
          url="{{ route('queue.index') }}"+$('#goDate form').serialize().load();
          location.reload();
      }
      return false;
  });

 $('#modal-queue form').validator().on('submit', function(e){
  if(!e.isDefaultPrevented()){
    if(save_method == "add") url="{{ route('queue.store') }}";
     $.ajax({
        url : url,
        type : "POST",
        data : $('#modal-queue form').serialize(),
       success : function(data){
          $('#modal-queue').modal('hide');

          location.reload();
        return false;
       },
       error : function(e){
         alert("Can not Save the Data!");
       }
     });
     return false;
 }
});
$('#modal-status form').validator().on('submit', function(e){
  if(!e.isDefaultPrevented()){
    var id = $('#id_status').val();
    if(save_method == "edit_status") url="queue/"+ id + "/update-status";
     $.ajax({
        url : url,
        type : "POST",
        data : $('#modal-status form').serialize(),
       success : function(data){
          $('#modal-status').modal('hide');
          location.reload();
        return false;
       },
       error : function(e){
         alert("Can not Save the Data!");
       }
     });
     return false;
 }
});
});
function queue(id) {
$("#idx").val(id);
save_method="add";
$('#modal-queue form')[0].reset();
$.ajax({
    url: "queue/" + id + "/edit",
    type: "GET",
    dataType: "JSON",
    success: function (data) {
        $('input[name=_method]').val('POST');
        $("#modal-queue").modal('show');
        $('#come_date').datepicker('setDate', 'today');

    
    },
    error: function () {
        alert("Can not Show the Data!");
    }
});
}
function status_queue(id) {
save_method="edit_status";
$("#id_status").val(id);
$('#modal-status form')[0].reset();
$.ajax({
    url: "queue/" + id + "/status",
    type: "GET",
    dataType: "JSON",
    success: function (data) {
        $('input[name=_method]').val('POST');
        $("#modal-status").modal('show');
        $("#status_payment").val(data.status_payment).change();
        $("#status_queue").val(data.status_queue).change();
        $("#id_therapist").val(data.therapist_id);
    
    },
    error: function () {
        alert("Can not Show the Data!");
    }
});
}
$(document).ready(function () {
$('#customer').keyup(function () {
    var query = $(this).val();
    if (query != '') {
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{ route('customer.keyword') }}",
            method: "POST",
            data: {
                query: query,
                _token: _token
            },
            dataType: "json",
            success: function (data) {
                $('#suggesstion-box').fadeIn();
                $('#suggesstion-box').html(data);
            }
        });
    } else {
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{ route('customer.all') }}",
            method: "POST",
            data: {
                query: query,
                _token: _token
            },
            dataType: "json",
            success: function (data) {
                $('#suggesstion-box').fadeIn();
                $('#suggesstion-box').html(data);
            }
        });
    }
});
});
function detailCustomer(id, induk, name, phone, birth) {
    $('#customer').val(induk);
    $('#suggesstion-box').fadeOut();
    $('#customer_id').val(id);
    $('#full').html(name);
    $('#contact').text(phone);
    $('#birthDate').text(birth);
}
</script>
@endsection
 
 
 

