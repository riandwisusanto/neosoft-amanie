@extends('base')

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
    div.dataTables_wrapper div.dataTables_processing {
        position: relative;
        border: 0;
    }

    .btn-date-custom {
        border: none;
        font-size: 16px;
    }

    .btn-date-custom:hover {
        background-color: transparent !important;
        cursor: default;
    }

    .btn-date-custom:active {
        outline: none !important;
        box-shadow: none !important;
    }
</style>
@endsection



@section('breadcrumb')
@parent
<li>Report</li>
<li>Collection</li>
<li>Outlet</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">COLLECTION BY OUTLET</span>
                        <p style="font-size:15px; font-weight: bold;"><i>Collection report based on outlet.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <div class="btn-float-right">
                            <button type="button" class="btn btn-default btn-date-custom">
                                <span id="btn_from">{{ $from }}</span>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="btn_to">{{ $to }}</span>
                            </button>
                            <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <h3>Collection By Outlet</h3>

                <table class="table table-striped table-bordered nowrap table-collection">
                    <thead>
                        <tr>
                            <th>Outlet</th>
                            <th>Num Payment</th>
                            <th>Balance Amount</th>
                            <th>Paid Amount</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="box-body">
                <h3>Invoice Detail</h3>
                <table class="table table-striped table-bordered nowrap table-invoice-detail" style="width:100%">
                    <thead>
                        <tr>
                            <th>Invoice Code</th>
                            <th>Payment Date</th>
                            <th>Type</th>
                            <th>Outlet</th>
                            <th>Customer Ref</th>
                            <th>Customer</th>

                            <th>Received By</th>
                            <th>Payment Type</th>
                            <th>Served 1</th>
                            <th>Served 2</th>
                            <th>Source</th>
                            <th>Amount</th>
                            <th>Paid</th>
                            <th>Remarks</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <div class="box-body">
                <h3>Invoice Balance</h3>
                <table class="table table-striped table-bordered nowrap table-invoice-balance" style="width:100%">
                    <thead>
                        <tr>
                            <th>Balance Code</th>
                            <th>Payment Date</th>
                            <th>Invoice Code</th>
                            <th>Outlet</th>
                            <th>Customer Ref</th>
                            <th>Customer</th>
                            <th>Received By</th>
                            <th>Payment Type</th>
                            <th>Served 1</th>
                            <th>Served 2</th>
                            <th>Source</th>
                            <th>Amount</th>
                            <th>Paid</th>
                            <th>Remarks</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('report.collection.outlet.form')
@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    var table_tot ,table_balance , table, from, to;

        $('#from, #to').on('keyup change',function(){
            $('#from, #to').each(function(){
                this.value=this.value.replace(/[^0-9]/g,'/');
            });
        });

        $('#from, #to').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });

        $(function(){
            $('#from').change(function (data) {
                $('#btn_from').html(data.target.value.replace(/[^0-9]/g,'/'));
            });

            $('#to').change(function (data) {
                $('#btn_to').html(data.target.value.replace(/[^0-9]/g,'/'));
            });

            $('#modal-form form').validator().on('submit', function(e){
                if(!e.isDefaultPrevented()){
                    $('#from, #to').each(function(){
                        this.value=this.value.replace(/[^0-9]/g,'-');
                    });
                    table.ajax.url("{{ route('detail.collection') }}?"+$('#modal-form form').serialize()).load();
                    table_tot.ajax.url( "{{ route('tot.collection') }}?"+$('#modal-form form').serialize()).load();
                    table_balance.ajax.url( "{{ route('balance.collection') }}?"+$('#modal-form form').serialize()).load();
                    $("#modal-form").modal('hide');
                }
                return false;
            });
            var d = new Date();
            var n = d.toLocaleDateString();
            table_tot = $('.table-collection').DataTable( {
                language: {processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
                scrollY:        "500px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                bSort:false,
                dom: 'Bfrtip',
                fixedColumns:   {
                    leftColumns: 1
                },
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'CBO-'+n
                    }
                ],
                "searching": false,
                "processing" : true,
                "serverside" : true,
                "ajax" : {
                    "url" : "{{ route('tot.collection') }}",
                    "type" : "GET"
                }
            });

            table = $('.table-invoice-detail').DataTable( {
                language: {processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
                scrollY:        "500px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                bSort:false,
                dom: 'Bfrtip',
                fixedColumns:   {
                    leftColumns: 1,
                    rightColumns: 1
                },
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'Detail-CBO-'+n
                    }
                ],
                "processing" : true,
                "serverside" : true,
                "ajax" : {
                    "url" : "{{ route('detail.collection') }}",
                    "type" : "GET"
                }
            });

            table_balance = $('.table-invoice-balance').DataTable( {
                language: {processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
                scrollY:        "500px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                bSort:false,
                dom: 'Bfrtip',
                fixedColumns:   {
                    leftColumns: 1
                },
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'Balance-CBO-'+n
                    }
                ],
                "processing" : true,
                "serverside" : true,
                "ajax" : {
                    "url" : "{{ route('balance.collection') }}",
                    "type" : "GET"
                }
            });
        });

        function selected_all() {
            if($("#chkall").is(':checked')){
                $("#outlet > option").prop("selected", "selected");
                $("#outlet").trigger("change");
            } else {
                $("#outlet").find('option').prop("selected", false);
                $("#outlet").trigger("change");
            }
        }

        function periodeForm(){
            $('#modal-form').modal('show');
        }

</script>
@endsection
