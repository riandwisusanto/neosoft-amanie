<div class="modal" id="modal-outlets" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
           <div class="modal-content">

        <form class="form-horizontal" id="form_data" data-toggle="validator" method="post">
        {{ csrf_field() }} {{ method_field('POST') }}

         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <i class="fa fa-times fa-lg"></i>
             </button>
             <h3 class="modal-title"></h3>
         </div>
            <div class="modal-body" >
                <input type="hidden" id="id" name="id">
                <div class="form-group">
                    <label for="name" class="col-md-3 control-label">Outlet Name *</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="code" class="col-md-3 control-label">Code</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-md-3 control-label">Address</label>
                    <div class="col-md-8">
                        <textarea class="form-control input-sm" id="address" name="address" rows="3" placeholder="Enter ..." required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="telephone" class="col-md-3 control-label">Telephone</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" id="telephone" name="telephone" placeholder="Telephone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fax" class="col-md-3 control-label">Fax</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="Fax" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fax" class="col-md-3 control-label">Logo</label>
                    <div class="col-md-8">
                        <input type="file" class="" id="logo" name="logo" placeholder="Logo"><br>
                        <div class="tampil-logo"></div>
                    </div>
                </div>
            </div>

             <div class="modal-footer">
                 <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                 <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
             </div>

        </form>

              </div>
           </div>
     </div>
