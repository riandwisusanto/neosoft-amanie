<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <div class="user-panel text-center">
            <img src="{{ asset('img/logoamanie.jpeg') }}" class="img-rounded" alt="User Image" width="80%">
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            {{-- <li class="header">MAIN NAVIGATION</li> --}}
            <hr>
            
            @if (array_intersect(['ADMINISTRATOR','SUPER_USER'], json_decode(Auth::user()->level)))
            <li><a href="{{ url('/') }}"><i class="fa fa-bar-chart text-neogray"></i> <span>{{ __('text.dashboard') }}</span></a></li>
            @endif
            
            <li class="treeview">
            <a href="#">
                <i class="text-neogray fa fa-sort-numeric-asc"></i> <span>QUEUE</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('queue-screen.index') }}" target="_blank"><i class="fa fa-angle-double-right text-navy"></i> <span>SCREEN</span></a></li>
                    <li><a href="{{ route('queue.index') }}"><i class="fa fa-angle-double-right text-navy"></i> <span>LIST</span></a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-calendar text-neogray"></i> <span>{{ __('text.appointments') }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ '/by-room' }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.room') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ '/by-therapist' }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.therapist') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ '/by-outlet' }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.byoutlets') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear text-neogray"></i> <span>{{ __('text.settings') }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('useraccess.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.useraccess') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('outlets.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.outlet') }}</span>
                        </a>
                    </li>
                    {{-- <li>
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Upload File E/U</span>
          </a>
        </li> --}}
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money text-neogray"></i> <span>{{ __('text.sales') }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('invoice-create.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.createinvoice') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('invoice-balance.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.invoicebalance') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('invoice-convert.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.convertinvoice') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('invoice-point.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.redeempoint') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            {{--  Customer --}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-address-book-o text-neogray"></i> <span>{{ __('text.patient') }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('customers.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.patientlist') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('customerInactive.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.inactivepatient') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('customerBirthdays.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.patientbirthday') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users text-neogray"></i> <span>{{ __('text.salesagents') }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('agent.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.agentregister') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('agent.list') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.agentlist') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('agent.active') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.activeagent') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            {{-- Item master --}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop text-neogray"></i> <span>{{ __('text.management') }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('products.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.products') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('treatments.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.treatments') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('packages.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.packages') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('banks.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.banks') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('consultants.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.consultants') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('therapists.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.therapist') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('groups.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.groups') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('activities.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.marketingactivites') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('marketingSources.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.marketingsources') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('rooms.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.rooms') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('cities.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.city') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('countries.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.country') }}</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('productpoints.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.productpoints') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('ranks.index') }}">
                            <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.loyaltyranks') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-truck text-neogray"></i> <span>{{ __('text.logistics') }}</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

            <ul class="treeview-menu">
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-angle-double-right text-neogray"></i> <span>{{ __('text.master') }}</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('suppliers.index') }}">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.supplier') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('warehouses.index') }}">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.warehouse') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('racks.index') }}">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.rack') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('new') }}/#/logistics/items">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.item') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-angle-double-right text-neogray"></i> <span>{{ __('text.purchasing') }}</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('new') }}/#/logistics/purchases/orders">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.purchaseorder') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('new') }}/#/logistics/purchases/receives">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.receiveitem') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>

                
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-angle-double-right text-neogray"></i> <span>{{ __('text.warehousing') }}</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('new') }}/#/logistics/reports/cards">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.stockcard') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('new') }}/#/logistics/reports/opname">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.stockopname') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('new') }}/#/logistics/adjustments">
                                <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.inventoryadjustment') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>

                
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-industry text-neogray"></i> <span>{{ __('text.manufacture') }}</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('new') }}/#/manufactures/work_orders">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.workorder') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('new') }}/#/manufactures/materials/releases">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.materialrelease') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('new') }}/#/manufactures/finishes">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.finishedproduct') }}</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-shopping-bag text-neogray"></i> <span>{{ __('text.onlineshop') }}</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('new') }}/#/malls/products">
                        <i class="fa fa-angle-double-right text-navy"></i> <span> {{ __('text.products') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('new') }}/#/malls/orders">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.salesorders') }}</span>
                    </a>
                </li>
            </ul>
        </li>

        @if (array_intersect(['ADMINISTRATOR','SUPER_USER','CASHIER', 'FRONTDESK', 'FINANCE', 'BRANCH_MANAGER'], json_decode(Auth::user()->level)))
        <li><a href="{{ route('message.index') }}"><i class="fa fa-whatsapp text-neogray"></i> <span>WA BLAST</span></a></li>
        @endif

        <!-- <li><a href="{{ route('dr-review.index') }}"><i class="fa fa-tv text-neogray"></i> <span>{{ __('text.doctorreview') }}</span></a></li> -->

        <li class="treeview">
            <a href="#">
                <i class="fa fa-file text-neogray"></i> <span>{{ __('text.reports') }}</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('index.collection.outlet') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.collectionbyoutlet') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('index.collection.activity') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.collectionbyactivity') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('index.collection.payment') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.collectionbypaymenttype') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('index.item.sold.outlet') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.itemsoldbysales') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('index.item.sold.collection') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.itemsoldbycollection') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('index.treatment-done.outlet') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.treatmentdonebyoutlet') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('index.treatment-done.therapist') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.treatmentdonebytherapist') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('index.product-done.outlet') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.productdonebyoutlet') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('index.revenue') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.uerevenue') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('index.commission.sales') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.salescommission') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('index.commission.consultant') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.consultantcommission') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('index.commission.therapist') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.therapistcommission') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('index.commission.therapist-doing') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.treatmentcommission') }}</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-question-circle text-neogray"></i> <span>FAQ</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('faq') }}">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.help') }}</span>
                    </a>
                </li>
                <li>
                    <a href="https://api.whatsapp.com/send?phone=6282314905082&text=Saya ingin bertanya sesuatu tentang Neosoft">
                        <i class="fa fa-angle-double-right text-navy"></i> <span>{{ __('text.helpdesk') }}</span>
                    </a>
                </li>
                
                <li>
                    <a href="{{ route('survey.index') }}">
                        <i class="fa fa-angle-double-right text-navy"></i><span>{{ __('text.survey') }}</span>
                    </a>
                </li>
            </ul>
        </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
