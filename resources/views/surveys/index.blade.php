@extends('base')

@section('title')
Detail Survey
@endsection

@section('breadcrumb')
@parent
<li>Detail Survey</li>
@endsection

@section('content')
                    
  <div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">SURVEY</span>
            </div>
            <div class="box-body">
                <form class="form form-survey" action="" method="POST">
                    {{ csrf_field() }} {{ method_field('POST') }}
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label class="control-label">Clinic Name</label>
                            <input type="hidden" name="clinic_name" value="{{config('app.name') }}" class="form-control" required>
                            <input type="text" name="clinic_name" value="{{config('app.name') }}" class="form-control" disabled>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label class="control-label">Consultant Name</label>
                            <select name="consultant_name" id="" class="form-control" required>
                                <option value="-" selected disabled>Please Select Consultant</option>
                                <option value="0">Azhar</option>
                                <option value="1">Beby</option>
                                <option value="2">Intan</option>
                                <option value="3">Jasmine</option>
                                <option value="4">Nugi</option>
                                <option value="5">Shinta</option>
                                <option value="6">Wahyu</option>
                            </select>
                        </div>    
                    </div>
                    <div class="row">
                            <div class="form-group col-sm-12">
                                <label class="control-label">1. Apakah konten pelatihan memenuhi harapan anda?</label>
                                <p>
                                    <label for="yes">
                                    <input type="radio" name="question_one" value="1" id="yes" class="with-gap" required> Ya
                                    </label>&nbsp; &nbsp;&nbsp;
                                    <label for="no">
                                    <input type="radio" name="question_one" value="0" id="no" class="with-gap" required>
                                    Tidak
                                    </label>&nbsp; &nbsp;&nbsp;

                                </p>
                            </div>    
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                                <label class="control-label" >2. Bagaimana anda menilai kualitas pelatihan?</label>
                                <p>
                                <label for="0">
                                <input type="radio" name="question_two" value="0" id="0" class="with-gap" required>
                                Tidak Bisa Diterima
                                </label>&nbsp; &nbsp;&nbsp;
                                <input type="radio" name="question_two" value="1" id="1" class="with-gap" required>
                                <label for="1">Cukup</label>&nbsp; &nbsp;&nbsp;
                                <input type="radio" name="question_two" value="2" id="2" class="with-gap" required>
                                <label for="2">Bisa Diterima</label>&nbsp; &nbsp;&nbsp;
                                <input type="radio" name="question_two" value="3" id="3" class="with-gap" required>
                                <label for="3">Biasa</label>&nbsp; &nbsp;&nbsp;
                                <input type="radio" name="question_two" value="4" id="4" class="with-gap" required>
                                <label for="4">Luar Biasa</label>&nbsp; &nbsp;&nbsp;
                                </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12">
                               <label>3. Bagaimana anda menilai kualitas instruktur?</label>
                               <p>
                                    <input type="radio" name="question_three" value="0" id="5" class="with-gap" required>
                                    <label for="5">Tidak Bisa Diterima</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_three" value="1" id="6" class="with-gap" required>
                                    <label for="6">Cukup</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_three" value="2" id="7" class="with-gap" required>
                                    <label for="7">Bisa Diterima</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_three" value="3" id="8" class="with-gap" required>
                                    <label for="8">Biasa</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_three" value="4" id="9" class="with-gap" required>
                                    <label for="9">Luar Biasa</label>&nbsp; &nbsp;&nbsp;
                                </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12">
                               <label>4. Berapa tingkat pengetahuan konten instruktur?</label>
                               <p>
                                    <input type="radio" name="question_four" value="0" id="10" class="with-gap" required>
                                    <label for="10">Tidak Bisa Diterima</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_four" value="1" id="11" class="with-gap" required>
                                    <label for="11">Cukup</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_four" value="2" id="12" class="with-gap" required>
                                    <label for="12">Bisa Diterima</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_four" value="3" id="13" class="with-gap" required>
                                    <label for="13">Biasa</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_four" value="4" id="14" class="with-gap" required>
                                    <label for="14">Luar Biasa</label>&nbsp; &nbsp;&nbsp;
                                </p>    
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                               <label>5. Bagaimana anda menilai organisasi dan persiapan instruktur?</label>
                               <p>
                                    <input type="radio" name="question_five" value="0" id="15" class="with-gap" required>
                                    <label for="15">Tidak Bisa Diterima</label> &nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_five" value="1" id="16" class="with-gap" required>
                                    <label for="16">Cukup</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_five" value="2" id="17" class="with-gap" required>
                                    <label for="17">Bisa Diterima</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_five" value="3" id="18" class="with-gap" required>
                                    <label for="18">Biasa</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_five" value="4" id="19" class="with-gap" required>
                                    <label for="19">Luar Biasa</label>&nbsp; &nbsp;&nbsp;
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12">
                               <label>6. Bagaimana antusiasme mereka?</label> <P>
                                    <input type="radio" name="question_six" value="0" id="20" class="with-gap" required>
                                    <label for="20">Tidak Bisa Diterima</label> &nbsp; &nbsp;&nbsp;
                                    <input type="radio" name="question_six" value="1" id="21" class="with-gap" required>
                                    <label for="21">Cukup</label>&nbsp; &nbsp;&nbsp;
                                    <input type="radio" name="question_six" value="2" id="22" class="with-gap" required>
                                    <label for="22">Bisa Diterima</label>&nbsp; &nbsp;&nbsp;
                                    <input type="radio" name="question_six" value="3" id="23" class="with-gap" required>
                                    <label for="23">Biasa</label>&nbsp; &nbsp;&nbsp;
                                    <input type="radio" name="question_six" value="4" id="24" class="with-gap" required>
                                    <label for="24">Luar Biasa</label>&nbsp; &nbsp;&nbsp;
                                </P>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                               <label>7. Apakah anda mempelajari sesuatu yang baru?</label> <P>
                                    <input type="radio" name="question_seven" value="1" id="25" class="with-gap" required>
                                    <label for="25">Ya</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_seven" value="0" id="26" class="with-gap" required>
                                    <label for="26">Tidak</label>&nbsp; &nbsp;&nbsp;
                                    </P>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                               <label>8. Apakah pelatihan itu relevan dengan kebutuhan anda?</label> <P>
                                    <input type="radio" name="question_eight" value="1" id="27" class="with-gap" required>
                                    <label for="27">Ya</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_eight" value="0" id="28" class="with-gap" required>
                                    <label for="28">Tidak</label>&nbsp; &nbsp;&nbsp;
                                </P>
                        </div>
                    </div>
                    <div class="row">
                    <div class="form-group col-sm-12">
                                <label>9. Apakah kursus atau praktis mudah diterapkan?</label> <P>
                                    <input type="radio" name="question_nine" value="1" id="29" class="with-gap" required>
                                    <label for="29">Ya</label>&nbsp; &nbsp;&nbsp;

                                    <input type="radio" name="question_nine" value="0" id="30" class="with-gap" required>
                                    <label for="30">Tidak</label>&nbsp; &nbsp;&nbsp;
                                </P>
                    </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                               <label>10. Apakah anda punya saran untuk kursus ini?</label> <br>
                                <textarea name="question_ten" cols="30" rows="5" class="form-control no-resize" required aria-required="true"></textarea>
                        </div>
                    </div>
                    </div>
                    <div class="box-footer text-left">
                        <button type="submit" class="btn btn-primary btn-save simpan"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
     $(function(){
        $('.form-survey').validator().on('submit', function(e){
                if(!e.isDefaultPrevented()){
                    $.ajax({
                        url : "{{ route('survey.store') }}",
                        type : "POST",
                        data : $('.form-survey').serialize(),
                        success : function(data){
                            if (data === "error") {
                                swal("Cannot Save Data", {
                                    button: false,
                                });
                            
                            }else{
                                swal({
                                    title: "Terimakasih atas Masukanya",
                                    icon: "success",
                                    buttons: {
                                        canceled:{
                                            text:'OKE',
                                            value: 'oke',
                                            className: 'swal-button btn-success'
                                        },
                                        
                                        
                                    },
                                    dangerMode: true,

                                })
                                .then((option) => {
                                    switch (option) {
                                        default:
                                            window.location.href = "/survey";
                                            break;
                                    break;
                                    }
                                });

                            }
                        }, error: function (jqXHR, textStatus, errorThrown){
                            console.log(jqXHR.responseText);
                            swal("Can not Save the Data ", {
                                button: false,
                            });
                        }
                    });
                    return false;

                }

        })
    });

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
@endsection