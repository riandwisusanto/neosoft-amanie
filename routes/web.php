<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']], function () {
    App::setLocale('en');
    Auth::routes(['register' => false]);
    Route::get('/home', 'DashboardController@index');
    Route::get('/', 'DashboardController@index');

    Route::resource('/medical-alert', 'MedicalAlertController');

    Route::get('/message/{id}/wa', 'MessageController@sendWhatsApp')->name('message.wa');
    Route::get('/message/data', 'MessageController@listData')->name('message.data');
    Route::resource('/message', 'MessageController');

    Route::resource('/item-alert', 'ItemAlertController');


    Route::post('/queue/{id}/screen', 'QueueController@screen_store')->name('queue.screen');
    Route::get('/queue-screen', 'QueueController@queueScreen')->name('queue-screen.index');

    Route::post('/queue/{id}/update-status', 'QueueController@status_store')->name('queue.statusStore');
    Route::get('/queue/{id}/status', 'QueueController@status')->name('queue.status');
    Route::resource('/queue', 'QueueController');
    
    Route::get('/new', function () {
        return view('neosoft');
    })->name('new');

    //appointment
    Route::get('/treat/{id}/{start}', 'AppointmentController@chooseProduct');
    Route::get('/by-therapist/{therapist}', 'AppointmentController@byTherapistwithChange');
    Route::get('/by-therapist', 'AppointmentController@byTherapist');
    Route::get('/by-outlet', 'AppointmentController@byOutlet');
    Route::get('/by-outlet/{id}', 'AppointmentController@byOutletwithChange')->name('outlet.change');
    Route::get('/by-room', 'AppointmentController@byRoom')->name('by-room');
    Route::get('/by-room/{id_customer}/{id_appointment}/{date}', 'AppointmentController@detailAppointment');
    
    Route::post('/appointment/{id}/storeReschedule','AppointmentController@storeReschedule')->name('appointment.reschedule.storeReschedule');

    Route::patch('/by-room/{id}/status', 'AppointmentController@changeStatus');
    Route::patch('/appointment/by-room/{id}/status', 'AppointmentController@changeStatus');
    Route::resource('/appointment', 'AppointmentController');

    Route::match(array('GET', 'POST'), '/dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('/all-outlet/', 'DashboardController@all_outlet')->name('all.outlet');
    Route::get('/invoice/dashboard/{id}', 'DashboardController@InvoiceToday')->name('invoice.dashboard');
    Route::match(array('GET', 'POST'), '/dashboard/geografis', 'DashboardController@geografis')->name('dashboard.geografis');
    Route::match(array('GET', 'POST'), '/dashboard/productranking', 'DashboardController@productRanking')->name('dashboard.productranking');
    Route::match(array('GET', 'POST'), '/dashboard/treatmentranking', 'DashboardController@treatmentRanking')->name('dashboard.treatmentranking');
    Route::match(array('GET', 'POST'), '/dashboard/alldata', 'DashboardController@allData')->name('dashboard.alldata');

    //invoice-create
    Route::get('invoice-create/{id_invoice}/sign', 'InvoiceController@sign')->name('invoice.sign');
    Route::post('invoice-create/{id_invoice}/sign', 'InvoiceController@updateSign')->name('invoice.update-sign');
    Route::get('invoice-create/{id_invoice}/print', 'InvoiceController@generatePdf')->name('invoice.print');
    Route::get('invoice-create/{id_invoice}/print-thermal', 'InvoiceController@generateThermalPdf')->name('invoice.print-thermal');
    Route::get('invoice-create/{id_invoice}/wa', 'InvoiceController@sendWhatsApp')->name('invoice.wa');
    Route::get('/invoice-create/{id}/chooseAnamnesa', 'InvoiceController@getAnamnesa');
    Route::get('/invoice-create/{id}/loadAnamnesa', 'InvoiceController@loadAnamnesa');
    Route::get('/invoice-convert/{id}/data', 'InvoiceConvertController@listData')->name('invoice-convert.data');
    Route::resource('/invoice-create', 'InvoiceController');

    //invoice-package
    Route::get('/packages/{id}/choosePackage', 'InvoiceController@getPackage');
    Route::get('/activity/{id}/chooseActivity', 'InvoiceController@getActivity');

    //invoice-convert
    Route::get('invoice-convert/{id_invoice}/print', 'InvoiceConvertController@generatePdf')->name('invoice-convert.print');
    Route::resource('/invoice-convert', 'InvoiceConvertController');

    Route::get('/invoice-balance/{id}/print', 'InvoiceBalanceController@generatePdf')->name('invoice-balance.print');
    Route::patch('/invoice-balance/{id}/void', 'InvoiceBalanceController@void')->name('invoice-balance.void');
    Route::get('/invoice-balance/{id}/invoice', 'InvoiceBalanceController@getInvoice');
    Route::get('/invoice-balance/{id}/loadInvoice', 'InvoiceBalanceController@loadInvoice');
    Route::resource('/invoice-balance', 'InvoiceBalanceController');

    //invoice-point
    Route::get('invoice-point/{id_invoice}/print', 'InvoicePointController@generatePdf')->name('invoice.point.print');
    Route::resource('/invoice-point', 'InvoicePointController');

    //product point
    Route::get('/productpoints/{id}/chooseProduct', 'ItemMaster\ProductPointController@chooseProduct');
    Route::get('/productpoints/data', 'ItemMaster\ProductPointController@listData')->name('productpoints.data');
    Route::resource('/productpoints', 'ItemMaster\ProductPointController');

    // Route::post('/package/keyword', 'InvoiceController@getDiscount')->name('discount.keyword');
    // Route::post('/package/all', 'InvoiceController@getAllDiscount')->name('discount.all');

    //customers
    Route::get('/customers/data', 'CustomerController@listData')->name('customers.data');
    Route::get('/customers/{id}/province', 'CustomerController@getProvince');
    Route::post('/customer/{id}/delete-outlet', 'CustomerController@destroyCustomerOutlet')->name('customers.outlet.destroy');
    Route::post('/customer/{id}/delete-consultant', 'CustomerController@destroyCustomerConsultant')->name('customers.consultant.destroy');
    Route::post('/customer/{id}/outlet', 'CustomerController@storeCustomerOutlet')->name('customers.outlet.store');
    Route::post('/customer/{id}/consultant', 'CustomerController@storeCustomerConsultant')->name('customers.consultant.store');
    Route::post('/customers/keyword', 'CustomerController@getCustomer')->name('customer.keyword');
    Route::post('/customers/all', 'CustomerController@getAllCustomer')->name('customer.all');
    Route::get('/customers/{id}/redeem-detail', 'CustomerController@redeemInvoice')->name('customer.redeem');
    Route::patch('/customers/{id}/void-redeem-invoice', 'CustomerController@voidInvoicePoint')->name('customer.void.redeem');
    
       
    Route::get('/{id}/editAnamnesa', 'CustomerController@editAnamnesa')->name('customer.anamnesa.editAnamnesa');
    Route::post('/customers/{id}/updateAnamnesa', 'CustomerController@updateAnamnesa')->name('customer.anamnesa.updateAnamnesa');
    Route::match(['put','patch'],'/customers/{id}/addAnamnesa', 'CustomerController@storeAnamnesa')->name('customer.anamnesa.storeAnamnesa');
    Route::match(['put','patch'],'/customer/{id}/delete-anamnesa', 'CustomerController@destroyAnamnesa')->name('customers.anamnesa.destroy');
    Route::get('/customers/{id}/chooseProduct', 'CustomerController@chooseProduct');

    Route::post('/customers/{id}/addConsignment', 'CustomerController@storeConsignment')->name('customer.consignment.storeConsignment');
    Route::get('/{id}/showPict', 'CustomerController@showPict')->name('consignment.showPict');
    Route::get('/{id}/showPictPhysical', 'CustomerController@showPictPhysical')->name('consignment.showPictPhysical');


    Route::post('/customers/{id}/addPhysical', 'CustomerController@storePhysical')->name('customer.physical.storePhysical');
    Route::post('/customer/{id}/delete-physical', 'CustomerController@destroyPhysical')->name('customer.physical.destroy');

    Route::resource('/customers', 'CustomerController');

    //customer birthday
    Route::get('/customer-birthdays', 'CustomerBirthdayController@index')->name('customerBirthdays.index');
    Route::get('/customer-birthdays/{id}/wa', 'CustomerBirthdayController@sendWhatsApp')->name('customerBirthdays.wa');
    Route::get('/customer-birthdays/data', 'CustomerBirthdayController@list')->name('customerBirthdays.data');
    Route::post('/customer-birthdays/wa', 'CustomerBirthdayController@store')->name('customerBirthdays.store');

    Route::get('/customer-inactive', 'CustomerInactiveController@index')->name('customerInactive.index');
    Route::get('/customer-inactive/{id}/wa', 'CustomerInactiveController@sendWhatsApp')->name('customerInactive.wa');
    Route::get('/customer-inactive/data', 'CustomerInactiveController@list')->name('customerInactive.data');
    Route::post('/customer-inactive/wa', 'CustomerInactiveController@store')->name('customerInactive.store');

    //active-invoice
    Route::get('/customers/{id}/detail', 'CustomerController@activeInvoice')->name('customer.active');
    Route::patch('/customers/{id}/void-active-invoice', 'CustomerController@voidActiveInvoice')->name('customer.void.active');
    Route::post('/customers/{id}/update-active', 'CustomerController@updateActiveInvoice')->name('customer.update.active');

    //treatment-balance
    Route::post('/customers/{id}/treatment-balance', 'CustomerController@treatmentBalance')->name('customer.treatment-balance');

    //usage
    Route::get('/usage-treatment/{id}/print', 'UsageController@printUsage')->name('usage-treatment.print');
    Route::patch('/usage-treatment/{id}/void', 'UsageController@voidUsage')->name('usage-treatment.void');
    Route::resource('/usage-treatment', 'UsageController');

    //user
    Route::post('/useraccess/reset', 'UserController@resetPassword')->name('useraccess.reset-password');
    Route::get('/changepassword', 'DashboardController@changePassword')->name('useraccess.change-password');
    Route::post('/updatepassword', 'DashboardController@updatePassword')->name('useraccess.update-password');
    Route::get('/useraccess/data', 'UserController@listData')->name('useraccess.data');
    Route::resource('/useraccess', 'UserController');

    //outlet
    Route::get('/outlets/data', 'OutletController@listData')->name('outlets.data');
    Route::resource('/outlets', 'OutletController');

    // ITEM MASTER

    //pointvalue
    Route::get('/pointValues/data', 'ItemMaster\PointValueController@listData')->name('pointValues.data');
    Route::resource('/pointValues', 'ItemMaster\PointValueController');

    //consultant
    Route::get('/consultants/data', 'ItemMaster\ConsultantController@listData')->name('consultants.data');
    Route::resource('/consultants', 'ItemMaster\ConsultantController');

    //therapist
    Route::get('/therapists/data', 'ItemMaster\TherapistController@listData')->name('therapists.data');
    Route::resource('/therapists', 'ItemMaster\TherapistController');

    //therapist schedule
    Route::get('/therapist-schedules/{id}/data', 'ItemMaster\TherapistScheduleController@listData')->name('therapist-schedules.data');
    Route::resource('/therapist-schedules', 'ItemMaster\TherapistScheduleController');

    //therapist schedule
    Route::get('/therapist-leaves/{id}/data', 'ItemMaster\TherapistLeaveController@listData')->name('therapist-leaves.data');
    Route::resource('/therapist-leaves', 'ItemMaster\TherapistLeaveController');

    //group
    Route::get('/groups/data', 'ItemMaster\GroupController@listData')->name('groups.data');
    Route::resource('/groups', 'ItemMaster\GroupController');

    //city
    Route::get('/cities/data', 'ItemMaster\CityController@listData')->name('cities.data');
    Route::resource('/cities', 'ItemMaster\CityController');

    //country
    Route::get('/countries/data', 'ItemMaster\CountryController@listData')->name('countries.data');
    Route::resource('/countries', 'ItemMaster\CountryController');

    //treatment
    Route::get('/treatments/data', 'ItemMaster\TreatmentController@listData')->name('treatments.data');
    Route::resource('/treatments', 'ItemMaster\TreatmentController');

    //product
    Route::get('/products/data', 'ItemMaster\ProductController@listData')->name('products.data');
    Route::resource('/products', 'ItemMaster\ProductController');

    //package
    Route::get('/packages/{id}/chooseProduct', 'ItemMaster\PackageController@chooseProduct');
    Route::get('/packages/data', 'ItemMaster\PackageController@listData')->name('packages.data');
    Route::resource('/packages', 'ItemMaster\PackageController');

    //room
    Route::get('/rooms/data', 'ItemMaster\RoomsController@listData')->name('rooms.data');
    Route::resource('/rooms', 'ItemMaster\RoomsController');

    //machine
    Route::get('/machines/data', 'ItemMaster\MachineController@listData')->name('machines.data');
    Route::resource('/machines', 'ItemMaster\MachineController');

    //market source
    Route::get('/marketingSources/data', 'ItemMaster\MarketingSourcesController@listData')->name('marketingSources.data');
    Route::resource('/marketingSources', 'ItemMaster\MarketingSourcesController');

    Route::get('/banks/data', 'ItemMaster\BankController@listData')->name('banks.data');
    Route::resource('/banks', 'ItemMaster\BankController');

    //marketing activities
    Route::get('/activitiesGroup/data', 'ItemMaster\ActivityGroupController@listActivityGroup')->name('activityGroup.data');
    Route::resource('/activityGroup', 'ItemMaster\ActivityGroupController');

    Route::get('/activities/data', 'ItemMaster\ActivityController@listActivity')->name('activity.data');
    Route::resource('/activities', 'ItemMaster\ActivityController');

    Route::get('/ranks/data', 'ItemMaster\RankController@listData')->name('ranks.data');
    Route::get('/ranks/customer/{id}', 'ItemMaster\RankController@rankCustomer')->name('ranks.customer');
    Route::get('/ranks/star/{id}', 'ItemMaster\RankController@rankStar')->name('ranks.star');
    Route::resource('/ranks', 'ItemMaster\RankController');

    Route::get('/suppliers/data', 'Logistik\SupplierController@listData')->name('suppliers.data');
    Route::resource('/suppliers', 'Logistik\SupplierController');

    //warehouse
    Route::get('/warehouses/data', 'Logistik\WarehouseController@listData')->name('warehouses.data');
    Route::resource('/warehouses', 'Logistik\WarehouseController');

    //uom
    Route::get('/uoms/data', 'Logistik\UomController@listData')->name('uoms.data');
    Route::resource('/uoms', 'Logistik\UomController');

    //category
    Route::get('/categories/data', 'Logistik\CategoryController@listData')->name('categories.data');
    Route::resource('/categories', 'Logistik\CategoryController');

    // items
    Route::get('/items/data', 'Logistik\ItemController@listData')->name('items.data');
    Route::resource('/items', 'Logistik\ItemController');

    // racks
    Route::get('/racks/data', 'Logistik\RackController@listData')->name('racks.data');
    Route::resource('/racks', 'Logistik\RackController');

    // racks
    Route::get('/product-mutations/data', 'Logistik\ProductMutationController@listData')->name('product-mutations.data');
    Route::get('/product-mutations/get-rack', 'Logistik\ProductMutationController@getRack')->name('product-mutations.get-rack');
    Route::resource('/product-mutations', 'Logistik\ProductMutationController');

    // PO
    Route::get('/purchase-orders/data', 'Logistik\PurchaseOrderController@listData')->name('purchase-orders.data');
    Route::post('/purchase-orders/update-status/{id}', 'Logistik\PurchaseOrderController@updateStatus')->name('purchase-orders.update-status');
    Route::get('/purchase-orders/{id}/print', 'Logistik\PurchaseOrderController@print')->name('purchase-orders.print');
    Route::resource('/purchase-orders', 'Logistik\PurchaseOrderController');

    // Bom
    Route::get('/bom/data', 'Logistik\BomController@listData')->name('bom.data');
    Route::resource('/bom', 'Logistik\BomController');

    // svc
    Route::get('/services/data', 'Logistik\ServiceController@listData')->name('services.data');
    Route::get('/services/get-treatment/{id}', 'Logistik\ServiceController@getTreatment')->name('services.get-treatment');
    Route::resource('/services', 'Logistik\ServiceController');

    // product locations
    Route::get('/product-locations/data', 'Logistik\ProductLocationController@listData')->name('product-locations.data');
    Route::resource('/product-locations', 'Logistik\ProductLocationController');

    // receive
    Route::get('/receives/data', 'Logistik\ReceiveController@listData')->name('receives.data');
    Route::get('/receives/{id}/print', 'Logistik\ReceiveController@print')->name('receives.print');
    Route::resource('/receives', 'Logistik\ReceiveController');

    // issued
    Route::get('/issued/data', 'Logistik\IssuedController@listData')->name('issued.data');
    Route::get('/issued/get-invoice/{id}', 'Logistik\IssuedController@getInvoice')->name('issued.get-invoice');
    Route::resource('/issued', 'Logistik\IssuedController');

    // retur
    Route::get('/returs/receive', 'Logistik\ReturController@getReceive')->name('returs.receive');
    Route::get('/returs/data', 'Logistik\ReturController@listData')->name('returs.data');
    Route::resource('/returs', 'Logistik\ReturController');

    Route::get('/work-orders/data', 'Logistik\WorkOrderController@listData')->name('work-orders.data');
    Route::get('/work-orders/get-bom/{id}', 'Logistik\WorkOrderController@getBom')->name('work-orders.get-bom');
    Route::resource('/work-orders', 'Logistik\WorkOrderController');

    Route::get('/work-in-processes/data', 'Logistik\WorkInProcessController@listData')->name('work-in-processes.data');
    Route::get('/work-in-processes/get-work-order/{id}', 'Logistik\WorkInProcessController@getWo')->name('work-in-processes.get-work-order');
    Route::resource('/work-in-processes', 'Logistik\WorkInProcessController');

    Route::get('/finish-goods/data', 'Logistik\FinishGoodController@listData')->name('finish-goods.data');
    Route::get('/finish-goods/get-work-in-process/{id}', 'Logistik\FinishGoodController@getWip')->name('finish-goods.get-work-in-process');
    Route::resource('/finish-goods', 'Logistik\FinishGoodController');

    // report
    Route::get('/stock-card/data', 'Logistik\StockCardController@listData')->name('stock-card.data');
    Route::resource('/stock-card', 'Logistik\StockCardController')->only('index');

    Route::get('/stock-card-recap/data', 'Logistik\StockCardRecapController@listData')->name('stock-card-recap.data');
    Route::resource('/stock-card-recap', 'Logistik\StockCardRecapController')->only('index');

    Route::get('/item-stock/data', 'Logistik\ItemStockController@listData')->name('item-stock.data');
    Route::resource('/item-stock', 'Logistik\ItemStockController')->only('index');
    // END

    //report
    Route::get('/collection/outlet/', 'ReportCollectionController@index_collection_outlet')->name('index.collection.outlet');
    Route::get('/detailCollection', 'ReportCollectionController@detailCollectionOutlet')->name('detail.collection');
    Route::get('/subtotalCollection', 'ReportCollectionController@subtotalCollectionOutlet')->name('subtotal.collection');
    Route::get('/grandtotalCollection', 'ReportCollectionController@grandtotalCollectionOutlet')->name('grandtotal.collection');
    Route::get('/totalCollection', 'ReportCollectionController@totalCollectionOutlet')->name('total.collection');
    //Route::get('/totDataCollection', 'ReportCollectionController@totCollectionOutlet')->name('tot.collection');
    //Route::get('/balanceDataCollection', 'ReportCollectionController@balanceCollectionOutlet')->name('balance.collection');

    Route::get('/index/collection/activity/', 'ReportCollectionController@index_collection_activity')->name('index.collection.activity');
    Route::get('/report/collection/activity', 'ReportCollectionController@collection_activity')->name('report.collection.activity');
    Route::post('/choose/activity', 'ReportCollectionController@choose_activity')->name('choose.activity');

    Route::get('/index/collection/payment/', 'ReportCollectionController@index_collection_payment')->name('index.collection.payment');
    Route::get('/payment_type_collection', 'ReportCollectionController@payment_type_collection')->name('payment_type.collection.payment');
    Route::get('/bank_payment_collection', 'ReportCollectionController@bank_payment_collection')->name('bank.collection.payment');
    Route::get('/detail_payment_collection', 'ReportCollectionController@detail_payment_collection')->name('detail.collection.payment');

    Route::get('/index/item/sold/collection', 'ReportItemController@index_item_sold_collection')->name('index.item.sold.collection');
    Route::match(array('GET', 'POST'), '/report/item/sold/collection', 'ReportItemController@item_sold_collection')->name('report.item.sold.collection');

    Route::get('/index/item/sold/', 'ReportItemController@index_item_sold_outlet')->name('index.item.sold.outlet');
    Route::match(array('GET', 'POST'), '/report/item/sold', 'ReportItemController@item_sold_outlet')->name('report.item.sold.outlet');

    Route::get('/index/treatment-done/outlet/', 'ReportDoneController@index_treatmentdone_outlet')->name('index.treatment-done.outlet');
    Route::get('/report/treatment-done/outlet/summary', 'ReportDoneController@treatmentdone_outlet_summary')->name('treatment-done.outlet.summary');
    Route::get('/report/treatment-done/outlet/detail', 'ReportDoneController@treatmentdone_outlet_detail')->name('treatment-done.outlet.detail');

    Route::get('/index/product-done/outlet/', 'ReportDoneController@index_productdone_outlet')->name('index.product-done.outlet');
    Route::get('/report/product-done/outlet/detail', 'ReportDoneController@productdone_outlet_detail')->name('product-done.outlet.detail');

    Route::get('/index/treatment-done/therapist/', 'ReportDoneController@index_treatmentdone_therapist')->name('index.treatment-done.therapist');
    Route::get('/report/treatment-done/therapist', 'ReportDoneController@treatmentdone_therapist')->name('treatment-done.therapist');

    Route::get('/index/revenue/', 'ReportRevenueController@index_revenue')->name('index.revenue');
    Route::get('/report/revenue', 'ReportRevenueController@revenue')->name('report.revenue');

    Route::get('/index/commission/sales', 'ReportCommission@indexCommissionSales')->name('index.commission.sales');
    Route::get('/report/commission/sales/summary', 'ReportCommission@summarySales')->name('report.summary.sales');
    Route::get('/report/commission/sales', 'ReportCommission@commissionSales')->name('report.commission.sales');

    Route::get('/index/commission/consultant', 'ReportCommission@indexCommissionConsultant')->name('index.commission.consultant');
    Route::get('/report/commission/consultant/summary', 'ReportCommission@summaryConsultant')->name('report.summary.consultant');
    Route::get('/report/commission/consultant', 'ReportCommission@commissionConsultant')->name('report.commission.consultant');

    Route::get('/index/commission/therapist', 'ReportCommission@indexCommissionTherapist')->name('index.commission.therapist');
    Route::get('/report/commission/therapist/summary', 'ReportCommission@summaryTherapist')->name('report.summary.therapist');
    Route::get('/report/commission/therapist', 'ReportCommission@commissionTherapist')->name('report.commission.therapist');

    Route::get('/index/commission/therapist-doing', 'ReportCommission@indexCommissionTherapistDoing')->name('index.commission.therapist-doing');
    Route::get('/report/commission/therapist-doing/summary', 'ReportCommission@summaryTherapistDoing')->name('report.summary.therapist-doing');
    Route::get('/report/commission/therapist-doing', 'ReportCommission@commissionTherapistDoing')->name('report.commission.therapist-doing');

    Route::get('/agent/list', 'AgentController@list')->name('agent.list');
    Route::get('/agent/listData', 'AgentController@listData')->name('agent.listData');
    Route::get('/agent/rejectData', 'AgentController@rejectData')->name('agent.rejectData');
    Route::get('/agent/active', 'AgentController@active')->name('agent.active');
    Route::get('/agent/activeData', 'AgentController@activeData')->name('agent.activeData');
    Route::patch('/agent/{id}/takeout', 'AgentController@takeOut')->name('agent.takeOut');
    Route::get('/agent/{id}/detail', 'AgentController@detailAgent');
    Route::patch('/agent/{id}/enable', 'AgentController@enableAgent')->name('agent.enableAgent');
    Route::get('/agent/client', 'AgentController@alreadyClient');
    Route::post('/agent/client/store', 'AgentController@storeClient')->name('agent.client.store');
    Route::resource('/agent', 'AgentController');

    Route::group(['prefix' => 'dr-review', 'as' => 'dr-review.'], function () {
        Route::get('', 'DoctorReviewController@index')->name('index');
        Route::get('upload', 'DoctorReviewController@upload')->name('upload');
        Route::post('upload', 'DoctorReviewController@uploadStore')->name('upload.store');

        Route::get('history', 'DoctorReviewController@history')->name('history');
        Route::get('history/{id}', 'DoctorReviewController@historyDetail')->name('history.detail');
        Route::delete('history/{id}', 'DoctorReviewController@historyDestroy')->name('history.destroy');

        Route::get('photo/{id}', 'DoctorReviewController@photo')->name('photo');

        Route::get('sandingkan', 'DoctorReviewController@sandingkan')->name('sandingkan');
        Route::post('sandingkan', 'DoctorReviewController@sandingkanCustomer')->name('sandingkan.customer');


        Route::post('get-photo', 'DoctorReviewController@getPhoto')->name('sandingkan.photo');
        Route::post('get-treatment', 'DoctorReviewController@getTreatment')->name('sandingkan.treatment');
    });

    Route::get('/anamnesas', 'AnamnesaController@render');

    Route::resource('survey','SurveyController'); 
    Route::get('/faq', 'FaqController@index')->name('faq');
});

Route::get('locale/{locale}', function($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});